#ifndef _PATH_HEAD_
#define _PATH_HEAD_

#include "cocos2d.h"

USING_NS_CC;

enum class PathDirection{
    LeftToRight = 1,
    RightToLeft = -1,
    TopToBot = -1,
    BotToTop = 1
};

class KKPoint : public Ref{
public:
    Vec2 point;
    KKPoint(Vec2 _point);
};

class Path
{
public:
    Vector<KKPoint*> arrVector;
    
    void clear();
    
    static void loadResource();
    
    inline void add(Path* _path){
        this->arrVector.pushBack(_path->arrVector);
        _path->arrVector.clear();
    }
};

class PathReturn : public Path{
public:
    static PathReturn* createAt_Y(float _y, float _toY,float _openWidth, float _distance,PathDirection _direction);
    
    static PathReturn* createAt_X(float _x,float _toX, float _openHeight, float _distance,PathDirection _direction);
};

class PathAcross : public Path{
public:
    static PathAcross* createAt_Y(float _fromY,float _toY,PathDirection _direction);
    
    static PathAcross* createAt_X(float _fromX,float _toX,PathDirection _direction);
    
};

class PathZicZac : public Path{
public:
    static PathZicZac* createAt_Y_With_Height(float _percentY, float _height,int _countZic,PathDirection _direction);
    
    static PathZicZac* createAt_X_With_Height(float _percentX, float _height,int _countZic,PathDirection _direction);
    
};

class PathParabon : public Path{
public:
    static PathParabon* createAt_Y_With_Height(float _percentY, float _height, PathDirection _direction);
    
    static PathParabon* createAt_X_With_Height(float _percenX, float _height, PathDirection _direction);
    
};

class PathZicZacSpace : public Path{
public:
    static PathZicZacSpace* createAt_Y_With_Height(float _percentY,float _perSector1, float _perSector2, float _height, int _countZic, PathDirection _direction);
    
    static PathZicZacSpace* createAt_X_With_Height(float _percentX,float _perSector1, float _perSector2, float _height, int _countZic, PathDirection _direction);
};

class PathLineCircle : public Path{
public:
    static PathLineCircle* createAt_Y(float _percentY, float _percentPosCircle, float _R,PathDirection _direction);
    
    static PathLineCircle* createAt_X(float _percentX, float _percentPosCircle, float _R,PathDirection _direction);
    
};

class PathFree : public Path{
public:
    static PathFree* createWithIndex(int _index);
};

class PathFreeClone : public Path{
public:
    static PathFreeClone* createWithIndex(int _index, float _range, float _angle);
};


class PathCircle : public Path{
public:
    static PathCircle* createAt_Y(float _percentY, float _radius, float _side,PathDirection _direction);
    
    static PathCircle* createAt_X(float _percentX, float _radius, float _side,PathDirection _direction);
    
};

class PathZicZacFree : public Path{
public:
    static PathZicZacFree* createAt_X(float _percentX,float _toPercentX, float _height,int _countZic,PathDirection _direction);
    
    static PathZicZacFree* createAt_Y(float _percentY,float _toPercentY, float _height,int _countZic,PathDirection _direction);
    
};

class PathAcrossConer : public Path{
public:
    static PathAcrossConer* createAt(float _range,float _option, PathDirection _direction);
};

#endif
