#include "Path.h"
#include "ResourcesNew.h"

static float sizeH;
static float sizeW;


void Path::loadResource()
{
    sizeW = 640;
    sizeH = 960;
}

KKPoint::KKPoint(Vec2 _point){
    point = _point;
    this->autorelease();
}

void Path::clear(){
    arrVector.clear();
}


PathReturn* PathReturn::createAt_Y(float _y, float _toY, float _openWidth, float _distance, PathDirection _direction){
    auto path = new (std::nothrow) PathReturn();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float y = _y * sizeH;
    float toY = _toY * sizeH;
    
    float _gapHeight = toY - y;
    
    Vec2 startPoint,endPoint,nextStartPoint,nextEndPoint;
    
    switch (_direction) {
        case PathDirection::LeftToRight:
            startPoint = Vec2(-100,y);
            endPoint = Vec2(-100,y + _gapHeight);
            break;
            
        case PathDirection::RightToLeft:
            startPoint = Vec2(sizeW+100,y);
            endPoint = Vec2(sizeW+100,y+_gapHeight);
            break;
            
        default:
            break;
    }
    
    nextStartPoint  = startPoint    + Vec2((int)_direction * _distance,0);
    nextEndPoint    = endPoint      + Vec2((int)_direction * _distance,0);
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    path->arrVector.pushBack(new KKPoint( nextStartPoint));
    
    float factorHeight = 180./40.;
    float factorDistance = _gapHeight/40.;
    
    for(int i = 0; i<=40;i++){
        float xx = i*factorDistance;
        float yy = (int)_direction * _openWidth * sin(CC_DEGREES_TO_RADIANS(i*factorHeight));
        path->arrVector.pushBack(new KKPoint( nextStartPoint+ Vec2(yy,xx)));
    }
    
    
    path->arrVector.pushBack(new KKPoint( nextEndPoint));
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    return path;
    
}

PathReturn* PathReturn::createAt_X(float _x, float _toX, float _openHeight, float _distance, PathDirection _direction){
    auto path = new (std::nothrow) PathReturn();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float x = _x * sizeW;
    float toX = _toX * sizeW;
    float _gapWidth = toX - x;
    
    Vec2 startPoint,endPoint,nextStartPoint,nextEndPoint;
    
    switch (_direction) {
        case PathDirection::BotToTop:
            startPoint = Vec2(x,-100);
            endPoint = Vec2(toX,-100);
            break;
            
        case PathDirection::TopToBot:
            startPoint = Vec2(x,sizeH+100);
            endPoint = Vec2(toX,sizeH + 100);
            break;
            
        default:
            break;
    }
    
    nextStartPoint  = startPoint    + Vec2(0,(int)_direction * _distance);
    nextEndPoint    = endPoint      + Vec2(0,(int)_direction * _distance);
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    path->arrVector.pushBack(new KKPoint( nextStartPoint));
    
    float factorHeight = 180./(80);
    float factorDistance = _gapWidth/(80);
    
    for(int i = 0; i<=80;i++){
        float xx = i*factorDistance;
        float yy = (int)_direction * _openHeight * sin(CC_DEGREES_TO_RADIANS(i*factorHeight));

        path->arrVector.pushBack(new KKPoint( nextStartPoint+ Vec2(xx,yy)));
    }
    
    
    path->arrVector.pushBack(new KKPoint( nextEndPoint));
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    return path;
    
}

PathAcross* PathAcross::createAt_Y(float _fromY, float _toY, PathDirection _direction){
    auto path = new (std::nothrow) PathAcross();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    
    Vec2 startPoint,endPoint;
    
    switch (_direction) {
        case PathDirection::LeftToRight:
            startPoint = Vec2(-100,_fromY*sizeH);
            endPoint = Vec2(sizeW+100,_toY*sizeH);
            break;
            
        case PathDirection::RightToLeft:
            endPoint = Vec2(-100,_fromY*sizeH);
            startPoint = Vec2(sizeW+100,_toY*sizeH);
            break;
            
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    
    return path;
    
    
}

PathAcross* PathAcross::createAt_X(float _fromX, float _toX, PathDirection _direction){
    auto path = new (std::nothrow) PathAcross();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    
    Vec2 startPoint,endPoint;
    
    switch (_direction) {
        case PathDirection::BotToTop:
            startPoint = Vec2(_fromX*sizeW,-100);
            endPoint = Vec2(_toX*sizeW,sizeH+100);
            break;
            
        case PathDirection::TopToBot:
            endPoint = Vec2(_fromX*sizeW,-100);
            startPoint = Vec2(_toX*sizeW,sizeH+100);
            break;
            
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    
    return path;
    
    
}


PathZicZac* PathZicZac::createAt_Y_With_Height(float _percentY, float _height, int _countZic, PathDirection _direction){
    auto path = new (std::nothrow) PathZicZac();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float y = _percentY * sizeH;
    
    Vec2 startPoint,endPoint;
    switch (_direction) {
        case PathDirection::LeftToRight:
            startPoint = Vec2(-100,y);
            endPoint = Vec2(sizeW+100,y);
            break;
        case PathDirection::RightToLeft:
            endPoint = Vec2(-100,y);
            startPoint = Vec2(sizeW+100,y);
            break;
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    float range = startPoint.getDistance(endPoint)/_countZic;
    
    for(int i = 1; i <= _countZic;i++){
        if (i%2 == 0){
            auto vecNextPoint = startPoint + Vec2((int)_direction * range*i,0);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }else{
            auto vecNextPoint = startPoint + Vec2((int)_direction *range*i,_height);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }
    }
    
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    return path;
}

PathZicZac* PathZicZac::createAt_X_With_Height(float _percentX, float _height, int _countZic, PathDirection _direction){
    auto path = new (std::nothrow) PathZicZac();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float x = _percentX * sizeW;
    
    Vec2 startPoint,endPoint;
    switch (_direction) {
        case PathDirection::BotToTop:
            startPoint = Vec2(x,-100);
            endPoint = Vec2(x,sizeH+100);
            break;
        case PathDirection::TopToBot:
            endPoint = Vec2(x,-100);
            startPoint = Vec2(x,sizeH+100);
            break;
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    float range = startPoint.getDistance(endPoint)/_countZic;
    
    for(int i = 1; i <= _countZic;i++){
        if (i%2 == 0){
            auto vecNextPoint = startPoint + Vec2(0,(int)_direction * range*i);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }else{
            auto vecNextPoint = startPoint + Vec2(_height,(int)_direction *range*i);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }
    }
    
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    return path;
}

PathParabon* PathParabon::createAt_Y_With_Height(float _percentY, float _height, PathDirection _direction){
    
    auto path = new (std::nothrow) PathParabon();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float y = _percentY * sizeH;
    Vec2 startPoint = Vec2(-100,y);
    Vec2 endPoint = Vec2(sizeW+100,y);
    switch (_direction) {
        case PathDirection::LeftToRight:
            startPoint = Vec2(-100,y);
            endPoint = Vec2(sizeW+100,y);
            break;
        case PathDirection::RightToLeft:
            endPoint = Vec2(-100,y);
            startPoint = Vec2(sizeW+100,y);
            break;
        default:
            break;
    }
    
    auto distance = startPoint.getDistance(endPoint);
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    int loop = 40;
    float factorHeight = 180./40.;
    float factorDistance = distance/40.;
    
    for(int i = 0; i <= loop;i++){
        float xx = i*factorDistance;
        float yy = _height * sin(CC_DEGREES_TO_RADIANS(i*factorHeight));
        if (_direction == PathDirection::LeftToRight){
            auto vecNextPoint = startPoint + Vec2(xx,yy);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }else{
            auto vecNextPoint = startPoint + Vec2(-xx,yy);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }
        
    }
    
    path->arrVector.pushBack(new KKPoint( endPoint));
    return path;
    
}

PathParabon* PathParabon::createAt_X_With_Height(float _percentX, float _height, PathDirection _direction){
    
    auto path = new (std::nothrow) PathParabon();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float x = _percentX * sizeW;
    Vec2 startPoint,endPoint;
    
    switch (_direction) {
        case PathDirection::BotToTop:
            startPoint = Vec2(x,-100);
            endPoint = Vec2(x,sizeH + 100);
            break;
        case PathDirection::TopToBot:
            startPoint = Vec2(x,sizeH + 100);
            endPoint = Vec2(x+100,-100);
            break;
        default:
            break;
    }
    
    auto distance = startPoint.getDistance(endPoint);
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    int loop = 40;
    float factorHeight = 180./40.;
    float factorDistance = distance/40.;
    
    for(int i = 0; i <= loop;i++){
        float yy = i*factorDistance;
        float xx = _height * sin(CC_DEGREES_TO_RADIANS(i*factorHeight));
        if (_direction == PathDirection::BotToTop){
            auto vecNextPoint = startPoint + Vec2(xx,yy);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }else{
            auto vecNextPoint = startPoint + Vec2(xx,-yy);
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }
        
    }
    
    path->arrVector.pushBack(new KKPoint( endPoint));
    return path;
    
}

PathZicZacSpace* PathZicZacSpace::createAt_Y_With_Height(float _percentY, float _perSector1, float _perSector2, float _height, int _countZic, PathDirection _direction){
    auto path = new (std::nothrow) PathZicZacSpace();
    
    if (!path){
        delete path;
        return nullptr;
    }
    
    float y = _percentY * sizeH;
    
    Vec2 startPoint, endPoint;
    
    switch (_direction) {
        case PathDirection::LeftToRight:{
            startPoint = Vec2(-100,y);
            endPoint = Vec2(sizeW+100,y);
            break;
        }
            
        case PathDirection::RightToLeft:{
            startPoint = Vec2(sizeW+100,y);
            endPoint = Vec2(-100, y);
            break;
        }
        default:{
            startPoint = Vec2(-100,y);
            endPoint = Vec2(sizeW+100,y);
            break;
        }
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    float widthForEachNode = sizeW/_countZic;       //cal width for each node
    float sector1 = _perSector1*widthForEachNode;          //cal how percent first range account for
    float sector2 = _perSector2*widthForEachNode;           //cal how percent second range account for
    float sectorCentral = (1-(_perSector1*2.0 + _perSector2*2.0))*widthForEachNode;     //cal how percent central range account for
    float sector3 = _perSector2*widthForEachNode;           //same as to the right side
    float sector4 = _perSector1*widthForEachNode;          //same as to the right side
    
    
    startPoint = startPoint + Vec2((int)_direction*100,0);
    
    for(int i = 1; i <= _countZic; i++){
        auto nextPoint1 = startPoint + Vec2((int)_direction*sector1,0);
        auto nextPoint2 = nextPoint1 + Vec2((int)_direction*sector2,_height);
        auto nextPoint3 = nextPoint2 + Vec2((int)_direction*sectorCentral,0);
        auto nextPoint4 = nextPoint3 + Vec2((int)_direction*sector3,-_height);
        
        path->arrVector.pushBack(new KKPoint(nextPoint1));
        path->arrVector.pushBack(new KKPoint(nextPoint2));
        path->arrVector.pushBack(new KKPoint(nextPoint3));
        path->arrVector.pushBack(new KKPoint(nextPoint4));
        
        startPoint = nextPoint4 + Vec2((int)_direction*sector4,0);
    }
    
    path->arrVector.pushBack(new KKPoint(endPoint));
    
    return path;
}

PathZicZacSpace* PathZicZacSpace::createAt_X_With_Height(float _percentX, float _perSector1, float _perSector2, float _height, int _countZic, PathDirection _direction){
    
    auto path = new (std::nothrow) PathZicZacSpace();
    
    if (!path){
        delete path;
        return nullptr;
    }
    
    float x = _percentX * sizeW;
    
    Vec2 startPoint, endPoint;
    
    switch (_direction) {
        case PathDirection::TopToBot:{
            startPoint = Vec2(x,sizeH+100);
            endPoint = Vec2(x,-100);
            break;
        }
            
        case PathDirection::BotToTop:{
            startPoint = Vec2(x,-100);
            endPoint = Vec2(x, sizeH+100);
            break;
        }
        default:{
            startPoint = Vec2(x,sizeH+100);
            endPoint = Vec2(x,-100);
            break;
        }
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    float widthForEachNode = sizeH/_countZic;       //cal width for each node
    float sector1 = _perSector1*widthForEachNode;          //cal how percent first range account for
    float sector2 = _perSector2*widthForEachNode;           //cal how percent second range account for
    float sectorCentral = (1-(_perSector1*2.0 + _perSector2*2.0))*widthForEachNode;     //cal how percent central range account for
    float sector3 = _perSector2*widthForEachNode;           //same as to the right side
    float sector4 = _perSector1*widthForEachNode;          //same as to the right side
    
    startPoint = startPoint + Vec2(0,(int)_direction*100);
    
    for(int i = 1; i <= _countZic; i++){
        auto nextPoint1 = startPoint + Vec2(0,(int)_direction*sector1);
        auto nextPoint2 = nextPoint1 + Vec2(_height,(int)_direction*sector2);
        auto nextPoint3 = nextPoint2 + Vec2(0,(int)_direction*sectorCentral);
        auto nextPoint4 = nextPoint3 + Vec2(-_height,(int)_direction*sector3);
        
        path->arrVector.pushBack(new KKPoint(nextPoint1));
        path->arrVector.pushBack(new KKPoint(nextPoint2));
        path->arrVector.pushBack(new KKPoint(nextPoint3));
        path->arrVector.pushBack(new KKPoint(nextPoint4));
        
        startPoint = nextPoint4 + Vec2(0,(int)_direction*sector4);
    }
    
    path->arrVector.pushBack(new KKPoint(endPoint));
    
    return path;
}


PathLineCircle* PathLineCircle::createAt_Y(float _percentY, float _percentPosCircle, float _R, PathDirection _direction){
    
    auto path = new (std::nothrow) PathLineCircle();
    
    if (!path){
        delete path;
        return NULL;
    }
    
    float y = _percentY * sizeH;
    Vec2 startPoint, endPoint;
    
    Vec2 posCircle = Vec2(_percentPosCircle * sizeW,y);
    Vec2 vecCoreCircle;
    float direct = _R/std::abs(_R);
    float range = 10;
    float maxRange = 360/range;
    
    
    switch (_direction) {
        case PathDirection::LeftToRight:{
            startPoint = Vec2(-100,y);
            endPoint = Vec2(sizeW+100,y);
            vecCoreCircle = posCircle + Vec2(0,_R);
            break;
        }
            
        case PathDirection::RightToLeft:{
            endPoint = Vec2(-100,y);
            startPoint = Vec2(sizeW+100,y);
            vecCoreCircle = posCircle + Vec2(0,-_R);
            break;
        }
            
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint(startPoint));
    path->arrVector.pushBack(new KKPoint(posCircle));
    
    float deg = CC_RADIANS_TO_DEGREES((posCircle-vecCoreCircle).getAngle());
    
    for(int i = 1; i <= maxRange; i++){
        //        CCLOG("%.2f",deg);
        deg+= (range*direct);
        float x = vecCoreCircle.x + std::abs(_R) * cos(CC_DEGREES_TO_RADIANS(deg));
        float y = vecCoreCircle.y + std::abs(_R) * sin(CC_DEGREES_TO_RADIANS(deg));
        
        path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
    }
    
    path->arrVector.pushBack(new KKPoint(posCircle));
    
    path->arrVector.pushBack(new KKPoint(endPoint));
    
    
    return path;
}

PathLineCircle* PathLineCircle::createAt_X(float _percentX, float _percentPosCircle, float _R, PathDirection _direction){
    
    auto path = new (std::nothrow) PathLineCircle();
    
    if (!path){
        delete path;
        return NULL;
    }
    
    
    float x = _percentX * sizeW;
    Vec2 startPoint, endPoint;
    
    Vec2 posCircle = Vec2(x,_percentPosCircle * sizeH);
    Vec2 vecCoreCircle;
    float direct = -(_R/(std::abs(_R)));
    float range = 10;
    float maxRange = 360/range;
    
    
    switch (_direction) {
        case PathDirection::BotToTop:{
            startPoint = Vec2(x,-100);
            endPoint = Vec2(x,sizeH+100);
            vecCoreCircle = posCircle + Vec2(_R,0);
            break;
        }
            
        case PathDirection::TopToBot:{
            endPoint = Vec2(x,-100);
            startPoint = Vec2(x,sizeH+100);
            vecCoreCircle = posCircle + Vec2(-_R,0);
            break;
        }
            
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint(startPoint));
    path->arrVector.pushBack(new KKPoint(posCircle));
    
    float deg = CC_RADIANS_TO_DEGREES((posCircle-vecCoreCircle).getAngle());
    
    for(int i = 1; i <= maxRange; i++){
        //        CCLOG("%.2f",deg);
        deg+= (range*direct);
        float x = vecCoreCircle.x + std::abs(_R) * cos(CC_DEGREES_TO_RADIANS(deg));
        float y = vecCoreCircle.y + std::abs(_R) * sin(CC_DEGREES_TO_RADIANS(deg));
        
        path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
    }
    
    path->arrVector.pushBack(new KKPoint(posCircle));
    
    path->arrVector.pushBack(new KKPoint(endPoint));
    
    
    return path;
}

//////////////////
PathFree* PathFree::createWithIndex(int _index){
    auto path = new (std::nothrow) PathFree();
    
    if (!path){
        delete path;
        return NULL;
    }
    
    auto _api = FileUtils::getInstance();
    
    
    
    //auto getPath = StringUtils::format("Path/Path%zd.plist",_index); use for ship project
    auto getPath = StringUtils::format("Path/Path%zd.plist",_index); //use for editor
    
    __Dictionary* _dic = NULL;
    if (_api->isFileExist(getPath))
        _dic = __Dictionary::createWithContentsOfFile(getPath.c_str());
    
    auto totalPoint = _dic->valueForKey("Size")->intValue();
    
    for(int i = 0; i < totalPoint; i++){
        auto _path = StringUtils::format("%s%d","Point",i+1);
        
        auto dicPoint = (__Dictionary*)_dic->objectForKey(_path);
        
        auto _x = dicPoint->valueForKey("x")->floatValue();
        auto _y = dicPoint->valueForKey("y")->floatValue();
        
        path->arrVector.pushBack(new KKPoint(Vec2(_x,_y)));
    }
    
    
    return path;
    
}

PathFreeClone* PathFreeClone::createWithIndex(int _index, float _range, float _angle){
    auto path = new (std::nothrow) PathFreeClone();
    
    if (!path){
        delete path;
        return NULL;
    }
    
    auto _api = FileUtils::getInstance();
    
    
    
    //auto getPath = StringUtils::format("Path/Path%zd.plist",_index); use for ship project
    auto getPath = StringUtils::format("Path/Path%zd.plist",_index); //use for editor
    
    __Dictionary* _dic = NULL;
    if (_api->isFileExist(getPath))
        _dic = __Dictionary::createWithContentsOfFile(getPath.c_str());
    
    auto totalPoint = _dic->valueForKey("Size")->intValue();
    
    for(int i = 0; i < totalPoint; i++){
        auto _path = StringUtils::format("%s%d","Point",i+1);
        
        auto dicPoint = (__Dictionary*)_dic->objectForKey(_path);
        
        auto _x = dicPoint->valueForKey("x")->floatValue();
        auto _y = dicPoint->valueForKey("y")->floatValue();
        
        float cloneX = _x + _range * cos(CC_DEGREES_TO_RADIANS(_angle));
        float cloneY = _y + _range * sin(CC_DEGREES_TO_RADIANS(_angle));
        path->arrVector.pushBack(new KKPoint(Vec2(cloneX,cloneY)));
    }
    
    
    return path;
    
}


////////

PathCircle* PathCircle::createAt_X(float _percentX, float _radius, float _side, PathDirection _direction){
    
    auto path = new (std::nothrow) PathCircle();
    
    if (!path){
        delete path;
        return NULL;
    }
    
    
    float x = _percentX/100 * sizeW;
    Vec2 startPoint, endPoint;
    
    Vec2 core;
    if (_side == 1){
        core = Vec2(x,sizeH);
        
        switch (_direction) {
            case PathDirection::LeftToRight:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(170));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(170));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(10));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(10));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 180; deg<=360; deg+=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                
                break;
            }
                
            case PathDirection::RightToLeft:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(10));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(10));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(170));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(170));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 360; deg>=180; deg-=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                break;
            }
                
            default:
                break;
        }
        
    }
    else{
        core = Vec2(x,0);
        
        switch (_direction) {
            case PathDirection::LeftToRight:{
                float z = cos(CC_DEGREES_TO_RADIANS(190));
                float zs = _radius*z;
                
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(190));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(190));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(-10));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(-10));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 180; deg>=0; deg-=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                
                break;
            }
                
            case PathDirection::RightToLeft:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(-10));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(-10));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(190));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(190));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 0; deg<=180; deg+=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                break;
            }
                
            default:
                break;
        }
    }
    
    
    return path;
}

PathCircle* PathCircle::createAt_Y(float _percentY, float _radius, float _side, PathDirection _direction){
    
    auto path = new (std::nothrow) PathCircle();
    
    if (!path){
        delete path;
        return NULL;
    }
    
    
    float y = _percentY/100 * sizeH;
    Vec2 startPoint, endPoint;
    
    Vec2 core;
    if (_side == 1){
        core = Vec2(0,y);
        
        switch (_direction) {
            case PathDirection::TopToBot:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(100));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(100));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(260));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(260));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 90; deg>=-260; deg-=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                
                break;
            }
                
            case PathDirection::BotToTop:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(260));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(260));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(100));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(100));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 270; deg<=450; deg+=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                break;
            }
                
            default:
                break;
        }
        
    }
    else{
        core = Vec2(sizeW,y);
        
        switch (_direction) {
            case PathDirection::TopToBot:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(80));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(80));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(280));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(280));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 90; deg<=260; deg+=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                
                break;
            }
                
            case PathDirection::BotToTop:{
                startPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(280));
                startPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(280));
                
                endPoint.x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(80));
                endPoint.y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(80));
                
                path->arrVector.pushBack(new KKPoint(startPoint));
                
                for(int deg = 270; deg>=70; deg-=10){
                    auto x = core.x + _radius * cos(CC_DEGREES_TO_RADIANS(deg));
                    auto y = core.y + _radius * sin(CC_DEGREES_TO_RADIANS(deg));
                    path->arrVector.pushBack(new KKPoint(Vec2(x,y)));
                }
                path->arrVector.pushBack(new KKPoint(endPoint));
                break;
            }
                
            default:
                break;
        }
    }
    
    
    return path;
}


PathZicZacFree* PathZicZacFree::createAt_Y(float _percentY, float _toPercentY, float _height, int _countZic, PathDirection _direction){
    auto path = new (std::nothrow) PathZicZacFree();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float y = _percentY/100 * sizeH;
    float toY = _toPercentY/100 * sizeH;
    
    
    Vec2 startPoint,endPoint;
    switch (_direction) {
        case PathDirection::LeftToRight:
            startPoint = Vec2(-100,y);
            endPoint = Vec2(sizeW+100,toY);
            break;
        case PathDirection::RightToLeft:
            endPoint = Vec2(-100,y);
            startPoint = Vec2(sizeW+100,toY);
            break;
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    float range = startPoint.getDistance(endPoint)/_countZic;
    float deg = CC_RADIANS_TO_DEGREES((endPoint-startPoint).getAngle());
    
    
    for(int i = 1; i <= _countZic;i++){
        if (i%2 == 0){
            Vec2 vecNextPoint;
            auto realRange = range*i;
            vecNextPoint.x = startPoint.x + realRange * cos(CC_DEGREES_TO_RADIANS(deg));
            vecNextPoint.y = startPoint.y + realRange * sin(CC_DEGREES_TO_RADIANS(deg));
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }else{
            Vec2 vecNextPoint;
            auto realRange = range*i;
            vecNextPoint.x = startPoint.x + realRange * cos(CC_DEGREES_TO_RADIANS(deg));
            vecNextPoint.y = startPoint.y + realRange * sin(CC_DEGREES_TO_RADIANS(deg));
            vecNextPoint.y = vecNextPoint.y + _height * sin(CC_DEGREES_TO_RADIANS(deg+90));
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }
    }
    
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    return path;
}

////////////


PathZicZacFree* PathZicZacFree::createAt_X(float _percentX, float _toPercentX, float _height, int _countZic, PathDirection _direction){
    auto path = new (std::nothrow) PathZicZacFree();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    float x = _percentX/100 * sizeW;
    float toX = _toPercentX/100 * sizeW;
    
    
    Vec2 startPoint,endPoint;
    switch (_direction) {
        case PathDirection::TopToBot:
            startPoint = Vec2(x,sizeH+100);
            endPoint = Vec2(toX,-100);
            break;
        case PathDirection::BotToTop:
            endPoint = Vec2(x,sizeH+100);
            startPoint = Vec2(toX,-100);
            break;
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint( startPoint));
    
    float range = startPoint.getDistance(endPoint)/_countZic;
    float deg = CC_RADIANS_TO_DEGREES((endPoint-startPoint).getAngle());
    
    
    for(int i = 1; i <= _countZic;i++){
        if (i%2 == 0){
            Vec2 vecNextPoint;
            auto realRange = range*i;
            vecNextPoint.x = startPoint.x + realRange * cos(CC_DEGREES_TO_RADIANS(deg));
            vecNextPoint.y = startPoint.y + realRange * sin(CC_DEGREES_TO_RADIANS(deg));
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }else{
            Vec2 vecNextPoint;
            auto realRange = range*i;
            vecNextPoint.x = startPoint.x + realRange * cos(CC_DEGREES_TO_RADIANS(deg));
            vecNextPoint.y = startPoint.y + realRange * sin(CC_DEGREES_TO_RADIANS(deg));
            vecNextPoint.x = vecNextPoint.x + _height * cos(CC_DEGREES_TO_RADIANS(deg+90));
            path->arrVector.pushBack(new KKPoint(vecNextPoint));
        }
    }
    
    path->arrVector.pushBack(new KKPoint( endPoint));
    
    return path;
}


////


PathAcrossConer* PathAcrossConer::createAt(float _range, float _option, PathDirection _direction){
    
    auto path = new (std::nothrow) PathAcrossConer();
    if (!path){
        CC_SAFE_DELETE(path);
        return nullptr;
    }
    
    Vec2 startPoint,endPoint;
    
    switch ((int)_option) {
        case 1:
        {
            switch (_direction) {
                case PathDirection::BotToTop:
                {
                    Vec2 rootL = Vec2(0,sizeH-_range);
                    Vec2 rootT = Vec2(_range,sizeH);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootT-rootL).getAngle());
                    startPoint.x = rootL.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootL.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootT.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootT.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                case PathDirection::TopToBot:
                {
                    Vec2 rootL = Vec2(0,sizeH-_range);
                    Vec2 rootT = Vec2(_range,sizeH);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootL - rootT).getAngle());
                    startPoint.x = rootT.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootT.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootL.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootL.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 2:
        {
            switch (_direction) {
                case PathDirection::BotToTop:
                {
                    Vec2 rootR = Vec2(sizeW,sizeH-_range);
                    Vec2 rootT = Vec2(sizeW - _range,sizeH);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootT-rootR).getAngle());
                    startPoint.x = rootR.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootR.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootT.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootT.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                case PathDirection::TopToBot:
                {
                    Vec2 rootR = Vec2(sizeW,sizeH-_range);
                    Vec2 rootT = Vec2(sizeW - _range,sizeH);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootR - rootT).getAngle());
                    startPoint.x = rootT.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootT.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootR.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootR.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 3:
        {
            switch (_direction) {
                case PathDirection::BotToTop:
                {
                    Vec2 rootL = Vec2(0,_range);
                    Vec2 rootB = Vec2(_range,0);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootB-rootL).getAngle());
                    startPoint.x = rootL.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootL.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootB.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootB.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                case PathDirection::TopToBot:
                {
                    Vec2 rootL = Vec2(0,_range);
                    Vec2 rootB = Vec2(_range,0);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootL - rootB).getAngle());
                    startPoint.x = rootB.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootB.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootL.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootL.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 4:
        {
            switch (_direction) {
                case PathDirection::BotToTop:
                {
                    Vec2 rootR = Vec2(sizeW,_range);
                    Vec2 rootB = Vec2(sizeW - _range,0);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootB-rootR).getAngle());
                    startPoint.x = rootR.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootR.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootB.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootB.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                case PathDirection::TopToBot:
                {
                    Vec2 rootR = Vec2(sizeW,_range);
                    Vec2 rootB = Vec2(sizeW - _range,0);
                    float rootDeg = CC_RADIANS_TO_DEGREES((rootR - rootB).getAngle());
                    startPoint.x = rootB.x - 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    startPoint.y = rootB.y - 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.x = rootR.x + 100 * cos(CC_DEGREES_TO_RADIANS(rootDeg));
                    endPoint.y = rootR.y + 100 * sin(CC_DEGREES_TO_RADIANS(rootDeg));
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    path->arrVector.pushBack(new KKPoint(startPoint));
    path->arrVector.pushBack(new KKPoint(endPoint));
    
    
    return path;
}
