
#ifndef LightShieldItem_hpp
#define LightShieldItem_hpp

#include "cocos2d.h"
#include "EnumManager.h"
#include "Lightning.hpp"
#include <SimpleAudioEngine.h>
using namespace CocosDenshion;

//#include "Mission.h"

USING_NS_CC;

enum class LightShieldState{
    DeActive = 0,
    Active = 1
};

class LightShieldEffect : public Sprite{
public:
    RepeatForever* animateActive;
    Sequence* animateDeActive;
    
    LightShieldState state;
    
    static void loadResource();
    
    static LightShieldEffect* getInstance();
    
    bool init();
    void start(Sprite* _spr);
    void stop();
    void clear();
    
    CREATE_FUNC(LightShieldEffect);
};


#endif /* LightShieldItem_hpp */
