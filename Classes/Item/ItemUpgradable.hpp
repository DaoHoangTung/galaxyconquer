

#ifndef ItemUpgradable_hpp
#define ItemUpgradable_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "GlobalVariable.hpp"
#include "LayourEffect.hpp"
#include "MyText.hpp"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace std;

enum ItemModelTag{
    gunTopModel = 0,
    gunTopRightModel = 1,
    gunRightModel = 2,
    gunBotRightModel = 3,
    gunBotModel = 4,
    gunBotLeftModel = 5,
    gunLeftModel = 6,
    gunTopLeftModel = 7,
    gravityModel = 8,
    rocketModel = 9
};

enum ItemModelState{
    activeModel = 1,
    disableModel = 0
};

enum CostLabelState{
    CostLabel_OPEN = 1,
    CostLabel_CLOSE = 0
};

class ItemModel : public ImageView{
    
public:
    Vec2 defaultPos;
    int zOrder;
    int type, level;
    int cost;
    
    ItemModelTag itemModelType;


    static void ResetModelState();
    static ItemModel* getItemModelType(ItemModelTag _item);
    static ItemModel* createModel(string _itemName,string _description, int _type, int _level);
    static void loadResource();
    static Vector<ItemModel*> getListModel();
    static void setDescriptionPanel(Label* _text);
    static void notifyStaticObject(ImageView* _imgCashLabel, ImageView* _imgCostLabel, TextBMFont* _bmfCost, TextBMFont* _bmfCash);
    static void resetState();
    static void clearAndClean();
    
    bool init(string _itemName,string _description, int _type,int _level);
    
    void checkState();
    void updateItemThroughReward();

    void setDefaultPost();
    

private:
    ItemModelState state;
    LayourEffect* effect;
    ParticleSystemQuad* maxLevelEffect; 
    
    string itemName,description;
    
    
    
    
    Label* createNotify(string _text,float _scale);
    
    void updateItem();

    void setCostLabelState(CostLabelState _state);
    void setLabelCostWithAnimation();
    void callBackClick(Ref* _item, Widget::TouchEventType type);
    
    void clear();
};

#endif /* ItemUpgradable_hpp */
