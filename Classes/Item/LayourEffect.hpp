
#ifndef LayourEffect_hpp
#define LayourEffect_hpp

#include <stdio.h>
#include "ui/CocosGUI.h"
#include "cocos2d.h"


using namespace cocos2d;
using namespace cocos2d::ui;

enum EffectType{
    DYNAMIC = 0,
};

class LayourEffect : public Node{
public:
    static LayourEffect* createWithSize(std::string _plistFile,Size _size,EffectType _type,float _speed,float _scale);
    bool initWithSprite(std::string _plistFile,Size _size,EffectType _type,float _speed,float _scale);
    
    void stop();
    void start();
    void clear();
private:
    EffectType type;
    
    Vector<ParticleSystemQuad*> vecPar;
    Vec2 _vecPoint[4];
    
    ParticleSystemQuad* effectT;
    ParticleSystemQuad* effectB;
    ParticleSystemQuad* effectL;
    ParticleSystemQuad* effectR;
    
    bool bStart;
    
    float speed,scale;
    
    RepeatForever* tAct,*bAct,*lAct,*rAct;
    
    LayourEffect();
    ~LayourEffect();
    
    void createDynamicEffect(Size _size, std::string _plistFile);
    
public:
    void changeParticleColor(Color4F _color4f);
    void changeParticleCount(int _total);
    void changeParticleScale(float _scale);
};
#endif /* LayourEffect_hpp */
