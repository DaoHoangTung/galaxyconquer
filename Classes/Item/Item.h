#ifndef _ITEM_H_
#define _ITEM_H_

#include "cocos2d.h"
#include "Ship.h"
#include "EnumManager.h"
USING_NS_CC;

class Coin : public Sprite
{
    
public:
    int value;
    int levelUpgrade;
    Vec2 _pos;
    
    PhysicsBody* body;
    bool isRecycle;
    
    TypeCoin typeCoin;
    
    RepeatForever* animateAct;
    FadeIn* fadeInAct;
    
    static void loadResource();
    static void setMissionlayerAndShip(Layer* _layer, Sprite* _ship);
    static void clearAllCoin();
    
    static bool checkAllCoinCleared();
    
    static void createCoinAtPos(Vec2 _pos, TypeCoin _typeCoin, int _value, int _levelUpgrade);

    static void createCoinAtRandomPos(Vec2 _pos, float _offsetX, float _offsetY, TypeCoin _typeCoin, int _count,int _value, int _levelUpgrade);
    void update(float dt);
    static int GetRealCashRecieve(TypeCoin _typeCoin, int _value);
    
    bool initCoin();
    void initBody();
    virtual void start();
    virtual void recycle();
    virtual void addPhysicBody(float dt);
};

class SilverCoin : public Coin
{
public:
    static SilverCoin* createSilverCoin();
    bool initSilverCoin();
};

class GoldCoin : public Coin
{
public:
    static GoldCoin* createGoldCoin();
    bool initGoldCoin();
};

class GoldBar : public Coin
{
public:
    static GoldBar* createGoldBar();
    bool initGoldBar();
    
};

class Diamond : public Coin
{
public:
    static Diamond* createDiamond();
    bool initDiamond();
    
};

class PowerShield : public Coin{
public:
    static PowerShield* createShield();
    bool initShield();
};

class PowerMedic : public Coin{
public:
    static PowerMedic* createMedic();
    bool initMedic();
};

class PowerLife : public Coin{
public:
    static PowerLife* createLife();
    bool initLife();
};

class MultiItem : public Coin{
public:
    bool init();
    
    CREATE_FUNC(MultiItem);
};
#endif
