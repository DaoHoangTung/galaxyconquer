
#include "LightShieldItem.hpp"
static bool isLoad = false;
static LightShieldEffect* instance = NULL;

void LightShieldEffect::loadResource(){
    if (!isLoad){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("CustomSprite/LightShieldEffect.plist");
        isLoad = true;
    }
}

LightShieldEffect* LightShieldEffect::getInstance(){
    
    if (instance == NULL){
        instance = LightShieldEffect::create();
        instance->retain();
    }
    
    return instance;
}

bool LightShieldEffect::init(){
    
    if (!Sprite::init()){
        return false;
    }
    
    this->state = LightShieldState::DeActive;
    
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 10; i++){
        auto name = __String::createWithFormat("LightShield%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.04);
    auto animate = Animate::create(animation);
    
    this->animateActive = RepeatForever::create(animate);
    this->animateActive->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    vecFrame.clear();
    
    for (int i = 10; i < 30; i++){
        auto name = __String::createWithFormat("LightShield%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation2 = Animation::createWithSpriteFrames(vecFrame, 0.04);
    auto animate2 = Animate::create(animation2);
    
    
    auto callBackDeActive = CallFuncN::create([&](Node* p){
        this->removeFromParentAndCleanup(true);
    });
    
    this->animateDeActive = Sequence::create(animate2,callBackDeActive, NULL);
    this->animateDeActive->retain();
    
    this->setScale(1.2);
    
    return true;
}

void LightShieldEffect::start(Sprite* _spr){
    if (state == LightShieldState::DeActive){
        _spr->addChild(this);
        this->setPosition(_spr->getContentSize()/2);
        this->stopAllActions();
        this->runAction(animateActive);
        this->state = LightShieldState::Active;
    }
}

void LightShieldEffect::stop(){
    if (state == LightShieldState::Active){
        this->stopAllActions();
        this->runAction(this->animateDeActive);
        state = LightShieldState::DeActive;
    }
}

void LightShieldEffect::clear(){
    this->stop();
    
    CC_SAFE_RELEASE_NULL(animateActive);
    CC_SAFE_RELEASE(animateDeActive);
}
