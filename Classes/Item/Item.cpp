#include "Item.h"

static Vector<Coin*> arrCoin[8];
static Vector<Coin*> arrCoinMoving;
static Vector<Coin*> arrTemp;

static Sprite* ship;
static Layer* missionLayer;
static bool _loadResource = false;



bool Coin::checkAllCoinCleared(){
    if (arrCoinMoving.size() > 0) return false;
    
    return true;
}

void Coin::loadResource()
{
    if (!_loadResource){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("BonusItem/Bonus.plist");
        arrCoinMoving.clear();

        for (int i = 0; i < 100; i++){
            arrCoin[0].pushBack(SilverCoin::createSilverCoin());
            arrCoin[1].pushBack(GoldCoin::createGoldCoin());
            arrCoin[2].pushBack(GoldBar::createGoldBar());
            arrCoin[3].pushBack(Diamond::createDiamond());
            
        }
        
        for(int i = 0; i < 20; i++){
            arrCoin[4].pushBack(PowerShield::createShield());
            arrCoin[5].pushBack(PowerMedic::createMedic());
            arrCoin[6].pushBack(PowerLife::createLife());
            arrCoin[7].pushBack(MultiItem::create());
        }
        _loadResource = true;
    }
    
    
    
}

void Coin::setMissionlayerAndShip(cocos2d::Layer *_layer, cocos2d::Sprite *_ship){
    missionLayer  = _layer;
    ship = _ship;
}

void Coin::clearAllCoin()
{
//    for (int i = 0; i < 4; i++){
//        for (auto coin : arrCoin[i]){
//            coin->clear();
//        }
//        arrCoin[i].clear();
//    }
    Vector<Coin*> arrTempCoin;
    for (auto coin : arrCoinMoving){
        arrTempCoin.pushBack(coin);
        //coin->recycle();
    }
    
    for (auto coin : arrTempCoin){
        coin->recycle();
    }
    
    missionLayer = NULL;
    ship = NULL;
//    CCLOG("Siver: %zd",arrCoin[0].size());
//    CCLOG("Coin: %zd",arrCoin[1].size());
//    CCLOG("Moving: %zd",arrCoinMoving.size());
    arrCoinMoving.clear();
    arrTempCoin.clear();
}

int Coin::GetRealCashRecieve(TypeCoin _typeCoin, int _value){
    int realCashRecieve;
    switch (_typeCoin) {
        case TypeCoin::typeSilverCoin:
            realCashRecieve = random(0.1*_value, 0.2*_value);
            break;
        case TypeCoin::typeGoldCoin:
            realCashRecieve = random(0.3*_value, 0.4*_value);
            break;
        case TypeCoin::typeGoldBar:
            realCashRecieve = random(0.5*_value, 0.6*_value);
            break;
        case TypeCoin::typeDiamond:
            realCashRecieve = random(0.7*_value, 0.8*_value);
            break;
        default:
            realCashRecieve = 0;
            break;
    }
    
    return realCashRecieve;
}

void Coin::createCoinAtPos(Vec2 _pos, TypeCoin _typeCoin, int _value, int _levelUpgrade)
{
    
    if (arrCoin[(int)_typeCoin].size() > 0){
        auto coin = arrCoin[(int)_typeCoin].at(0);
        coin->_pos = _pos;
        coin->value = GetRealCashRecieve(_typeCoin, _value);
        coin->setPosition(coin->_pos);
        coin->typeCoin = _typeCoin;
        coin->levelUpgrade = _levelUpgrade;
        coin->start();
        arrCoinMoving.pushBack(coin);
        arrCoin[(int)_typeCoin].eraseObject(coin);

    }
    
}

static int TOTAL= 0;
void Coin::createCoinAtRandomPos(Vec2 _pos, float _offsetX, float _offsetY, TypeCoin _typeCoin, int _count,int _value, int _levelUpgrade)
{
    //CCLOG("TOTAL: %zd",arrCoinMoving.size());
    for (int i = 0; i < _count; i++){
        if (arrCoin[(int)_typeCoin].size() > 0){
            auto coin = arrCoin[(int)_typeCoin].at(0);
            //auto coin = SilverCoin::createSilverCoin();
            
            coin->_pos = _pos;
            float randX = random(-_offsetX, _offsetX);
            float randY = random(-_offsetY, _offsetY);
            
            coin->value = GetRealCashRecieve(_typeCoin, _value);
            
            coin->_pos = coin->_pos + Vec2(randX, randY);
            coin->setPosition(coin->_pos);
            
            coin->typeCoin = _typeCoin;
            coin->levelUpgrade = _levelUpgrade;
            coin->start();
            arrCoinMoving.pushBack(coin);

            arrCoin[(int)_typeCoin].eraseObject(coin);
            TOTAL++;
            //CCLOG("TOTAL: %zd",i);
        }
        else{
            break;
        }
    }
    
  //  CCLOG("TOTAL: %zd",arrCoinMoving.size());

    
}

bool Coin::initCoin()
{
    if (!Sprite::init()){
        return false;
    }
    
    this->value = 0;
    this->animateAct = nullptr;
    this->fadeInAct = FadeIn::create(0.3f);
    this->fadeInAct->retain();
    
    
    return true;
}

void Coin::start()
{
    isRecycle = false;
    missionLayer->addChild(this);
    this->setOpacity(0);
    this->runAction(animateAct);
    this->runAction(fadeInAct);
    this->scheduleOnce(schedule_selector(Coin::addPhysicBody), 0.01);
    this->scheduleUpdate();
    
}

void Coin::addPhysicBody(float dt)
{
    this->body->setEnabled(true);
    this->setPhysicsBody(this->body);
}

void Coin::recycle()
{
    if (!isRecycle){
        this->stopAllActions();
        this->removeFromParentAndCleanup(true);

        this->body->setEnabled(true);
        this->body->removeFromWorld();
        this->unscheduleUpdate();
        
        arrCoin[(int)this->typeCoin].pushBack(this);
        arrCoinMoving.eraseObject(this);
//        CCLOG("Siver: %zd",arrCoin[0].size());
//        CCLOG("Coin: %zd",arrCoin[1].size());
//        CCLOG("Coin Moving %zd",arrCoinMoving.size());
        isRecycle = true;
    }

}



void Coin::initBody()
{
    body = PhysicsBody::createCircle(this->_contentSize.width/2, PhysicsMaterial(0.1f, 1.0f, 0.0f));
    body->setCategoryBitmask((int)PhysicCategory::Item);
    body->setCollisionBitmask((int)PhysicCategory::None);
    body->setContactTestBitmask((int)PhysicCategory::PlayerShip);
    
    body->retain();
}


void Coin::update(float dt)
{
    Vec2 vecMove;
    
    if (this->getPosition() != this->_pos){
        this->setPosition(this->_pos);
    }
    
        float maxDistance;
    

        Vec2 distance = ship->getPosition() - this->getPosition();
        auto realShip = static_cast<Ship*>(ship);

        switch (this->levelUpgrade)
        {
            case 0:
                vecMove = Vec2(0, -1);
                break;
            case 1:
                vecMove = distance;
                vecMove.normalize();
                maxDistance = 100+ realShip->bonusGravityArea;
                vecMove *= 2;
                break;
            case 2:
                vecMove = distance;
                vecMove.normalize();
                maxDistance = 150+ realShip->bonusGravityArea;
                vecMove *= 3;
                break;
            case 3:
                vecMove = distance;
                vecMove.normalize();
                maxDistance = 250+ realShip->bonusGravityArea;
                vecMove *= 4;
                break;
            default:
                break;
        }
        
        
    if (this->levelUpgrade > 0 && ship->getPosition().getDistance(this->getPosition()) < maxDistance){
        this->_pos = this->_pos+ vecMove;
            if (this->getPositionY() < 0){
                this->recycle();
            }
        }else{
            this->_pos = this->_pos + Vec2(0,-1);

            //this->setPosition(this->getPosition() + Vec2(0,-1));
            if (this->getPositionY() < 0){
                this->recycle();
            }
        }
    
    
    
}






SilverCoin* SilverCoin::createSilverCoin()
{
    auto silver = new (std::nothrow) SilverCoin();
    if (silver){
        if (silver->initSilverCoin()){
            silver->autorelease();
        }
        else{
            CC_SAFE_DELETE(silver);
        }
    }
    return silver;
}

bool SilverCoin::initSilverCoin()
{
    if (!Coin::initCoin()){
        return false;
    }
    
    this->value = 5;
    this->typeCoin = TypeCoin::typeSilverCoin;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 20; i++){
        auto name = __String::createWithFormat("SilverCoin%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(2.);
    this->initBody();
    
    
    return true;
}

GoldCoin* GoldCoin::createGoldCoin()
{
    auto gold = new (std::nothrow) GoldCoin();
    if (gold){
        if (gold->initGoldCoin()){
            gold->autorelease();
        }
        else{
            CC_SAFE_DELETE(gold);
        }
    }
    return gold;
}

bool GoldCoin::initGoldCoin()
{
    if (!Coin::initCoin()){
        return false;
    }
    
    this->value = 10;
    this->typeCoin = TypeCoin::typeGoldCoin;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 20; i++){
        auto name = __String::createWithFormat("GoldCoin%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(2.);
    this->initBody();
    
    
    return true;
}



GoldBar* GoldBar::createGoldBar()
{
    auto bar = new (std::nothrow) GoldBar();
    if (bar){
        if (bar->initGoldBar()){
            bar->autorelease();
        }
        else{
            CC_SAFE_DELETE(bar);
        }
    }
    return bar;
}

bool GoldBar::initGoldBar()
{
    if (!Coin::initCoin()){
        return false;
    }
    
    this->value = 15;
    this->typeCoin = TypeCoin::typeGoldBar;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 40; i++){
        auto name = __String::createWithFormat("GoldBar%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(1.5);
    this->initBody();
    
    
    return true;
}


Diamond* Diamond::createDiamond()
{
    auto diamond = new (std::nothrow) Diamond();
    if (diamond){
        if (diamond->initDiamond()){
            diamond->autorelease();
        }
        else{
            CC_SAFE_DELETE(diamond);
        }
    }
    return diamond;
}

bool Diamond::initDiamond()
{
    if (!Coin::initCoin()){
        return false;
    }
    
    this->value = 20;
    this->typeCoin = TypeCoin::typeDiamond;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 20; i++){
        auto name = __String::createWithFormat("Diamond%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(2.);
    this->initBody();
    
    
    return true;
}

PowerShield* PowerShield::createShield()
{
    auto shield = new (std::nothrow) PowerShield();
    if (shield){
        if (shield->initShield()){
            shield->autorelease();
        }
        else{
            CC_SAFE_DELETE(shield);
        }
    }
    return shield;
}

bool PowerShield::initShield()
{
    if (!Coin::initCoin()){
        return false;
    }
    

    this->typeCoin = TypeCoin::typeDiamond;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 16; i++){
        auto name = __String::createWithFormat("PowerupShield%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(1.5);
    this->initBody();
    
    return true;
}

PowerMedic* PowerMedic::createMedic(){
    auto medic = new (std::nothrow) PowerMedic();
    
    if (medic){
        if (medic->initMedic()){
            medic->autorelease();
        }else{
            CC_SAFE_DELETE(medic);
        }
    }
    
    return medic;
}

bool PowerMedic::initMedic(){
    
    if (!Coin::initCoin()){
        return false;
    }
    
    this->typeCoin = TypeCoin::typeMedic;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 16; i++){
        auto name = __String::createWithFormat("PowerupMedic%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(1.5);
    this->initBody();

    return true;
}

PowerLife* PowerLife::createLife(){
    auto life = new (std::nothrow) PowerLife();
    
    if (life){
        if (life->initLife()){
            life->autorelease();
        }else{
            CC_SAFE_DELETE(life);
        }
    }
    
    return life;
}

bool PowerLife::initLife(){
    
    if (!Coin::initCoin()){
        return false;
    }
    
    
    this->typeCoin = TypeCoin::typeLife;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 16; i++){
        auto name = __String::createWithFormat("PowerupExtraLife%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(1.5);
    this->initBody();
    
    
    return true;
}

bool MultiItem::init(){
    if (!Coin::initCoin()){
        return false;
    }
    
    this->typeCoin = TypeCoin::typeMulti;
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < 16; i++){
        auto name = __String::createWithFormat("PowerupMultiplier%i.png", i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.03);
    auto animate = Animate::create(animation);
    
    this->animateAct = RepeatForever::create(animate);
    this->animateAct->retain();
    this->setSpriteFrame(vecFrame.at(0));
    
    this->setScale(1.5);
    this->initBody();
    return true;
}

