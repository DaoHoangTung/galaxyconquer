
#include "LayourEffect.hpp"



LayourEffect::LayourEffect(){
    effectT = NULL;
    effectB = NULL;
    effectR = NULL;
    effectL = NULL;
    
    bStart = false;
}

LayourEffect::~LayourEffect(){
}

LayourEffect* LayourEffect::createWithSize(std::string _plistFile,Size _size,EffectType _type,float _speed,float _scale){
    
    auto obj = new (std::nothrow) LayourEffect();
    
    if (obj){
        if (!obj->initWithSprite(_plistFile,_size,_type,_speed,_scale)){
            CC_SAFE_DELETE(obj);
        }
    }
    
    return obj;
}


bool LayourEffect::initWithSprite(std::string _plistFile,Size _size,EffectType _type,float _speed,float _scale){
    
    if (!Node::init()){
        return false;
    }
    this->setAnchorPoint(Vec2(0.5,0.5));
    this->speed = _speed;
    
    type = _type;
    this->scale = _scale;
    
    switch (_type) {
            case DYNAMIC:
            {
                createDynamicEffect(_size,_plistFile);
            }
            break;
            
        default:
            break;
    }
    
    
    return true;
}


void LayourEffect::createDynamicEffect(cocos2d::Size _size, std::string _plistFile){
    float _w = _size.width;
    float _h = _size.height;
    
    
    effectL = ParticleSystemQuad::create(_plistFile);
    effectR = ParticleSystemQuad::create(_plistFile);
    effectT = ParticleSystemQuad::create(_plistFile);
    effectB = ParticleSystemQuad::create(_plistFile);
    
    effectL->setScale(scale);
    effectR->setScale(scale);
    effectT->setScale(scale);
    effectB->setScale(scale);
    
    effectL->setAnchorPoint(Vec2(0.5,0.5));
    effectR->setAnchorPoint(Vec2(0.5,0.5));
    effectT->setAnchorPoint(Vec2(0.5,0.5));
    effectB->setAnchorPoint(Vec2(0.5,0.5));
    
    
    float _margin = 0;
    float __margin= 0;
    
    Vec2 _posLB = Vec2(-_w/2-_margin,-_h/2+__margin);
    Vec2 _posLT = Vec2(-_w/2-_margin,_h/2);
    Vec2 _posRT = Vec2(_w/2+_margin,_h/2);
    Vec2 _posRB = Vec2(_w/2+_margin,-_h/2+__margin);
    
    effectL->setPosition(_posLB);
    effectR->setPosition(_posRT);
    effectT->setPosition(_posLT);
    effectB->setPosition(_posRB);
    
    _vecPoint[0] = _posLB;
    _vecPoint[1] = _posRT;
    _vecPoint[2] = _posLT;
    _vecPoint[3] = _posRB;


    auto _moveLB_to_LT = MoveTo::create(this->speed, _posLT);
    auto _moveLT_to_RT = MoveTo::create(this->speed, _posRT);
    auto _moveRT_to_RB = MoveTo::create(this->speed, _posRB);
    auto _moveRB_to_LB = MoveTo::create(this->speed, _posLB);
    
    
    lAct = RepeatForever::create(Sequence::create( _moveLB_to_LT->clone(),
                                                  _moveLT_to_RT->clone(),
                                                  _moveRT_to_RB->clone(),
                                                  _moveRB_to_LB->clone(), NULL));
    
    tAct = RepeatForever::create(Sequence::create( _moveLT_to_RT->clone(),
                                                  _moveRT_to_RB->clone(),
                                                  _moveRB_to_LB->clone(),
                                                  _moveLB_to_LT->clone(), NULL));
    
    rAct = RepeatForever::create(Sequence::create(_moveRT_to_RB->clone(),
                                                  _moveRB_to_LB->clone(),
                                                  _moveLB_to_LT->clone(),
                                                  _moveLT_to_RT->clone(), NULL));
    
    bAct = RepeatForever::create(Sequence::create(_moveRB_to_LB->clone(),
                                                  _moveLB_to_LT->clone(),
                                                  _moveLT_to_RT->clone(),
                                                  _moveRT_to_RB->clone(), NULL));
    
    lAct->retain();
    tAct->retain();
    bAct->retain();
    rAct->retain();
    
    this->addChild(effectL);
    this->addChild(effectR);
    this->addChild(effectT);
    this->addChild(effectB);
    
    vecPar.pushBack(effectL);
    vecPar.pushBack(effectR);
    vecPar.pushBack(effectT);
    vecPar.pushBack(effectB);

}


void LayourEffect::changeParticleColor(cocos2d::Color4F _color4f){
    for(auto _par : vecPar){
        _par->setStartColor(_color4f);
    }
}

void LayourEffect::changeParticleCount(int _total){
    for(auto _par : vecPar){
        _par->setTotalParticles(_total);
    }
}

void LayourEffect::changeParticleScale(float _scale){
    for(auto _par : vecPar){
        _par->setScale(_scale);
    }
}

void LayourEffect::stop(){
    if (!bStart) return;
    for(auto _par : vecPar){
        _par->stopSystem();
        if (type == EffectType::DYNAMIC){
            _par->stopAllActions();
        }
    }
    
    bStart = false;
}

void LayourEffect::start(){
    if (bStart) return;
    for(auto _par : vecPar){
        _par->setPosition(_vecPoint[vecPar.getIndex(_par)]);
        _par->resetSystem();
    }
    
    if (type == EffectType::DYNAMIC){
        effectB->runAction(bAct);
        effectL->runAction(lAct);
        effectR->runAction(rAct);
        effectT->runAction(tAct);
    }
    
    
    bStart = true;
}

void LayourEffect::clear(){
    this->stop();
    
    for(auto _par : vecPar){
        _par->stopSystem();
    }
    
    for(auto _par : vecPar){
        this->removeChild(_par);
    }
    
    
}
