
#include "ItemUpgradable.hpp"
static Vector<ItemModel*> listModel;
static Vector<__String*> descriptionItem;
static Vector<__String*> nameItem;
static Label* descriptionPanel = NULL;
static ImageView* imgCashLabel = NULL;
static ImageView* imgCostLabel = NULL;
static TextBMFont* bmfCost = NULL;
static TextBMFont* bmfCash = NULL;

ItemModel* ItemModel::createModel(string _itemName,string _description, int _type, int _level){
    auto model = new (std::nothrow) ItemModel();
    
    if (model){
        model->init(_itemName,_description, _type,_level);
    }else{
        CC_SAFE_DELETE(model);
    }
    
    return model;
}

Vector<ItemModel*> ItemModel::getListModel(){
    return listModel;
}

void ItemModel::setDescriptionPanel(Label* _text){
    descriptionPanel = _text;
}

void ItemModel::notifyStaticObject(cocos2d::ui::ImageView *_imgCashLabel, cocos2d::ui::ImageView *_imgCostLabel, cocos2d::ui::TextBMFont *_bmfCost,TextBMFont* _bmfCash){
    imgCashLabel = _imgCashLabel;
    imgCostLabel = _imgCostLabel;
    bmfCost = _bmfCost;
    bmfCash = _bmfCash;
}

static bool load = false;
void ItemModel::loadResource(){
    if (!load){
        descriptionItem.pushBack(__String::create("fire large bullets from front gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from front-right gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from right gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from back-right gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from back gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from back-left gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from left gun"));
        descriptionItem.pushBack(__String::create("fire large bullets from front-left gun"));
        descriptionItem.pushBack(__String::create("attract treasure to your ship"));
        descriptionItem.pushBack(__String::create("fire heat-seeking-rockets at front"));
        descriptionItem.pushBack(__String::create("improve attack speed"));
        descriptionItem.pushBack(__String::create("enhance ship's armor"));
        descriptionItem.pushBack(__String::create("Increase damage of plasma shield"));
        descriptionItem.pushBack(__String::create("Improve plasma shield's duration"));
        descriptionItem.pushBack(__String::create("Increase plasma shield's range"));
        descriptionItem.pushBack(__String::create("Reduce plasma shield's recharge time"));



        nameItem.pushBack(__String::create("UpgradeGunN"));
        nameItem.pushBack(__String::create("UpgradeGunNE"));
        nameItem.pushBack(__String::create("UpgradeGunE"));
        nameItem.pushBack(__String::create("UpgradeGunSE"));
        nameItem.pushBack(__String::create("UpgradeGunS"));
        nameItem.pushBack(__String::create("UpgradeGunSW"));
        nameItem.pushBack(__String::create("UpgradeGunW"));
        nameItem.pushBack(__String::create("UpgradeGunNW"));
        nameItem.pushBack(__String::create("UpgradeMagnet"));
        nameItem.pushBack(__String::create("UpgradeRocket"));
        nameItem.pushBack(__String::create("UpgradeROF"));
        nameItem.pushBack(__String::create("UpgradeSpeed"));
        nameItem.pushBack(__String::create("aborbShieldDamage"));
        nameItem.pushBack(__String::create("aborbShieldDuration"));
        nameItem.pushBack(__String::create("aborbShieldRange"));
        nameItem.pushBack(__String::create("aborbShieldRecharge"));

        
        for (int i =0; i < GLOBAL::GET_UPGRADE_ITEM_LEVEL_SIZE(); i++) {
            listModel.pushBack(ItemModel::createModel(nameItem.at(i)->getCString(), descriptionItem.at(i)->getCString(), i, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX(i)));
        }
        
//        for(auto model : listModel){
//            model->checkState();
//        }
//        
        load = true;
        
        
    }
}

ItemModel* ItemModel::getItemModelType(ItemModelTag _item){
    for(auto model : listModel){
        if (model->itemModelType == _item){
            return model;
        }
    }
    
    return NULL;
}

void ItemModel::ResetModelState(){
    for(auto model : listModel){
        model->checkState();
    }
}

bool ItemModel::init(string _itemName,string _description, int _type,int _level){
    
    if (!ImageView::init()){
        return false;
    }
    
    this->itemName = _itemName;
    this->type = _type;
    this->level = _level;
    this->description = _description;
    this->itemModelType = static_cast<ItemModelTag>(type);
    this->cost = GLOBAL::GET_ITEM_COST(type);
    
    maxLevelEffect = ParticleSystemQuad::create("NewParticle/maxLevelEffect.plist");
    maxLevelEffect->setScale(0.4);
    maxLevelEffect->setPosition(this->getContentSize()/2);
    maxLevelEffect->retain();
    
    this->checkState();
    
    
    this->effect = LayourEffect::createWithSize("NewParticle/layoutEffect2.plist", this->getContentSize(), EffectType::DYNAMIC, 1,0.3);
    effect->setPosition(this->getContentSize()/2);

    
    
    
    
    this->setTouchEnabled(true);
    
    this->addTouchEventListener(CC_CALLBACK_2(ItemModel::callBackClick, this));
    return true;
}

void ItemModel::checkState(){
    int cost = GLOBAL::GET_ITEM_COST(type);
    int userCash = GLOBAL::GET_CASH();
    
    __String* path;
    
    if (level >= 3){
        state = ItemModelState::disableModel;
        path = __String::createWithFormat("BTN%s%d.png",itemName.c_str(),level-1);
        if (maxLevelEffect->getParent() == NULL){
            maxLevelEffect->setPosition(this->getContentSize()/2);
            this->addChild(maxLevelEffect);
            maxLevelEffect->resetSystem();
        }else
            maxLevelEffect->setPosition(this->getContentSize()/2);
    }else{
        if (cost <= userCash){
            state = ItemModelState::activeModel;
            path = __String::createWithFormat("BTN%s%d.png",itemName.c_str(),level);
        }else{
            state = ItemModelState::disableModel;
            path = __String::createWithFormat("BTNdisable%s%d.png",itemName.c_str(),level);
        }
    }
    
    this->loadTexture(path->getCString(),Widget::TextureResType::PLIST);
}

Label* ItemModel::createNotify(string _text, float _scale){
    auto notify = Label::createWithBMFont("fonts/BauCua_Red.fnt", _text);
    notify->setMaxLineWidth(800);
    notify->setVerticalAlignment(TextVAlignment::CENTER);
    notify->setHorizontalAlignment(TextHAlignment::CENTER);
    notify->setPosition(this->getContentSize()/2 );
    notify->setPosition(notify->getPosition() + Vec2(0,this->getContentSize().height/3));
    notify->setScale(_scale-0.25);
    
    auto scene = this->getScene();
    MyText::AdaptLabelToScreen(scene, (Node*)this,notify);
    
    
    auto scaleUp = EaseElasticOut::create(ScaleTo::create(1., _scale), 0.2);
    auto moveBy = MoveBy::create(1.5, Vec2(0,50));
    auto fadeOut = FadeOut::create(1.75);
    auto action1 = Spawn::create(scaleUp,moveBy,DelayTime::create(0.5),fadeOut, NULL);
    auto action2 = Sequence::create(action1, RemoveSelf::create(), NULL);
    this->addChild(notify);
    notify->runAction(action2);
    
    return notify;
}


void ItemModel::updateItem(){
    
    auto parEffect = ParticleSystemQuad::create("NewParticle/upgradeEffect2.plist");
    parEffect->setAutoRemoveOnFinish(true);
    parEffect->setScale(0.4);
    parEffect->setPosition(this->getContentSize()/2);
    this->addChild(parEffect);
    
    this->createNotify(StringUtils::format("Level %d !",level+1), 1);

    
    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_upgradeSound, false);
    
    GLOBAL::INCREASE_UPGRADE_ITEM_LEVEL_AT_INDEX(type); //Increase level item
    GLOBAL::SET_CASH(GLOBAL::GET_ITEM_COST(type)*-1); //Sub Cash
    GLOBAL::SET_ITEM_COST(type); //Increase item cost
    
    level = GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX(type);
    cost = GLOBAL::GET_ITEM_COST(type);
    bmfCash->setString(GLOBAL::GET_CASH_STRING());
    
    for(auto model : listModel){
        model->checkState();
    }
    
}

void ItemModel::updateItemThroughReward(){
    
    GLOBAL::INCREASE_UPGRADE_ITEM_LEVEL_AT_INDEX(type); //Increase level item
    GLOBAL::SET_ITEM_COST(type); //Increase item cost
    
    level = GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX(type);
    cost = GLOBAL::GET_ITEM_COST(type);
    
    this->checkState();
}

void ItemModel::setCostLabelState(CostLabelState _state){
    switch (_state) {
        case CostLabel_OPEN:
        {
            auto moveBy = MoveBy::create(2,Vec2(-60 - imgCostLabel->getContentSize().width/2,0));
            auto action1 = EaseElasticOut::create(moveBy, 0.9);
            
            imgCostLabel->runAction(action1);

        }
            break;
            
        case CostLabel_CLOSE:
        {
            auto moveBy = MoveBy::create(2,Vec2(60 + imgCostLabel->getContentSize().width/2,0));
            auto action1 = EaseElasticOut::create(moveBy, 0.5);
            
            imgCostLabel->runAction(action1);
            
        }
            break;

            
        default:
            break;
    }
}

void ItemModel::setDefaultPost(){
    defaultPos = this->getPosition();
}

void ItemModel::setLabelCostWithAnimation(){
    float scaleX = 1;
    float scaleY = 1;
    auto flipUp = ScaleTo::create(0.25, scaleX, 0);
    auto flipDown = ScaleTo::create(0.25, scaleX, scaleY);
    auto func = CallFuncN::create([&](Node* sender){
        if (level < 3)
            bmfCost->setString(StringUtils::format("$%s",GLOBAL::GET_COST_STRING(this->cost).c_str()));
        else
            bmfCost->setString("Max level");
    });
    
    auto action1 = Sequence::create(flipUp,func,flipDown, NULL);
    bmfCost->runAction(action1);
}

static ItemModel* lastSelected = NULL;

void ItemModel::callBackClick(cocos2d::Ref *_item, Widget::TouchEventType type){
    
    if (type == TouchEventType::ENDED && lastSelected != this){
        descriptionPanel->setString(this->description);
        this->setLocalZOrder(100);
        if (lastSelected == NULL){
            this->setCostLabelState(CostLabel_OPEN);
            if (level < 3)
                bmfCost->setString(StringUtils::format("$%s",GLOBAL::GET_COST_STRING(this->cost).c_str()));
            else
                bmfCost->setString("Max level");
        }else{
            setLabelCostWithAnimation();
        }
        
        auto scaleUp = EaseElasticOut::create(ScaleTo::create(1., 1.15), 0.2);
        this->runAction(scaleUp);
        
        this->addChild(this->effect);
        this->effect->start();
        
        if (lastSelected != NULL && lastSelected != this){
            auto scaleDown = EaseElasticOut::create(ScaleTo::create(1., 1.), 0.2);
            lastSelected->runAction(scaleDown);
            lastSelected->removeChild(lastSelected->effect);
            lastSelected->effect->stop();
            lastSelected->setLocalZOrder(lastSelected->zOrder);
        }
        
        lastSelected = this;
    }else if (type == TouchEventType::ENDED && lastSelected != NULL && state == ItemModelState::activeModel){
        this->updateItem();
        ItemModel::resetState();
        this->setCostLabelState(CostLabel_CLOSE);
    }else if (type == TouchEventType::ENDED && lastSelected != NULL && state == ItemModelState::disableModel && level >=3){
        this->createNotify("Max level already !", 1);
    }else if (type == TouchEventType::ENDED && lastSelected != NULL && state == ItemModelState::disableModel){
        this->createNotify("Not enough cash", 0.5);
    }
}


void ItemModel::clear(){
    maxLevelEffect->removeFromParentAndCleanup(true);
    maxLevelEffect->release();
    effect->release();
}

void ItemModel::resetState(){
    if (lastSelected == NULL) return;
    
    auto scaleDown = EaseElasticOut::create(ScaleTo::create(1., 1.), 0.2);
    lastSelected->runAction(scaleDown);
    lastSelected->removeChild(lastSelected->effect);
    lastSelected->effect->stop();
    lastSelected->setLocalZOrder(lastSelected->zOrder);
}

void ItemModel::clearAndClean(){
    for(auto _item : listModel){
        auto item = (ItemModel*)_item;
        item->clear();
    }
    
    listModel.clear();
    descriptionItem.clear();
    nameItem.clear();
    load = false;
}

