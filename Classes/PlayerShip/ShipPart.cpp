//
//  ShipPart.cpp
//  GalaxyConquer-mobile
//
//  Created by terraHeart on 25/4/20.
//

#include "ShipPart.hpp"

ShipPart::ShipPart(int level, bool enabled, string description)
{
    this->level = level;
    this->enabled = enabled;
    this->description = description;
}

void levelUp()
{
    if(level < 3)
    {
        level += 1;
    }
}

void enable()
{
	if (enabled == false)
		enabled = true;
}

void disable()
{
	if (enabled == true)
		enabled = false;
}

int getLevel()
{
    return this->level;
}

bool getEnabled()
{
    return this->enabled;
}

string getDescription()
{
    return this->description;
}
