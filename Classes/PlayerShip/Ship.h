#ifndef __SHIP_H__
#define __SHIP_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PlayerGun.h"
#include "GlobalVariable.hpp"
#include "GlobalVariable.hpp"
#include "SpecialEffect.hpp"
#include "MyText.hpp"
#include "App42API.h"
USING_NS_CC;

class Ship:
public Sprite
{
public:
    static void loadGamelayer(Layer* _gameLayer);
            
    typedef std::function<void()> ccShipActiveShield;
    typedef std::function<void()> ccShipReborn;
        
    ccShipActiveShield onShipActiveShield;
    ccShipReborn onShipReborn;
    PhysicsBody* body;


    bool hasExplored;
    bool invunerable;

    float shieldDuration, maxShieldDuration;
    float medicBonus;
    float abortDamageTakenRate;
    float bonusGravityArea;
    float attackSpeedRate;
    float hpRatePerKilling;
    float factorSpeed,defaultSpeed;
    float armor,armorBonus;
    
    ShipSpeedState speedState;
    
    LoadingBar* hpBar;
    
    PlayerGun* playerLightGun[8];
    PlayerGun* playerRocketGun;
    Vector<PlayerGun*> arrLightGun;
    
    float hp,maxHp,halfHP,defaultMaxHp;

    static void loadResource();
    static int getLevel();
    static float getDmgBonus();
    static float getDmgRocketBonus();
    
    
    static Ship* createShip(float startX, float startY);
    
    bool initShip(float startX, float startY);
    void regenHp(float _percent);
    
    void addEffectWhenBeingDamaged();
    void clear();
    
    void updateTimer(float dt);
    
    void ReBorn(LoadingBar* _bar);
    void gainExp(int _value,LoadingBar* _bar,TextBMFont* _text);
    void onMedicRecieved();
    
    void RegenHpPerSecond(float dt);
    void TakeDamage(float _dmg);
        
    CC_SYNTHESIZE(bool, isAllowToShot, IsAllowToShot);
    
private:
    Vector<__Float*> vecI3_MaxHpStack;
    Vector<__Float*> vecG3_armorStack;
    
    bool limitEffect;
    bool activeSkillOncePerLife;
    
    int countRegenHp = 0;
};

#endif
