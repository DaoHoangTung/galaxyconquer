#include "Ship.h"
#include "TextManager.hpp"
using namespace cocos2d::ui;
using namespace cocos2d;
static bool shipToFrameCache = false;
static Layer* gameLayer = NULL;

void Ship::loadResource(){
    if (!shipToFrameCache){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Ship/PlayerShip.plist");
        shipToFrameCache = true;
    }
}

void Ship::loadGamelayer(cocos2d::Layer *_gameLayer){
    gameLayer = _gameLayer;
}

Ship* Ship::createShip(float startX, float startY){
    auto ship = new (std::nothrow) Ship();
    
    if (ship){
        if (ship->initShip(startX, startY)){
            
        }
        else{
            CC_SAFE_DELETE(ship);
        }
    }
    
    return ship;
}

#define _LIGHT_DAMAGE_ 20

bool Ship::initShip(float startX, float startY){
    
    if (!Sprite::init()){
        return false;
    }
    loadResource();
    
    this->onShipReborn = NULL;
    this->onShipActiveShield = NULL;
    
    this->invunerable = false;
    this->limitEffect = false;
    this->isAllowToShot = false;
    this->hasExplored = false;
    
    this->maxHp = defaultMaxHp ;
    this->armor =10;
    this->hp = this->maxHp;
    this->halfHP = 0.5 * this->maxHp;
    this->medicBonus = 0;
  
    auto extraAtkRate = 100;
        
    float rateFactor = (float)extraAtkRate / 100.;

    this->attackSpeedRate = rateFactor + 0.05;

    this->speedState = ShipSpeedState::ShipSpeedState_NORMAL;
    //body = PhysicsBody::createCircle(_contentSize.width/2, PhysicsMaterial(0.1f, 1.0f, 0.0f));
    this->defaultSpeed = GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)UpgradeItemOrder::itemAmor) * 0.03 + 0.11;
    
    this->activeSkillOncePerLife = true;
    this->armorBonus = 0;
 
    this->countRegenHp = 0;
    this->factorSpeed = defaultSpeed;
    
    
    //if (COCOS2D_DEBUG){
        CCLOG("[Ship] - Ship::initShip with movement speed: %.2f",this->factorSpeed);
        CCLOG("[Ship] - Ship::initShip with max hp: %.2f",this->maxHp);
        CCLOG("[Ship] - Ship::initShip with attack rate: %.2f",this->attackSpeedRate);
        CCLOG("[Ship] - Ship::initShip with aborb damage percent: %.2f",this->abortDamageTakenRate);
        CCLOG("[Ship] - Ship::initShip with Shield's duration: %.2f",this->maxShieldDuration);
    //}
    
    
    Vector<SpriteFrame*> shipFrame;
    for (int i = 0; i < 30; i++){
        char name[32] = { 0 };
        sprintf(name, "PlayerShip%d.png", i);
        shipFrame.reserve(30);
        shipFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name));
    }
    
    auto animation = Animation::createWithSpriteFrames(shipFrame, 0.2f);
    auto animate = Animate::create(animation);
    
    this->setSpriteFrame(shipFrame.at(0));
    this->setPosition(Vec2(startX, startY));
    
    auto repeatAnimate = RepeatForever::create(animate);
    
    this->runAction(repeatAnimate);
    
    shipFrame.clear();
    
    
    
    playerLightGun[(int)PlayerLightGun::Top] = new (std::nothrow) GunTop(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::Top));
    
    playerLightGun[(int)PlayerLightGun::TopLeft] = new (std::nothrow) GunTopLeft(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::TopLeft));
    
    playerLightGun[(int)PlayerLightGun::TopRight] = new (std::nothrow) GunTopRight(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::TopRight));
    
    playerLightGun[(int)PlayerLightGun::Bot] = new (std::nothrow) GunBot(_LIGHT_DAMAGE_ ,15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::Bot));
    
    playerLightGun[(int)PlayerLightGun::BotLeft] = new (std::nothrow) GunBotLeft(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::BotLeft));
    
    playerLightGun[(int)PlayerLightGun::BotRight] = new (std::nothrow) GunBotRight(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::BotRight));
    
    playerLightGun[(int)PlayerLightGun::Right] = new (std::nothrow) GunRight(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::Right));
    
    playerLightGun[(int)PlayerLightGun::Left] = new (std::nothrow) GunLeft(_LIGHT_DAMAGE_, 15, GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)PlayerLightGun::Left));
    
    for(int i = 0; i < 8;i++){
        if (GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX(i) > 0){
            arrLightGun.pushBack(playerLightGun[i]);
        }
    }
    
    body = PhysicsBody::createBox(Size(_contentSize.width, _contentSize.height), PhysicsMaterial(0.1f, 1.0f, 0.0f));
    body->setDynamic(true);
    body->setCategoryBitmask((int)PhysicCategory::PlayerShip);
    body->setCollisionBitmask((int)PhysicCategory::None);
    body->setContactTestBitmask((int)PhysicCategory::combo1);
    body->retain();
    body->setGravityEnable(false);
    body->setRotationEnable(false);
    body->setGroup((int)PhysicsGroup::GROUP_0);
    this->setTag((int)TagObject::Ship);
    
    return true;
}

void Ship::gainExp(int _value,LoadingBar* _bar,TextBMFont* _text){
    float value = (float)_value*0.7;
    
    float rand = random((float)0.5,(float)1.0);
    float defaultExp = (float)value * (float)rand;
    float expBonus = 0.;
    
}

void Ship::addEffectWhenBeingDamaged(){
    auto effectDamaged = ParticleSystemQuad::create("NewParticle/shipDamaged.plist");
    effectDamaged->resetSystem();
    effectDamaged->setAutoRemoveOnFinish(true);
    effectDamaged->setRotation(random(0, 360));
    effectDamaged->setScale(1);
    effectDamaged->setPosition(this->getContentSize()/2);
    this->addChild(effectDamaged);
    
    if (this->hp <= halfHP && !limitEffect){
        limitEffect = true;
        
        auto effectBorderBackground = ParticleSystemQuad::create("NewParticle/shipGetHitEffect.plist");
        effectBorderBackground->resetSystem();
        effectBorderBackground->setAutoRemoveOnFinish(true);
        effectBorderBackground->cocos2d::Node::setScale(1.7695,2.7589);
        effectBorderBackground->setPosition(315.04,466.74);
        gameLayer ->addChild(effectBorderBackground,1000);
        
        auto func = CallFuncN::create([&](Node* p){
            this->limitEffect = false;
        });
        
        this->runAction(Sequence::create(DelayTime::create(1),func, NULL));
    }
}

void Ship::ReBorn(cocos2d::ui::LoadingBar *_bar){
    this->hp = this->maxHp;
    GLOBAL::SET_MULTI_POINT(1);
    _bar->setPercent(100);
    
    SpecialEffect::CreateSpecialEffectAtTarget(this, "RebornEffect", 20, 0.07, 1.2);
    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_Reincarnate, false);
    
//    MyText::CreateAtLoc(this->getPosition() + Vec2(0,this->getContentSize().height/2),2, __String::create("REBORN!"),MyFont::OrangeGradient);
    
    if (this->onShipActiveShield != NULL)
        this->onShipActiveShield();
    
    this->activeSkillOncePerLife = true;
}

void Ship::clear()
{
    vecI3_MaxHpStack.clear();
    vecG3_armorStack.clear();
    
    this->unscheduleUpdate();
    this->unscheduleAllCallbacks();
    this->stopAllActions();
    this->removeFromParentAndCleanup(true);
    this->body->removeFromWorld();
    
    this->body = nullptr;
    for (auto gun : arrLightGun){
        CC_SAFE_RELEASE_NULL(gun);
    }
    arrLightGun.clear();
    
    CC_SAFE_RELEASE_NULL(this->body);
    CC_SAFE_RELEASE_NULL(this->playerRocketGun);
    this->removeAllChildren();

}
