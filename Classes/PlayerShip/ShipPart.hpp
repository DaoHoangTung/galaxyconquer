//
//  ShipPart.hpp
//  GalaxyConquer-mobile
//
//  Created by terraHeart on 25/4/20.
//

#ifndef ShipPart_hpp
#define ShipPart_hpp

#include <stdio.h>
#include <string>
using namespace std;

class ShipPart
{
private:
    int level;
    bool enabled;
    string description;

public:
    ShipPart(int level, bool enabled, string description);
    void levelUp();
	void enable();
	void disable();
    int getLevel();
    bool getEnabled();
    string getDescription();
};

#endif /* ShipPart_hpp */
