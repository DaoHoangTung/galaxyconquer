//
//  UpgradeState.cpp
//  GalaxyConquer-mobile
//
//  Created by terraHeart on 22/4/20.
//

#include <stdio.h>
#include "UpgradeState.h"
using namespace std;

UpgradeState::UpgradeState()
{
    for(int i = 0; i < GUN_TOTAL; i++)
    {
        if(i == N)
        {
            gunDirections[i] = ShipPart(1, true, "Gun fire north");
          
        }
        else
        {
            gunDirections[i] = new ShipPart(0, false, "");
        }
        
        shieldDuration = new ShipPart(0, false, "");
        shieldRange = new ShipPart(0, false, "");
        shieldRecharge = new ShipPart(0, false, "");
        shieldDamage = new ShipPart(0, false, "");

		magnet = new ShipPart(0, false, "");
		rof = new ShipPart(0, false, "");
		speed = new ShipPart(0, false, "");
		rocket = new ShipPart(0, false, "");
        
    }
}

ShipPart getGun(int direction)
{
    ShipPart* gun = &gunDirections[direction];
    return gun;
}

ShipPart getSpeed()
{
    return speed;
}

ShipPart getShieldDamage()
{
	return shieldDamage;
}

ShipPart getShieldDuration()
{
	return shieldDuration;
}

ShipPart getShieldRange()
{
	return shieldRange;
}

ShipPart getShieldRecharge()
{
	return shieldRecharge;
}

ShipPart getMagnet()
{
	return magnet;
}

ShipPart getROF()
{
	return rof;
}

ShipPart getSpeed()
{
	return speed;
}

ShipPart getRocket()
{
	return rocket;
}
