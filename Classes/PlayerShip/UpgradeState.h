//
//  UpgradeState.h
//  GalaxyConquer
//
//  Created by terraHeart on 22/4/20.
//

#ifndef UpgradeState_h
#define UpgradeState_h
#include "cocos2d.h"
#include "GlobalVariable.hpp"
#include "ShipPart.hpp"

USING_NS_CC;
using namespace std;

class UpgradeState
{
private:
#define N 0
#define NE 1
#define E 2
#define SE 3
#define S 4
#define SW 5
#define W 6
#define NW 7
#define GUN_TOTAL 8
    
    ShipPart gunDirections[GUN_TOTAL];
    ShipPart shieldDamage, shieldDuration, shieldRange, shieldRecharge;
    ShipPart magnet, rof, speed, rocket;
    
public:
    UpgradeState();
    
    ShipPart getGun(int direction);
    ShipPart getShieldDamage;
    ShipPart getShieldRange;
    ShipPart getShieldRecharge;
    ShipPart getMagnet;
    ShipPart getROF;
    ShipPart getSpeed;
	ShipPart getRocket;
};

#endif /* UpgradeState_h */
