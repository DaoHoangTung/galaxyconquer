//
//  MissionScene.hpp
//  GalaxyConquer
//
//  Created by KuKulKan on 3/22/20.
//

#ifndef MissionScene_hpp
#define MissionScene_hpp

#include "GlobalVariable.hpp"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Ship.h"
#include "PlayerBullet.h"
#include "MyMoveTo.h"
#include "Wave.h"
#include "Bullet.h"
#include "Path.h"
#include "Mission.h"
#include "Explosion.h"
#include "Item.h"
#include "SpecialEffect.hpp"
#include "LightningSprite.hpp"
#include "MyText.hpp"
#include "LobbyScene.hpp"
#include "TextManager.hpp"
#include "GunLightningV1.hpp"
#include <cocostudio/ActionTimeline/CSLoader.h>
#include "Boss.hpp"

USING_NS_CC;
using namespace cocos2d::ui;

enum MissionSceneChild{
    tagImgBackground1 = 27,
    tagImgBackground2 = 28,
    
    tagSprShipHpBar = 41,
    tagSprBossHpBar = 33,
    tagSprShipShieldBar = 32,
    tagSprShipAborbShield = 48,
    
    tagLbShipHp = 39,
    tagLbBossHp = 42,
    tagLbShipShield = 43,
    tagLbShipAborbShield = 49,
    
    tagTxtCash = 35,
    tagTxtScore = 36,
    tagTxtLife = 38,
    tagTxtLevel = 49,
    
    tagLbLevelExp = 44
};

class MissionScene :
public Layer
{
    protected:
        ListView* listLife;
        ImageView* dummyLife;
        
        TextBMFont* lScore;
        TextBMFont* lMulti;
        TextBMFont* lCash;
        TextBMFont* lLevel;
        LoadingBar* lbHp;
        LoadingBar* lbBossHp;
        LoadingBar* lbShield;
        LoadingBar* lbAborbShield;
        
        LoadingBar* lbExp;
        
        MoveTo* moveBar;
        MoveTo* moveSpriteBar;
        MoveTo* moveLevel;
        
        Sprite* sprHp;
        Sprite* sprBossHp;
        Sprite* sprShield;
        Sprite* sprAborbShield;
        
        Sequence* checkTap;
        
        int tapCount;
        
        void storeChild();
        void modifyChild();
    public:
        static MissionScene* getInstance();
        static Scene* createScene();
        static void SetMissionSceneLayer(MissionScene* layer);
        static void CreateShakeEffect(float _duration, float _range);

        Vec2 originalMainLayerPos;
        float shakeDuration, shakeRange,shakeTimeElapse;
    
        Mission* mission;

    
        Node* rootNode;
        
        ParticleSystemQuad* shipExplosionEffect;

        Vector<PhysicsBody*> vecBarBody;
        Vector<Sprite*> vecBar;
        Vector<LoadingBar*> vecLoadingBar;
        Vec2 rootPosBar[4];
        Vec2 approDistanceLoadingBar;
        
        Node* bgNode;
        ImageView* firstBackground;
        ImageView* secondBackground;
        Node* bgGalaxyNode;
                
        EventListenerTouchOneByOne* listener;
        EventListenerPhysicsContact* contactListener;
        EventListenerPhysicsContactWithGroup* contactListener2;
    
        Vec2 deltaTouch;
    
        virtual bool init();
        void createPlayerShip();
        
        virtual void BackGroundAction();
    
        void shake(float _duration, float _range);
        void updateShake(float dt);
    
        virtual void onEnterTransitionDidFinish();
        virtual bool onTouchBegan(Touch *touch, Event *event);
        virtual void onTouchMove(cocos2d::Touch *touch, cocos2d::Event *event);
    
        void update(float delta);
        void Shot(float dt);
        void ShipIsReady();
    void onSetScore(double score);
        void callBackBossAppear();
    void BossExplosion(Boss *_boss);
        void clear();
        
        void onLoadFromCCBFile();
        void onBackGroundDidOutSide(ImageView* _background);
            
        bool onContactBegan(PhysicsContact &contact);
        void onContactSeparate(cocos2d::PhysicsContact &contact);
        bool checkCollision_Bar_vs_Ship(cocos2d::Node *nodeA, cocos2d::Node *nodeB);
    bool checkCollision_Enemy_vs_Bullet(cocos2d::Node *nodeA, cocos2d::Node *nodeB);
    bool checkCollision_Boss_vs_PlayerBullet(cocos2d::Node *nodeA, cocos2d::Node *nodeB);
    
    
        void readMapInfo(int _mapIndex);
        
        CREATE_FUNC(MissionScene);

        CC_SYNTHESIZE(Ship*, playerShip, PlayerShip);
};
#endif /* MissionScene_hpp */
