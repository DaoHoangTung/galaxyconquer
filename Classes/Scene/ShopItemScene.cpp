

#include "ShopItemScene.hpp"
#include "UpgradeState.hpp"
static ShopItemScene* instance = NULL;


static int sizeH,sizeW;

Scene* ShopItemScene::createScene(){
    auto scene = Scene::create();
    
    auto layer = ShopItemScene::create();
    
    instance = layer;
    
    if (layer){
        scene->addChild(layer);
    }else{
        CC_SAFE_DELETE(layer);
    }
    
    return scene;
}


static bool load = false;
void ShopItemScene::loadResource(){
    if (!load){
        sizeW = Director::getInstance()->getWinSize().width;
        sizeH = Director::getInstance()->getWinSize().height;

        auto sprCache = SpriteFrameCache::getInstance();
        auto path1 = "NewShopItemScene/ShopItemActive.plist";
        auto path2 = "NewShopItemScene/ShopItemDisable.plist";
        
        if (!sprCache->isSpriteFramesWithFileLoaded(path1))
            sprCache->addSpriteFramesWithFile(path1);
        
        if (!sprCache->isSpriteFramesWithFileLoaded(path2))
            sprCache->addSpriteFramesWithFile(path2);
        
        load = true;
    }
}

bool ShopItemScene::init(){
    if (!Layer::init()){
        return false;
    }
    
    rootNode = CSLoader::createNode("ShopItemScene.csb");

    saveChildToLocalVariable();
	modifyLocalVariable();
    
    this->addChild(rootNode);
    
    GLOBAL::getInstance()->currentScene = GameStateScene::tag_ShopScene;

    
    return true;
}

void ShopItemScene::saveChildToLocalVariable() 
{
	string gunDirectionName[] = { 'nGun', 'neGun', 'eGun', 'seGun', 'sGun', 'swGun', 'wGun','nwGun'};
	for (int i = 0; i < 8 i++)
	{
		gunDirectionIcon[i] = (Button*)rootNode->getChildByName(gunDirectionName[i]);
		gunDirectionIcon[i]->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));
	}
	speedIcon = (Button*)rootNode->getChildByName('speed');
	speedIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	rocketIcon = (Button*)rootNode->getChildByName('rocket');
	rocketIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	rofIcon = (Button*)rootNode->getChildByName('rof');
	rofIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	magnetIcon = (Button*)rootNode->getChildByName('magnet');
	magnetIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	shieldDamageIcon = (Button*)rootNode->getChildByName('shieldDamage');
	shieldDamageIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	shieldDurationIcon = (Button*)rootNode->getChildByName('shieldDuration');
	shieldDurationIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	shieldRangeIcon = (Button*)rootNode->getChildByName('shieldRange');
	shieldRangeIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

	shieldRechargeIcon = (Button*)rootNode->getChildByName('shieldRecharge');
	shieldRechargeIcon->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

    auto backButton = (Button*)rootNode->getChildByName('back');
    backButton->addTouchEventListener(CC_CALLBACK2(ShopItemScene::callBackBtnClick, this));

}

void ShopItemScene::modifyLocalVariables()
{

}

//Should be called to change the icon whenever its switch enabled or a shipPart levels up
void ShopItemScene::modifyIcons(Button* icon)
{

}

void ShopItemScene::callBackBtnClick(cocos2d::Ref* pSender, Widget::TouchEventType type)
{
    auto _button = (Button*)pSender;

    switch (type) {
    case cocos2d::ui::Widget::TouchEventType::ENDED:
    {
        string name = _button->getName();

        switch (name) {
        case 'back':
        {
            _director->replaceScene(TransitionFade::create(1, LobbyScene::createScene()));

            break;
        }

		case 'rof':
		{
			ShipPart* rof = upgradeState->getROF();
			rof->levelUp();
			if (rof->getEnabled == false)
				rof->enable();
			break;
		}

		case 'speed':
		{
			ShipPart* speed = upgradeState->getSpeed();
			speed->levelUp();
			if (speed->getEnabled == false)
				speed->enable();
			break;
		}

		case 'magnet':
		{
			ShipPart* magnet = upgradeState->getMagnet();
			magnet->levelUp();
			if (speed->getEnabled == false)
				speed->enable();
			break;
		}

		case 'rocket':
		{
			ShipPart* rocket = upgradeState->getRocket();
			rocket->levelUp();
			if (rocket->getEnabled == false)
				rocket->enable();
			break;
		}

		case 'shieldDuration':
		{
			ShipPart* duration = upgradeState->getShieldDuration();
			duration->levelUp();
			if (level->getEnabled == false)
				level->enable();
			break;
		}

		case 'shieldRange':
		{
			ShipPart* range = upgradeState->getShieldRange();
			range->levelUp();
			if (range->getEnabled == false)
				range->enable();
			break;
		}

		case 'shieldRecharge':
		{
			ShipPart* recharge = upgradeState->getShieldRecharge();
			recharge->levelUp();
			if (recharge->getEnabled == false)
				recharge->enable();
			break;
		}

		case 'shieldDamage':
		{
			ShipPart* damage = upgradeState->getShieldDamage();
			damage->levelUp();
			if (damage->getEnabled == false)
				damage->enabled();
			break;
		}
        default:
            break;
        }
    }
    break;

    default:
        break;
    }
}
