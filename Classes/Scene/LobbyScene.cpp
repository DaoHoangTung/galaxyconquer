//
//  LobbyScene.cpp
//  Ship
//
//  Created by KuKulKan on 9/9/17.
//
//

#include "LobbyScene.hpp"
#include "GlobalVariable.hpp"
#include <cocostudio/ActionTimeline/CSLoader.h>

#include "MissionScene.hpp"

static std::string _famousSentence[14];
static std::string _author[14];

static bool isLoad = false;
static float sx= 0.01;
static float sy = 0.01;
static int CUR_SELECTED_ITEM = 0;

void LobbyScene::loadResource(){
    if (!isLoad){
        int tempInt = 0;
        _famousSentence[tempInt]  =   "I have a simple philosophy: Fill what's empty. Empty what full. Scratch where it itches";
        _author[tempInt++]          =   "Alice Roosevelt";
        
        _famousSentence[tempInt]    =   "Life is 10% what happens to you and 90% how you respond to it";
        _author[tempInt++]          =   "Lou Holtz";
        
        _famousSentence[tempInt]    =   "Nothing in life is to be feared, it's to be understood. Now is the time to understand more, so that we may fear less";
        _author[tempInt++]          =   "Marie Curie";
        
        _famousSentence[tempInt]    =   "What we think determines what happens to us, so if we want to change our lives, we need to stretch our minds";
        _author[tempInt++]          =   "Wayne Dyer";
        
        _famousSentence[tempInt]    =   "People have diffỉrent reasons for the way they live their life. You cannot put everyone's reasons in the same box";
        _author[tempInt++]          =   "Kevin Spacey";
        
        _famousSentence[tempInt]    =   "Two things are infinite: the universe and human stupidity; and I’m not sure about the universe";
        _author[tempInt++]          =   "Albert Einstein";
        
        _famousSentence[tempInt]    =   "Innovation distinguishes between a leader and a follower";
        _author[tempInt++]          =   "Steve Jobs";
        
        _famousSentence[tempInt]    =   "I cried because I had no shoes until I saw a man who had no feet";
        _author[tempInt++]          =   "Helen Keller";
        
        _famousSentence[tempInt]    =   "But what is happiness except the simple harmony between a man and the life he leads?";
        _author[tempInt++]          =   "Albert Camus";
        
        _famousSentence[tempInt]    =   "Life is partly what we make it, and partly what is made by the friends whom we choose";
        _author[tempInt++]          =   "Jehyi Hsieh";
        
        
        _famousSentence[tempInt]    =   "I’m not a tech guy. I’m looking at the technology with the eyes of my customers, normal people’s eyes";
        _author[tempInt++]          =   "Jack Ma";
        
        _famousSentence[tempInt]    =   "The difference between a boss and a leader: a boss says, 'Go' whereas a leader says, 'Let's go!'";
        _author[tempInt++]          =   "E.M. Kelly";
        
        _famousSentence[tempInt]    =   "I work all of my life to hope to become a staff of Apple Company";
        _author[tempInt++]          =   "Me!";
        
        
        
        
        for(int i = 0; i < 14; i++){
            _famousSentence[i].insert(0, "\"");
            _famousSentence[i].append("\"", _famousSentence[i].size());
        }
        
        isLoad = true;
    }
}

Scene* LobbyScene::createScene(){
    auto scene = Scene::create();
    
    auto layer = LobbyScene::create();
    
    if (layer){
        scene->addChild(layer);
    }else{
        CC_SAFE_DELETE(layer);
    }
    
    return scene;
}


bool LobbyScene::init(){
    
    if (!Layer::init()){
        return false;
    }
    
    
    rootNode = CSLoader::createNode("LobbyScene.csb");

    
    saveChildToLocalVariable();
    modifyLocalVariable();
    
    lastItem = NULL;
    
    this->addChild(rootNode);
    
    GLOBAL::getInstance()->currentScene = GameStateScene::tag_LobbyScene;

    return true;
}


void LobbyScene::saveChildToLocalVariable(){
    listView = (ListView*)rootNode->getChildByTag(1);
    backGround = (ImageView*)rootNode->getChildByTag(999);
    
    auto node = (Node*)rootNode->getChildByTag(7);
    leftBar = (ImageView*)node->getChildByTag(2);
    rightBar = (ImageView*)node->getChildByTag(3);
    nodeButton = (Node*)rootNode->getChildByTag(6);
    imgBoss = (ImageView*)node->getChildByTag(1);
    
    textDummy1 = (TextBMFont*)rootNode->getChildByTag(8);
    textDummy2 = (TextBMFont*)rootNode->getChildByTag(9);
    textGuide = (TextBMFont*)rootNode->getChildByTag(11);
    textTick = (TextBMFont*)rootNode->getChildByTag(12);
    
    backGroundEffect = (ParticleSystemQuad*)rootNode->getChildByTag(36);
    
    {
        auto btnShopGold = (Button*)rootNode->getChildByTag(2869);
        auto btnShopSP = (Button*)rootNode->getChildByTag(2871);
        btnShopGold->addTouchEventListener(CC_CALLBACK_2(LobbyScene::callBackBtnClick, this));
        btnShopSP->addTouchEventListener(CC_CALLBACK_2(LobbyScene::callBackBtnClick, this));
        
        btnShopGold->setPosition(btnShopGold->getPosition() + Vec2(-100,0));
        btnShopSP->setPosition(btnShopSP->getPosition() + Vec2(100,0));
        btnShopGold->runAction(EaseIn::create( MoveBy::create(1.5, Vec2(100,0)),0.5));
        btnShopSP->runAction(EaseIn::create( MoveBy::create(1.5, Vec2(-100,0)),0.5));
    }
}

void LobbyScene::modifyLocalVariable(){
    auto item = listView->getCenterItemInCurrentView();
    item->setTouchEnabled(true);
    int currentIndex = (int)listView->getIndex(item);
        //if (COCOS2D_DEBUG)
            CCLOG("[LobbyScene] - %s at %d",item->getName().c_str(),currentIndex);
    
    for(auto item : listView->getItems()){
        if (item->getTag() != 100 && item->getTag() != 99){
            item->setOpacity(125);
        
        item->addTouchEventListener(CC_CALLBACK_2(LobbyScene::callBackItemClick, this));
        
        if (GLOBAL::GET_BOSS_STATUS(item->getTag())){
            auto image = (ImageView*)item;
            image->loadTexture(StringUtils::format("BossDescription%d.jpg",image->getTag()),Widget::TextureResType::PLIST);
        }else{
            auto image = (ImageView*)item;
            image->loadTexture(StringUtils::format("BossDescriptionDisable%d.jpg",image->getTag()),Widget::TextureResType::PLIST);
        }
        
        auto newImgBoss = ImageView::create();
        newImgBoss->loadTexture(StringUtils::format("Boss%d.png",item->getTag()),Widget::TextureResType::PLIST);
        newImgBoss->setTag(item->getTag());
        arrImgBoss.pushBack(newImgBoss);
        }
    }
    
    //--Modify background
    auto rotate = RotateBy::create(10, 10);
    auto repeat = RepeatForever::create(rotate);
    backGround->runAction(repeat);
    
    listView->addEventListener(CC_CALLBACK_2(LobbyScene::callBackListViewScroll, this));
    listView->setScrollBarEnabled(false);
    //--Modify background
    
    
    //--Modify Bar
    leftBar->setPosition(leftBar->getPosition() + Vec2(-50,0));
    rightBar->setPosition(rightBar->getPosition() + Vec2(50,0));
    //--Modify Bar
    
    
    //--Modify group button
    for(auto button : nodeButton->getChildren()){
        auto _button = (Button*)button;
        _button->addTouchEventListener(CC_CALLBACK_2(LobbyScene::callBackBtnClick, this));
        arrDefaultPoint.pushBack(LobbyPoint::createPoint(button->getPosition()));
    }
    
    
    //--Modify border effect
    borderEffect = ParticleSystemQuad::create("NewParticle/lobbyBossBorder.plist");
    borderEffect->retain();
    
    item->addChild(borderEffect);
    borderEffect->setPosition(item->getContentSize()/2);
    borderEffect->cocos2d::Node::setScale(0.5,0.5);
    
    
    //--Modify text
    
    textDummy1->setString("");
    labelDescription = Label::createWithBMFont("fonts/BauCua_Green.fnt", "");
    labelDescription->setMaxLineWidth(500);
    labelDescription->setAnchorPoint(Vec2(0.5,1));
    labelDescription->setVerticalAlignment(TextVAlignment::CENTER);
    labelDescription->setHorizontalAlignment(TextHAlignment::CENTER);
    labelDescription->setPosition(textDummy1->getPosition());
    labelDescription->setSkewX(15);
    labelDescription->setString(_famousSentence[0]);
    rootNode->addChild(labelDescription);
    defaultPosLabelDescription = labelDescription->getPosition();
    
    textDummy2->setString("");
    labelAuthor = Label::createWithBMFont("fonts/BauCua_Red.fnt", "");
    labelAuthor->setMaxLineWidth(500);
    labelAuthor->setAnchorPoint(Vec2(0.5,1));
    labelAuthor->setVerticalAlignment(TextVAlignment::CENTER);
    labelAuthor->setHorizontalAlignment(TextHAlignment::CENTER);
    labelAuthor->setPosition(textDummy2->getPosition());
    labelAuthor->setString(_author[0]);
    rootNode->addChild(labelAuthor);
    defaultPosLabelAuthor = labelAuthor->getPosition();
    
    //--Modify pos img boss
    defaultPosImgBoss = imgBoss->getPosition();
    
    //--Modify text guide;
    auto _fadeOut = FadeOut::create(3);
    auto _fadeIn = FadeIn::create(1);
    auto action = Sequence::create(_fadeOut,_fadeIn,DelayTime::create(2), NULL);
    textGuide->runAction(RepeatForever::create(action));
    
    //--Modify textTime;
    textTick->setString("");
    
    
    //--Modify sx,sy
    sx = 0.01;
    sy = 0.01;
    
    
    //Btn for MissionRewardLayer
    btnMissionReward = (Button*)rootNode->getChildByTag(1662);
    btnMissionReward->addTouchEventListener(CC_CALLBACK_2(LobbyScene::callBackBtnClick, this));
    
    auto spanw1 = Spawn::create(ScaleTo::create(2., 1.3),RotateTo::create(2., 30), NULL);
    auto spanw2 = Spawn::create(ScaleTo::create(2., 1.),RotateTo::create(2., 0), NULL);

    btnMissionReward->runAction(RepeatForever::create(Sequence::create(spanw1,DelayTime::create(0.8),spanw2,DelayTime::create(0.8), NULL)));
    
    //Effect for Btn Reward
    imgRewardEffect = (ImageView*)rootNode->getChildByTag(718);
    
    imgEventEffect = (ImageView*)rootNode->getChildByTag(2873);
    this->callBackSpecialEventLayerExit();
    
    
    //Btn for MissionRewardLayer
    btnEvent = (Button*)rootNode->getChildByTag(888);
    btnEvent->addTouchEventListener(CC_CALLBACK_2(LobbyScene::callBackBtnClick, this));
    
//
//    auto spanw3 = Spawn::create(ScaleTo::create(2., 1.3),RotateTo::create(2., 30), NULL);
//    auto spanw4 = Spawn::create(ScaleTo::create(2., 1.),RotateTo::create(2., 0), NULL);
//
//    btnEvent->runAction(RepeatForever::create(Sequence::create(spanw3,DelayTime::create(0.8),spanw4,DelayTime::create(0.8), NULL)));
    
    
    
}

void LobbyScene::onEnterTransitionDidFinish(){
//    Layer::onEnterTransitionDidFinish();
    
    auto action1 = EaseElasticOut::create(MoveBy::create(1, Vec2(50,0)), 1);
    auto action2 = EaseElasticOut::create(MoveBy::create(1, Vec2(-50,0)), 1);
    leftBar->runAction(action1);
    rightBar->runAction(action2);
    
    if (CUR_SELECTED_ITEM > 1 && GLOBAL::GET_BOSS_STATUS(CUR_SELECTED_ITEM)){
        listView->scrollToItem(CUR_SELECTED_ITEM, Vec2(0.5,0), Vec2(0.5,0));
    }

    
    this->schedule(schedule_selector(LobbyScene::layerItemShake), 3);
    
 
}

void LobbyScene::callBackMissionRewardLayerExit(){
    
}

void LobbyScene::callBackSpecialEventLayerExit(){
    
}

void LobbyScene::callBackListViewScroll(cocos2d::Ref *target, ScrollView::EventType type){
    auto _listView = (ListView*)target;
    
    if (gameStart)return;
    
    switch (type) {
        case cocos2d::ui::ScrollView::EventType::SCROLLING:
        case cocos2d::ui::ScrollView::EventType::CONTAINER_MOVED:
        {
            auto item = (ImageView*)_listView->getCenterItemInCurrentView();
            int currentIndex = (int)listView->getIndex(item);
            //if (COCOS2D_DEBUG)
                CCLOG("[LobbyScene] - %s at index: %d",item->getName().c_str(), currentIndex);
            if (item->getTag() == 100 || item->getTag() == 99)
                return;
            if (item != lastItem && lastItem != NULL){
                auto scaleDown = ScaleTo::create(2, 1);
                auto action1 = EaseElasticOut::create(scaleDown, 0.5);
                lastItem->runAction(action1);
                lastItem->setOpacity(125);
                lastItem->setTouchEnabled(false);
                
                auto scaleUp = ScaleTo::create(2, 1.5);
                auto action2 = EaseElasticOut::create(scaleUp, 0.5);
                item->runAction(action2);
                item->setOpacity(255);
                
                lastItem = item;
                borderEffect->removeFromParentAndCleanup(true);
                SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_buttonOver, false);
                item->addChild(borderEffect);
                item->setTouchEnabled(true);
                borderEffect->setPosition(item->getContentSize()/2);
                borderEffect->cocos2d::Node::setScale(0.52,0.52);
                
                if (GLOBAL::GET_BOSS_STATUS(currentIndex)){
                    auto newImgBoss = arrImgBoss.at(currentIndex-1);
                    newImgBoss->loadTexture(StringUtils::format("Boss%d.png",item->getTag()),Widget::TextureResType::PLIST);
                    newImgBoss->setPosition(imgBoss->getPosition());
                    imgBoss->getParent()->addChild(newImgBoss);
                    imgBoss->removeFromParentAndCleanup(true);
                    imgBoss = newImgBoss;
                }else{
                    auto newImgBoss = arrImgBoss.at(currentIndex-1);
                    newImgBoss->loadTexture(StringUtils::format("Boss%dDisable.png",item->getTag()),Widget::TextureResType::PLIST);
                    newImgBoss->setPosition(imgBoss->getPosition());
                    imgBoss->getParent()->addChild(newImgBoss);
                    imgBoss->removeFromParentAndCleanup(true);
                    imgBoss = newImgBoss;
                }
                
                labelDescription->setString(_famousSentence[currentIndex-1]);
                labelAuthor->setString(_author[currentIndex-1]);
                CUR_SELECTED_ITEM = currentIndex;
                
                this->unschedule(schedule_selector(LobbyScene::updateTick));
                this->unschedule(schedule_selector(LobbyScene::bossShake));
                tick = 0;
                sx = 0.01;
                sy = 0.01;
                if (!textGuide->isVisible()){
                    textGuide->setVisible(true);
                }
                
            }else if (lastItem == NULL){
                lastItem = item;
                
                auto scaleUp = ScaleTo::create(2, 1.5);
                auto action2 = EaseElasticOut::create(scaleUp, 0.5);
                item->runAction(action2);
                
                item->setOpacity(255);
                
            }
            
        }
            break;
            
        default:
            break;
    }
}

void LobbyScene::callBackItemClick(cocos2d::Ref *pSender, Widget::TouchEventType type){
    auto item = (ImageView*)pSender;
    
    if (gameStart) return;
    
    if (!GLOBAL::GET_BOSS_STATUS(item->getTag())) return;
    
    switch (type) {
        case cocos2d::ui::Widget::TouchEventType::BEGAN:
        {
            textGuide->setVisible(false);
            this->schedule(schedule_selector(LobbyScene::updateTick), 1);
        }
            break;
            
        case cocos2d::ui::Widget::TouchEventType::MOVED:
//            tick = 0;
            break;
        
        case cocos2d::ui::Widget::TouchEventType::ENDED:
        case cocos2d::ui::Widget::TouchEventType::CANCELED:
            this->unschedule(schedule_selector(LobbyScene::updateTick));
            this->unschedule(schedule_selector(LobbyScene::bossShake));
            sx = 0.01;
            sy = 0.01;
            tick = 0;
            textGuide->setVisible(true);
            break;
            
            
        default:
            break;
    }
}

void LobbyScene::callBackBtnClick(cocos2d::Ref *pSender, Widget::TouchEventType type){
    auto _button = (Button*)pSender;
    
    switch (type) {
        case cocos2d::ui::Widget::TouchEventType::ENDED:
        {
            int tag = _button->getTag();

            switch (tag) {
                case tagBtnUpgrade:
                {
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerTeleOut, false);

                    _director->replaceScene(TransitionFade::create(1, ShopItemScene::createScene()));
                    this->scheduleOnce(schedule_selector(LobbyScene::clear), 0.1);
                    break;
                }
                    
                case tagBtnQuest:
                {
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerTeleOut, false);
//
//
//                    _director->replaceScene(TransitionFade::create(1, GalaxyChallengeRankScene::getScene()));
                    
                    break;
                }
                    
                case tagBtnBonus:
                {
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerTeleOut, false);

//                    _director->replaceScene(TransitionFade::create(1, SkillScene::createScene()));
//
//
//
                    break;
                }
                    
                case tagBtnShopz:
                {
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerTeleOut, false);
                    
//                    _director->replaceScene(TransitionFade::create(1, IAPScene::createScene()));
                    break;
                }
                    
                case tagBtnBack:
                {
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerTeleOut, false);

//
//                    _director->replaceScene(TransitionFade::create(1, MainMenuScene::createScene()));
//
                    this->scheduleOnce(schedule_selector(LobbyScene::clear), 0.1);
                    break;
                }
                    
                case tagBtnRank:
                {
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerTeleOut, false);
                    
//
//                    _director->replaceScene(TransitionFade::create(1, RankScene::getScene()));
                    
                    this->scheduleOnce(schedule_selector(LobbyScene::clear), 0.1);
                    break;
                }
            
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

void LobbyScene::layerItemShake(float dt){
    
    for(auto model : nodeButton->getChildren()){
        int index = (int)nodeButton->getChildren().getIndex(model);
        int range = random(5, 10);
        float angle = random(0, 360);
        float x = arrDefaultPoint.at(index)->x + range * cos(CC_DEGREES_TO_RADIANS(angle));
        float y = arrDefaultPoint.at(index)->y + range * sin(CC_DEGREES_TO_RADIANS(angle));
        
        auto move = MoveTo::create(3, Vec2(x, y));
        model->runAction(move);
    }
    
    {
        int range = random(5, 10);
        float angle = random(0, 360);
        float x = defaultPosLabelDescription.x + range * cos(CC_DEGREES_TO_RADIANS(angle));
        float y = defaultPosLabelDescription.y + range * sin(CC_DEGREES_TO_RADIANS(angle));
        
        auto move = MoveTo::create(3, Vec2(x, y));
        labelDescription->runAction(move);
        
    }
    
    {
        int range = random(5, 10);
        float angle = random(0, 360);
        float x = defaultPosLabelAuthor.x + range * cos(CC_DEGREES_TO_RADIANS(angle));
        float y = defaultPosLabelAuthor.y + range * sin(CC_DEGREES_TO_RADIANS(angle));
        
        auto move = MoveTo::create(3, Vec2(x, y));
        labelAuthor->runAction(move);
        
    }
    
    {
        if (textGuide->isVisible()){
            int range = random(15, 25);
            float angle = random(0, 360);
            float x = defaultPosImgBoss.x + range * cos(CC_DEGREES_TO_RADIANS(angle));
            float y = defaultPosImgBoss.y + range * sin(CC_DEGREES_TO_RADIANS(angle));
            
            auto move = MoveTo::create(3, Vec2(x, y));
            imgBoss->runAction(move);
        }
    }
}


void LobbyScene::bossShake(float dt){
    sx+= 0.03;
    sy+=0.07;
    int range = random(sx, sy);
    float angle = random(0, 360);
    float x = defaultPosImgBossShake.x + range * cos(CC_DEGREES_TO_RADIANS(angle));
    float y = defaultPosImgBossShake.y + range * sin(CC_DEGREES_TO_RADIANS(angle));
    
    imgBoss->setPosition(Vec2(x,y));
}

void LobbyScene::updateTick(float dt){
    tick++;
    
    textTick->stopAllActions();
    textTick->setScale(4);
    textTick->setOpacity(255);
    
    if (tick==1){
        this->schedule(schedule_selector(LobbyScene::bossShake), 1/60);
        defaultPosImgBossShake = imgBoss->getPosition();
    }

    if (tick == 4){
        gameStart = true;
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Duel_fp, false);
        tick = 0;
        textTick->setString("Fight !");
        this->addUntouchableLayer();
        this->textGuide->setVisible(false);
        
        auto scaleDown = ScaleTo::create(1, 2.5);
        auto action = EaseElasticOut::create(scaleDown, 0.75);
        textTick->runAction(action);
        
        auto scaleUp = ScaleTo::create(1, 5);
        auto fadeOut = FadeOut::create(0.75);
        auto action2 = EaseElasticOut::create(scaleUp, 1);
        auto spawn = Spawn::create(action2,fadeOut, NULL);
        imgBoss->runAction(spawn);

        listView->setTouchEnabled(false);
        this->schedule(schedule_selector(LobbyScene::startGameIn), 5);
        this->unschedule(schedule_selector(LobbyScene::bossShake));
        this->unschedule(schedule_selector(LobbyScene::updateTick));

    }else{
        textTick->setString(StringUtils::format("%d !",4-tick));
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Chaos_Bolt_target, false);
        auto scaleDown = ScaleTo::create(1, 2.5);
        auto fadeOut = FadeOut::create(0.75);
        auto action = EaseElasticOut::create(scaleDown, 0.75);
        auto spawn = Spawn::create(action,fadeOut, NULL);
        textTick->runAction(spawn);
    }
}

void LobbyScene::startGameIn(float dt){
    if (GLOBAL::GET_BOSS_STATUS(imgBoss->getTag())){
        
        GLOBAL::SET_CURRENT_MISSION_INDEX(imgBoss->getTag());
        _director->replaceScene(TransitionFade::create(1, MissionScene::createScene()));
        
        listView->setTouchEnabled(false);
        this->scheduleOnce(schedule_selector(LobbyScene::clear), 0.1);
    }
}

void LobbyScene::addUntouchableLayer(){
    auto layer = Layer::create();
    this->setContentSize(this->getContentSize());
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(LobbyScene::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, layer);
    
    this->addChild(layer);
}

bool LobbyScene::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event){
    return true;
}

void LobbyScene::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event){
    
}


void LobbyScene::clear(float dt){
    gameStart = true;
    btnMissionReward->stopAllActions();
    imgRewardEffect->stopAllActions();
    this->unscheduleAllCallbacks();
    arrImgBoss.clear();
    for(auto point : arrDefaultPoint){
        point->release();
    }
    
    arrDefaultPoint.clear();
    borderEffect->removeFromParentAndCleanup(true);
    borderEffect->release();
    
}

