
#ifndef ShopItemScene_hpp
#define ShopItemScene_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "MyText.hpp"
#include "GlobalVariable.hpp"
#include "ItemUpgradable.hpp"
#include "UpgradeState.hpp"
#include <cocostudio/ActionTimeline/CSLoader.h>
#include "MissionScene.hpp"



USING_NS_CC;
using namespace cocos2d::ui;


class ShopItemScene : public Layer{
public:
    static void loadResource();
    static Scene* createScene();
    virtual bool init();
    CREATE_FUNC(ShopItemScene);

private:
    Node* rootNode;

	Button* gunDirectionIcon[8];
	Button* rocketIcon;
	Button* shieldDamageIcon;
	Button* shieldRechargeIcon;
	Button* shieldDurationIcon;
	Button* shieldRangeIcon;
	Button* magnetIcon;
	Button* rofIcon;
	Button* speedIcon;

	UpgradeState* upgradeState;

    void saveChildToLocalVariable();
	void modifyLocalVariables();

    void modifyIcons();
    void callBackBtnClick(Ref* pSender, Widget::TouchEventType type);

    

    
};


#endif /* ShopItemScene_hpp */
