//
//  MissionScene.cpp
//  GalaxyConquer
//
//  Created by KuKulKan on 3/22/20.
//

#include "MissionScene.hpp"
#include "TextCashManager.hpp"
#include "SuperUnit.hpp"
#include "MissionManager.hpp"
static float GAME_SEC_ELAPSE = 0;

static int sizeW, sizeH;
using namespace cocos2d::ui;
using namespace CocosDenshion;
static MissionScene* mainSceneLayer = NULL;
static bool collision = false; //Check lightning collise with boss just one !

MissionScene* MissionScene::getInstance(){
    return mainSceneLayer;
}

Scene* MissionScene::createScene(){
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setGravity(Vec2(0,0));
    auto layer = MissionScene::create();
    GLOBAL::SET_MULTI_POINT(1);
    
    
    scene->addChild(layer);
    mainSceneLayer = layer;
    
    SoundBand::getInstance()->playBackGround();
    return scene;
}

bool MissionScene::init(){
    if (!Layer::init()){
        return false;
    }
    
    sizeW = Director::getInstance()->getWinSize().width;
    sizeH = Director::getInstance()->getWinSize().height;
    
    
    this->tapCount = 0;
    rootNode = CSLoader::createNode("MissionScene.csb");

    this->addChild(rootNode);
    this->storeChild();
    this->modifyChild();
    
    this->createPlayerShip();
    this->onLoadFromCCBFile();
    this->BackGroundAction();
    
    //Load player bullet resources.
    PlayerBullet::loadResource((Layer*)rootNode);
    LightBullet::loadLightBullet();
    
    //Set up listener for physic
    contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(MissionScene::onContactBegan, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
    
    contactListener2 = EventListenerPhysicsContactWithGroup::create((int)PhysicsGroup::GROUP_0);
    contactListener2->EventListenerPhysicsContactWithGroup::onContactSeparate = CC_CALLBACK_1(MissionScene::onContactSeparate, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener2, this);
    
        GLOBAL::getInstance()->currentScene = GameStateScene::tag_MissionScene;

    
    return true;
}

void MissionScene::storeChild(){
    
    
    auto imgBGScore = (ImageView*)rootNode->getChildByTag(46);
    imgBGScore->setLocalZOrder(5);
    

    auto _node = (Node*)imgBGScore->getChildByTag(34);

    listLife = (ListView*)_node->getChildByTag(tagTxtLife);
    lCash = (TextBMFont*)_node->getChildByTag(tagTxtCash);
    lScore = (TextBMFont*)_node->getChildByTag(tagTxtScore);
    lMulti = (TextBMFont*)lScore->getChildByTag(1);

    lLevel = (TextBMFont*)rootNode->getChildByTag(47)->getChildByTag(tagTxtLevel);
    
    dummyLife = (ImageView*)rootNode->getChildByTag(1029);
    
    auto _nodeBar = (Node*)rootNode->getChildByTag(30);
    _nodeBar->setLocalZOrder(3);
    
    lbHp = (LoadingBar*)rootNode->getChildByTag(30)->getChildByTag(tagLbShipHp);
    lbBossHp = (LoadingBar*)rootNode->getChildByTag(30)->getChildByTag(tagLbBossHp);
    lbShield = (LoadingBar*)rootNode->getChildByTag(30)->getChildByTag(tagLbShipShield);
    lbAborbShield = (LoadingBar*)rootNode->getChildByTag(30)->getChildByTag(tagLbShipAborbShield);
    
    lbExp = (LoadingBar*)rootNode->getChildByTag(47)->getChildByTag(tagLbLevelExp);
    
    sprHp = (Sprite*)rootNode->getChildByTag(30)->getChildByTag(tagSprShipHpBar);
    sprBossHp = (Sprite*)rootNode->getChildByTag(30)->getChildByTag(tagSprBossHpBar);
    sprShield = (Sprite*)rootNode->getChildByTag(30)->getChildByTag(tagSprShipShieldBar);
    sprAborbShield = (Sprite*)rootNode->getChildByTag(30)->getChildByTag(tagSprShipAborbShield);

    
    bgNode = (Node*)rootNode->getChildByTag(29);
    firstBackground = (ImageView*)bgNode->getChildByTag(tagImgBackground1);
    secondBackground = (ImageView*)bgNode->getChildByTag(tagImgBackground2);
    bgNode->setVisible(false);
    
    bgGalaxyNode = (Node*)rootNode->getChildByTag(161);
    bgGalaxyNode->setVisible(false);
    
    shipExplosionEffect = (ParticleSystemQuad*)rootNode->getChildByTag(1035);
    shipExplosionEffect->stopSystem();

    vecBar.pushBack(sprHp);
    vecBar.pushBack(sprShield);
    vecBar.pushBack(sprBossHp);
    vecBar.pushBack(sprAborbShield);

    vecLoadingBar.pushBack(lbHp);
    vecLoadingBar.pushBack(lbShield);
    vecLoadingBar.pushBack(lbBossHp);
    vecLoadingBar.pushBack(lbAborbShield);

}

void MissionScene::modifyChild(){
    GAME_SEC_ELAPSE = 0;
    listLife->removeAllItems();
    listLife->forceDoLayout();
    listLife->setScrollBarEnabled(false);
    listLife->setEnabled(false);
    
    int curLife = GLOBAL::GET_LIFE();
    
    for(int i = 0; i < GLOBAL::getInstance()->maxLife; i++){
        auto imgLife = (ImageView*)dummyLife->clone();
        if (curLife > i)
            imgLife->setColor(Color3B::WHITE);
        else
            imgLife->setColor(Color3B(77, 77, 77));

        listLife->pushBackCustomItem(imgLife);
    }
    
    listLife->forceDoLayout();

    GLOBAL::SET_LIFE(GLOBAL::GET_LIFE());
    GLOBAL::STORE_LABEL_SCORE(lScore);
    lCash->setString("$" + GLOBAL::GET_CASH_STRING());
    
}

void MissionScene::onLoadFromCCBFile(){
    
    approDistanceLoadingBar = sprHp->getPosition() - lbHp->getPosition();
    
    for(auto bar : vecBar){
        auto barBody = PhysicsBody::createBox(bar->getContentSize(),PhysicsMaterial(0.1f,1.0f,0.0f));
        barBody->setDynamic(true);
        barBody->setCategoryBitmask((int)PhysicCategory::Bar);
        barBody->setCollisionBitmask((int)PhysicCategory::None);
        barBody->setContactTestBitmask((int)PhysicCategory::PlayerShip);
        barBody->setGravityEnable(false);
        barBody->setRotationEnable(false);
        barBody->setGroup((int)PhysicsGroup::GROUP_0);
        bar->setTag((int)TagObject::Bar);
        vecBarBody.pushBack(barBody);
        rootPosBar[vecBar.getIndex(bar)] = bar->getPosition();
    }
    
    this->moveBar = MoveTo::create(1, lbExp->getPosition());
    this->moveLevel = MoveTo::create(1, lLevel->getPosition());
    
    lbExp->setPosition(lbExp->getPosition() - Vec2(0,200));
    lLevel->setPosition(lLevel->getPosition() - Vec2(0,200));
    
    lbBossHp->setPosition(lbBossHp->getPosition() + Vec2(50,0));
    sprBossHp->setPosition(sprBossHp->getPosition() + Vec2(50,0));
    
    lbShield->setPosition(lbShield->getPosition() + Vec2(-50,0));
    sprShield->setPosition(sprShield->getPosition() + Vec2(-50,0));
    
    moveBar->retain();
    moveLevel->retain();
}


void MissionScene::BackGroundAction(){
    bgNode->setVisible(true);
    auto onDidOutSide = CallFuncN::create(CC_CALLBACK_0(MissionScene::onBackGroundDidOutSide, this,firstBackground));
    firstBackground->runAction(Sequence::createWithTwoActions(MoveTo::create(40, Vec2(firstBackground->getPosition() + Vec2(0,sizeH))), onDidOutSide));
    auto onDidOutSide2 = CallFuncN::create(CC_CALLBACK_0(MissionScene::onBackGroundDidOutSide, this,secondBackground));
    secondBackground->runAction(Sequence::createWithTwoActions(MoveTo::create(80, Vec2(secondBackground->getPosition() + Vec2(0,sizeH*2))), onDidOutSide2));
}

void MissionScene::CreateShakeEffect(float _duration, float _range){
    mainSceneLayer->shake(_duration, _range);
}

void MissionScene::shake(float _duration, float _range){
    shakeDuration = _duration;
    shakeRange = _range;
    shakeTimeElapse = 0;
    if (!this->isScheduled(schedule_selector(MissionScene::updateShake)))
        this->schedule(schedule_selector(MissionScene::updateShake), 0.03);
}

void MissionScene::updateShake(float dt){
    shakeTimeElapse+= dt;
    if (shakeTimeElapse <= shakeDuration){
        float per = shakeTimeElapse/shakeDuration;
        float tempRange = shakeRange - shakeRange*per;
        
        int range = tempRange;
        float angle = random(0, 360);
        float x = originalMainLayerPos.x + range * cos(CC_DEGREES_TO_RADIANS(angle));
        float y = originalMainLayerPos.y + range * sin(CC_DEGREES_TO_RADIANS(angle));
        
        this->setPosition(Vec2(x,y));
    }else{
        this->unschedule(schedule_selector(MissionScene::updateShake));
    }
    
}

void MissionScene::onBackGroundDidOutSide(ImageView* _background){
    if (_background->getTag() == tagImgBackground1){
        secondBackground->stopAllActions();
        _background->setPosition(secondBackground->getPosition() - Vec2(0,sizeH));
        auto onDidOutSide2 = CallFuncN::create(CC_CALLBACK_0(MissionScene::onBackGroundDidOutSide, this,secondBackground));
        secondBackground->runAction(Sequence::createWithTwoActions(MoveTo::create(40, Vec2(secondBackground->getPosition() + Vec2(0,sizeH))), onDidOutSide2));
    }else if (_background->getTag() == tagImgBackground2){
        firstBackground->stopAllActions();
        _background->setPosition(firstBackground->getPosition() - Vec2(0,sizeH));
        auto onDidOutSide = CallFuncN::create(CC_CALLBACK_0(MissionScene::onBackGroundDidOutSide, this,firstBackground));
        firstBackground->runAction(Sequence::createWithTwoActions(MoveTo::create(40, Vec2(firstBackground->getPosition() + Vec2(0,sizeH))), onDidOutSide));
    }
    
    auto onDidOutSide = CallFuncN::create(CC_CALLBACK_0(MissionScene::onBackGroundDidOutSide, this,_background));
    _background->runAction(Sequence::createWithTwoActions(MoveTo::create(80, Vec2(_background->getPosition() + Vec2(0,sizeH*2))), onDidOutSide));
    
}

void MissionScene::createPlayerShip(){
    
    mission = MissionManager::GetMission(1);
    
    mission->onBossAppearCallback = CC_CALLBACK_0(MissionScene::callBackBossAppear, this);
    
    CCLOG("MissionScene::createPlayerShip()");
    
    setPlayerShip(Ship::createShip(320, -50));
    getPlayerShip()->hpBar = vecLoadingBar.at(0); //0 -> loadingbar of hp
        
    rootNode->addChild(getPlayerShip(), 4);
    rootNode->addChild(TextManager::getInstance(),2);
    rootNode->addChild(mission, 1);
//    this->readMapInfo(GLOBAL::GET_CURRENT_MISSION_INDEX());
    
    
    this->schedule(schedule_selector(MissionScene::Shot));
    this->scheduleUpdate();
    
    Bullet::setMissionLayer((Layer*)mission);
    PlayerBullet::loadResource((Layer*)mission);
    Explosion::setMissionLayer((Layer*)mission);
    Unit::setMissionLayer((Layer*)mission);
    Wave::setMissionlayer((Layer*)mission);
    MyText::setMissionLayer((Layer*)mission);
    Coin::setMissionlayerAndShip((Layer*)mission, (Sprite*)getPlayerShip());
    LightningSprite::loadMissionLayer((Layer*)mission);
    SpecialEffect::loadMissionLayer((Layer*)mission);
    Ship::loadGamelayer((Layer*)rootNode);
    TextCashManager::loadGameLayer((Layer*)rootNode);
    
}

void MissionScene::onEnterTransitionDidFinish(){
    Layer::onEnterTransitionDidFinish();

    auto moveToPosStart = MoveBy::create(1, Vec3(0, 200, 0));
    auto calFunc = CallFuncN::create(CC_CALLBACK_0(MissionScene::ShipIsReady, this));
    
    auto sed = Sequence::create(moveToPosStart, calFunc, nullptr);
    getPlayerShip()->runAction(sed);
    
    if (GLOBAL::GET_CURRENT_MISSION_INDEX() == GLOBAL::GET_DEVIL_SQUARE_MAP_INDEX()){
    }
    
    mission->start();
}

static Vec2 touchPoint;
bool MissionScene::onTouchBegan(Touch* touch, Event* event){
    tapCount++;
    
    if (tapCount == 2){
        tapCount = 0;
        this->stopAction(checkTap);
    }else{
        auto func = CallFuncN::create([&](Node* pSender){
            this->tapCount = 0;
        });
        
        checkTap = Sequence::create(DelayTime::create(0.25),func, NULL);
        this->runAction(checkTap);
    }
    
    return true;
}

void MissionScene::onTouchMove(Touch* touch, Event* event){
    this->deltaTouch = touch->getDelta();
}

static float widthShip,heightShip;

void MissionScene::ShipIsReady(){
    getPlayerShip()->setPhysicsBody(getPlayerShip()->body);
    
    getPlayerShip()->setIsAllowToShot(true);
    
    for(auto bar : vecBar){
        bar->setPhysicsBody(vecBarBody.at(vecBar.getIndex(bar)));
    }
    
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(MissionScene::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(MissionScene::onTouchMove, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    widthShip = getPlayerShip()->getContentSize().width;
    heightShip = getPlayerShip()->getContentSize().height;
    
    lLevel->runAction(moveLevel);
    lbExp->runAction(moveBar);
    
}

static bool shotSound = false;

void MissionScene::Shot(float dt){
    if (getPlayerShip()->getIsAllowToShot()){
        for (auto lightGun : getPlayerShip()->arrLightGun){
            float reductionPercent = getPlayerShip()->attackSpeedRate;
            float realCooldown = 0.25f;
            
            CCLOG("[MissionScene] - MissionScene::Shot Bullet with ShipRate: %.2f, realCD: %.2f",reductionPercent,realCooldown);
            
            if (lightGun->elapseCooldown >= realCooldown){
                lightGun->fire(getPlayerShip()->getPosition());
                lightGun->elapseCooldown = 0;
                
                if (!shotSound){
                    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerShoot, false);

                    shotSound = true;
                }
            }
            else{
                lightGun->elapseCooldown += dt;
            }
            
        }
        
        shotSound = false;
    }
    
}

void MissionScene::onSetScore(double score){
    GLOBAL::SET_SCORE(score);
}

void MissionScene::BossExplosion(Boss *_boss){
    shake(2, 8);
    
    for (int index = 0; index < (360/15); index++) {
        Explosion::createExplosionTowardAngle(_boss->getPosition(), 15*index, 50, ExplosionType::LargeExplosion);
    }
    
    LightningSprite::StopAllMovingLightning();
    
    _boss->clear();
    _boss = nullptr;
    
//    for (auto rocket : Rocket::getArrRocketMoving()){
//        Explosion::createExplosionAtPos(rocket->getPosition(), 5, ExplosionType::SmallExplosion);
//        rocket->recycle();
//    }
    
    Bullet::reloadBullet();
    
    this->unschedule(schedule_selector(MissionScene::Shot));

    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyBossExplode, false);
    vecLoadingBar.at(2)->setPercent(0);
    
    {
        int curMapIndex = GLOBAL::GET_CURRENT_MISSION_INDEX();
        GLOBAL::SET_BOSS_LEVEL(GLOBAL::GET_CURRENT_MISSION_INDEX()-1, 5);
        GLOBAL::getInstance()->isWin = true;
        if (GLOBAL::GET_CURRENT_MISSION_INDEX() < 13 && !GLOBAL::GET_BOSS_STATUS(curMapIndex+1)){
            GLOBAL::SET_BOSS_STATUS(curMapIndex+1,true);
        }
    }
    
}

static Vector<Bullet*> arrBulletRemoving;

void MissionScene::update(float delta)
{
    collision = false;
    
    {
        GAME_SEC_ELAPSE+= delta;
        if (GAME_SEC_ELAPSE>= 1){
            GLOBAL::getInstance()->totalGameSec++;
            GAME_SEC_ELAPSE = 0.;
        }
    }
    
    if (lMulti != NULL && lScore != NULL)
        lMulti->setPosition(Vec2(lScore->getContentSize().width+3,13.82));

    
    if (playerShip !=NULL){
        float shipX = getPlayerShip()->getPositionX();
        float shipY = getPlayerShip()->getPositionY();
        
        Vec2 vecMove = MAX(getPlayerShip()->factorSpeed,0) * deltaTouch;
        
        getPlayerShip()->setPosition(getPlayerShip()->getPosition() + vecMove);
        
        
        if (shipX + vecMove.x < widthShip/2){
            getPlayerShip()->setPositionX(widthShip/2);
        }
        
        if (shipX + vecMove.x > sizeW-widthShip/2){
            getPlayerShip()->setPositionX(sizeW - widthShip/2);
        }
        
        if (shipY + vecMove.y < heightShip/2){
            getPlayerShip()->setPositionY(heightShip/2);
        }
        
        if (shipY + vecMove.y > (sizeH-75)-heightShip/2){
            getPlayerShip()->setPositionY((sizeH-75) - heightShip/2);
            
        }
    }
    
    lbHp->setPosition(sprHp->getPosition() - approDistanceLoadingBar);
    lbShield->setPosition(sprShield->getPosition() - approDistanceLoadingBar);
    lbBossHp->setPosition(sprBossHp->getPosition() - approDistanceLoadingBar);
    lbAborbShield->setPosition(sprAborbShield->getPosition() - approDistanceLoadingBar);

    if (playerShip != NULL){
        if (getPlayerShip()->invunerable){
            lbShield->setPercent((getPlayerShip()->shieldDuration/getPlayerShip()->maxShieldDuration)*100);
        }else{
            if (lbShield->getPercent() != 0){
                lbShield->setPercent(0);
            }
        }
    }
    
    
    //Order unit attack
   if (Unit::getArrUnitCurrentOnMap().size() > 0){
       for (auto unit : Unit::getArrUnitCurrentOnMap()){
           unit->attack(delta);
       }
   }
    
    //Enemy bullet action
    Explosion::update();
    
    for (auto bullet : Bullet::getArrBulletMoving()){
        bullet->setPosition(bullet->getPosition() + bullet->vectorMove);
        if (bullet->getPositionX() > sizeW || bullet->getPositionX() < 0 || bullet->getPositionY() > (sizeH-75) || bullet->getPositionY() < 0){
            arrBulletRemoving.pushBack(bullet);
        }
    }
    
    for (auto bullet : arrBulletRemoving){
        bullet->stop();
    }
    
    arrBulletRemoving.clear();
}

bool MissionScene::onContactBegan(PhysicsContact &contact)
{
    
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    if (!nodeA || !nodeB){
        return false;
    }
    
    
    if (this->checkCollision_Bar_vs_Ship(nodeA, nodeB))
        return true;
    
    if (this->checkCollision_Enemy_vs_Bullet(nodeA, nodeB))
    return true;
    
    if (this->checkCollision_Boss_vs_PlayerBullet(nodeA, nodeB))
    return true;
    
    return true;
}

void MissionScene::onContactSeparate(cocos2d::PhysicsContact &contact){
    
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    if (nodeA == NULL || nodeB == NULL){return;}
    
    if ((nodeA->getTag() == (int)TagObject::Bar && nodeB->getTag() == (int)TagObject::Ship)
        || (nodeA->getTag() == (int)TagObject::Ship && nodeB->getTag() == (int)TagObject::Bar)){
        
        bool isABarVsBShip = nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bar && nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::PlayerShip;
        
        bool isAShipVsBBar = nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::PlayerShip && nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bar;
        
        if (isABarVsBShip){
            auto bar = dynamic_cast<Sprite*>(nodeA);
            auto loadingBar = vecLoadingBar.at(vecBar.getIndex(bar));
            loadingBar->setOpacity(255);
            bar->setOpacity(255);
            
            auto backToPos = MoveTo::create(1, rootPosBar[vecBar.getIndex(bar)]);
            bar->runAction(backToPos);
            
        }else if (isAShipVsBBar){
            auto bar = dynamic_cast<Sprite*>(nodeB);
            auto loadingBar = vecLoadingBar.at(vecBar.getIndex(bar));
            loadingBar->setOpacity(255);
            bar->setOpacity(255);
            
            auto backToPos = MoveTo::create(1, rootPosBar[vecBar.getIndex(bar)]);
            bar->runAction(backToPos);
            
        }
    }
    
}

bool MissionScene::checkCollision_Bar_vs_Ship(cocos2d::Node *nodeA, cocos2d::Node *nodeB){
    if ((nodeA->getTag() == (int)TagObject::Bar && nodeB->getTag() == (int)TagObject::Ship)
        || (nodeA->getTag() == (int)TagObject::Ship && nodeB->getTag() == (int)TagObject::Bar)){
        
        bool isABarVsBShip = nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bar && nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::PlayerShip;
        
        bool isAShipVsBBar = nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::PlayerShip && nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bar;
        
        if (isABarVsBShip){
            auto bar = dynamic_cast<Sprite*>(nodeA);
            
            auto loadingBar = vecLoadingBar.at(vecBar.getIndex(bar));
            loadingBar->setOpacity(125);
            bar->setOpacity(125);
            
            bar->stopAllActions();
            return true;
        }else if (isAShipVsBBar){
            auto bar = dynamic_cast<Sprite*>(nodeB);
            
            auto loadingBar = vecLoadingBar.at(vecBar.getIndex(bar));
            loadingBar->setOpacity(125);
            bar->setOpacity(125);
            
            bar->stopAllActions();
            return true;
        }
    }
    
    return false;
}

void MissionScene::callBackBossAppear(){
    sprBossHp->runAction(MoveBy::create(1.5, Vec2(-50,0)));
}

void MissionScene::readMapInfo(int _mapIndex){
    mission = Mission::create();

    //Move the boss hp's sprite to the scene
    mission->onBossAppearCallback = CC_CALLBACK_0(MissionScene::callBackBossAppear, this);

    //Add all the wave to scene hiarachy
    for(auto item : mission->arrWave){
        mission->addChild(item);
    }
}


bool MissionScene::checkCollision_Enemy_vs_Bullet(cocos2d::Node *nodeA, cocos2d::Node *nodeB){
    bool isAEnemyVsBBullet = nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Enemy && nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bullet;
    bool isBEnemyVsABullet = nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Enemy && nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bullet;
    
    if (isAEnemyVsBBullet || isBEnemyVsABullet){
        if (isAEnemyVsBBullet){
            auto unit = dynamic_cast<Unit*>(nodeA);
            auto bullet = dynamic_cast<PlayerBullet*>(nodeB);
            
            unit->takeDamage(bullet->dmg);
            if (unit->hp <= 0){                unit->dropItem((int)GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)UpgradeItemOrder::itemGravity));
                unit->Explode(bullet);
                
                onSetScore(unit->maxHp * 1 * GLOBAL::GET_MULTI_POINT());
                getPlayerShip()->gainExp(unit->maxHp, lbExp,lLevel);
                unit->setState(UnitState::Dead);
//                this->CheckScoreSkill();
                
                
            }else{
                unit->barStart();
                SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyBulletPain, false);            }
            
            
            bullet->recycle();
        }
        else if (isBEnemyVsABullet){
            auto unit = dynamic_cast<Unit*>(nodeB);
            auto bullet = dynamic_cast<PlayerBullet*>(nodeA);
            
            unit->takeDamage(bullet->dmg);
            
            if (unit->hp <= 0){
                unit->dropItem((int)GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)UpgradeItemOrder::itemGravity));
                
                unit->Explode(bullet);
                
                onSetScore(unit->maxHp * GLOBAL::GET_SCORE_FACTOR() * GLOBAL::GET_MULTI_POINT());
                
                getPlayerShip()->gainExp(unit->maxHp, lbExp,lLevel);
                unit->setState(UnitState::Dead);
                
            }else{
                unit->barStart();
                
                SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyBulletPain, false);
            }
            
            bullet->recycle();
        }
        
        return true;
    }
    
    return false;
}


bool MissionScene::checkCollision_Boss_vs_PlayerBullet(cocos2d::Node *nodeA, cocos2d::Node *nodeB){
    bool isABoss_vs_BLightBullet = nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Boss && nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bullet;
    
    bool isBBoss_vs_ALightBullet = nodeB->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Boss && nodeA->getPhysicsBody()->getCategoryBitmask() == (int)PhysicCategory::Bullet;
    
    if ((isABoss_vs_BLightBullet || isBBoss_vs_ALightBullet) && !collision){
        if (isABoss_vs_BLightBullet){
            auto tempBoss = dynamic_cast<Boss*>(nodeA);
            auto tempBullet = dynamic_cast<PlayerBullet*>(nodeB);
            
            collision = true;
            if (!tempBoss->invunerable){
                if (tempBoss->hp > 0){
        
                    tempBoss->takeDamage(tempBullet->dmg);
                    
                }else{
//                    getPlayerShip()->regenHpPerKilling(tempBoss->maxHp);
                    tempBoss->dropItem(GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)UpgradeItemOrder::itemGravity));
                    
                    onSetScore(tempBoss->maxHp * GLOBAL::GET_SCORE_FACTOR() * GLOBAL::GET_MULTI_POINT());
                    getPlayerShip()->gainExp(tempBoss->maxHp, lbExp,lLevel);
                    BossExplosion(tempBoss);
//                    this->CheckScoreSkill();
                    return true;
                }
                
                float tempX = (float)tempBoss->hp/(float)tempBoss->maxHp;
                vecLoadingBar.at(2)->setColor(Color3B(255. -255.*tempX, 255.*tempX, 0));
                
                vecLoadingBar.at(2)->setPercent(100*MAX(0, (float)tempBoss->hp/(float)tempBoss->maxHp));
                
                
            }
            tempBullet->recycle();
            return true;
        }else if (isBBoss_vs_ALightBullet){
            auto tempBoss = dynamic_cast<Boss*>(nodeB);
            auto tempBullet = dynamic_cast<PlayerBullet*>(nodeA);
            
            collision = true;
            if (!tempBoss->invunerable){
                if (tempBoss->hp > 0){
                    tempBoss->takeDamage(tempBullet->dmg);
                    
                }else{
                      tempBoss->dropItem(GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX((int)UpgradeItemOrder::itemGravity));
                    
                    onSetScore(tempBoss->maxHp * GLOBAL::GET_SCORE_FACTOR() * GLOBAL::GET_MULTI_POINT());
                    getPlayerShip()->gainExp(tempBoss->maxHp, lbExp,lLevel);
                    
                    BossExplosion(tempBoss);

                    return true;
                    
                }
                
                float tempX = (float)tempBoss->hp/(float)tempBoss->maxHp;
                vecLoadingBar.at(2)->setColor(Color3B(255. -255.*tempX, 255.*tempX, 0));
                vecLoadingBar.at(2)->setPercent(100*MAX(0, (float)tempBoss->hp/(float)tempBoss->maxHp));
                
            }
            tempBullet->recycle();
            return true;
        }
    }
    return false;
}
