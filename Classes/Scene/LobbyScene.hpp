//
//  LobbyScene.hpp
//  Ship
//
//  Created by KuKulKan on 9/9/17.
//
//

#ifndef LobbyScene_hpp
#define LobbyScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "MyText.hpp"
#include "GlobalVariable.hpp"
#include "SoundBand.hpp"
#include <cocostudio/ActionTimeline/CSLoader.h>
#include "ShopItemScene.hpp"

enum LobbySceneChild{
    tagBtnUpgrade = 1,
    tagBtnQuest = 2,
    tagBtnBonus = 3,
    tagBtnShopz = 4,
    tagBtnBack = 5,
    tagBtnRank= 6

};


USING_NS_CC;
using namespace cocos2d::ui;

class LobbyPoint : public Ref{
public:
    float x,y;
    
    static LobbyPoint* createPoint(Vec2 vec){
        auto point = new LobbyPoint();
        point->x = vec.x;
        point->y = vec.y;
        return point;
    }
    
    
};

class LobbyScene : public Layer{
public:
    static void loadResource();
    static Scene* createScene();
    
    
    CREATE_FUNC(LobbyScene);
    
private:
    
    Node* rootNode;
    Node* nodeButton;
    ListView* listView;
    
    ParticleSystemQuad* borderEffect;
    ParticleSystemQuad* backGroundEffect;

    ImageView* lastItem;
    ImageView* backGround;
    ImageView* leftBar, *rightBar;
    ImageView* imgBoss;
    ImageView* imgRewardEffect, *imgEventEffect;
    
    Vector<ImageView*> arrImgBoss;
    
    Vector<LobbyPoint*> arrDefaultPoint;
    
    TextBMFont* textDummy1;
    TextBMFont* textDummy2;
    TextBMFont* textGuide;
    TextBMFont* textTick;
    
    Button* btnMissionReward, *btnEvent;
    
    Label* labelDescription;
    Label* labelAuthor;
    
    Vec2 defaultPosLabelAuthor;
    Vec2 defaultPosLabelDescription;
    Vec2 defaultPosImgBoss;
    Vec2 defaultPosImgBossShake;
    
    bool gameStart = false;
    
    int tick;
    
    bool init();
    
    void saveChildToLocalVariable();
    void modifyLocalVariable();
    
    void callBackListViewScroll(cocos2d::Ref *target, ScrollView::EventType type);
    
    void callBackItemClick(Ref* pSender, Widget::TouchEventType type);
    
    void callBackBtnClick(Ref* pSender, Widget::TouchEventType type);
    
    bool onTouchBegan(Touch* touch, Event* event);
    void onTouchEnded(Touch* touch, Event* event);
    
    void addUntouchableLayer();
    
    void onEnterTransitionDidFinish();
    void callBackMissionRewardLayerExit();
    void callBackSpecialEventLayerExit();

    void layerItemShake(float dt);
    void updateTick(float dt);
    void startGameIn(float dt);
    void bossShake(float dt);
    void clear(float dt);
    
};


#endif /* LobbyScene_hpp */
