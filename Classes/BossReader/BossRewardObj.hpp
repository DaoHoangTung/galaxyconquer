
#ifndef BossRewardObj_hpp
#define BossRewardObj_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class BossRewardObj : public Ref{
public:
    int silver,gold,bar,diamond,value,count;
    
    static BossRewardObj* createWith(int _silver, int _gold, int _bar, int _diamond, int _value, int _count){
        
        auto obj = new (std::nothrow) BossRewardObj();
        
        obj->silver = _silver;
        obj->gold = _gold;
        obj->bar = _bar;
        obj->diamond = _diamond;
        obj->value = _value;
        obj->count = _count;
        
        obj->autorelease();
        return obj;
    }
};

#endif /* BossRewardObj_hpp */
