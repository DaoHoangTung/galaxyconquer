
#ifndef ActionRow_hpp
#define ActionRow_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "Gun.h"

using namespace std;
using namespace cocos2d::ui;
using namespace cocos2d;


class ActionRow : public Ref{
public:
    ActionTag actTag;
    
    string actName;
    
    FireTag fireTag;
        
    int index; //index in arrActRow
    int gunIndex1; //index in arrGun
    int gunIndex2;
    int gunIndex3;
    int num;
    
    bool init();
    float getValue();
    
    Vector<__Float*> arrField;
    
    CREATE_FUNC(ActionRow);
    
private:
    void clear();
};

#endif /* ActionRow_hpp */
