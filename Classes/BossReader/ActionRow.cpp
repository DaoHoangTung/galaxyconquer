
#include "ActionRow.hpp"


bool ActionRow::init(){
    actTag = ActionTag::ActionTag_MoveTo;
    actName = "Move To";
    fireTag = FireTag::FireTag_Start;
    
    num = 0;
    return true;
}


float ActionRow::getValue(){
    float value = arrField.at(num)->getValue();
    num++;
    return value;
}


void ActionRow::clear(){
    
}
