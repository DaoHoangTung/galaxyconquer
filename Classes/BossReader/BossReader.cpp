
#include "BossReader.hpp"
#include "Boss.hpp"
#include "GlobalVariable.hpp"
#include "BossRewardObj.hpp"

static bool isLoad = false;
static BossReader* instance = NULL;




void BossReader::loadResource(){
    if (!isLoad){
        
        int _size = 0;
        
        auto path = StringUtils::format("BossInfo/mapinfo.plist");
        
        if (FileUtils::getInstance()->isFileExist(path.c_str())){
            auto _dic = __Dictionary::createWithContentsOfFile(path.c_str());
            
            _size = _dic->valueForKey("TotalBoss")->intValue();
        }
        
        for(int i = 1; i <= _size; i++){
            BossReader::getInstance()->ReadBossInfo(i);
        }

        isLoad = true;
    }
}

BossReader* BossReader::getInstance(){
    if (instance == NULL){
        instance = new (std::nothrow) BossReader();
        instance->retain();
    }
    
    return instance;
}

void BossReader::ReadBossInfo(int _index){
    if (_index == 14 || _index == 15) return; //boss at 14 and 15 are empty !
    
    /*
     Remove all item in list view                (listObject)
     Clear list GunRow and list ActRow           (arrGunRow, arrActRow)
     Clear list Gun (arrGun) and list gunName    (arrGun, arrGunName)
     Clear list action                           (arrAction)
     
     */
    arrGunRow.clear();
    arrActRow.clear();
    
    /*
     root
        BossType    (int)                   save to BossSelection (tag)
        TotalActRow (int)                   create act row
        TotalGunRow (int)                   create gun row
        ActRowList (dictionary)
             Row1
                 actName (string)
                 Button
                     btnAction   (string)
                     btnGun1     (string)
                     btnGun2     (string)
                     btnGun3     (string)
                 Enum
                     FireTag     (int)
                     ActionTag   (int)
                 GunIndex
                     Gun1        (int)
                     Gun2        (int)
                     Gun3        (int)
                 TextField
                     TotalField  (int)
                     ListField
                         Field1  (string)
                         ....
     
     
     
     GunRowList (dictionary)
         Row1
         btnGun  (string)
         gunName (string)
         gunTag  (int)
         TextField
             TotalField  (int)
             ListField
                 Field0
                 Field1
                 .....
     */
    
    int bossTag = 1;
    auto path = StringUtils::format("BossInfo/Boss%zd.plist",_index);
    auto _api = FileUtils::getInstance();
    
    BossRewardObj* rewardInfoObj = NULL;
    
    if (_api->isFileExist(path)){
        __Dictionary* rootDic = __Dictionary::createWithContentsOfFile(path.c_str());
        
        
        __Dictionary* rewardInfo = (__Dictionary*)rootDic->objectForKey("RewardInfo");
        if (rewardInfo != NULL){
            rewardInfoObj = BossRewardObj::createWith(rewardInfo->valueForKey("silver")->intValue()
                                                      , rewardInfo->valueForKey("gold")->intValue()
                                                      , rewardInfo->valueForKey("bar")->intValue()
                                                      , rewardInfo->valueForKey("diamond")->intValue()
                                                      , rewardInfo->valueForKey("value")->intValue()
                                                      , rewardInfo->valueForKey("count")->intValue());
        }
        
         bossTag = rootDic->valueForKey("BossType")->intValue();

        int totalActRow = rootDic->valueForKey("TotalActRow")->intValue();
        int totalGunRow = rootDic->valueForKey("TotalGunRow")->intValue();
        
        __Dictionary* listActRow = (__Dictionary*)rootDic->objectForKey("ActRowList");
        for(int i = 0; i< totalActRow; i++){
            __Dictionary* row = (__Dictionary*)listActRow->objectForKey(StringUtils::format("Row%d",i).c_str());
            
            string actName = row->valueForKey("actName")->getCString();
            
            auto new_act = ActionRow::create();
            
            arrActRow.pushBack(new_act);
            new_act->index = (int)arrActRow.size();
            new_act->actName = actName;

            
            __Dictionary* readEnum = (__Dictionary*)row->objectForKey("Enum");
            new_act->fireTag = (FireTag)readEnum->valueForKey("FireTag")->intValue();
            new_act->actTag = (ActionTag)readEnum->valueForKey("ActionTag")->intValue();
            
            __Dictionary* readGunIndex = (__Dictionary*)row->objectForKey("GunIndex");
            new_act->gunIndex1 = readGunIndex->valueForKey("Gun1")->intValue();
            new_act->gunIndex2 = readGunIndex->valueForKey("Gun2")->intValue();
            new_act->gunIndex3 = readGunIndex->valueForKey("Gun3")->intValue();
            
            __Dictionary* readTextField = (__Dictionary*)row->objectForKey("TextField");
            int readTotalField = readTextField->valueForKey("TotalField")->intValue();
            
            __Dictionary* readListField = (__Dictionary*)readTextField->objectForKey("ListField");
            
            for(int y = 0; y < readTotalField; y++){
                auto readField = readListField->valueForKey(StringUtils::format("Field%d",y))->floatValue();
                
                new_act->arrField.pushBack(__Float::create(readField));
            }
        }
        
        
        __Dictionary* listGunRow = (__Dictionary*)rootDic->objectForKey("GunRowList");
        for(int i = 0; i < totalGunRow; i++){
            __Dictionary* row = (__Dictionary*)listGunRow->objectForKey(StringUtils::format("Row%d",i).c_str());
            
            auto new_gun = GunRow::create();
            arrGunRow.pushBack(new_gun);
            
            new_gun->index = (int)arrGunRow.size();
            new_gun->gunName = row->valueForKey("gunName")->getCString();
            new_gun->gunTag = (GunTypeTag)row->valueForKey("gunTag")->intValue();
            
            __Dictionary* readTextField = (__Dictionary*)row->objectForKey("TextField");
            int readTotalField = readTextField->valueForKey("TotalField")->intValue();
            
            __Dictionary* readListField = (__Dictionary*)readTextField->objectForKey("ListField");
            
            for(int y = 0; y < readTotalField; y++){
                auto readField = readListField->valueForKey(StringUtils::format("Field%d",y))->floatValue();
                
                new_gun->arrField.pushBack(__Float::create(readField));
            }
        }
    }
    
    auto boss = Boss::createBoss(bossTag, GLOBAL::GET_DEFAULT_LEVEL_HP(_index-1)*10, arrGunRow, arrActRow);
    if (rewardInfoObj != NULL)
        boss->setBossRewardInfo(rewardInfoObj);
    
}
