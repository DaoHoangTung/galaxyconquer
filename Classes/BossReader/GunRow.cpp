
#include "GunRow.hpp"
#include "GunLightningV1.hpp"
//#include "GunCustomz.hpp"


bool GunRow::init(){
    gunTag = GunTypeTag::GunTypeTag_AOE; //BY DEFAULT
    num = 0;
    return true;
}

float GunRow::getValue(){
    float value = arrField.at(num)->getValue();
    num++;
    return value;
}

Gun* GunRow::getGun(){
    num = 0;
    
    switch (gunTag) {
        case GunTypeTag_AOE:
            gunPos = Vec2(getValue(),getValue());
            return AOEGun::createAOEGunWithShootingPerAngle(getValue(), (BulletType)getValue(), getValue());
            break;
        case GunTypeTag_Lightning:
            gunPos = Vec2(getValue(),getValue());
            return GunLightning::createGunLightning(getValue(), getValue());
            break;
            
        case GunTypeTag_HarasBot:
            gunPos = Vec2(getValue(),getValue());
            return GunHarasBot::createGunHarasBotWithNDirection(getValue(), getValue(), (BulletType)getValue(), getValue());
            break;
        
        case GunTypeTag_LightningV1:{
            gunPos = Vec2(getValue(),getValue());
            return GunLightningV1::createGunLightningV1(getValue(), getValue(), (Direction)getValue());
            }
            break;
            
//        case GunTypeTag_Customized:
//            return GunCustomz::createWithFileName(gunName, getValue());
//            break;
        default:
            break;
    }
    
    CCLOG("[GunRow] - No suitable tag gun");
    return NULL;
}


void GunRow::clear(){
    
}
