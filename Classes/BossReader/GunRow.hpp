
#ifndef GunRow_hpp
#define GunRow_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "EnumManager.h"
#include "Gun.h"


using namespace std;
using namespace cocos2d::ui;
using namespace cocos2d;
class GunRow : public Ref{
public:
    
    GunTypeTag gunTag;   //TAG_AOE BY DEFAULT
    
    string gunName;

    int index;
    int num;
    
    Vec2 gunPos;
    
    Vector<__Float*> arrField;
    
    bool init();
    
    Gun* getGun();
    
    CREATE_FUNC(GunRow);
    
private:
    float getValue();
    void clear();
};
#endif /* GunRow_hpp */
