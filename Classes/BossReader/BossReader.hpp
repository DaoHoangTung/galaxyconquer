
#ifndef BossReader_hpp
#define BossReader_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "GunRow.hpp"
#include "ActionRow.hpp"

using namespace std;
using namespace cocos2d::ui;
using namespace cocos2d;

class BossReader : public Ref{
    
public:
    
    static BossReader* getInstance();
    static void loadResource();
    

private:
    
    Vector<GunRow*> arrGunRow;
    Vector<ActionRow*> arrActRow;
    
    
    void ReadBossInfo(int _index);
    
    
};
#endif /* BossReader_hpp */
