//
//  GlobalVariable.hpp
//  Ship
//
//  Created by KuKulKan on 8/6/16.
//
//

#ifndef GlobalVariable_H_hpp
#define GlobalVariable_H_hpp

#include <stdio.h>
#include <cstdint>
#include <audio/include/SimpleAudioEngine.h>
#include "cocos2d.h"
#include "EnumManager.h"
#include "ui/CocosGUI.h"
#include "App42API.h"
#include <json/rapidjson.h>
#include "external/json/document.h"


USING_NS_CC;
using namespace cocos2d::ui;
using namespace CocosDenshion;
using namespace std;
using namespace rapidjson;

static const char* AdminCollection = "Admin";

class GLOBAL : public cocos2d::Ref{
public:
    //BEGIN-GAME INFO DATA
    static const int version = 4;
    string timer_TOP_EVENT;
    bool connection;
    int latestVersion;
    int seasonNumber;
    //END-GAME INFO DATA

    static size_t findCaseInsensitive(std::string data, std::string toSearch, size_t pos = 0);
    
    int rewardDevilSpace;
    
    bool isWin;
    int totalEnemy, totalWave, totalGameSec;
    int totalEnemyKilled, totalWaveDestroyed;
    float rateBoss;
    
    string testType;
    string usrName;
    string usrPwd;
    string dbName;
    
    int starPoint;
    int maxLife;
    int defaultSkillSlot;
    int bonusSkillSlot;
    
    int bonusCashDoingRate;
    bool isRated;
    bool autoCastSkill;
    
    float itBonusExp;
    float itBonusDmg;
    float itBonusArmor;
    float shieldAborbDamagePercent,shieldAborbDamagePercentBonus;
    
    float scrLimitValue;
    
    uint64_t challengeScore;
    uint64_t CUR_challengeScore;
    
    GameStateScene currentScene;
    LoginState loginState;
    LoginMethod loginMethod;
    
    static GLOBAL* getInstance();
    static void RegisterCashLabel(TextBMFont* _labelCash);

    bool init();
    void SetScoreLimitedByRank(int rank);

    
    CREATE_FUNC(GLOBAL);
    
    static void initVar();
    static string GET_API_KEY();
    static string GET_API_SECRET();
    static string GET_LEADERBOARD_NAME();
    static string GET_GALAXY_CHALLENGE_LEADERBOARD();

    static void INCREASE_UPGRADE_ITEM_LEVEL_AT_INDEX(int _index);
    static int GET_UPGRADE_ITEM_LEVEL_AT_INDEX(int _index);
    static int GET_UPGRADE_ITEM_LEVEL_SIZE();
    static int GET_DEVIL_SQUARE_MAP_INDEX();
    static bool GET_BOSS_STATUS(int _index);
    static void SET_BOSS_STATUS(int _index,bool _bool);
    
    static int GET_CURRENT_MISSION_INDEX();
    static void SET_CURRENT_MISSION_INDEX(int _index);
    
    static void STORE_LABEL_SCORE(TextBMFont* _label);
    static uint64_t GET_SCORE();
    static float GET_SCORE_FACTOR();
    static void SET_SCORE(long _score);
    static void SET_CHALLENGE_SCORE(long _score);

    static int GET_LIFE();
    static bool SET_LIFE(int _life);
    
    static int GET_CASH();
    static std::string GET_CASH_STRING();
    static int GET_CASH_FACTOR();
    static void SET_CASH(int _cash);
    
    static int GET_ITEM_COST(int _index);
    static void SET_ITEM_COST(int _index);
    
    static std::string GET_COST_STRING(int _num);
    static std::string GET_SCORE_STRING(uint64_t _score);

    static int GET_DEFAULT_LEVEL_HP(int _level);
    
    static float GET_BOSS_SHIELD_ABORT_DAMAGE();
    
    static int GET_BOSS_LEVEL(int _index);
    static void SET_BOSS_LEVEL(int _index, int _value);
    
    static int GET_MULTI_POINT();
    static void SET_MULTI_POINT(int _index);
    
    static float GET_COOLDOWN(SkillTag _tag);
    
    static void updateGameData();
    
    static int getCoinValue();
    static void ResetScore();
    
    static string getTypeTimer(ItemGroupStatusType groupType);
    
    static string GetCustomString(string _space, int _value);
    
    
    static App42Object* getItemCostJSON();
    static App42Object* getItemLevelJSON();
    
    static App42Object* getBossStatusJSON();
    static void loadBossStatusJSON(rapidjson::Value &object);

    
    static void loadItemCostJSON(rapidjson::Value &object);
    static void loadItemLevelJSON(rapidjson::Value &object);
    static void loadGlobalInfo(int _cash, uint64_t _score, int _life);
    static void loadGameInfoJSON(rapidjson::Value &object);

    static void clear();
    };
#endif /* GlobalVariable_hpp */


