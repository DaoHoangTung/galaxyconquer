//
//  SoundBand.cpp
//  Ship
//
//  Created by KuKulKan on 9/18/17.
//
//

#include "SoundBand.hpp"

#define SOUND_MODE 1

static SoundBand* instance = NULL;


SoundBand* SoundBand::getInstance(){
    if (instance == NULL){
        instance = SoundBand::create();
        instance->retain();
    }
    
    return instance;
}


bool SoundBand::init(){
    //Use ShipAPI - ReadFileName to extract the path
    
    arrSoundPath = {
        "Sound/aborbShieldActive.wav",
        "Sound/BesserkSkillActive.wav",
        "Sound/BlackHole.wav",
        "Sound/buttonOver.mp3",
        "Sound/Chaos_Bolt_target.mp3",
        "Sound/DruidOfTheTalonMissileHit2.wav",
        "Sound/Duel_fp.mp3",
        "Sound/Electric_Vortex.mp3",
        "Sound/naruto_victory.mp3",
        "Sound/pHeartBeat.wav",
        "Sound/pLevelUp.wav",
        "Sound/ScoreSkillActive.wav",
        "Sound/Sfx_Battle.mp3",
        "Sound/Sfx_BGM.mp3",
        "Sound/Sfx_Button_Click.mp3",
        "Sound/Sfx_CollectPowerup.mp3",
        "Sound/Sfx_CollectStar.mp3",
        "Sound/Sfx_CollectTreasure.mp3",
        "Sound/Sfx_Electro.mp3",
        "Sound/Sfx_ElectroPain.mp3",
        "Sound/Sfx_EnemyBossExplode.mp3",
        "Sound/Sfx_EnemyBossSiren.mp3",
        "Sound/Sfx_EnemyBulletPain.mp3",
        "Sound/Sfx_EnemyBulletShoot.mp3",
        "Sound/Sfx_EnemyExplode.mp3",
        "Sound/Sfx_ExtraLife.mp3",
        "Sound/Sfx_Health.mp3",
        "Sound/Sfx_HitPlayer.mp3",
        "Sound/Sfx_Multiplier.mp3",
        "Sound/Sfx_PlayerExplode.mp3",
        "Sound/Sfx_PlayerHyperspeed.mp3",
        "Sound/Sfx_PlayerShipActivate.mp3",
        "Sound/Sfx_PlayerShoot.mp3",
        "Sound/Sfx_PlayerTeleOut.mp3",
        "Sound/Sfx_Reincarnate.mp3",
        "Sound/Sfx_RocketHit.mp3",
        "Sound/Sfx_RocketLaunch.mp3",
        "Sound/Sfx_Scrape.mp3",
        "Sound/Sfx_ShieldOff.mp3",
        "Sound/Sfx_ShieldOn.mp3",
        "Sound/upgradeSound.wav",
        "CustomSprite/Kame/kame.mp3",
        "Sound/rewardSound.mp3",
        "Sound/multipoint_effect.mp3",
        "Sound/rocketRain_launch.mp3"


    };
    
    
    arrBackGroundPath = {
        "Sound/Sfx_Battle.mp3",
        "Sound/Sfx_ThemeMenu.mp3",
		"Sound/Sfx_Lobby.mp3"
    };
    
    loadSound();
    CCLOG("Audio Instance: %d",AudioEngine::getMaxAudioInstance());
    return true;
}


void SoundBand::loadSound(){
    for (const auto& audioFile : arrSoundPath)
    {
        AudioEngine::preload(audioFile,[&](bool isSuccess) {
            if (isSuccess)
                CCLOG("[SoundBand] - Preload successfully: %s",audioFile.c_str());
            else
                CCLOG("[SoundBand] - Preload failed: %s",audioFile.c_str());
        });
    }
    
    for (const auto& audioFile : arrBackGroundPath)
    {
        AudioEngine::preload(audioFile,[&](bool isSuccess) {
            if (isSuccess)
                CCLOG("[SoundBand] - Preload background successfully: %s",audioFile.c_str());
            else
                CCLOG("[SoundBand] - Preload background failed: %s",audioFile.c_str());
        });
    }
}

void SoundBand::playBackGround(){
#if SOUND_MODE
    AudioEngine::stop(bgMusic);
   
    bgMusic =  AudioEngine::play2d("Sound/Sfx_Battle.mp3",true);
#endif
}

void SoundBand::PauseMusic(){
    AudioEngine::pauseAll();
}

void SoundBand::ResumeMusic(){
    AudioEngine::resumeAll();
}

void SoundBand::playBackGroundAt(BackGround_Tag _bgTag){
    AudioEngine::stop(bgMusic);
    bgMusic =  AudioEngine::play2d(arrBackGroundPath[(int)_bgTag],true);
}

int SoundBand::playSoundAt(Sound_Tag _soundTag,bool _loop){
#if SOUND_MODE
    int i ;
    if (_soundTag == Sound_Tag::SoundTag_Electric_Vortex)
        i = AudioEngine::play2d(arrSoundPath[(int)_soundTag],_loop,0.2f);
    else
        i = AudioEngine::play2d(arrSoundPath[(int)_soundTag],_loop,1.0f);
//    AudioEngine::setFinishCallback(i, [](int id, string name){
//        AudioEngine::stop(id);
//    });
//    
    return i;
    #endif
    return 0;
}


void SoundBand::stopCurrentBackgroundMusic(){
    AudioEngine::stop(bgMusic);
}

void SoundBand::stopSoundAt(int _int){
    AudioEngine::stop(_int);
}


