#ifndef _EXPLOSION_H_
#define _EXPLOSION_H_

#include "cocos2d.h"
#include "EnumManager.h"
USING_NS_CC;

class Explosion : public Sprite
{
public:
    Sequence* moveAction;
    
    Vec2 vectorMove;
    
    ExplosionType type;
    ExplosionType arrType;
    
    static void loadResource();
    static void clearExplosion();
    static void stop();
    static void setMissionLayer(Layer* _layer);
    
    static void createExplosionTowardAngleBetweenPos(Vec2 _pos, Vec2 _toPos,int _speed, ExplosionType _explosionType);
    static void createExplosionAtPos(Vec2 _pos, int _speed, ExplosionType _explosionType);
    static void createExplosionTowardAngle(Vec2 _pos, float _angle, int _speed, ExplosionType _explosionType);
    
    static Explosion* createExplosionWithType(ExplosionType _explosionType);
    
    static void update();
    
    bool initExplosionWithType(ExplosionType _explosionType);
    
    static Vector<Explosion*> getArrExplosion(int _index);
    static Vector<Explosion*> getArrExplosionMoving();
    
    
    void didFinishAction();
    void clear();
    
protected:
private:
};

#endif