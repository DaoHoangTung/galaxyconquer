//
//  ShakeObject.cpp
//  GalaxyCommander
//
//  Created by KuKulKan on 2/21/18.
//

#include "ShakeObject.hpp"


ShakeObject* ShakeObject::createWith(cocos2d::Node *_target, float _range, float _speed, float _period){
    ShakeObject* obj = new (std::nothrow) ShakeObject();
    
    if (obj){
        if (obj->initShakeObject(_target,_range,_speed,_period)){
            obj->autorelease();
        }else{
            CC_SAFE_DELETE(obj);
        }
    }
    
    return obj;
}

bool ShakeObject::initShakeObject(cocos2d::Node *_target, float _range, float _speed, float _period){
    
    if (!Node::init())
        return false;
    
    
    target = _target;
    
    defaultPos = target->getPosition();
    
    speed = _speed;
    range = _range;
    act = NULL;
    
    schedule(schedule_selector(ShakeObject::updateShake), _period);
    
    return true;
}


void ShakeObject::updateShake(float dt){
    if (act != NULL)
        target->stopAction(act);
    
    float r = random((float)-range, (float)range);
    float deg = random(0, 360);
    
    float x = defaultPos.x  + r * cos(CC_DEGREES_TO_RADIANS(deg));
    float y = defaultPos.y  + r * sin(CC_DEGREES_TO_RADIANS(deg));
    
    act = MoveTo::create(speed, Vec2(x,y));
    
    target->runAction(act);
}
