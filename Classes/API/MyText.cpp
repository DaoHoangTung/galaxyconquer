
#include "MyText.hpp"

static Layer* missionLayer;
static bool isLoad = false;
static Vector<MyText*> vecReleaseText;

    
void MyText::ReleaseAllObject(){
    vecReleaseText.clear();
}

void MyText::loadResource(){
    if (!isLoad){
        
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("CustomSprite/Lightning.plist");
        
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("CustomSprite/lightningz.plist");
        
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("CustomSprite/zap_1_blue.plist");

        
//        for(int i = 0; i < 20;i++){
//            arrMyTextWaiting.pushBack(MyText::createMyText(1.5));
//        }
        isLoad = true;
    }
    
}

void MyText::setMissionLayer(cocos2d::Layer *_layer){
    missionLayer = _layer;
}

void MyText::CreateAtLoc(cocos2d::Vec2 _pos,float _scale, cocos2d::__String *_string,MyFont _font){
    
    auto tempText = MyText::createMyText(_scale,0,MyTextAlign::Middle,_font);
        tempText->setPosition(_pos);
        tempText->setString(_string->getCString());
        tempText->start();
}

void MyText::CreateAtLoc(cocos2d::Vec2 _pos, float _scale, cocos2d::__String *_string, float _duration,MyFont _font){
    auto tempText = MyText::createMyText(_scale,_duration,MyTextAlign::Middle,_font);
    tempText->setPosition(_pos);
    tempText->setString(_string->getCString());
    tempText->start();
}

void MyText::CreateAtLoc(cocos2d::Vec2 _pos, float _scale, cocos2d::__String *_string, float _duration, MyTextAlign _align,MyFont _font){
    auto tempText = MyText::createMyText(_scale,_duration,_align,_font);
    tempText->setPosition(_pos);
    tempText->setString(_string->getCString());
    tempText->start();
}

void MyText::CreateAtLoc(cocos2d::Sprite *_target, cocos2d::Vec2 _pos, float _scale, cocos2d::__String *_string, float _duration, MyTextAlign _align,MyFont _font){
    auto tempText = MyText::createMyText(_scale,_duration,_align,_font);
    tempText->setPosition(_pos);
    tempText->setString(_string->getCString());
    tempText->startWithTarget(_target);
}

MyText* MyText::createMyText(float _scale,float _duration,MyTextAlign _align,MyFont _font){
    auto text = new (std::nothrow) MyText();
    
    if (text){
        text->initMyText(_scale,_duration,_align,_font);
        text->autorelease();
    }else{
        CC_SAFE_DELETE(text);
    }
    
    return text;
}


bool MyText::initMyText(float _scale,float _duration,MyTextAlign _align,MyFont _font){
    
    if (!TextBMFont::init()){
        return false;
    }
    
    switch (_align) {
        case MyTextAlign::Left:
            this->setAnchorPoint(Vec2(0,0.5));
            break;
        case MyTextAlign::Right:
            this->setAnchorPoint(Vec2(1,0.5));
            break;
        case MyTextAlign::Middle:
            this->setAnchorPoint(Vec2(0.5,0.5));
            break;
        default:
            break;
    }
    
    switch (_font) {
        case MyFont::BlueGradient:
            this->setFntFile("CustomFont/ScoreSkillFont.fnt");
            break;
        case MyFont::BrownGradient:
            this->setFntFile("NewCustomFont/myFontHelloWorld.fnt");
            break;
        case MyFont::OrangeGradient:
            this->setFntFile("NewCustomFont/SpecialFontKuKulKan.fnt");
            break;
        default:
            break;
    }
    this->setScale(_scale/2);
    
    auto fadeIn = FadeIn::create(0.1);
    auto fadeOut = FadeOut::create(0.6);

    auto duration = DelayTime::create(_duration);
    
    auto moveOn = MoveBy::create(0.5, Vec2(0,50));
    auto scaleUp = ScaleTo::create(0.1, _scale+0.2);
    auto scaleDown = ScaleTo::create(0.1, _scale);

    auto twoScale = Sequence::create(scaleUp,scaleDown,duration,fadeOut, NULL);
    auto firstAct = Spawn::create(fadeIn,moveOn,twoScale, NULL);
    auto calClear = CallFuncN::create(CC_CALLBACK_0(MyText::clear, this));
    
    listAct= Sequence::create(firstAct,calClear, NULL);
    listAct->retain();

    return true;
}
void MyText::AdaptLabelToScreen(cocos2d::Scene *_scene,Node* _node, cocos2d::Label *_label){
    
    if (_scene != NULL){
        auto sizeW = Director::getInstance()->getWinSize().width;
        Vec2 realPos = _scene->convertToWorldSpace(_node->convertToWorldSpace(_label->getPosition()));
        if (_label->getContentSize().width/2 > realPos.x){
            _label->setAnchorPoint(Vec2(0,0.5));
            auto newPos = Vec2(0,realPos.y);
            auto converPos = _node->convertToNodeSpace(_scene->convertToNodeSpace(newPos));
            
            _label->setPosition(converPos);
        }else if (_label->getContentSize().width/2 + realPos.x > sizeW){
            _label->setAnchorPoint(Vec2(1,0.5));
            auto newPos = Vec2(sizeW,realPos.y);
            auto converPos = _node->convertToNodeSpace(_scene->convertToNodeSpace(newPos));
            _label->setPosition(converPos);
        }
        
    }
}

void MyText::start(){
    this->runAction(this->listAct);
    missionLayer->addChild(this,4);
}

void MyText::startWithTarget(cocos2d::Sprite *_target){
    this->runAction(this->listAct);
    missionLayer->addChild(this,4);
    deltaDistance = this->getPosition() - _target->getPosition();
    this->target = _target;
    this->schedule(schedule_selector(MyText::updatePosition), 1/32);
}

void MyText::updatePosition(float d){
    if (target != NULL){
        this->setPosition(target->getPosition() + deltaDistance);
    }
}

void MyText::clear(){
    vecReleaseText.pushBack(this);
    this->stopAllActions();
    this->unscheduleAllCallbacks();
    this->target = NULL;
    CC_SAFE_RELEASE_NULL(listAct);
    this->removeFromParentAndCleanup(true);

    //arrMyTextWaiting.pushBack(this);
    //arrTextMoving.eraseObject(this);
}
