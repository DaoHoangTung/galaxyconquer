//
//  SpecialEffect.hpp
//  Ship
//
//  Created by KuKulKan on 8/18/16.
//
//

#ifndef SpecialEffect_hpp
#define SpecialEffect_hpp

#include "cocos2d.h"
#include "EnumManager.h"
#include <SimpleAudioEngine.h>
using namespace CocosDenshion;

//#include "Mission.h"

USING_NS_CC;

class SpecialEffect : public Sprite{
public:
    Sprite* target;
    
    Sequence* animationAct;
    static void loadResource();
    
    static void ReleaseAllObject();
    
    static void loadMissionLayer(Layer* _layer);
    
    static void CreateSpecialEffectAtPos(Vec2 _pos,std::string _pathEffect, int _countFrame,float _speed,float _scale);
    
    static void CreateSpecialEffectAtTarget(Sprite* _target,std::string _pathEffect, int _countFrame,float _speed,float _scale);
    
    static SpecialEffect* createSpecialEffect(std::string _pathEffect, int _countFrame,float _speed);
    
    bool initSpecialEffect(std::string _pathEffect, int _countFrame,float _speed);
    
    void start();
    void clear();
    
};
#endif /* SpecialEffect_hpp */
