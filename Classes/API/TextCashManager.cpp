//
//  TextCashManager.cpp
//  GalaxyCommander
//
//  Created by KuKulKan on 3/11/18.
//

#include "TextCashManager.hpp"
static TextCashManager* instance = NULL;

static Vector<Text*> arrTextWaiting;
static Vector<Text*> arrTextMoving;
static Layer* gameLayer = NULL;

void TextCashManager::loadGameLayer(cocos2d::Layer *_gameLayer){
    gameLayer = _gameLayer;
}

TextCashManager* TextCashManager::getInstance(){
    if (instance == NULL){
        instance = TextCashManager::create();
        instance->retain();
    }
    
    return instance;
}


bool TextCashManager::init(){
    auto rootNode =  CSLoader::createNode("NewDummy/TextCash.csb");
    auto dummyCash = (Text*)rootNode->getChildByTag(1);
    
    for(int i = 0; i < 50; i++){
        auto cloneDummy = (Text*)dummyCash->clone();
        arrTextWaiting.pushBack(cloneDummy);
    }
    return true;
}

void TextCashManager::createTextCashAt(cocos2d::Vec2 _pos,string _s, TypeCoin type){
    if (arrTextWaiting.size() > 0){
        auto temp = arrTextWaiting.at(0);
        temp->setScale(random((float)0.4, (float)0.6));
        temp->setString(_s.c_str());
        temp->setPosition(_pos);
        
        switch (type) {
            case TypeCoin::typeSilverCoin:
                temp->setColor(Color3B(255,255,255));
                break;
                
            case TypeCoin::typeGoldBar:
            case TypeCoin::typeGoldCoin:
                temp->setColor(Color3B(255, 120, 0));
                break;
                
            case TypeCoin::typeDiamond:
                temp->setColor(Color3B(144, 238, 144));
                break;
                
            default:
                break;
        }
        
        auto skewUp = SkewTo::create(1, random((float)0.0, (float)0.3), random((float)0.0, (float)0.3));
        
        auto scaleUp = EaseElasticOut::create(ScaleTo::create(1, random((float)0.8, (float)1.3)), 0.5);
        auto fadeOut = FadeOut::create(1);
        auto jump = JumpBy::create(1,Vec2(random(-100, 100),random(0,110)),random(20, 100), 1);
        auto act = Spawn::create(scaleUp,skewUp,fadeOut,jump, NULL);
        
        auto func = CallFuncN::create([&](Node* p){
            p->removeFromParentAndCleanup(true);
            p->setOpacity(255);
            TextCashManager::getInstance()->stop(p);
        });
        
        auto mainAct = Sequence::create(act,func, NULL);
        temp->runAction(mainAct);
        
        gameLayer->addChild(temp);
        
        arrTextMoving.pushBack(temp);
        arrTextWaiting.erase(0);
    }
}

void TextCashManager::stop(cocos2d::Node *p){
    auto temp = (Text*)p;
    arrTextWaiting.pushBack(temp);
    arrTextMoving.eraseObject(temp);
}
