//
//  GlobalVariable.cpp
//  Ship
//
//  Created by KuKulKan on 8/6/16.
//
//

#include "GlobalVariable.hpp"

#define _MAX_ITEM_ 16
#define _MAX_SKILL_ 10

#define TIMER_EXP "Timer_EXP"
#define TIMER_DMG "Timer_DMG"
#define TIMER_ARMOR "Timer_ARMOR"

#define API_KEY "124dc5f128b56ce21b1083a092ca885e9e076b0fafb0fa27943ed3a73ce5bbac"
#define API_SECRET "e46f7bc50b727b9085551ae684be4916077e1a73906d25cfcb8fde20c120b93e"
#define DATABASE_NAME "GAMEDATA"
#define LEADERBOARD_NAME "Season1"
#define GALAXY_CHALLENGE_LEADERBOARD "GalaxyChallenge"

static int UPGRADE_ITEM_LEVEL[_MAX_ITEM_];
static bool BOSS_STATUS[14];
static int CURRENT_MISSION_INDEX = 13;
static int LIFE;
static uint64_t SCORE;
static float SCORE_FACTOR;
static int CASH;
static int CASH_FACTOR;
static int ITEM_COST[_MAX_ITEM_];
static int DEFAULT_LEVEL_HP[28];
static float BOSS_SHIELD_ABORT_DAMAGE[14];
static int BOSS_LEVEL[14];
static int multi_point = 1;

static float skill_cooldown[_MAX_SKILL_];

bool loadVar = false;
static GLOBAL* _instance = NULL;
static TextBMFont* labelCash = NULL;

#define DEVIL_SQUARE_MAP_INDEX 15
#define DEVIL_SPACE_REWARD 1000000
//////////////////////////////////////////////////


GLOBAL* GLOBAL::getInstance(){
    if (_instance == NULL){
        _instance = GLOBAL::create();
        _instance->retain();
    }
    
    return _instance;
}

string GLOBAL::GET_API_KEY(){
    return API_KEY;
}

string GLOBAL::GET_API_SECRET(){
    return API_SECRET;
}

string GLOBAL::GET_LEADERBOARD_NAME(){
    return StringUtils::format("Season%d",GLOBAL::getInstance()->seasonNumber);
}

string GLOBAL::GET_GALAXY_CHALLENGE_LEADERBOARD(){
    return GALAXY_CHALLENGE_LEADERBOARD;
}

void GLOBAL::RegisterCashLabel(cocos2d::ui::TextBMFont *_labelCash){
    labelCash = _labelCash;
}


bool GLOBAL::init(){
    
    dbName = DATABASE_NAME;
    loginState = LoginState::Connection_Failed;
    loginMethod = LoginMethod::method_login;
    
    maxLife = 1;
    starPoint = 0;
    itBonusExp = 0.;
    itBonusDmg = 0.;
    itBonusArmor = 0.;
    scrLimitValue = 1;
    challengeScore = 0;
    
    rewardDevilSpace = DEVIL_SPACE_REWARD;
    
    defaultSkillSlot = 2;
    bonusSkillSlot = 0;
    
    shieldAborbDamagePercent = 0.6;
    shieldAborbDamagePercentBonus = 0;
    
    bonusCashDoingRate = 5000;
    isRated = false;
    
    autoCastSkill = false;
//    totalEnemyKilled = 100;
//    totalEnemy = 250;
//    totalWaveDestroyed = 23;
//    totalWave = 100;
//    totalGameSec = 495;
    
    
    GLOBAL::initVar();
    
    return true;
}


void GLOBAL::initVar(){
    
    if (!loadVar){
        
        
        LIFE = 0;
        SCORE = 0;
        SCORE_FACTOR = 0.1;
        multi_point= 1;
        

        int size = *(&UPGRADE_ITEM_LEVEL + 1) - UPGRADE_ITEM_LEVEL;
        for(int i = 0; i < size; i++){
            UPGRADE_ITEM_LEVEL[i] = 0;
        }
        
        CASH = 10000;
        CASH_FACTOR = 5;
        
        for(int i = 0;i<8;i++){
            ITEM_COST[i] = 2500;
        }
        
        UPGRADE_ITEM_LEVEL[0] = 1; //initial gun
        ITEM_COST[0] = 3750;
        
        ITEM_COST[(int)UpgradeItemOrder::itemGravity] = 3500;
        ITEM_COST[(int)UpgradeItemOrder::itemRocket] = 7000;
        ITEM_COST[(int)UpgradeItemOrder::itemAttackRate] = 8500;
        ITEM_COST[(int)UpgradeItemOrder::itemAmor] = 5000;
        ITEM_COST[(int)UpgradeItemOrder::itemAborbShieldDamage] = 4000;
        ITEM_COST[(int)UpgradeItemOrder::itemAborbShieldDuration] = 4000;
        ITEM_COST[(int)UpgradeItemOrder::itemAborbShieldRange] = 4000;
        ITEM_COST[(int)UpgradeItemOrder::itemAborbShieldRecharge] = 4000;
        
        BOSS_SHIELD_ABORT_DAMAGE[0] = 0.1;
        BOSS_SHIELD_ABORT_DAMAGE[1] = 0.15;
        BOSS_SHIELD_ABORT_DAMAGE[2] = 0.2;
        BOSS_SHIELD_ABORT_DAMAGE[3] = 0.25;
        BOSS_SHIELD_ABORT_DAMAGE[4] = 0.3;
        BOSS_SHIELD_ABORT_DAMAGE[5] = 0.35;
        BOSS_SHIELD_ABORT_DAMAGE[6] = 0.4;
        BOSS_SHIELD_ABORT_DAMAGE[7] = 0.45;
        BOSS_SHIELD_ABORT_DAMAGE[8] = 0.5;
        BOSS_SHIELD_ABORT_DAMAGE[9] = 0.55;
        BOSS_SHIELD_ABORT_DAMAGE[10] = 0.60;
        BOSS_SHIELD_ABORT_DAMAGE[11] = 0.65;
        BOSS_SHIELD_ABORT_DAMAGE[12] = 0.70;
        BOSS_SHIELD_ABORT_DAMAGE[13] = 0.75;

        {
            int sizeBossStatus = *(&BOSS_STATUS + 1) - BOSS_STATUS;
            for(int i = 0;i< sizeBossStatus;i++){
                GLOBAL::SET_BOSS_STATUS(i, false);
            }
            
            GLOBAL::SET_BOSS_STATUS(1, true);
        }

        int sizeBoss = *(&BOSS_LEVEL + 1) - BOSS_LEVEL;
        int factor = 5;
        DEFAULT_LEVEL_HP[0] = 1000;

        for (int i = 1; i < sizeBoss; i++) {
            BOSS_LEVEL[i] = (i+1) * (factor++);
            DEFAULT_LEVEL_HP[i] = DEFAULT_LEVEL_HP[i-1] + DEFAULT_LEVEL_HP[i-1]*0.8;
            CCLOG("[GLOBAL] - Boss init Default HP: %d",DEFAULT_LEVEL_HP[i]);
        }

        skill_cooldown[(int)SkillTag::Skill_Lightning] = 10;  //lightning skill
        skill_cooldown[(int)SkillTag::Skill_Besserk] = 12;  //beserk skill
        skill_cooldown[(int)SkillTag::Skill_BlackHole] = 15; //blackhole skill
        skill_cooldown[(int)SkillTag::Skill_Kame] = 25; //blackhole skill
        skill_cooldown[(int)SkillTag::Skill_RocketRain] = 8;  //rocket rain
        
        loadVar = true;

    }
}

App42Object* GLOBAL::getItemCostJSON(){
    App42Object* object = new App42Object();
    for(int i = 0; i < _MAX_ITEM_; i++){
        auto keyCost = StringUtils::format("it_cost%d",i);
        object->setObject(keyCost.c_str(),ITEM_COST[i]);
    }
    return object;
}

App42Object* GLOBAL::getItemLevelJSON(){
    App42Object* object = new App42Object();
    for(int i = 0; i < _MAX_ITEM_; i++){
        auto keyCost = StringUtils::format("it_lvl%d",i);
        object->setObject(keyCost.c_str(),UPGRADE_ITEM_LEVEL[i]);
    }
    return object;
}

void GLOBAL::loadItemCostJSON(rapidjson::Value &object){
    for(int i = 0; i < _MAX_ITEM_; i++){
        auto keyCost = StringUtils::format("it_cost%d",i).c_str();
        ITEM_COST[i] = object[keyCost].GetInt();
        
//        auto item = ItemModel::getListModel().at(i);
//        item->cost = ITEM_COST[i];
        
    }
    
}


void GLOBAL::loadItemLevelJSON(rapidjson::Value &object){
    for(int i = 0; i < _MAX_ITEM_; i++){
        auto keyCost = StringUtils::format("it_lvl%d",i).c_str();
        UPGRADE_ITEM_LEVEL[i] = object[keyCost].GetInt();
        
//        auto item = ItemModel::getListModel().at(i);
//        item->level = UPGRADE_ITEM_LEVEL[i];
    }
    
//    ItemModel::ResetModelState();
}

void GLOBAL::loadGlobalInfo(int _cash,  uint64_t _score, int _life){
    CASH = _cash;
    SCORE = _score;
    LIFE = _life;
    
}


void GLOBAL::updateGameData(){
//    auto data = UserDefault::getInstance();
//
//    for(int i = 0; i <_MAX_ITEM_; i++){
//        auto keyCost = StringUtils::format("item_cost%d",i);
//        data->setIntegerForKey(keyCost.c_str(), ITEM_COST[i]);
//
//        auto keyUpgradeLevel = StringUtils::format("item_level%d",i);
//        data->setIntegerForKey(keyUpgradeLevel.c_str(), UPGRADE_ITEM_LEVEL[i]);
//    }
//
//    data->setIntegerForKey("userCash", CASH);
//    data->flush();
}

void GLOBAL::SetScoreLimitedByRank(int rank){
    if (rank > 10){
        scrLimitValue = 1.0;
    }else{
        scrLimitValue = (float)rank/10.;
    }
}

int GLOBAL::GET_MULTI_POINT(){
    return multi_point;
}

void GLOBAL::SET_MULTI_POINT(int _index){
    multi_point = _index;
}

int GLOBAL::GET_BOSS_LEVEL(int _index){
    return BOSS_LEVEL[_index];
}
void GLOBAL::SET_BOSS_LEVEL(int _index, int _value){
    BOSS_LEVEL[_index]+= _value;
    DEFAULT_LEVEL_HP[_index] = 10*BOSS_LEVEL[_index];

}

float GLOBAL::GET_BOSS_SHIELD_ABORT_DAMAGE(){
    return BOSS_SHIELD_ABORT_DAMAGE[CURRENT_MISSION_INDEX-1];
}

int GLOBAL::GET_DEFAULT_LEVEL_HP(int _level){
    return DEFAULT_LEVEL_HP[_level];
}

//////////////////////////////////////////////////
void GLOBAL::INCREASE_UPGRADE_ITEM_LEVEL_AT_INDEX(int _index){
    if (UPGRADE_ITEM_LEVEL[_index] <3){
        UPGRADE_ITEM_LEVEL[_index]+= 1;
    }
}

int GLOBAL::GET_UPGRADE_ITEM_LEVEL_AT_INDEX(int _index){
    return UPGRADE_ITEM_LEVEL[_index];
}

int GLOBAL::GET_UPGRADE_ITEM_LEVEL_SIZE(){
    return *(&UPGRADE_ITEM_LEVEL + 1) - UPGRADE_ITEM_LEVEL;
}


bool GLOBAL::GET_BOSS_STATUS(int _index){
    return BOSS_STATUS[_index];
}

void GLOBAL::SET_BOSS_STATUS(int _index,bool _bool){
    BOSS_STATUS[_index] = _bool;
}

int GLOBAL::GET_CURRENT_MISSION_INDEX(){
    return CURRENT_MISSION_INDEX;
}

void GLOBAL::SET_CURRENT_MISSION_INDEX(int _index){
    CURRENT_MISSION_INDEX = _index;
}

////////////////////////////////////////
static TextBMFont* labelScore;

void GLOBAL::STORE_LABEL_SCORE(cocos2d::ui::TextBMFont *_label){
    labelScore = _label;
}

void GLOBAL::ResetScore(){
    SCORE = 0;
}
uint64_t GLOBAL::GET_SCORE(){
    return SCORE;
}

float GLOBAL::GET_SCORE_FACTOR(){
    return SCORE_FACTOR;
}

void GLOBAL::SET_SCORE(long _score){
    long  bonusScore = MAX(1, _score * _instance->scrLimitValue);
    SCORE+= bonusScore;
    
    std::string str = StringUtils::format("%" PRIu64,SCORE);
    std::string sScore = "";
    // 9 999 999
    while (str.length() > 3) {
        size_t length = str.length();
        
        std::string tempStr = str.substr(length-3,length);
        
        str = str.erase(length-3,length);
        if (str.length() != 0){
            tempStr = "," + tempStr;
        }
        sScore = tempStr+sScore;
    }
    
    labelScore->setString(str + sScore);
    
    labelScore->stopAllActions();
    labelScore->setScale(MIN((float)labelScore->getScale()+0.2,(float)1.9));
    labelScore->runAction(EaseElasticOut::create(ScaleTo::create(0.5, 1.5),1));
    
//    UserDefault::getInstance()->setIntegerForKey("score", SCORE);
//    UserDefault::getInstance()->flush();
}

void GLOBAL::SET_CHALLENGE_SCORE(long _score){
    long  bonusScore = MAX(0, _score);
    GLOBAL::getInstance()->challengeScore += bonusScore;
    
    std::string str = StringUtils::format("%" PRIu64,GLOBAL::getInstance()->challengeScore);
    std::string sScore = "";
    // 9 999 999
    while (str.length() > 3) {
        size_t length = str.length();
        
        std::string tempStr = str.substr(length-3,length);
        
        str = str.erase(length-3,length);
        if (str.length() != 0){
            tempStr = "," + tempStr;
        }
        sScore = tempStr+sScore;
    }
    
    labelScore->setString(str + sScore);
    
    labelScore->stopAllActions();
    labelScore->setScale(MIN((float)labelScore->getScale()+0.2,(float)1.4));
    labelScore->runAction(EaseElasticOut::create(ScaleTo::create(0.5, 1.),1));
}

int GLOBAL::GET_LIFE(){
    return LIFE;
}
bool GLOBAL::SET_LIFE(int _life){
    LIFE = _life;
    if (LIFE > GLOBAL::getInstance()->maxLife){
        LIFE = GLOBAL::getInstance()->maxLife;
        return false;
    }
    
    return true;
}


//////////////////////////////////////////

int GLOBAL::GET_CASH(){
    return CASH;
}

int GLOBAL::GET_CASH_FACTOR(){
    return CASH_FACTOR;
}

void GLOBAL::SET_CASH(int _cash){
    CASH += _cash;
    
    if (CASH > 9999999){
        CASH = 9999999;
    }
    
    
    if (labelCash != nullptr && labelCash != NULL){
        auto castLabel = dynamic_cast<TextBMFont*>(labelCash);
        if (castLabel != NULL)
            castLabel->setString(GET_CASH_STRING().c_str());
    }
    
//    UserDefault::getInstance()->setIntegerForKey("userCash", CASH);

}

std::string GLOBAL::GET_CASH_STRING(){
    
    std::string str = StringUtils::format("%zd",CASH);
    std::string sGold = "";
    // 9 999 999
    while (str.length() > 3) {
        size_t length = str.length();
        
        std::string tempStr = str.substr(length-3,length);
        
        str = str.erase(length-3,length);
        if (str.length() != 0){
            tempStr = "," + tempStr;
        }
        sGold = tempStr+sGold;
    }
    
    return str + sGold;
}

std::string GLOBAL::GetCustomString(string _space, int _value)
{
    std::string str = StringUtils::format("%zd",_value);
    std::string sGold = "";
    // 9 999 999
    while (str.length() > 3) {
        size_t length = str.length();
        
        std::string tempStr = str.substr(length-3,length);
        
        str = str.erase(length-3,length);
        if (str.length() != 0){
            tempStr = _space + tempStr;
        }
        sGold = tempStr+sGold;
    }
    
    return str + sGold;
}


/////////////////////

int GLOBAL::GET_ITEM_COST(int _index){
    return ITEM_COST[_index];
}

void GLOBAL::SET_ITEM_COST(int _index){
    if (_index < 8){
        int level = GET_UPGRADE_ITEM_LEVEL_AT_INDEX(_index);
        int oldCost = ITEM_COST[_index];
        int newCost = oldCost * (level +0.5);
        ITEM_COST[_index] = newCost;
    }else{
        int level = GET_UPGRADE_ITEM_LEVEL_AT_INDEX(_index);
        int oldCost = ITEM_COST[_index];
        int newCost = oldCost * (level +1.5);
        ITEM_COST[_index] = newCost;
    }
    
}

///////////////////////////////

std::string GLOBAL::GET_COST_STRING(int _num){
    std::string str = StringUtils::format("%zd",_num);
    std::string sGold = "";
    
    while (str.length() > 3) {
        size_t length = str.length();
        
        std::string tempStr = str.substr(length-3,length);
        
        str = str.erase(length-3,length);
        if (str.length() != 0){
            tempStr = "," + tempStr;
        }
        sGold = tempStr+sGold;
    }

    
    return str + sGold;
}

std::string GLOBAL::GET_SCORE_STRING(uint64_t _score){
    std::string str = StringUtils::format("%" PRIu64,_score);
    std::string sGold = "";
    
    while (str.length() > 3) {
        size_t length = str.length();
        
        std::string tempStr = str.substr(length-3,length);
        
        str = str.erase(length-3,length);
        if (str.length() != 0){
            tempStr = "," + tempStr;
        }
        sGold = tempStr+sGold;
    }
    
    
    return str + sGold;
}


float GLOBAL::GET_COOLDOWN(SkillTag _tag){
    return skill_cooldown[(int)_tag];
}

int GLOBAL::GET_DEVIL_SQUARE_MAP_INDEX(){
    return DEVIL_SQUARE_MAP_INDEX;
}

int GLOBAL::getCoinValue(){
    return 10 * CURRENT_MISSION_INDEX * GET_MULTI_POINT();
}

string GLOBAL::getTypeTimer(ItemGroupStatusType groupType){
    switch (groupType) {
        case ItemGroupStatusType_EXP:
            return TIMER_EXP;
            break;
            
        case ItemGroupStatusType_ARMOR:
            return TIMER_ARMOR;
            break;
            
        case ItemGroupStatusType_DMG:
            return TIMER_DMG;
            break;
            
        default:
            break;
    }
    
    return "";
}

App42Object* GLOBAL::getBossStatusJSON(){
    auto bossSize = *(&BOSS_STATUS + 1) - BOSS_STATUS;

    App42Object* obj = new App42Object();
    for(int i = 0; i < bossSize; i++){
        obj->setObject(StringUtils::format("%d",i).c_str(), BOSS_STATUS[i]);
    }
    
    return obj;
}

void GLOBAL::loadBossStatusJSON(rapidjson::Value &object){
    auto bossSize = *(&BOSS_STATUS + 1) - BOSS_STATUS;
    
    for(int i = 0; i < bossSize; i++){
        auto key = StringUtils::format("%d",i).c_str();
        BOSS_STATUS[i] = object[key].GetBool();
    }
}

void GLOBAL::loadGameInfoJSON(rapidjson::Value &object){
    GLOBAL::getInstance()->connection = object["connect"].GetBool();
    GLOBAL::getInstance()->latestVersion = object["ver"].GetInt();
    GLOBAL::getInstance()->timer_TOP_EVENT = object["tTop"].GetString();
    GLOBAL::getInstance()->seasonNumber = object["Season"].GetInt();
}

size_t GLOBAL::findCaseInsensitive(std::string data, std::string toSearch, const size_t pos)
{
    // Convert complete given String to lower case
    std::transform(data.begin(), data.end(), data.begin(), ::tolower);
    // Convert complete given Sub String to lower case
    std::transform(toSearch.begin(), toSearch.end(), toSearch.begin(), ::tolower);
    // Find sub string in given string
    return data.find(toSearch, pos);
}

void GLOBAL::clear(){
    loadVar = false;
    _instance->release();
    _instance = NULL;
}

