
#ifndef MyText_hpp
#define MyText_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
USING_NS_CC;
using namespace cocos2d::ui;

enum class MyTextAlign{
    Left,
    Right,
    Middle,
};

enum class MyFont{
    OrangeGradient,
    BlueGradient,
    BrownGradient,
    
};

class MyText : public TextBMFont{
public:
    Sequence* listAct;
    
    Sprite* target;
    
    Vec2 deltaDistance;
    
    static void ReleaseAllObject();
    static MyText* createMyText(float _scale,float _duration,MyTextAlign _align,MyFont _font);
    static void CreateAtLoc(Vec2 _pos,float _scale,__String* _string,MyFont _font);
    
    static void CreateAtLoc(Vec2 _pos,float _scale,__String* _string, float _duration,MyFont _font);
    
    static void CreateAtLoc(Vec2 _pos,float _scale,__String* _string, float _duration,MyTextAlign _align,MyFont _font);
    
    static void CreateAtLoc(Sprite* _target,Vec2 _pos,float _scale,__String* _string, float _duration,MyTextAlign _align,MyFont _font);
    
    static void setMissionLayer(Layer* _layer);
    static void loadResource();
    
    static void AdaptLabelToScreen(Scene* _scene,Node* _node, Label* _label);
    
    bool initMyText(float _scale,float _duration,MyTextAlign _align,MyFont _font);
    
    
    
    void clear();
    void start();
    void startWithTarget(Sprite* _target);
    void updatePosition(float d);
};

#endif /* MyText_hpp */
