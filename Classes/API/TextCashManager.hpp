//
//  TextCashManager.hpp
//  GalaxyCommander
//
//  Created by KuKulKan on 3/11/18.
//

#ifndef TextCashManager_hpp
#define TextCashManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "EnumManager.h"

using namespace std;
using namespace cocos2d::ui;
using namespace cocos2d;

USING_NS_CC;

class TextCashManager : public Ref{
public:
    static void loadGameLayer(Layer* _gameLayer);
    static TextCashManager* getInstance();
    
    void createTextCashAt(Vec2 _pos,string _s, TypeCoin type);
    void stop(Node* p);
    
    bool init();
    CREATE_FUNC(TextCashManager);
private:
    
};
#endif /* TextCashManager_hpp */
