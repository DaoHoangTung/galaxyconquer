//
//  SoundBand.hpp
//  Ship
//
//  Created by KuKulKan on 9/18/17.
//
//

#ifndef SoundBand_hpp
#define SoundBand_hpp

#include "cocos2d.h"
#include <SimpleAudioEngine.h>
#include "EnumManager.h"
#include "audio/include/AudioEngine.h"


USING_NS_CC;


using namespace cocos2d;
using namespace std;
using namespace cocos2d::experimental;

enum BackGround_Tag{
    SFX_ThemeMenu = 1,
	SFX_Lobby = 2
};

enum Sound_Tag{
    SoundTag_aborbShieldActive = 0,
    SoundTag_BesserkSkillActive = 1,
    SoundTag_BlackHole = 2,
    SoundTag_buttonOver = 3,
    SoundTag_Chaos_Bolt_target = 4,
    SoundTag_DruidOfTheTalonMissileHit2 = 5,
    SoundTag_Duel_fp = 6,
    SoundTag_Electric_Vortex = 7,
    SoundTag_naruto_victory = 8,
    SoundTag_pHeartBeat = 9,
    SoundTag_pLevelUp = 10,
    SoundTag_ScoreSkillActive = 11,
    SoundTag_Sfx_Battle = 12,
    SoundTag_Sfx_BGM = 13,
    SoundTag_Sfx_Button_Click = 14,
    SoundTag_Sfx_CollectPowerup = 15,
    SoundTag_Sfx_CollectStar = 16,
    SoundTag_Sfx_CollectTreasure = 17,
    SoundTag_Sfx_Electro = 18,
    SoundTag_Sfx_ElectroPain = 19,
    SoundTag_Sfx_EnemyBossExplode = 20,
    SoundTag_Sfx_EnemyBossSiren = 21,
    SoundTag_Sfx_EnemyBulletPain = 22,
    SoundTag_Sfx_EnemyBulletShoot = 23,
    SoundTag_Sfx_EnemyExplode = 24,
    SoundTag_Sfx_ExtraLife = 25,
    SoundTag_Sfx_Health = 26,
    SoundTag_Sfx_HitPlayer = 27,
    SoundTag_Sfx_Multiplier = 28,
    SoundTag_Sfx_PlayerExplode = 29,
    SoundTag_Sfx_PlayerHyperspeed = 30,
    SoundTag_Sfx_PlayerShipActivate = 31,
    SoundTag_Sfx_PlayerShoot = 32,
    SoundTag_Sfx_PlayerTeleOut = 33,
    SoundTag_Sfx_Reincarnate = 34,
    SoundTag_Sfx_RocketHit = 35,
    SoundTag_Sfx_RocketLaunch = 36,
    SoundTag_Sfx_Scrape = 37,
    SoundTag_Sfx_ShieldOff = 38,
    SoundTag_Sfx_ShieldOn = 39,
    SoundTag_upgradeSound = 40,
    SoundTag_kame = 41,
    SoundTag_rewardSound = 42,
    SoundTag_multipoint_effect = 43,
    SoundTag_rocketRain_launch = 44
};


class SoundBand : public Ref{
    
public:
    static SoundBand* getInstance();
    
    
    bool init();
    CREATE_FUNC(SoundBand);
    
    void loadSound();
    void playBackGround();
    void playBackGroundAt(BackGround_Tag _bgTag);
    
    int playSoundAt(Sound_Tag _soundTag,bool _loop);
    
    void stopCurrentBackgroundMusic();
    void stopSoundAt(int _int);
    void PauseMusic();
    void ResumeMusic();
private:
    int bgMusic;
    
    vector<string> arrSoundPath;
    vector<string> arrBackGroundPath;
};
#endif /* SoundBand_hpp */
