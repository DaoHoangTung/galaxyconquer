//
//  ShakeObject.hpp
//  GalaxyCommander
//
//  Created by KuKulKan on 2/21/18.
//

#ifndef ShakeObject_hpp
#define ShakeObject_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "EnumManager.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace CocosDenshion;

class ShakeObject : public Node{
public:
    static ShakeObject* createWith(Node* _target,float _range, float _speed, float _period);
    
    bool initShakeObject(Node* _target,float _range, float _speed, float _period);
    
private:
    Node* target;
    
    MoveTo* act;
    Vec2 defaultPos;
    
    float range, speed;
    
    void updateShake(float dt);
};

#endif /* ShakeObject_hpp */
