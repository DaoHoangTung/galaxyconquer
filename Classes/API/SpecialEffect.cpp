//
//  SpecialEffect.cpp
//  Ship
//
//  Created by KuKulKan on 8/18/16.
//
//

#include "SpecialEffect.hpp"

static Vector<SpecialEffect*> vecSpecialEffect;
static Layer* missionLayer;
static Vector<SpecialEffect*> vecReleaseEffect;

void SpecialEffect::ReleaseAllObject(){
    vecReleaseEffect.clear();
}

void SpecialEffect::loadMissionLayer(cocos2d::Layer *_layer){
    missionLayer = _layer;
}



void SpecialEffect::CreateSpecialEffectAtPos(cocos2d::Vec2 _pos, std::string _pathEffect, int _countFrame, float _speed, float _scale){
    
    auto effect = SpecialEffect::createSpecialEffect(_pathEffect, _countFrame, _speed);
    
    effect->setPosition(_pos);
    effect->setScale(_scale);
    effect->start();
    vecSpecialEffect.pushBack(effect);
}

void SpecialEffect::CreateSpecialEffectAtTarget(cocos2d::Sprite *_target, std::string _pathEffect, int _countFrame, float _speed, float _scale){
    auto effect = SpecialEffect::createSpecialEffect(_pathEffect, _countFrame, _speed);
    
    effect->target = _target;
    effect->setScale(_scale);
    effect->start();
    vecSpecialEffect.pushBack(effect);
}

SpecialEffect* SpecialEffect::createSpecialEffect(std::string _pathEffect, int _countFrame, float _speed){
    
    auto effect = new (std::nothrow) SpecialEffect();
    
    if (effect){
        if (effect->initSpecialEffect(_pathEffect, _countFrame, _speed)){
            effect->autorelease();
        }else{
            CC_SAFE_DELETE(effect);
        }
    }
    
    return effect;
}


bool SpecialEffect::initSpecialEffect(std::string _pathEffect, int _countFrame, float _speed){
    
    if (!Sprite::init()){
        return false;
    }
    
    this->target = NULL;
    
    auto frameCache = SpriteFrameCache::getInstance();
    
    if (!frameCache->isSpriteFramesWithFileLoaded("CustomSprite/" + _pathEffect+".plist")){
        frameCache->addSpriteFramesWithFile("CustomSprite/" + _pathEffect+".plist");
    }
    
    Vector<SpriteFrame*> vecFrame;
    
    for (int i = 0; i < _countFrame; i++){
        auto name = __String::createWithFormat("%s%i.png",_pathEffect.c_str(),i);
        vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(name->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, _speed);
    auto animate = Animate::create(animation);
    auto callBackClear = CallFuncN::create(CC_CALLBACK_0(SpecialEffect::clear, this));
    
    animationAct = Sequence::create(animate,callBackClear, NULL);
    animationAct->retain();
    
    this->setSpriteFrame(vecFrame.at(0));
    
    vecFrame.clear();
    
    return true;
}

void SpecialEffect::start(){
    if (target != NULL){
        this->setPosition(target->getContentSize()/2);
        target->addChild(this);
        this->runAction(animationAct);
    }else{
        this->runAction(animationAct);
        missionLayer->addChild(this);
    }
   
}

void SpecialEffect::clear(){
    vecReleaseEffect.pushBack(this);
    this->stopAllActions();
    this->removeFromParentAndCleanup(true);
    this->unscheduleAllCallbacks();
    vecSpecialEffect.eraseObject(this);
    animationAct->release();
}

