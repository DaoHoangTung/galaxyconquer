#include "Explosion.h"


static bool isLoading = false;
static Vector<Explosion*> arrExplosion[10];//0: small, 1:medium, 2:large
static Vector<Explosion*> arrExplosionMoving;

static Layer* missionLayer;

void Explosion::setMissionLayer(cocos2d::Layer *_layer){
    missionLayer = _layer;
}

void Explosion::loadResource()
{
    if (!isLoading){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Explosion/Explosion.plist");
        
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("CustomSprite/EnergyExplosion.plist");
        
        for (int i = 0; i < 50; i++){
            arrExplosion[0].pushBack(Explosion::createExplosionWithType(ExplosionType::SmallExplosionA));
            arrExplosion[0].pushBack(Explosion::createExplosionWithType(ExplosionType::SmallExplosionB));
            arrExplosion[1].pushBack(Explosion::createExplosionWithType(ExplosionType::MediumExplosionA));
            arrExplosion[1].pushBack(Explosion::createExplosionWithType(ExplosionType::MediumExplosionB));
            arrExplosion[2].pushBack(Explosion::createExplosionWithType(ExplosionType::LargeExplosionA));
            arrExplosion[2].pushBack(Explosion::createExplosionWithType(ExplosionType::LargeExplosionB));
            arrExplosion[9].pushBack(Explosion::createExplosionWithType(ExplosionType::EnergyExplosion));
        }
        
        isLoading = true;
    }
}

void Explosion::update(){
    if (arrExplosionMoving.size() > 0){
        for (auto tempExplosion : arrExplosionMoving){
            tempExplosion->setPosition(tempExplosion->getPosition() + tempExplosion->vectorMove);
            
            tempExplosion->vectorMove = tempExplosion->vectorMove - 0.1*tempExplosion->vectorMove;
        }
    }
}

void Explosion::createExplosionTowardAngleBetweenPos(Vec2 _pos, Vec2 _toPos, int _speed, ExplosionType _explosionType)
{
    Vec2 vectorDis = _toPos - _pos;
    float angle = vectorDis.getAngle();
    
    if (arrExplosion[(int)_explosionType].size() > 0){
        int randIndex = random(0, (int)arrExplosion[(int)_explosionType].size() - 1);
        
        auto explosion = arrExplosion[(int)_explosionType].at(randIndex);
        explosion->vectorMove = Vec2(_speed*cos(angle), _speed*sin(angle));
        explosion->setPosition(_pos);
        explosion->setVisible(true);
        explosion->arrType = _explosionType;
        explosion->runAction(explosion->moveAction);
        arrExplosionMoving.pushBack(explosion);
        
        arrExplosion[(int)_explosionType].erase(randIndex);
        
        missionLayer->addChild(explosion,3);
    }
    
}

void Explosion::createExplosionAtPos(Vec2 _pos, int _speed, ExplosionType _explosionType)
{
    
    if (arrExplosion[(int)_explosionType].size() > 0){
        int randIndex = random(0, (int)arrExplosion[(int)_explosionType].size() - 1);
        
        auto explosion = arrExplosion[(int)_explosionType].at(randIndex);
        
        explosion->setPosition(_pos);
        explosion->setVisible(true);
        explosion->arrType = _explosionType;
        explosion->runAction(explosion->moveAction);
        arrExplosionMoving.pushBack(explosion);
        
        arrExplosion[(int)_explosionType].erase(randIndex);
        missionLayer->addChild(explosion,3);
    }
}

void Explosion::createExplosionTowardAngle(Vec2 _pos, float _angle, int _speed, ExplosionType _explosionType)
{
    float angle = CC_DEGREES_TO_RADIANS(_angle);
    
    if (arrExplosion[(int)_explosionType].size() > 0){
        int randIndex = random(0, (int)arrExplosion[(int)_explosionType].size() - 1);
        
        auto explosion = arrExplosion[(int)_explosionType].at(randIndex);
        explosion->vectorMove = Vec2(_speed*cos(angle), _speed*sin(angle));
        explosion->setPosition(_pos);
        explosion->setVisible(true);
        explosion->arrType = _explosionType;
        explosion->runAction(explosion->moveAction);
        arrExplosionMoving.pushBack(explosion);
        
        arrExplosion[(int)_explosionType].erase(randIndex);
        missionLayer->addChild(explosion,3);
    }
}

Explosion* Explosion::createExplosionWithType(ExplosionType _explosionType)
{
    Explosion* explosion = new (std::nothrow) Explosion();
    
    if (explosion){
        if (explosion->initExplosionWithType(_explosionType)){
            explosion->autorelease();
        }
    }
    else{
        CC_SAFE_DELETE(explosion);
    }
    
    
    return explosion;
}

bool Explosion::initExplosionWithType(ExplosionType _explosionType)
{
    if (!Sprite::init()){
        return false;
    }
    
    type = _explosionType;
    
    __String name;
    
    switch (type)
    {
        case ExplosionType::SmallExplosionA:
            name = "SmallExplosionA";
            break;
        case ExplosionType::SmallExplosionB:
            name = "SmallExplosionB";
            break;
        case ExplosionType::MediumExplosionA:
            name = "MediumExplosionA";
            break;
        case ExplosionType::MediumExplosionB:
            name = "MediumExplosionB";
            break;
        case ExplosionType::LargeExplosionA:
            name = "LargeExplosionA";
            break;
        case ExplosionType::LargeExplosionB:
            name = "LargeExplosionB";
            break;
        case ExplosionType::EnergyExplosion:
            name = "EnergyExplosion";
            break;
        default:
            name = "SmallExplosionA";
            break;
    }
    
    Vector<SpriteFrame*> vecFrame;
    if (_explosionType == ExplosionType::EnergyExplosion){
        for (int i = 0; i < 40; i++){
            auto _name = __String::createWithFormat("%s%i.png", name.getCString(), i);
            vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(_name->getCString()));
            
        }
    }else{
        for (int i = 0; i < 25; i++){
            auto _name = __String::createWithFormat("%s%i.png", name.getCString(), i);
            vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(_name->getCString()));
            
        }
    }
    
    
    auto animation = Animation::createWithSpriteFrames(vecFrame,0.04);
    auto animate = Animate::create(animation);
    
    auto didFinishAct = CallFuncN::create(CC_CALLBACK_0(Explosion::didFinishAction, this));
    
    moveAction = Sequence::createWithTwoActions(animate, didFinishAct);
    moveAction->retain();
    
    this->setSpriteFrame(vecFrame.at(0));
    this->setVisible(false);
    
    return true;;
}

void Explosion::stop()
{
    for (auto explosion : arrExplosionMoving){
        explosion->stopAction(explosion->moveAction);
        explosion->setVisible(false);
        arrExplosion[(int)explosion->arrType].pushBack(explosion);
    }
    
    arrExplosionMoving.clear();
}

void Explosion::didFinishAction()
{
    this->stopAction(this->moveAction);
    this->setVisible(false);
    arrExplosionMoving.eraseObject(this);
    arrExplosion[(int)this->arrType].pushBack(this);
    this->removeFromParentAndCleanup(true);
    
}

Vector<Explosion*> Explosion::getArrExplosion(int _index)
{
    return arrExplosion[_index];
}

Vector<Explosion*> Explosion::getArrExplosionMoving()
{
    return arrExplosionMoving;
}

void Explosion::clear()
{
    if (arrExplosionMoving.contains(this)){
        this->stopAction(this->moveAction);
        this->setVisible(false);
    }
    
    CC_SAFE_RELEASE_NULL(moveAction);
}

void Explosion::clearExplosion()
{
    for (int i = 0; i < 3; i++){
        for (auto explosion : arrExplosion[i]){
            explosion->clear();
        }
        arrExplosion[i].clear();
    }
    
    for (auto explosion : arrExplosionMoving){
        explosion->clear();
    }
    
    arrExplosionMoving.clear();
    
}

