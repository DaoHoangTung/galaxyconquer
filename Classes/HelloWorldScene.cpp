#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#include "Scene/LobbyScene.hpp"
USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto rootNode = CSLoader::createNode("MainScene.csb");

	//All CCLOG should be seen in the debug console when visual studio is run in debug mode
	auto start = dynamic_cast<cocos2d::ui::Button*>(rootNode->getChildByName("start"));
	if (start) 
	{
		start->addClickEventListener([=](Ref*)
		{
            _director->replaceScene(LobbyScene::createScene());
		});
	}

	auto howto = dynamic_cast<cocos2d::ui::Button*>(rootNode->getChildByName("howto"));
	if (howto)
	{
		howto->addClickEventListener([=](Ref*)
			{
				CCLOG("How to Play");
			});
	}

	auto exit = dynamic_cast<cocos2d::ui::Button*>(rootNode->getChildByName("exit"));
	if (exit)
	{
		exit->addClickEventListener([=](Ref*)
			{
				CCLOG("Exit Game");
			});
	}

	Sprite* earth = (Sprite*)rootNode->getChildByName("earth");
	auto rotate = RotateBy::create(10, 30)	;
	auto repeat = RepeatForever::create(rotate);
	earth->runAction(repeat);

    addChild(rootNode);
    
    
    


    return true;
}
