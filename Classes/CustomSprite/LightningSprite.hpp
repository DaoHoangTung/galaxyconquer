
#ifndef _Lightning_Sprite_hpp
#define _Lightning_Sprite_hpp

#include "cocos2d.h"
#include "EnumManager.h"
#include "Lightning.hpp"
#include <SimpleAudioEngine.h>
using namespace CocosDenshion;

//#include "Mission.h"
using namespace cocos2d::experimental;

USING_NS_CC;

class LightningSprite : public Layer{
public:
    Vector<Lightning*> lightningMember;
    
    Node* target;
    bool isRecycle;
    bool isStartWithPoint;
    LightningClass _class;
    int sound;
    
    
    float fromAngle,toAngle,duration;
    
    static void ReleaseAllObject();
    static void StopAllMovingLightning();
    
    static void loadResource();
    static void loadMissionLayer(Layer* _layer);
    static LightningSprite* CreateLightningEffect(Node* _target, float _fromAngle, float _toAngle, float _duration,LightningClass _class2,float _dmg);
    
    static LightningSprite* CreateLightningEffect(Node* _target, float _fromAngle, float _toAngle, float _duration,LightningClass _class2,float _dmg, LightningColor _color);

    
    static LightningSprite* createLightning(LightningClass _class);
    bool initLightningSprite(LightningClass _class);
    
    void SetLightningColor(LightningColor _color);

    void callFadeOutLightning();
    void start(float _dmg);
    void startWithPoint(float _dmg);

    void stop(float dt);
    
    
    
};

#endif /* LightningSprite_hpp */
