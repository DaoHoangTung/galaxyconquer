
#ifndef Lightning_hpp
#define Lightning_hpp

#include "cocos2d.h"
#include "EnumManager.h"
USING_NS_CC;

enum class LightningClass{
    forPlayer = 0,
    forEnemy = 1
};

enum class LightningColor{
    Lightning_BLUE = 0,
    Lightning_RED = 1
};


class Lightning : public Sprite{
public:
    Node* caster;
    
    RepeatForever* animation;
    FadeOut* fadeOut;
    Vec2 pos;
    
    PhysicsBody* body;
    float dmg;
    
    bool isRecycle;
    static Lightning* createLightning(Vec2 _pos,LightningClass _class);
    bool initLightning(Vec2 _pos,LightningClass _class);
    
    void start(float _dmg);
    void startFadeOut();
    void callBackFadeOut();
    void stop();
    void stopImmediately();
    
};
#endif /* Lightning_hpp */
