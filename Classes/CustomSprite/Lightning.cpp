
#include "Lightning.hpp"


Lightning* Lightning::createLightning(cocos2d::Vec2 _pos,LightningClass _class){
    Lightning* lightning = new (std::nothrow) Lightning();
    
    if (lightning){
        if (lightning->initLightning(_pos,_class)){
            //lightning->autorelease();
        }else{
            CC_SAFE_DELETE(lightning);
        }
    }
    
    return lightning;
}

bool Lightning::initLightning(cocos2d::Vec2 _pos,LightningClass _class){
    
    if (!Sprite::init()){
        return false;
    }
    
    Vector<SpriteFrame*> vecFrame;
    if (_class == LightningClass::forEnemy){
        for (int i = 1; i < 7; i++){
            auto _name = __String::createWithFormat("%i.png",i);
            vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(_name->getCString()));
    
        }
    }else{
        for (int i = 1; i <= 20; i++){
//            auto _name = __String::createWithFormat("lightningz%i.png",i);
            auto _name = __String::createWithFormat("zap_1_blue_%i.png",i);
            vecFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(_name->getCString()));
            
        }
    }

    
    auto animationFrame = Animation::createWithSpriteFrames(vecFrame,0.06);
    auto animateFrame = Animate::create(animationFrame);
    this->animation = RepeatForever::create(animateFrame);
    this->animation->retain();
    
    this->setSpriteFrame(vecFrame.at(0));
    this->fadeOut = FadeOut::create(1);
    this->fadeOut->retain();
    
    this->setAnchorPoint(Vec2(0.5,0));
    
    this->setPosition(_pos);
    pos = _pos;
    
    //this->setColor(Color3B::YELLOW);
    switch (_class) {
        case LightningClass::forEnemy :
            this->body = PhysicsBody::createBox(Size(this->_contentSize.width, this->_contentSize.height), PhysicsMaterial(0.1f, 1.0f, 0.0f));
            this->body->setDynamic(true);
            this->body->setCategoryBitmask((int)PhysicCategory::Lightning);
            this->body->setCollisionBitmask((int)PhysicCategory::None);
            this->body->setContactTestBitmask((int)PhysicCategory::PlayerShip);
            this->body->retain();
            break;
        case LightningClass::forPlayer:
            this->setColor(Color3B::WHITE);
            this->body = PhysicsBody::createBox(Size(this->_contentSize.width*0.5, this->_contentSize.height), PhysicsMaterial(0.1f, 1.0f, 0.0f));
            this->body->setDynamic(true);
            this->body->setCategoryBitmask((int)PhysicCategory::Lightning);
            this->body->setCollisionBitmask((int)PhysicCategory::None);
            this->body->setContactTestBitmask((int)PhysicCategory::combo3);
            this->body->retain();
            
        default:
            break;
    }
    
    
    isRecycle = false;
    
    return true;
}

void Lightning::start(float _dmg){
    if (!isRecycle){
        isRecycle = true;
        this->setOpacity(255); //If not, this sprite seem to disapear from layer because it's invisible due to opacity = 0.
        
        this->dmg = _dmg;
        this->setPosition(pos);
        this->setPhysicsBody(body);
        this->runAction(animation);
    }
}

void Lightning::stop(){
    
}

void Lightning::stopImmediately(){
    if (isRecycle){
        this->body->removeFromWorld();
        this->stopAllActions();
        this->removeFromParentAndCleanup(true);
        isRecycle = false;
    }
}

void Lightning::startFadeOut(){
    this->body->removeFromWorld();
    
    auto callBackFadeOut = CallFuncN::create(CC_CALLBACK_0(Lightning::callBackFadeOut, this));
    
    auto sequenceAct = Sequence::create(fadeOut,callBackFadeOut, NULL);
    
    this->runAction(sequenceAct);
}

void Lightning::callBackFadeOut(){
    if (isRecycle){
        this->body->removeFromWorld();
        this->stopAllActions();
        this->removeFromParentAndCleanup(true);
        isRecycle = false;
    }
}
