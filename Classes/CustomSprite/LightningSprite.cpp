
#include "LightningSprite.hpp"
#include "audio/include/AudioEngine.h"


static bool isLoad = false;
static Vector<LightningSprite*> vecLightning[2];

static Vector<LightningSprite*> vecLightningMoving;
static Vector<LightningSprite*> vecReleaseLightning;

static Layer* missionLayer;
static Vector<__Integer*> arrSound;

void LightningSprite::StopAllMovingLightning(){
    while (vecLightningMoving.size()>0) {
        auto lightning = vecLightningMoving.at(0);
        lightning->stop(0);
    }
    
    
    for(auto sound : arrSound){
        SoundBand::getInstance()->stopSoundAt(sound->getValue());
    }
    
    arrSound.clear();
}
void LightningSprite::ReleaseAllObject(){
    vecReleaseLightning.clear();
}
void LightningSprite::loadMissionLayer(cocos2d::Layer *_layer){
    missionLayer = _layer;
}

void LightningSprite::loadResource(){
    if (!isLoad){
        for(int i = 0; i < 200;i++){
            vecLightning[(int)LightningClass::forEnemy].pushBack(LightningSprite::createLightning(LightningClass::forEnemy));
        }
        
        for(int i = 0; i < 50;i++){
            vecLightning[(int)LightningClass::forPlayer].pushBack(LightningSprite::createLightning(LightningClass::forPlayer));
            
        }
        
        isLoad = true;
    }
}


LightningSprite* LightningSprite::CreateLightningEffect(Node *_target, float _fromAngle, float _toAngle, float _duration,LightningClass _class,float _dmg){
    
    if (vecLightning[(int)_class].size()>0){
        auto sprite = vecLightning[(int)_class].at(0);
        sprite->target = _target;
        sprite->fromAngle = _fromAngle;
        sprite->toAngle = _toAngle;
        sprite->duration = _duration;
        sprite->start(_dmg);
        vecLightningMoving.pushBack(sprite);
        vecLightning[(int)_class].eraseObject(sprite);
        return sprite;
    }
    
    return NULL;
}

LightningSprite* LightningSprite::CreateLightningEffect(Node *_target, float _fromAngle, float _toAngle, float _duration,LightningClass _class,float _dmg,LightningColor _color){
    
    if (vecLightning[(int)_class].size()>0){
        auto sprite = vecLightning[(int)_class].at(0);
        sprite->target = _target;
        sprite->fromAngle = _fromAngle;
        sprite->toAngle = _toAngle;
        sprite->duration = _duration;
        sprite->SetLightningColor(_color);
        sprite->start(_dmg);
        vecLightningMoving.pushBack(sprite);
        vecLightning[(int)_class].eraseObject(sprite);
        return sprite;
    }
    
    return NULL;
}

LightningSprite* LightningSprite::createLightning(LightningClass _class){

    LightningSprite* layer = new (std::nothrow) LightningSprite();
    
    if (layer){
        
        if (layer->initLightningSprite(_class)){;
            //layer->autorelease();
        }else{
            CC_SAFE_DELETE(layer);
        }
    }
    
    return layer;
}

bool LightningSprite::initLightningSprite(LightningClass _class2){
    if (!Layer::init()){
        return false;
    }
    
    this->_class = _class2;
    
    this->isStartWithPoint = false;
    this->setAnchorPoint(Vec2(0,0));
    //this->setColor(Color3B::GRAY);
    
    Vec2 tempPos = Vec2(0,0);
    this->isRecycle = true;
    
    if (_class == LightningClass::forEnemy)
        for(int i = 0;i<6;i++){
            auto lightning = Lightning::createLightning(tempPos,_class);
            //lightning->start();
            this->lightningMember.pushBack(lightning);
            
            tempPos = tempPos + Vec2(0,lightning->getContentSize().height);
        }
    else
        for(int i = 0;i<2;i++){
            auto lightning = Lightning::createLightning(tempPos,_class);
            //lightning->start();
            this->lightningMember.pushBack(lightning);
            
            tempPos = tempPos + Vec2(0,lightning->getContentSize().height-76);
        }
    return true;
}

void LightningSprite::start(float _dmg){
    this->isRecycle = false;
    CCLOG("LightningSprite] - Start !");
    if (_class == LightningClass::forEnemy)
        
        this->sound = SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_Electro, true);
    else
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Electric_Vortex, false);
    
    arrSound.pushBack(__Integer::create(sound));
    this->setRotation(fromAngle);
    
    
    for(auto lightning : this->lightningMember){
        if (target && target!= NULL)
            lightning->caster = target;
        lightning->start(_dmg);
        this->addChild(lightning);
    }
    
    auto rotate = RotateTo::create(duration, toAngle);
    auto callBackRotate = CallFuncN::create(CC_CALLBACK_0(LightningSprite::callFadeOutLightning, this));
    auto sequenceAct = Sequence::create(rotate,callBackRotate, NULL);
    
    this->runAction(sequenceAct);
    
    this->setPosition(target->getContentSize()/2);
    target->addChild(this,200);

}


void LightningSprite::stop(float dt){
    if (!isRecycle){
        for(auto lightning : this->lightningMember){
            lightning->stopImmediately();
        }
        if (this->_class == LightningClass::forEnemy)
            SoundBand::getInstance()->stopSoundAt(sound);
        
        this->unscheduleAllCallbacks();
        this->stopAllActions();
        this->removeFromParentAndCleanup(true);
        target = NULL;
        vecLightning[(int)_class].pushBack(this);
        vecLightningMoving.eraseObject(this);
        CCLog("[LightningSprite] - stop --vecLightningMoving: %d --vecLightning[%d]: %d", vecLightningMoving.size(),(int)_class,vecLightning[(int)_class].size());
        isRecycle = true;
        isStartWithPoint = false;
    }
    
    
}


void LightningSprite::callFadeOutLightning(){
    for(auto lightning : this->lightningMember){
        lightning->startFadeOut();
    }

    SoundBand::getInstance()->stopSoundAt(this->sound);
    scheduleOnce(schedule_selector(LightningSprite::stop), 1.1);
}

void LightningSprite::SetLightningColor(LightningColor _color){
    for(auto _lightning : lightningMember){
        switch (_color) {
            case LightningColor::Lightning_BLUE:
                _lightning->setColor(Color3B::WHITE);
                break;
                
            case LightningColor::Lightning_RED:
                _lightning->setColor(Color3B::RED);
                break;
                
            default:
                break;
        }
    }
}
