//
//  TextManager.cpp
//  Ship
//
//  Created by KuKulKan on 10/18/17.
//

#include "TextManager.hpp"
#include "GlobalVariable.hpp"
static TextManager* instance = NULL;
static string path[10];
static Vector<TextDamage*> vecLabel;
static Vector<TextDamage*> vecLabelWaiting;




void TextManager::loadResource(){
    getInstance();
    
    for(int i = 0; i < 50; i++){
        auto bmf = TextDamage::create();
        bmf->onCallBack = CC_CALLBACK_1(TextManager::onCallBackFinish, instance);
        bmf->retain();
        vecLabelWaiting.pushBack(bmf);
    }
}


TextManager* TextManager::getInstance(){
    if (instance == NULL){
        path[0] = "fonts/BauCua_Green.fnt";
        path[1] = "fonts/BauCua_Red.fnt";
        path[2] = "fonts/Roboto_Name.fnt";
        path[3] = "fonts/Roboto_CoinWin.fnt";
        path[4] = "fonts/Roboto_Title.fnt";
        path[5] = "fonts/Roboto_wheel.fnt";
        path[6] = "fonts/Roboto_Yellow.fnt";
        path[7] = "fonts/BauCua_Red2.fnt";
        instance =  TextManager::create();
        instance->retain();
    }
    return instance;
}


Label* TextManager::createTextDamage(float dmg,Vec2 pos, float size,float duration, TextFontType typeFont){
    
    
    if (vecLabelWaiting.size() > 0){
        string path = getInstance()->getFont(typeFont);
        string s_dmg;
        if (dmg < 1)
            s_dmg = StringUtils::format("-%.2f",dmg);
        else
            s_dmg = GLOBAL::GetCustomString(",", dmg);
        
        auto bmf = vecLabelWaiting.at(0);
        vecLabelWaiting.erase(0);
        
        bmf->setBMFontFilePath(path);
        bmf->setString(s_dmg);
        bmf->setScale(0.1);

        vecLabel.pushBack(bmf);

        bmf->setPosition(pos);
        instance->addChild(bmf);
        bmf->start();
        
        //if (COCOS2D_DEBUG){
            CCLOG("[TextManager] - vecLabel: %d",(int)vecLabel.size());
            CCLOG("[TextManager] - vecLabelWaiting: %d",(int)vecLabelWaiting.size());
        //}

        return bmf;
    }else{
        //if (COCOS2D_DEBUG)
            CCLOG("[TextManager] - WARNING: TextBMF exceeding limitation !!");
    }
    

    return NULL;
}

bool TextManager::init(){
    if (!Layer::init())
        return false;
    
    auto width = Director::getInstance()->getWinSize().width;
    auto height = Director::getInstance()->getWinSize().height;
    this->setContentSize(Size(width,height));
    this->setPosition(Vec2(0,0));
    
    return true;
}

string TextManager::getFont(TextFontType type){
    return path[(int)type];
}

void TextManager::clear(){
    for(auto label : vecLabel){
        label->stopAllActions();
        label->removeFromParentAndCleanup(true);
    }
    
    vecLabel.clear();
}

void TextManager::onCallBackFinish(TextDamage *obj){
    vecLabelWaiting.pushBack(obj);
    vecLabel.eraseObject(obj);
    
    //if (COCOS2D_DEBUG){
        CCLOG("[TextManager] - vecLabel: %d",(int)vecLabel.size());
        CCLOG("[TextManager] - vecLabelWaiting: %d",(int)vecLabelWaiting.size());
    //}
    
}
