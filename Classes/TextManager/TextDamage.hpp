//
//  TextDamage.hpp
//  Ship
//
//  Created by KuKulKan on 10/18/17.
//

#ifndef TextDamage_hpp
#define TextDamage_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "EnumManager.h"

using namespace std;
using namespace cocos2d;
using namespace cocos2d::ui;

class TextDamage : public Label{
public:
    typedef std::function<void(TextDamage*)> ccCallBackFinish;
    ccCallBackFinish onCallBack;
    
    bool init();
    
    void start();
    CREATE_FUNC(TextDamage);
    
private:
    Sequence* action;
};

#endif /* TextDamage_hpp */
