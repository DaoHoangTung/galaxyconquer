//
//  TextDamage.cpp
//  Ship
//
//  Created by KuKulKan on 10/18/17.
//

#include "TextDamage.hpp"


bool TextDamage::init(){
    
    if (!Label::init())
        return false;
    
    this->onCallBack = NULL;
    
    auto scaleUp = EaseElasticOut::create(ScaleTo::create(1, 1.), 0.5);
    auto fadeOut = FadeOut::create(1);
    auto jump = JumpBy::create(1,Vec2(random(-200, 200),random(0,110)),random(20, 100), 1);
    auto act = Spawn::create(scaleUp,fadeOut,jump, NULL);
    
    auto func = CallFuncN::create([&](Node* p){
        this->removeFromParentAndCleanup(true);
        this->setOpacity(255);
        if (this->onCallBack != NULL)
            this->onCallBack(this);
    });
    
    action = Sequence::create(act,func, NULL);
    action->retain();
    return true;
}

void TextDamage::start(){
    this->runAction(action);
}
