//
//  TextManager.hpp
//  Ship
//
//  Created by KuKulKan on 10/18/17.
//

#ifndef TextManager_hpp
#define TextManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "EnumManager.h"
#include "TextDamage.hpp"

using namespace std;
using namespace cocos2d;
using namespace cocos2d::ui;

enum TextFontType{
    F_BauCua_Green = 0,
    F_BauCua_Red = 1,
    F_Roboto_Name = 2,
    F_Roboto_CoinWin = 3,
    F_Roboto_Title = 4,
    F_Roboto_wheel = 5,
    F_Roboto_Yellow = 6,
    F_BauCua_Red_2 = 7
};

class TextManager : public Layer{
public:
    static TextManager* getInstance();
    static void loadResource();
    static Label* createTextDamage(float dmg, Vec2 pos, float size,float duration, TextFontType typeFont);
    
    bool init();
    void clear();
    
    void onCallBackFinish(TextDamage* obj);
    
    string getFont(TextFontType type);
    
    CREATE_FUNC(TextManager);
};





#endif /* TextManager_hpp */
