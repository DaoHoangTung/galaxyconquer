
#ifndef Boss_hpp
#define Boss_hpp

#include "cocos2d.h"
#include <SimpleAudioEngine.h>
#include "ui/CocosGUI.h"
#include "ActionRow.hpp"
#include "GunRow.hpp"
#include "Gun.h"
#include "SuperUnit.hpp"
#include "BossRewardObj.hpp"

USING_NS_CC;
class Boss : public SuperUnit{
public:
    int countItem;
    int currentAct;
    int maxAct;
    
    
    int tag;
    static Boss* getBossAtTag(int _tag);
    static Boss* getCurrentBoss();
    static void setCurrentBoss(Boss* _boss);
    static void clearCurrentBoss();
    static void clearVecBoss();
    
    static Boss* createBoss(int _tag, int _hp,Vector<GunRow*> _arrGunRow, Vector<ActionRow*> _arrActRow);
    
    bool initBoss(int _tag, int _hp,Vector<GunRow*> _arrGunRow, Vector<ActionRow*> _arrActRow);
    
    bool invunerable;
//    int hp,maxHp;
    
    float dmgAbort;
    
    void start();
    virtual void clear();
    virtual void takeDamage(float _dmg);
    void dropItem(int _lvlGravity) override;
    
    void setBossRewardInfo(BossRewardObj* _rewardInfo);
    
    void addEffect(int _index);
    void setGunFacing();
    void fire();
    void calRateBoss();
    

private:
    PhysicsBody* body;
    
    Vec2 initPosition;

    
    Vector<Gun*> arrGun;
    Vector<FiniteTimeAction*> arrAction;
    
    Vector<GunRow*> arrGunRow;
    Vector<ActionRow*> arrActRow;
    RepeatForever* listAction;
    
    FiniteTimeAction* getAction(ActionRow* _actRow,float _duration);
    
    int silverRate,goldRate,barRate,diamondRate,value, count;

    
    void createGun();
    void createAction();
    
    void startAttack(float dt);
        //
    

    
};

#endif /* Boss_hpp */
