
#ifndef SuperUnit_hpp
#define SuperUnit_hpp


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "EnumManager.h"
#include "Gun.h"
#include "Path.h"
#include "PlayerBullet.h"


USING_NS_CC;
using namespace cocos2d::ui;

class SuperUnit : public Sprite{
public:
    bool init(double _maxHP, double _hp);
    virtual void dropItem(int _lvlGravity);
    
    double maxHp;
    double hp;
//protected:
//    CC_SYNTHESIZE(float, maxHp, MaxHp);
//    CC_SYNTHESIZE(float, hp, Hp);

};
#endif /* SuperUnit_hpp */
