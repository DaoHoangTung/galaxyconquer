#include "Unit.h"
#include "Explosion.h"
#include "Item.h"
#include "MyMoveTo.h"
#include "TextManager.hpp"
#include "MissionScene.hpp"

static bool unitToFrameCache = false;
static Vector<Unit*> arrUnitCurrentOnMap;
static Vector<Unit*> arrReleaseUnit;
static Vector<RepeatForever*> arrReleaseAnimateAct;
static Layer* missionLayer;
static SpriteFrameCache* spriteCache;

void Unit::setMissionLayer(cocos2d::Layer *_layer){
    missionLayer = _layer;
}

Vector<RepeatForever*> Unit::getArrReleaseAnimate(){
    return arrReleaseAnimateAct;
}

Vector<Unit*> Unit::getArrReleaseUnit(){
    return arrReleaseUnit;
}
void Unit::loadResource(){
    if (!unitToFrameCache){
        
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Enemy/EnemySprite.plist");
        
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Enemy/BossSprite.plist");
        
        spriteCache = SpriteFrameCache::getInstance();
        unitToFrameCache = true;
    }
}

Unit* Unit::CreateUnit(__String _name, int _hp, Gun* _gun, float _atkCooldown, Path* _path, float _moveSpeed){
    
    auto unit = new (std::nothrow) Unit();
    
    if (unit){
        if (unit->initUnit(_name,_hp,_gun,_atkCooldown,_path,_moveSpeed,false)){
        }
        else{
            delete unit;
            unit = nullptr;
        }
        
        return unit;
    }
    
    return unit;
    
}


bool Unit::initUnit(__String _name, int _hp, Gun* _gun, float _atkCooldown, Path* _path, float _moveSpeed,bool _animate)
{
    if (!SuperUnit::init(_hp,_hp)){
        return false;
    }
    
    this->currentNodePath = 0;
    this->state = UnitState::Alive;
//    this->maxHp = _hp;
//    this->hp = maxHp;
    this->startingPoint = _path->arrVector.at(0)->point;
    this->atkCooldown = _atkCooldown;
    this->facing = 0;
    this->mainGun = _gun;
    this->animate = NULL;
    this->orderAttack = false;
    this->attackElapseTime = 0;
    this->vecFrame.clear();
    this->unitName = _name;
    this->isAnimate = _animate;
    this->isCleared = false;
    this->isSpecialUnit = false;
    this->hasDied = false;
    this->setScale(1.3);
    
    if (_animate){
        for (int i = 0; i <= 15; i++){
            this->vecFrame.reserve(30);
            char name[40] = { 0 };
            sprintf(name, "%s%d.png", _name.getCString(), i);;
            auto sprite = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
            this->vecFrame.pushBack(sprite);
        }
        
        this->setSpriteFrame(this->vecFrame.at(0));
    }else{
        char name[40] = { 0 };
        sprintf(name, "%s%d.png", unitName.getCString(), 0);;
        this->setSpriteFrame(name);
    }
    
    this->didFinishMove = CallFuncN::create(CC_CALLBACK_0(Unit::didFinishPath,this));
    this->didFinishMove->retain();
    
    this->hpBar = LoadingBar::create("Default/LoadingBarFile.png");
    this->hpBar->setDirection(cocos2d::ui::LoadingBar::Direction::LEFT);
    this->hpBar->setColor(Color3B::RED);
    float tempX = this->getContentSize().width / this->hpBar->getContentSize().width;
    this->hpBar->setScale(tempX,0.3);
    this->hpBar->setAnchorPoint(Vec2(0.5,0.5));
    this->hpBar->setPercent(100);
    this->hpBar->retain();
    
    auto fadeBar = cocos2d::FadeTo::create(1.5, 0);
    auto fadeBarFinish = cocos2d::CallFuncN::create(CC_CALLBACK_0(Unit::barFinishFade, this));
    
    this->barAction = Sequence::create(fadeBar,fadeBarFinish, NULL);
    this->barAction->retain();
    
    //this->setScale(1.5);
    
    this->mainGun->setPosition(this->getContentSize()/2);
    this->addChild(this->mainGun);
    
    body = PhysicsBody::createCircle(this->getContentSize().width/2, PhysicsMaterial(0.1f, 1.0f, 0.0f));
    body->setDynamic(true);
    body->setCategoryBitmask((int)PhysicCategory::Enemy);
    body->setCollisionBitmask((int)PhysicCategory::None);
    body->setContactTestBitmask((int)PhysicCategory::combo2);
    body->retain();
    this->setTag((int)PhysicCategory::Enemy);
    //
    return true;
}
void Unit::barStart(){
    if (this->getState() != UnitState::Dead){
        this->hpBar->setOpacity(255);
        this->hpBar->stopAllActions();
        float tempX = (float)this->hp/(float)this->maxHp;
        this->hpBar->setColor(Color3B(255. -255.*tempX, 255.*tempX, 0));
        this->hpBar->setPercent(100* tempX);
        missionLayer->addChild(this->hpBar);
        this->hpBar->runAction(this->barAction);
    }
}

void Unit::barFinishFade(){
    this->hpBar->removeFromParentAndCleanup(true);
}

void Unit::takeDamage(float _dmg){
    float bonusDamage = 2;
    float bonusItemDamage = 1;
    float totalBonus = bonusDamage + bonusItemDamage;
    
    _dmg*= (1.+totalBonus);
    
    //if (COCOS2D_DEBUG)
        CCLOG("[Unit] : take damage - bonusDamage: %.2f - bonusItem: %.2f  - final damage: %.2f",bonusDamage,bonusItemDamage,_dmg);
    
    if (this->isSpecialUnit){
        float dmgRecieved = _dmg - dmgAbort*_dmg;
        this->hp-= dmgRecieved;
        TextManager::createTextDamage(dmgRecieved, this->getPosition(), 0.7, 1, TextFontType::F_BauCua_Red);
    }else{
        this->hp-= _dmg;
        TextManager::createTextDamage(_dmg, this->getPosition(), 0.7, 1, TextFontType::F_BauCua_Red);
    }
    
//    ParticleSystemQuad* effect = ParticleSystemQuad::create("NewParticle/enemyGetHit.plist");
//    effect->setAutoRemoveOnFinish(true);
//    effect->setPosition(this->getPosition());
//    effect->resetSystem();
//    effect->setScale(2);
//    missionLayer->addChild(effect);

}

void Unit::setUnitFacing(float _angle, bool _radian)
{
    float angle;
    if (_radian){
        angle = CC_RADIANS_TO_DEGREES(_angle);
    }
    else{
        angle = _angle;
    }
    
    //CCLOG("%f", angle);
    if (angle < 0){ //Neu goc < 0 tuc la nam trong khoang 0 -> -180 theo chieu trai qua phai, ta chuyen ve goc do duong theo chieu kim dong ho 0 -> 360
        angle*= -1;
        angle += 270;
    }
    else
    {
        angle = 270 - angle; //Neu goc > 0 tuc la nam trong khoang 0 -> 180 theo chieu trai qua phai, ta chuyen ve goc do duong theo chieu kim dong ho 0 -> 360
    }
    //CCLOG("%f", angle);
    while (angle>360)
    {
        angle -= 360;          //Goc phai luon nam trong khoang 0 -> 360 de khi tinh ra index khong bi sai
    }
    //CCLOG("%f", angle);
    int index = angle / 22.5;
    //CCLOG("%f", index);
    float nearbyAngle = index * 22.5;
    
    if (angle - nearbyAngle >= 11.25){
        index++;
    }
    if (index >= 16) {
        index = 0;
    }
    
    
    //Facing cua sprite khong match voi goc tinh boi system.
    //Goc system 0 - 360 anti-clock
    //Goc sprite bat dau tu goc 90 deg cua goc system clockwide
    
    if (index == 0)
        this->setFacing(index * 22.5 + 90);
    else if (index >= 1 && index < 5)
        this->setFacing(90 - index * 22.5);
    else if (index >= 5 && index < 9)
        this->setFacing(360 - (index * 22.5 -90));
    else if (index >= 9 && index < 13)
        this->setFacing(270 - index * 22.5 + 180);
    else if (index >= 13)
        this->setFacing(450 - index * 22.5);
    
    
    if (!this->isAnimate){
        char name[40] = { 0 };
        sprintf(name, "%s%d.png", unitName.getCString(), index);;
        this->setSpriteFrame(name);
    }
    //this->setSpriteFrame(this->vecFrame.at(index));
    
    this->mainGun->angle = this->getFacing();
}

Vector<Unit*> Unit::getArrUnitCurrentOnMap()
{
    return arrUnitCurrentOnMap;
}

void Unit::startAttack()
{
    this->orderAttack = true;
    //this->schedule(schedule_selector(Unit::attack), atkCooldown, CC_REPEAT_FOREVER,1);
}

void Unit::attack(float dt)
{
    if (getState() == UnitState::Dead){return;}
    
    this->hpBar->setPosition(this->getPosition() + Vec2(0,this->getContentSize().height/2));
    if (this->orderAttack){
        if (this->attackElapseTime >= this->getAtkCoolDown()){
            if (this->mainGun->bulletType == BulletType::LIGHTNING){
                this->mainGun->fire();
            }else{
//                this->mainGun->fire(facing);
                this->mainGun->fire();
                //SimpleAudioEngine::getInstance()->playEffect("Sound/Sfx_EnemyBulletShoot.mp3",false);
            }
            this->attackElapseTime = 0;
        }else{
            this->attackElapseTime+= dt;
        }
    }
}

void Unit::didFinishPath()
{
    this->state = UnitState::Dead;
}

void Unit::start()
{
    this->runAction(moveActions);
    if (animate != NULL){
        this->runAction(animate);
    }
    this->startAttack();
    this->setVisible(true);
    this->body->setEnabled(true);
    this->setPhysicsBody(this->body);
    arrUnitCurrentOnMap.pushBack(this);
//    missionLayer->addChild(this,1);
}


void Unit::clear()
{
    if (!isCleared){
        for(auto bullet : this->mainGun->vecLightningBullet){
            bullet->stop(0);
        }
        this->mainGun->vecLightningBullet.clear();
        this->mainGun->clear();
        this->mainGun->removeFromParentAndCleanup(true);
        
        this->stopAllActions();
        this->unscheduleAllCallbacks();
        
        this->body->removeFromWorld();
        this->body->setEnabled(false);
        
        this->orderAttack = false;
        
        this->hpBar->stopAllActions();
        this->hpBar->removeFromParentAndCleanup(true);
        this->hpBar->setVisible(false);
        
        
        CC_SAFE_RELEASE_NULL(this->body);
        CC_SAFE_RELEASE_NULL(this->hpBar);
        CC_SAFE_RELEASE_NULL(this->barAction);
        CC_SAFE_RELEASE_NULL(this->moveActions);
        if (this->isAnimate){
            vecFrame.clear();
            CC_SAFE_RELEASE_NULL(this->animate);
            //arrReleaseAnimateAct.pushBack(this->animate);
        }
        CC_SAFE_RELEASE_NULL(this->didFinishMove);
        CC_SAFE_RELEASE_NULL(this->mainGun);
        
        arrUnitCurrentOnMap.eraseObject(this);
        this->removeFromParentAndCleanup(true);
        
        isCleared = true;
    }
}

int Unit::getUnitHp()
{
    return this->hp;
}



Unit* AnimateUnit::CreateAnimateUnit(__String _name, int _hp, Gun* _gun, float _atkCooldown, Path* _path, float _moveSpeed)
{
    auto unit = new (std::nothrow) AnimateUnit();
    
    if (unit){
        if (unit->initAnimateUnit(_name, _hp, _gun, _atkCooldown, _path, _moveSpeed)){
            unit->autorelease();
        }
        else
        {
            delete unit;
            unit = nullptr;
        }
    }
    
    return unit;
}

bool AnimateUnit::initAnimateUnit(__String _name, int _hp, Gun* _gun, float _atkCooldown, Path* _path, float _moveSpeed)
{
    if (!Unit::initUnit(_name, _hp, _gun, _atkCooldown, _path, _moveSpeed,true)){
        return false;
    }
    
    auto animation = Animation::createWithSpriteFrames(vecFrame, 0.09f);
    auto animate = Animate::create(animation);
    
    this->animate = RepeatForever::create(animate);
    this->animate->retain();
    
    int nextNodePath = this->currentNodePath + 1;
    Vector<FiniteTimeAction*> arrMoveAction;
    
    Vector<Unit*> vec;
    
    while (nextNodePath < _path->arrVector.size())
    {
        Vec2 currentPoint = _path->arrVector.at(currentNodePath)->point;
        Vec2 nextPoint = _path->arrVector.at(nextNodePath)->point;
        float distance = currentPoint.getDistance(nextPoint);
        float duration = distance / _moveSpeed;
        
        arrMoveAction.pushBack(MyMoveTo::create(duration, nextPoint));
        
        currentNodePath++;
        nextNodePath = currentNodePath + 1;
    }
    
//    for(auto point : _path->arrVector){
//        point->release();
//    }
//    
//    _path->arrVector.clear();
    
    auto makeActionMove = Sequence::create(arrMoveAction);
    this->moveActions = Sequence::createWithTwoActions(makeActionMove,didFinishMove);
    this->moveActions->retain();
    
    return true;
}

Unit* NonAnimateUnit::CreateNonAnimateUnit(__String _name, int _hp, Gun* _gun, float _atkCooldown, Path* _path, float _moveSpeed)
{
    auto unit = new (std::nothrow) NonAnimateUnit();
    
    if (unit){
        if (unit->initNonAnimateUnit(_name, _hp, _gun, _atkCooldown, _path, _moveSpeed)){
            unit->autorelease();
        }
        else
        {
            delete unit;
            unit = nullptr;
        }
    }
    
    return unit;
}

bool NonAnimateUnit::initNonAnimateUnit(__String _name, int _hp, Gun* _gun, float _atkCooldown, Path* _path, float _moveSpeed)
{
    if (!Unit::initUnit(_name, _hp, _gun, _atkCooldown, _path, _moveSpeed,false)){
        return false;
    }
    
    int nextNodePath = this->currentNodePath + 1;
    Vector<FiniteTimeAction*> arrMoveAction;
    
    while (nextNodePath < _path->arrVector.size())
    {
        Vec2 currentPoint = _path->arrVector.at(currentNodePath)->point;
        Vec2 nextPoint = _path->arrVector.at(nextNodePath)->point;
        float distance = currentPoint.getDistance(nextPoint);
        float duration = distance / _moveSpeed;
        
        arrMoveAction.pushBack(MyMoveTo::create(duration, nextPoint));
        
        currentNodePath++;
        nextNodePath = currentNodePath + 1;
    }
    
//    for(auto point : _path->arrVector){
//        point->release();
//    }
//    
//    _path->arrVector.clear();
    
    auto makeActionMove = Sequence::create(arrMoveAction);
    this->moveActions = Sequence::createWithTwoActions(makeActionMove, didFinishMove);
    this->moveActions->retain();
    
    return true;
}


Unit* SpecialUnit::CreateSpecialUnit(cocos2d::__String _name, int _hp, Gun *_gun, float _atkCooldown, Path *_path, float _moveSpeed, int _countItem, TypeCoin _typeCoin){
    
    auto unit = new (std::nothrow) SpecialUnit();
    
    if (unit){
        if (unit->initSpecialUnit(_name, _hp, _gun, _atkCooldown, _path, _moveSpeed, _countItem, _typeCoin)){
            unit->autorelease();
        }
        else
        {
            delete unit;
            unit = nullptr;
        }
    }
    
    return unit;
}

bool SpecialUnit::initSpecialUnit(cocos2d::__String _name, int _hp, Gun *_gun, float _atkCooldown, Path *_path, float _moveSpeed, int _countItem, TypeCoin _typeCoin){
    
    if (!AnimateUnit::initAnimateUnit(_name, _hp, _gun, _atkCooldown, _path, _moveSpeed)){
        return false;
    }
    
    this->countItemDrop = _countItem;
    this->typeCoin = typeCoin;
    this->isSpecialUnit = true;
    this->dmgAbort = 0.5;
    
    return true;
}

void Unit::Explode(PlayerBullet* _bullet){
    if (_bullet == NULL){
        if (this->isSpecialUnit){
            Explosion::createExplosionAtPos(this->getPosition(), 15, ExplosionType::EnergyExplosion);
            MissionScene::getInstance()->shake(1, 5);

            SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerExplode, false);
        }else{
            Explosion::createExplosionAtPos(this->getPosition(), 15, ExplosionType::MediumExplosion);
            SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyExplode, false);
        }
        
        return;
    }
    
    if (this->isSpecialUnit){
        Explosion::createExplosionAtPos(this->getPosition(), 15, ExplosionType::EnergyExplosion);
        
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerExplode, false);
        MissionScene::getInstance()->shake(1, 5);

    }else{
    
        switch (_bullet->bulletType) {
            case PlayerBulletType::Rocket:
               //do sth
                break;
            case PlayerBulletType::Light:
                Explosion::createExplosionAtPos(this->getPosition(), 15, ExplosionType::MediumExplosion);
                break;
            default:
                break;
        }
        
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyExplode, false);

    }
    
}

void Unit::Explode(Lightning* _bullet){
    
    if (this->isSpecialUnit){
        Explosion::createExplosionAtPos(this->getPosition(), 15, ExplosionType::EnergyExplosion);
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_PlayerExplode, false);
        MissionScene::getInstance()->shake(1, 5);
    }else{
        
        Explosion::createExplosionAtPos(this->getPosition(), 15, ExplosionType::MediumExplosion);
        
        SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyExplode, false);
        
    }
    
}

void Unit::dropItem(int _lvlGravity){
    if (!hasDied){
        GLOBAL::getInstance()->totalEnemyKilled++;
        hasDied = true;
    }
    
    
    int dropRate = random(0, 1000);
    int dropShield = random(0, 350);
    int dropMedic = random(0,250);
    int dropLife = random(0,750);
    int dropMulti = random(0,250);
    
    if (GLOBAL::getInstance()->GET_CURRENT_MISSION_INDEX() == GLOBAL::GET_DEVIL_SQUARE_MAP_INDEX()){
        dropRate = random(0, 700);
        dropShield = 1000;
        dropMedic = 1000;
        dropLife = 1000;
        dropMulti = 1000;
    }
    
    int medicChane = 15;
    
    
    if (dropShield < 2){
        Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeShield, this->maxHp, _lvlGravity);
    }else if (dropMedic < medicChane){
        Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeMedic, this->maxHp, _lvlGravity);
    }else if (dropLife < 2){
        Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeLife, this->maxHp, _lvlGravity);
    }else if (dropMulti < 5){
        Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeMulti, this->maxHp, _lvlGravity);

    }else{
        int coinValue = GLOBAL::getCoinValue();
        
        if (dropRate < 60){
            Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeSilverCoin, coinValue, _lvlGravity);
        }else if (dropRate>= 60 && dropRate < 85){
            Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeGoldCoin, coinValue, _lvlGravity);
        }else if (dropRate>= 85 && dropRate < 95){
            Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeGoldBar, coinValue, _lvlGravity);
        }else if (dropRate>= 95 && dropRate < 100){
            Coin::createCoinAtPos(this->getPosition(), TypeCoin::typeDiamond, coinValue, _lvlGravity);
        }
    }
}


void SpecialUnit::dropItem(int _lvlGravity){
    if (!hasDied){
        GLOBAL::getInstance()->totalEnemyKilled++;
        hasDied = true;
    }
    
    int coinValue = GLOBAL::getCoinValue();

    Coin::createCoinAtRandomPos(this->getPosition(), 40, 100, TypeCoin::typeSilverCoin, this->countItemDrop, coinValue,_lvlGravity);
}
