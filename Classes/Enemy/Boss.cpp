
#include "MissionScene.hpp"
#include "Boss.hpp"
#include "GlobalVariable.hpp"
#include "PhysicsShapeCache.h"
#include "SoundBand.hpp"
#include "TextManager.hpp"
#include "Item.h"
static Vector<Boss*> vecBoss;

static Boss* staticBoss = NULL;

static Boss* arrBoss[14];

Boss* Boss::getCurrentBoss(){
    return staticBoss;
}

void Boss::clearCurrentBoss(){
    if (staticBoss != NULL)
        staticBoss->clear();
}

void Boss::clearVecBoss(){
    vecBoss.clear();
}

Boss* Boss::getBossAtTag(int _tag){
    if (_tag > 12){
        return vecBoss.at(_tag-3);
    }
    return vecBoss.at(_tag-1);
}
void Boss::setCurrentBoss(Boss *_boss){
    staticBoss = _boss;
}

Boss* Boss::createBoss(int _tag, int _hp,Vector<GunRow*> _arrGunRow, Vector<ActionRow*> _arrActRow){
    
    auto boss = new (std::nothrow) Boss();
    
    if (boss){
        boss->initBoss(_tag, _hp, _arrGunRow, _arrActRow);
    }else{
        CC_SAFE_DELETE(boss);
    }
    
    return boss;
}

bool Boss::initBoss(int _tag, int _hp, Vector<GunRow*> _arrGunRow, Vector<ActionRow*> _arrActRow){
    
    if (!SuperUnit::init(_hp,_hp)){
        return false;
    }
    
    auto _path = StringUtils::format("Boss%d.png",_tag);
    auto frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(_path.c_str());
    this->setSpriteFrame(frame);
//    this->maxHp = _hp;
    
    
    
    this->arrGunRow = _arrGunRow;
    this->arrActRow = _arrActRow;
    this->createGun();
    this->createAction();
    this->listAction = RepeatForever::create(Sequence::create(arrAction));
    this->listAction->retain();
    this->countItem = 20;
    this->dmgAbort = GLOBAL::GET_BOSS_SHIELD_ABORT_DAMAGE();
    this->maxAct = (int)arrAction.size();
    
    
    for(auto gun : arrGun){
        this->addChild(gun);
    }
//
//    body = PhysicsBody::createBox(this->getContentSize(), PhysicsMaterial(0.1f, 1.0f, 0.0f));
        body = PhysicsShapeCache::getInstance()->createBodyWithName("Boss1");
    body->setDynamic(true);
    body->setCategoryBitmask((int)PhysicCategory::Boss);
    body->setCollisionBitmask((int)PhysicCategory::None);
    body->setContactTestBitmask((int)PhysicCategory::combo2);
    body->setEnabled(false);
    body->retain();
    //this->start();
    
    tag = _tag;
//    arrBoss[_tag] = this;
    vecBoss.pushBack(this);
    this->retain();
    
    return true;
}

void Boss::start(){
    this->setRotation(0);
    this->currentAct = 0;
    this->hp = this->maxHp;
    this->invunerable = true;
    
//
    
    for(auto _gun : arrGun){
        auto lightningGun = dynamic_cast<GunLightningV1*>(_gun);
        if (lightningGun != NULL){
//            lightningGun->onDamageEnemy = CC_CALLBACK_2(MissionScene::callBackEnemyLightningDamage, MissionScene::getInstance());
        }
    }
    
    staticBoss = this;
    float sizeW = 640;
    float sizeH = 960;
    
    this->setPosition(Vec2(sizeW/2,sizeH + this->getContentSize().height));

    this->initPosition = Vec2(sizeW/2,640);
    auto moveToCenter = MoveTo::create(5, this->initPosition);
    
    this->runAction(moveToCenter);
    this->scheduleOnce(schedule_selector(Boss::startAttack), 8);
    
    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_Sfx_EnemyBossSiren, false);
    
    this->body->setEnabled(true);
    this->setPhysicsBody(body);

}

void Boss::startAttack(float dt){
    
    invunerable = false;
    auto speed = Speed::create(listAction, 1);
    this->runAction(speed);
}

void Boss::addEffect(int _index){
    auto _gun = arrGun.at(_index);
    auto effect = ParticleSystemQuad::create("NewParticle/bossFireEffect.plist");
    effect->setPosition(_gun->getContentSize()/2);
    effect->setAutoRemoveOnFinish(true);
    _gun->addChild(effect);
    effect->setScale(2);
    
    SoundBand::getInstance()->playSoundAt(Sound_Tag::SoundTag_DruidOfTheTalonMissileHit2, false);
}

void Boss::setBossRewardInfo(BossRewardObj* _rewardInfo){
    silverRate = _rewardInfo->silver;
    goldRate = _rewardInfo->gold + silverRate;
    barRate = _rewardInfo->bar + goldRate;
    diamondRate = _rewardInfo->diamond + barRate;
    value = _rewardInfo->value;
    count = _rewardInfo->count;
}


void Boss::dropItem(int _lvlGravity){
    for(int i = 0; i < count; i++){
        int rand = random(1, 100);
        float angle = random(0, 360);
        
        float offX = 100*cos(CC_DEGREES_TO_RADIANS(angle));
        float offY = 150*sin(CC_DEGREES_TO_RADIANS(angle));

        
        if (rand < silverRate)
            Coin::createCoinAtRandomPos(this->getPosition(), offX, offY, TypeCoin::typeSilverCoin, 1, value, _lvlGravity);
        else if (rand <= goldRate)
            Coin::createCoinAtRandomPos(this->getPosition(), offX, offY, TypeCoin::typeGoldCoin, 1, value, _lvlGravity);
        else if (rand <= barRate)
            Coin::createCoinAtRandomPos(this->getPosition(), offX, offY, TypeCoin::typeGoldBar, 1, value, _lvlGravity);
        else if (rand <= diamondRate)
            Coin::createCoinAtRandomPos(this->getPosition(), offX, offY, TypeCoin::typeDiamond, 1, value, _lvlGravity);
    }
}

void Boss::clear(){
    this->stopAllActions();
    this->body->removeFromWorld();
    this->removeFromParentAndCleanup(true);
    Boss::setCurrentBoss(NULL);
}

void Boss::takeDamage(float _dmg){
    float dmgRecieved = _dmg - (float)_dmg*dmgAbort;
    
    
    this->hp = MAX(0,this->hp-dmgRecieved);
    
    TextManager::createTextDamage(dmgRecieved, this->getPosition(), 1, 1.2, TextFontType::F_BauCua_Red);
    
    //if (COCOS2D_DEBUG)
        CCLOG("[Boss] : HP--%d  DamageTaken--%.2f  DamageAborb: %.2f  RealDamage: %.2f",this->hp,dmgRecieved,dmgAbort,_dmg);
    
}

void Boss::calRateBoss(){
    float rate = MAX(0, 1.-(float)hp / (float)maxHp);
    float rateBoss = rate * 2.5;
    rateBoss = MIN(rateBoss, 2.5);
    GLOBAL::getInstance()->rateBoss = rateBoss;
}

void Boss::fire(){
    this->setGunFacing();
    auto _index = this->currentAct;
    
    auto _indexGun1 = arrActRow.at(_index)->gunIndex1;
    auto _indexGun2 = arrActRow.at(_index)->gunIndex2;
    auto _indexGun3 = arrActRow.at(_index)->gunIndex3;
    auto fireType = arrActRow.at(_index)->fireTag;
    
    switch (fireType) {
        case FireTag_End:
        case FireTag_Middle:
        case FireTag_Start:
            arrGun.at(_indexGun1)->fire();
            this->addEffect(_indexGun1);
            break;
            
        case FireTag_GUN_1_2:
            arrGun.at(_indexGun1)->fire();
            arrGun.at(_indexGun2)->fire();
            this->addEffect(_indexGun1);
            this->addEffect(_indexGun2);
            break;
            
        case FireTag_GUN_1_3:
            arrGun.at(_indexGun1)->fire();
            arrGun.at(_indexGun3)->fire();
            this->addEffect(_indexGun1);
            this->addEffect(_indexGun3);
            break;
            
        case FireTag_GUN_2_3:
            arrGun.at(_indexGun2)->fire();
            arrGun.at(_indexGun3)->fire();
            this->addEffect(_indexGun2);
            this->addEffect(_indexGun3);
            break;
            
        case FireTag_GUN_1_2_3:
            arrGun.at(_indexGun1)->fire();
            arrGun.at(_indexGun2)->fire();
            arrGun.at(_indexGun3)->fire();
            this->addEffect(_indexGun1);
            this->addEffect(_indexGun2);
            this->addEffect(_indexGun3);
            break;
            
        default:
            break;
    }

}

void Boss::createGun(){
    if (arrGun.size()>0)
        arrGun.clear();
    
    for(auto gunRow : arrGunRow){
        auto gun = gunRow->getGun();
        gun->angle = 270;
        arrGun.pushBack(gun);
        gun->setPosition(Vec2(gunRow->gunPos));

    }
    
    CCLOG("[Boss] - List gun: %d",(int) arrGun.size());
}

FiniteTimeAction* Boss::getAction(ActionRow* _actRow,float _duration){
    switch (_actRow->actTag) {
        case ActionTag_MoveTo:
            return MoveTo::create(_duration, Vec2(_actRow->getValue(),_actRow->getValue()));
            break;
            
        case ActionTag_JumpTo:
            return JumpTo::create(_duration, Vec2(_actRow->getValue(),_actRow->getValue()), _actRow->getValue(), _actRow->getValue());
            break;
            
        case ActionTag_RotateBy:
            return RotateBy::create(_duration, _actRow->getValue());
            break;
            
        case ActionTag_EaseInMoveTo:
            return EaseIn::create(MoveTo::create(_duration,Vec2(_actRow->getValue(), _actRow->getValue())), _actRow->getValue());
            break;
        case ActionTag_EaseOutMoveTo:
            return EaseOut::create(MoveTo::create(_duration,Vec2(_actRow->getValue(), _actRow->getValue())), _actRow->getValue());
            break;
            
        default:
            break;
    }
    
    CCLOG("[Boss] - getAction = NULL");
    return NULL;
}

void Boss::createAction(){
    arrAction.clear();
    
    for(ActionRow* actRow : arrActRow){
        actRow->num = 0;
        auto duration = actRow->getValue();
        
        
        auto mainAct = getAction(actRow, duration);
        
        auto delayTime = actRow->getValue();
        
        auto fireAct = CallFuncN::create(CC_CALLBACK_0(Boss::fire, this));
        
        int countFire = actRow->getValue();
        CCLOG("[Boss] - CreateAction --countFire: %d",countFire);
        FireTag fireTag = (FireTag)actRow->getValue();
        float delayFireTime = duration/countFire;
        
        Repeat* fireAction = NULL;
        
        CCLOG("[Boss] - FireTag: %d",(int)fireTag);
        switch (fireTag) {
            case FireTag_Start:
            {
                auto action1 = Sequence::create(fireAct,DelayTime::create(delayFireTime), NULL);
                fireAction = Repeat::create(action1, countFire);
            }
                break;
                
            case FireTag_Middle:
            case FireTag_GUN_1_2:
            case FireTag_GUN_1_3:
            case FireTag_GUN_2_3:
            case FireTag_GUN_1_2_3:
            {
                auto action1 = Sequence::create(DelayTime::create(delayFireTime/2),fireAct,DelayTime::create(delayFireTime/2), NULL);
                fireAction = Repeat::create(action1, countFire);
                
            }
                break;
                
            case FireTag_End:
            {
                auto action1 = Sequence::create(DelayTime::create(delayFireTime),fireAct, NULL);
                fireAction = Repeat::create(action1, countFire);
            }
                break;
                
            default:
                break;
        }
        
        auto spawn = Spawn::create(mainAct,fireAction, NULL);
        
        auto func = CallFuncN::create([&](Node* p){
            this->currentAct++;
            if (this->currentAct == this->maxAct)
                this->currentAct = 0;
        });
        
        if (delayTime > 0){
            arrAction.pushBack(Sequence::create(spawn,DelayTime::create(delayTime),func, NULL));
        }else{
            arrAction.pushBack(Sequence::create(spawn,func, NULL));
        }
    }
}

void Boss::setGunFacing(){
    for(Gun* gun : arrGun){
        gun->angle = 270 - staticBoss->getRotation();
    }
}


