#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "LobbyScene.hpp"
#include "Unit.h"
#include "Item.h"
#include "LightShieldItem.hpp"
#include "Path.h"
#include "Bullet.h"
#include "LightningSprite.hpp"
#include "BossReader.hpp"
#include "Explosion.h"
#include "MyText.hpp"
#include "PhysicsShapeCache.h"
#include "ShopItemScene.hpp"

static cocos2d::Size designResolutionSize = cocos2d::Size(640, 960);


USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
                glview = GLViewImpl::createWithRect("Ship", Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
        #else
                glview = GLViewImpl::create("Ship");
        #endif
                director->setOpenGLView(glview);
            }
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
            glview->setFrameZoomFactor(0.75);
        #endif
            

    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::EXACT_FIT);

    // turn on display FPS
    director->setDisplayStats(true);
    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    FileUtils::getInstance()->addSearchPath("res");
    
    
    
    //Load game resources
    
    PhysicsShapeCache::getInstance()->addShapesWithFile("CustomBody/BossBody.plist");

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ShopItem/ShopItemActive.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("ShopItem/ShopItemDisable.plist");
    
    LobbyScene::loadResource();
    Unit::loadResource();
    Coin::loadResource();
    LightShieldEffect::loadResource();
    Path::loadResource();
    Bullet::loadBullet();
    MyText::loadResource();
    LightningSprite::loadResource();
    BossReader::loadResource();
    Explosion::loadResource();
    MyText::loadResource();
    ShopItemScene::loadResource();
    
    // create a scene. it's an autorelease object
    auto scene = HelloWorld::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
