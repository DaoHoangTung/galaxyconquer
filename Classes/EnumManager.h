#ifndef _ENUM_MANAGER_
#define  _ENUM_MANAGER_

#include "SoundBand.hpp"
#include <json/rapidjson.h>
#include "external/json/document.h"

using namespace rapidjson;

enum class PropertiesTag{
    tagHP = 0,
    tagCount = 1,
    tagAttackSpeed = 2,
    tagAfterTime = 3,
    tagPriority = 4,
    tagMoveSpeed = 5,
    tagPeriod = 6,
    tagItemCount = 7,
    tagItemType = 8
};

enum class GameStateScene{
    tag_LoadingScene = 0,
    tag_MenuScene = 1,
    tag_LobbyScene = 2,
    tag_ShopScene = 3,
    tag_QuestScene = 4,
    tag_SkillScene = 5,
    tag_MissionScene = 6,
    tag_RankScene = 7,
    tag_IAPScene = 8,
    tag_CreditScene = 9,
    tag_InstructiontScene = 10,
    tag_ExtraUpgradeScene = 11,
    tag_WinLoseScene = 12,
    tag_GalaxyChallengeRankScene = 13


};

enum class LoginState{
    Authenticate_Failed = 0,
    Connection_Failed = 1,
    CheckNewUser_Failed = 2,
    Exit_Failed = 3
};

enum class LoginMethod{
    method_login = 0,
    method_create = 1,
    method_facebook = 2
};

enum class SpriteType{
    NON_ANIMATED = 0,
    ANIMATED = 1,
    SPECIAL = 2
};

enum class LevelState {
    LevelLoading = 0,
    LevelStart = 1,
    LevelBeginEnd = 2,
    LevelEnd = 3
};

enum class PlayerBulletType{
    Light = 0,
    Rocket = 1,
};

enum class PlayerLightBullet{
    TopLeft = 7,
    Top = 0,
    TopRight = 1,
    Right = 2,
    BotRight = 3,
    Bot = 4,
    BotLeft = 5,
    Left = 6,
    
};

enum class UpgradeItemOrder{
    itemGunT = 0,
    itemGunTR = 1,
    itemGunR = 2,
    itemGunBR = 3,
    itemGunB = 4,
    itemGunBL = 5,
    itemGunL = 6,
    itemGunTL = 7,
    itemGravity = 8,
    itemRocket = 9,
    itemAttackRate = 10,
    itemAmor = 11,
    itemAborbShieldDamage = 12,
    itemAborbShieldDuration = 13,
    itemAborbShieldRange = 14,
    itemAborbShieldRecharge = 15
};

enum class PlayerLightGun{
    TopLeft = 7,
    Top = 0,
    TopRight = 1,
    Right = 2,
    BotRight = 3,
    Bot = 4,
    BotLeft = 5,
    Left = 6,
    
};

enum class Upgrade {
    ChuaCo = 0,
    FirstUpgrade = 1,
    SecondUpgrade = 2,
    FinalUpgrade = 3
};

enum class ShipState {
    NormalState = 0,
    BeginActiveShield = -1,
    ActiveShield = 1,
    BeginInvulnerable = -2,
    Invulnerable = 2,
    BeginDeath = -3,
    Death = 3
};

enum class ShipSpeedState{
    ShipSpeedState_ZERO = 1,
    ShipSpeedState_NORMAL = 2
};

enum class BarState {
    fadeIn = 0,
    fadeOut = 1
};

enum class ItemState {
    touch = 0,
    untouch = 1
};

enum class TouchState {
    Ngoai = 0,
    TrenItem = 1
};

enum class UnitState {
    Alive = 0,
    Dead = 1
};

enum class BulletType {
    RED_SMALL = 0,
    RED_MEDIUM = 1,
    RED_LARGE = 2,
    RED_EXTRA = 3,
    GREEN_SMALL = 4,
    GREEN_MEDIUM = 5,
    GREEN_LARGE = 6,
    GREEN_EXTRA = 7,
    LAZER_RED = 8,
    LAZER_NONRED = 9,
    LIGHTNING = 10
};

enum class ExplosionType {
    SmallExplosion = 0,
    MediumExplosion = 1,
    LargeExplosion = 2,
    
    SmallExplosionA = 3,
    SmallExplosionB = 4,
    MediumExplosionA = 5,
    MediumExplosionB = 6,
    LargeExplosionA = 7,
    LargeExplosionB = 8,
    
    EnergyExplosion = 9
};

enum class ShipHpLevel {
    BeginHP = 50,
    FirstHP = 70,
    SecondHP = 110,
    FinalHP = 170
};

enum class ShipSpeedLevel {
    BeginShipSpeed = 2,
    FirstShipSpeed = 3,
    SecondShipSpeed = 4,
    FinalShipSpeed = 5
};

enum class TypeCoin {
    typeSilverCoin = 0,
    typeGoldCoin = 1,
    typeGoldBar = 2,
    typeDiamond = 3,
    typeShield = 4,
    typeMedic = 5,
    typeLife = 6,
    typeMulti = 7,
    typeTreasure = TypeCoin::typeDiamond | TypeCoin::typeGoldBar | TypeCoin::typeGoldCoin | TypeCoin::typeSilverCoin | TypeCoin::typeShield | TypeCoin::typeMedic | TypeCoin::typeLife | TypeCoin::typeMulti,
};

enum class TypeMedal {
    typeSilverMedal = 0,
    typeGoldMedal = 1,
    typeDiamondMedal = 2
};

enum class TypeOtherItem {
    typeMultiplier = 0,
    typeShield = 1,
    typeLife = 2
};

enum class BossState {
    stateNull = -1,
    stateBeginStart = 0,
    stateStart = 1,
    stateBeginActive = 2,
    stateActive = 3,
    stateBeginDead = 4,
    stateDead = 5
};

enum class typeLazer {
    typeRedLazer = 0,
    typeNonRedLazer = 1
};

enum class RocketState {
    stateTarget = 0,
    stateNonTarget = 1
};

enum class MissionState {
    stateLock = 0,
    stateUnlock = 1
};

enum class ComboType {
    ComboStart = 0,
    Combo20 = 1,
    Combo40 = 2,
    Combo80 = 3,
    Combo120 = 4,
    Combo160 = 5,
    Combo200 = 6,
    Combo240 = 7,
    Combo280 = 8,
    Combo300 = 9,
    ComboImmortal = 10,
    ComboBreak = 11
};

enum class PhysicCategory{
    None = 0,
    Enemy = (1 << 0),
    Bullet = (1 << 1),
    BackGround = (1 << 2),
    Item = (1 << 3),
    PlayerShip = (1 << 4),
    Bar = (1 << 5),
    EnemyBullet = (1 << 6),
    Boss = (1 << 7),
    Lightning = (1 << 8),
    AborbShield = (1 << 9),
    Kame = (1 << 10),
    BuletDestroyer = (1 << 11),
    RocketAoE = (1 << 12),
    NodeAoE = (1 << 13),
    
    combo1 = PhysicCategory::Bar | PhysicCategory::Item | PhysicCategory::EnemyBullet | PhysicCategory:: Boss | PhysicCategory::Enemy | PhysicCategory::Lightning,
    combo2 = PhysicCategory::Bullet | PhysicCategory::PlayerShip | PhysicCategory::Lightning | PhysicCategory::AborbShield | PhysicCategory::Kame | RocketAoE | NodeAoE,
    combo3 = PhysicCategory::Enemy | PhysicCategory::Boss,
    
    combo4 = PhysicCategory::AborbShield | PhysicCategory::PlayerShip | BuletDestroyer,
    
    combo5 = PhysicCategory::EnemyBullet | PhysicCategory::Enemy,
    
    combo6 = PhysicCategory::Boss | PhysicCategory::Enemy,

};

enum class PhysicsGroup{
    GROUP_0 = 1, //Include bar and ship only
};

enum class TagObject{
    Bar = 1,
    Ship = 2
};

enum GunTypeTag{
    GunTypeTag_AOE = 1,
    GunTypeTag_Lightning = 2,
    GunTypeTag_HarasBot = 3,
    GunTypeTag_LightningV1 = 4,
    GunTypeTag_Customized = 5

};

enum FireTag{
    FireTag_Start = 1,
    FireTag_Middle = 2,
    FireTag_End = 3,
    FireTag_GUN_1_2 = 4,
    FireTag_GUN_1_3 = 5,
    FireTag_GUN_2_3 = 6,
    FireTag_GUN_1_2_3 = 7
};

enum ActionTag{
    ActionTag_MoveTo = 1,
    ActionTag_JumpTo = 2,
    ActionTag_RotateBy = 3,
    ActionTag_EaseInMoveTo = 4,
    ActionTag_EaseOutMoveTo = 5};

enum SkillTag{
    Skill_Lightning = 0,
    Skill_Besserk = 1,
    Skill_BlackHole = 2,
    Skill_Kame = 3,
    Skill_RocketRain = 4
};

enum SkillModelTag{
    SkillModel_A1 = 0,
    SkillModel_A2 = 1,
    SkillModel_A3 = 2,
    SkillModel_B1 = 3,
    SkillModel_B2 = 4,
    SkillModel_B3 = 5,
    SkillModel_C1 = 6,
    SkillModel_C2 = 7,
    SkillModel_C3 = 8,
    SkillModel_D1 = 9,
    SkillModel_D2 = 10,
    SkillModel_D3 = 11,
    SkillModel_E1 = 12,
    SkillModel_E2 = 13,
    SkillModel_E3 = 14,
    SkillModel_F1 = 15,
    SkillModel_F2 = 16,
    SkillModel_F3 = 17,
    SkillModel_G1 = 18,
    SkillModel_G2 = 19,
    SkillModel_G3 = 20,
    SkillModel_H1 = 21,
    SkillModel_H2 = 22,
    SkillModel_H3 = 23,
    SkillModel_I1 = 24,
    SkillModel_I2 = 25,
    SkillModel_I3 = 26,
    SkillModel_J1 = 27,
    SkillModel_J2 = 28,
    SkillModel_J3 = 29,
    SkillModel_K1 = 30,
    SkillModel_K2 = 31,
    SkillModel_K3 = 32,
    SkillModel_L1 = 33,
    SkillModel_L2 = 34,
    SkillModel_L3 = 35

};

enum SkillLayerTag{
    SkillLayer_A = 0,
    SkillLayer_B = 1,
    SkillLayer_C = 2,
    SkillLayer_D = 3,
    SkillLayer_E = 4,
    SkillLayer_F = 5,
    SkillLayer_G = 6,
    SkillLayer_H = 7,
    SkillLayer_I = 8,
    SkillLayer_J = 9,
    SkillLayer_K = 10,
    SkillLayer_L = 11

};

enum SkillCastModelTag{
    SkillCast_A = 4,
    SkillCast_B = 7,
    SkillCast_C = 9,
    SkillCast_D = 10,
    SkillCast_E = 12,
    SkillCast_F = 11,
    SkillCast_None = 999
};

enum Direction{
    D_TOP = 1,
    D_RIGHT = 2,
    D_BOTTOM = 3,
    D_LEFT = 4
};

enum DonateRewardState{
    RewardState_LOCK = 1,
    RewardState_OPEN = 2,
    RewardState_CLOSE = 3
};

enum SPItemState{
  SPItemState_OPEN = 1,
    SPItemState_CLOSE = 2
};

enum GoldItemState{
    GoldItemState_OPEN = 1,
    GoldItemState_CLOSE = 2
};

enum SPItemType{
    SPItemType_EXP1 = 1,
    SPItemType_EXP2 = 2,
    SPItemType_EXP3 = 3,
    SPItemType_DMG = 4,
    SPItemType_ARMOR = 5,
    SPItemType_RocketRain = 6,
    SPItemType_EnergyBall = 7,
    SPItemType_SkillSlot = 8
};

enum GoldItemType{
  GoldItemType_EXP1 = 1,
    GoldItemType_EXP2 = 2,
    GoldItemType_EXP3 = 3,
    GoldItemType_DMG = 4,
    GoldItemType_ARMOR = 5

};

enum DataType{
    Data_Admin = 0,
  Data_StarPoint = 1,
    Data_DonateReward = 2,
    Data_FreeCoin = 3,
    Data_ShopSP= 4,
    Data_ShopGold = 5,
    Data_StatusElement = 6,
    Data_CoreData = 7,
    Data_ExtraUpgradeInfo = 8,
    Data_WinLoseInfo = 9,
    Data_MissionReward = 10,
    Data_Ship = 11,
    Data_NotifyReward = 12
};

typedef struct DataTime
{
    int year;
    int month;
    int day;
    int h;
    int m;
    int s;
}DataTime;

enum ItemStatusType{
    ItemStatusType_GOLD_EXP_1 = 1,
    ItemStatusType_GOLD_EXP_2 = 2,
    ItemStatusType_GOLD_EXP_3 = 3,
    ItemStatusType_SP_EXP_1 = 4,
    ItemStatusType_SP_EXP_2 = 5,
    ItemStatusType_SP_EXP_3 = 6,
    ItemStatusType_GOLD_DMG = 7,
    ItemStatusType_GOLD_ARMOR = 8,
    ItemStatusType_SP_DMG = 9,
    ItemStatusType_SP_ARMOR = 10
};

enum ItemGroupStatusType{
    ItemGroupStatusType_EXP = 1,
    ItemGroupStatusType_DMG = 2,
    ItemGroupStatusType_ARMOR = 3,
    ItemGroupStatusType_CASUAL = 4

};

enum CSBFileType{
    CSBFile_RatingEvent = 1,
    CSBFile_SquareEvent = 2,
    CSBFile_SquareEventWarning = 3,
    CSBFile_ConfirmLogoutDialog = 4,
    CSBFile_VersionInfo = 5


};

enum MissionRewardElementStatus{
    MissionRewardElement_LOCK = 1,
    MissionRewardElement_UNLOCK = 2,
    MissionRewardElement_DONE = 3
};

enum SpecialEventType{
    SpecialEventType_DevilSpace = 1,
    SpecialEventType_BountySpace = 2,
    SpecialEventType_MinerEvent = 3,
    SpecialEventType_TopReward = 4

};

enum ShipUpgradeType{
    ShipUpgradeType_Damage = 1,
    ShipUpgradeType_Hp = 2,
    ShipUpgradeType_Armor = 3,
    ShipUpgradeType_Level = 4,
    ShipUpgradeType_RocketDamage = 5,
    ShipUpgradeType_RocketNumber = 6,
    ShipUpgradeType_Exp = 7,
    ShipUpgradeType_MaxExp = 8
};

#endif
