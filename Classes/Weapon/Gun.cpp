#include "Gun.h"

//Implement class Gun
//Implement class Gun
//Implement class Gun

Gun* Gun::createGun(BulletType _bulletType, float _dmg)
{
    auto gun = new (std::nothrow) Gun();
    
    if (gun){
        if (gun->initGun(_bulletType, _dmg)){
            //gun->autorelease();
        }
        else
        {
            delete gun;
            gun = nullptr;
        }
    }
    
    return gun;
}

Vec2 Gun::globalVec2(){
    auto thisParent = this->getParent();
    if (thisParent != NULL){
        if (thisParent->getParent() != NULL)
        return thisParent->getParent()->convertToWorldSpace(thisParent->convertToWorldSpace(this->getPosition()));
    }
    
    return Vec2(320,480);
}

bool Gun::initGun(BulletType _bulletType, float _dmg)
{
    if (!Node::init()){
        return false;
    }
    
    this->bulletType = _bulletType;
    this->dmg = _dmg;
    this->setAnchorPoint(Vec2(0.5,0.5));
    
    return true;
}

void Gun::fire( float angle){}
void Gun::fire(Vec2 _attackerPos){}
void Gun::fire(cocos2d::Sprite *_caster){}
void Gun::fire(){}
void Gun::fire(Vec2 _attackerPos, float angle){}


Gun* Gun::clone()
{
    return createGun(this->bulletType, this->dmg);
}

void Gun::addUnit(cocos2d::Sprite *unit){}
void Gun::removeUnit(cocos2d::Sprite *unit){}
void Gun::clear(){};

//Implement class NormalGun
//Implement class NormalGun
//Implement class NormalGun

NormalGun* NormalGun::createNormalGunWithShootingPerAngle(float _addAngle, BulletType _bulletType, float _dmg)
{
    auto normalGun = new (std::nothrow) NormalGun();
    
    if (normalGun){
        if (normalGun->initNormalGunWithShootingPerAngle(_addAngle, _bulletType,_dmg)){
            //normalGun->autorelease();
        }
        else
        {
            delete normalGun;
            normalGun = nullptr;
        }
    }
    return normalGun;
}

bool NormalGun::initNormalGunWithShootingPerAngle(float _addAngle, BulletType _bulletType, float _dmg)
{
    if (!Gun::initGun(_bulletType, _dmg)){
        return false;
    }
    
    this->addAngle = _addAngle;
    
    
    return true;
}


void NormalGun::fire(float _angle)
{
    attackerPos = globalVec2();
    angle = _angle;
    
    Bullet::createBulletAtPos(attackerPos, _angle + addAngle, 10, bulletType, dmg);
    Bullet::createBulletAtPos(attackerPos, _angle - addAngle, 10, bulletType, dmg);
}

void NormalGun::fire()
{
    attackerPos = globalVec2();
    
    Bullet::createBulletAtPos(attackerPos, angle + addAngle, 10, bulletType, dmg,(Layer*)this->getParent()->getParent());
    Bullet::createBulletAtPos(attackerPos, angle - addAngle, 10, bulletType, dmg,(Layer*)this->getParent()->getParent());
}

NormalGun* NormalGun::clone()
{
    return createNormalGunWithShootingPerAngle(this->addAngle, this->bulletType, this->dmg);
}

//Implement class AOEGun
//Implement class AOEGun
//Implement class AOEGun

AOEGun* AOEGun::createAOEGunWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg)
{
    auto aoeGun = new (std::nothrow) AOEGun();
    
    if (aoeGun){
        if (aoeGun->initAOEGunWithShootingPerAngle(_perAngle, _bulletType, _dmg)){
            //aoeGun->autorelease();
        }
        else
        {
            delete aoeGun;
            aoeGun = nullptr;
        }
    }
    return aoeGun;
}

bool AOEGun::initAOEGunWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg)
{
    if (!Gun::initGun(_bulletType,_dmg)){
        return false;
    }
    
    this->perAngle = _perAngle;
    return true;
}

void AOEGun::fire()
{
    int count = 360 / perAngle;
    for (int i = 0; i < count; i++){
        Bullet::createBulletAtPos(globalVec2(), perAngle*i, 5, bulletType,dmg,(Layer*)this->getParent()->getParent());
    }
    
}

AOEGun* AOEGun::clone(){
    return createAOEGunWithShootingPerAngle(this->perAngle, bulletType, this->dmg);
}

//Implement class LazerGunAOE
//Implement class LazerGunAOE
//Implement class LazerGunAOE

LazerGunAOE* LazerGunAOE::createLazerGunAOEWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg)
{
    auto lazerGunAOE = new (std::nothrow) LazerGunAOE();
    
    if (lazerGunAOE){
        if (lazerGunAOE->initLazerGunAOEWithShootingPerAngle(_perAngle, _bulletType,_dmg)){
            //lazerGunAOE->autorelease();
        }
        else{
            delete lazerGunAOE;
            lazerGunAOE = nullptr;
        }
    }
    return lazerGunAOE;
}

bool LazerGunAOE::initLazerGunAOEWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg)
{
    if (!Gun::initGun(_bulletType,_dmg)){
        return false;
    }
    this->perAngle = _perAngle;
    return true;
}

void LazerGunAOE::fire(Vec2 _attackerPos, float _angle)
{
    int count = 360 / perAngle;
    for (int i = 0; i < count; i++){
        Bullet::createBulletAtPos(_attackerPos, perAngle*i + _angle, 5, bulletType,dmg);
    }
}

//Implement class GunHarasBot
//Implement class GunHarasBot
//Implement class GunHarasBot

GunHarasBot* GunHarasBot::createGunHarasBotWithNDirection(int _count, float _addAngle, BulletType _bulletType, float _dmg)
{
    auto gunHarasBot = new (std::nothrow) GunHarasBot();
    
    if (gunHarasBot){
        if (gunHarasBot->initGunHarasBotWithNDirection(_count, _addAngle,_bulletType,_dmg)){
            //gunHarasBot->autorelease();
        }
        else{
            delete gunHarasBot;
            gunHarasBot = nullptr;
        }
    }
    return gunHarasBot;
}

bool GunHarasBot::initGunHarasBotWithNDirection(int _count, float _addAngle, BulletType _bulletType, float _dmg)
{
    if (!Gun::initGun(_bulletType,_dmg)){
        return false;
    }
    
    this->count = _count;
    this->addAngle = _addAngle;
    return true;
}

void GunHarasBot::fire()
{
    for (int i = 0; i < this->count; i++) {
        if (i == 0){
            Bullet::createBulletAtPos(globalVec2(), 270, 2, bulletType,dmg,(Layer*)this->getParent()->getParent());
        }
        else if (i % 2 == 0){
            Bullet::createBulletAtPos(globalVec2(), 270+(i-1)*addAngle, 2, bulletType,dmg,(Layer*)this->getParent()->getParent());
        }
        else{
            Bullet::createBulletAtPos(globalVec2(), 270 + i*-addAngle, 2, bulletType,dmg,(Layer*)this->getParent()->getParent());
        }
    }
}

/////////////////////////////////////////////////////


GunLightning* GunLightning::createGunLightning(int _countDirection, float _dmg){
    auto gunLightning = new (std::nothrow) GunLightning();
    
    if (gunLightning){
        if (gunLightning->initGunLightning(_countDirection,_dmg,LightningColor::Lightning_BLUE)){;
            //gunHarasBot->autorelease();
        }
        else{
            delete gunLightning;
            gunLightning = nullptr;
        }
    }
    return gunLightning;
}

GunLightning* GunLightning::createGunLightning(int _countDirection, float _dmg, int _color){
    auto gunLightning = new (std::nothrow) GunLightning();
    
    if (gunLightning){
        if (gunLightning->initGunLightning(_countDirection,_dmg,(LightningColor)_color)){;
            //gunHarasBot->autorelease();
        }
        else{
            delete gunLightning;
            gunLightning = nullptr;
        }
    }
    return gunLightning;
}


bool GunLightning::initGunLightning(int _countDirection, float _dmg, LightningColor _color){
    if (!Gun::initGun(BulletType::LIGHTNING,_dmg)){
        return false;
    }
    
    this->countDirection = _countDirection;
    this->color = _color;
    
    this->onDamageEnemy= NULL;
    return true;
    
    
}


void GunLightning::activeDOT(){
    this->schedule(schedule_selector(GunLightning::updateDamageOverTime), 0.03);
    auto func = CallFuncN::create([&](Node* pSender){
        this->unschedule(schedule_selector(GunLightning::updateDamageOverTime));
        //if (COCOS2D_DEBUG)
            CCLOG("[GunLightning] - stop");
    });
    
    this->runAction(Sequence::create(DelayTime::create(2),func, NULL));
}


void GunLightning::fire(){
    float angle = random((float)0, (float)360);
    
    for(int i = 0;i < this->countDirection;i++){
        auto lightningSprite = LightningSprite::CreateLightningEffect(this, angle, angle + 15, 2,LightningClass::forEnemy,this->dmg,this->color);
         angle+= (360/countDirection);
        if (lightningSprite == NULL) continue;
        this->vecLightningBullet.pushBack(lightningSprite);
       
    }
    
    this->activeDOT();
}

void GunLightning::addUnit(cocos2d::Sprite *unit){
    this->arrUnit.pushBack(unit);
    //if (COCOS2D_DEBUG)
        CCLOG("[GunLightning] - Add unit: %d",(int)arrUnit.size());
}

void GunLightning::removeUnit(cocos2d::Sprite *unit){
    this->arrUnit.eraseObject(unit);
    //if (COCOS2D_DEBUG)
        CCLOG("[GunLightning] - Remove unit: %d",(int)arrUnit.size());
}

void GunLightning::updateDamageOverTime(float dt){
    if (onDamageEnemy != NULL){
        for(auto enemy : arrUnit){
            this->onDamageEnemy(enemy,this->dmg);
            //if (COCOS2D_DEBUG)
                CCLOG("[GunLightning] - Damage unit: %d",this->dmg);

        }
    }
}

GunLightning* GunLightning::clone(){
    auto gun = createGunLightning(this->countDirection,this->dmg,(int)this->color);
    gun->onDamageEnemy = this->onDamageEnemy;
    return gun;
}

void GunLightning::clear(){
    arrUnit.clear();
}
