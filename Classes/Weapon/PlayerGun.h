#ifndef _PLAYER_GUN_H_
#define _PLAYER_GUN_H_

#include "cocos2d.h"
#include "PlayerBullet.h"
#include <SimpleAudioEngine.h>
USING_NS_CC;

class PlayerGun : public Ref
{
public:
    float dmg, speed, upgrade;
    float attackCooldown, elapseCooldown;
    
    void initPlayerGun(float _dmg,int _speed,int _upgrade,float _attackCooldown);
    virtual void fire(Vec2 _pos);
    virtual void fire(Vec2 _pos, Sprite* _target);
};

class GunTop : public PlayerGun{
public:
    GunTop(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunTopLeft : public PlayerGun{
public:
    GunTopLeft(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunTopRight : public PlayerGun{
public:
    GunTopRight(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunBot : public PlayerGun{
public:
    GunBot(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunBotLeft : public PlayerGun{
public:
    GunBotLeft(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunBotRight : public PlayerGun{
public:
    GunBotRight(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunRight : public PlayerGun{
public:
    GunRight(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};

class GunLeft : public PlayerGun{
public:
    GunLeft(float _dmg, int _speed, int _upgrade);
    void fire(Vec2 _pos);
};
#endif
