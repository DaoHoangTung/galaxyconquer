#include "PlayerBullet.h"
using namespace cocos2d::ui;


static Layer* missionLayer;
static int sizeW, sizeH;

void PlayerBullet::loadResource(Layer* _layer){
    missionLayer = _layer;
    sizeW = Director::getInstance()->getWinSize().width;
    sizeH = Director::getInstance()->getWinSize().height;
}

bool PlayerBullet::initPlayerBullet(float _dmg, int _speed)
{
    if (!Sprite::init()){
        return false;
    }
    
    this->dmg = _dmg;
    this->speed = _speed;
//    this->isRecycle = false;
    return true;
}

void PlayerBullet::start()
{
    
}

void PlayerBullet::recycle()
{
    
}

void PlayerBullet::clear()
{
    
}

void PlayerBullet::clearAllBullet()
{
    LightBullet::clearLightBullet();
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

static Vector<LightBullet*> arrLightBullet[8][3];
static Vector<LightBullet*> arrLightBulletMoving;
static bool lightBulletToFrameCache = false;
#define _LIGHT_BULLET_SPEED_DEFAULT_ 15


void LightBullet::createLightBulletAtPos(Vec2 _pos, PlayerLightBullet _type, int _level,float _dmg)
{
    auto w = arrLightBullet[(int)_type][_level-1];
    
    if (arrLightBullet[(int)_type][_level - 1].size() > 0){
        auto bullet = arrLightBullet[(int)_type][_level - 1].at(0);
        
        bullet->dmg = _dmg;
        bullet->setPosition(_pos);
        bullet->start();
        
        arrLightBulletMoving.pushBack(bullet);
        arrLightBullet[(int)_type][_level - 1].eraseObject(bullet);
    }
}


void LightBullet::loadLightBullet()
{
    if (!lightBulletToFrameCache){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Bullet/TopBullet.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Bullet/RightBullet.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Bullet/BotRightBullet.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Bullet/TopRightBullet.plist");
        
        for (int i = 0; i < 100; i++){
            arrLightBullet[0][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Top, 1));
            arrLightBullet[1][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::TopRight, 1));
            arrLightBullet[2][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Right, 1));
            arrLightBullet[3][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::BotRight, 1));
            arrLightBullet[4][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Bot, 1));
            arrLightBullet[5][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::BotLeft, 1));
            arrLightBullet[6][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Left, 1));
            arrLightBullet[7][0].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::TopLeft, 1));
            
            arrLightBullet[0][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Top, 2));
            arrLightBullet[1][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::TopRight, 2));
            arrLightBullet[2][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Right, 2));
            arrLightBullet[3][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::BotRight, 2));
            arrLightBullet[4][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Bot, 2));
            arrLightBullet[5][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::BotLeft, 2));
            arrLightBullet[6][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Left, 2));
            arrLightBullet[7][1].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::TopLeft, 2));
            
            arrLightBullet[0][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Top, 3));
            arrLightBullet[1][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::TopRight, 3));
            arrLightBullet[2][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Right, 3));
            arrLightBullet[3][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::BotRight, 3));
            arrLightBullet[4][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Bot, 3));
            arrLightBullet[5][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::BotLeft, 3));
            arrLightBullet[6][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::Left, 3));
            arrLightBullet[7][2].pushBack(LightBullet::create(0, _LIGHT_BULLET_SPEED_DEFAULT_, PlayerLightBullet::TopLeft, 3));
        }
        lightBulletToFrameCache = true;
    }
    
}


LightBullet* LightBullet::create(float _dmg, int _speed, PlayerLightBullet _typeLightBullet, int _level)
{
    LightBullet* bullet = new (std::nothrow) LightBullet();
    if (bullet){
        if (bullet->initLightBullet(_dmg, _speed,_typeLightBullet,_level)){
            bullet->autorelease();
        }
        else{
            CC_SAFE_DELETE(bullet);
        }
    }
    
    return bullet;
}

bool LightBullet::initLightBullet(float _dmg, int _speed, PlayerLightBullet _typeLightBullet, int _level)
{
    if (!PlayerBullet::initPlayerBullet(_dmg, _speed)){
        return false;
    }
    
    this->level = _level;
    this->typeLightBullet = _typeLightBullet;
    this->bulletType = PlayerBulletType::Light;
    
    
    __String name;
    
    switch (_typeLightBullet)
    {
        case PlayerLightBullet::TopLeft:
            name = "BotRightBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(135)), speed*sin(CC_DEGREES_TO_RADIANS(135)));
            break;
        case PlayerLightBullet::Top:
            name = "TopBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(90)), speed*sin(CC_DEGREES_TO_RADIANS(90)));
            break;
        case PlayerLightBullet::TopRight:
            name = "TopRightBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(45)), speed*sin(CC_DEGREES_TO_RADIANS(45)));
            break;
        case PlayerLightBullet::Right:
            name = "RightBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(0)), speed*sin(CC_DEGREES_TO_RADIANS(0)));
            break;
        case PlayerLightBullet::BotRight:
            name = "BotRightBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(-45)), speed*sin(CC_DEGREES_TO_RADIANS(-45)));
            break;
        case PlayerLightBullet::Bot:
            name = "TopBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(-90)), speed*sin(CC_DEGREES_TO_RADIANS(-90)));
            break;
        case PlayerLightBullet::BotLeft:
            name = "TopRightBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(-135)), speed*sin(CC_DEGREES_TO_RADIANS(-135)));
            break;
        case PlayerLightBullet::Left:
            name = "RightBullet";
            this->vectorMove = Vec2(speed*cos(CC_DEGREES_TO_RADIANS(180)), speed*sin(CC_DEGREES_TO_RADIANS(180)));
            break;
        default:
            break;
    }
    
    int fromIndex = 0, toIndex = 0;
    switch (_level)
    {
        case 1:
            fromIndex = 10;
            toIndex = 19;
            break;
        case 2:
            fromIndex = 20;
            toIndex = 29;
            break;
        case 3:
            fromIndex = 30;
            toIndex = 39;
            break;
        default:
            break;
    }
    
    Vector<SpriteFrame*> bulletFrame;
    for (int i = fromIndex; i < toIndex; i++){
        auto frameName = __String::createWithFormat("%s%d.png", name.getCString(), i);
        bulletFrame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName->getCString()));
    }
    
    auto animation = Animation::createWithSpriteFrames(bulletFrame, 0.02f);
    auto animate = Animate::create(animation);
    
    this->setSpriteFrame(bulletFrame.at(0));
    
    animateAct = RepeatForever::create(animate);
    animateAct->retain();
    
    
    body = PhysicsBody::createBox(Size(this->_contentSize.width, this->_contentSize.height), PhysicsMaterial(0.1f, 1.0f, 0.0f));
    body->setDynamic(true);
    body->setCategoryBitmask((int)PhysicCategory::Bullet);
    body->setCollisionBitmask((int)PhysicCategory::None);
    body->setContactTestBitmask((int)PhysicCategory::combo3);
    body->setEnabled(false);
    body->retain();
    
    this->setTag((int)PhysicCategory::Bullet);
    this->setVisible(false);
    
    isRecycle = true;
    bulletFrame.clear();
    
    
    return true;
}

void LightBullet::start()
{
    if (isRecycle){
    missionLayer->addChild(this,2);
    this->setVisible(true);
    this->runAction(this->animateAct);
        if (!body->isEnabled()){
            this->body->setEnabled(true);
            this->setPhysicsBody(this->body);
        }
    this->schedule(schedule_selector(LightBullet::move));
    
    isRecycle = false;
    }
}

void LightBullet::recycle()
{
    if (!isRecycle){
        this->removeFromParentAndCleanup(true);
        this->unschedule(schedule_selector(LightBullet::move));
        this->setVisible(false);
        this->stopAllActions();
        this->body->removeFromWorld();
        this->body->setEnabled(false);
        this->setPosition(Vec2(-150, -150));
        
        arrLightBullet[(int)this->typeLightBullet][this->level - 1].pushBack(this);
        
        isRecycle = true;
    }
    
}

void LightBullet::move(float dt)
{
    this->setPosition(this->getPosition() + vectorMove);
    float x = this->getPositionX();
    float y = this->getPositionY();
    if (x < 0 || x > sizeW || y < 0 || y > (sizeH-75)){
        this->recycle();
        arrLightBulletMoving.eraseObject(this);
    }
}

void LightBullet::clear()
{
    if (arrLightBulletMoving.contains(this)){
        this->removeFromParentAndCleanup(true);
        this->unschedule(schedule_selector(LightBullet::move));
        this->setVisible(false);
        this->stopAllActions();
        this->body->removeFromWorld();
        this->setPosition(Vec2(-150, -150));
    }
    
    CC_SAFE_RELEASE_NULL(this->body);
    CC_SAFE_RELEASE_NULL(this->animateAct);
}

void LightBullet::clearLightBullet()
{
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 8; j++){
            for (auto bullet : arrLightBullet[j][i]){
                bullet->clear();
            }
            arrLightBullet[j][i].clear();
        }
    }
    
    for (auto bullet : arrLightBulletMoving){
        bullet->clear();
    }
    
    arrLightBulletMoving.clear();
}

void LightBullet::recycleAllLightBullet(){
    for (auto bullet : arrLightBulletMoving){
        bullet->recycle();
    }
    
    arrLightBulletMoving.clear();
}
