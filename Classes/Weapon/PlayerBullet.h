#ifndef _PLAYER_BULLET_H__
#define _PLAYER_BULLET_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "EnumManager.h"
#include "ResourcesNew.h"

USING_NS_CC;

class PlayerBullet :
public Sprite
{
public:
    PhysicsBody* body;
    RepeatForever* animateAct;
    PlayerBulletType bulletType;
    
    Vec2 vectorMove;
    float dmg, speed;
    
    bool isRecycle;
    
    static void loadResource(Layer* _layer);
    static void clearAllBullet();
    bool initPlayerBullet(float _dmg,int _speed);
    virtual void start();
    virtual void recycle();
    virtual void clear();
    
    
};

class LightBullet : public PlayerBullet{
public:
    int level;
    PlayerLightBullet typeLightBullet;
    
    static void loadLightBullet();
    static void clearLightBullet();
    static void recycleAllLightBullet();
    static void createLightBulletAtPos(Vec2 _pos,PlayerLightBullet _type,int _level, float _dmg);
    static LightBullet* create(float _dmg, int _speed, PlayerLightBullet _typeLightBullet,int _level);
    bool initLightBullet(float _dmg, int _speed, PlayerLightBullet _typeLightBullet, int _level);
    void start();
    void recycle();
    void move(float dt);
    void clear();
    
    void setRocketFacing(float _angle);
};
#endif
