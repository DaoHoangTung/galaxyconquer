#ifndef _GUN_H_
#define  _GUN_H_

#include "cocos2d.h"
#include "EnumManager.h"
#include "LightningSprite.hpp"
#include "Bullet.h"
USING_NS_CC;

class Gun : public Node
{
public:
    Vec2 attackerPos;
    Vector<LightningSprite*> vecLightningBullet;
    BulletType bulletType;
    
    float angle;
    
    int dmg;
    
    static Gun* createGun(BulletType _bulletType,float _dmg);
    
    bool initGun(BulletType _bulletType, float _dmg);
    
    Vec2 globalVec2();
    
    virtual void fire();
    virtual void fire(float angle);
    virtual void fire(Vec2 _attackerPos);
    virtual void fire(Sprite* _caster);
    virtual void fire(Vec2 _attackerPos, float angle);

    virtual void addUnit(Sprite* unit);
    virtual void removeUnit(Sprite* unit);
    virtual Gun* clone();
    virtual void clear();
};




class NormalGun : public Gun
{
public:
    float addAngle;
    
    static NormalGun* createNormalGunWithShootingPerAngle(float _addAngle, BulletType _bulletType,float _dmg);
    bool initNormalGunWithShootingPerAngle(float _addAngle, BulletType _bulletType, float _dmg);
    
    void fire(float _angle);
    void fire();

    NormalGun* clone();
};

class AOEGun : public Gun
{
public:
    float perAngle;
    
    static AOEGun* createAOEGunWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg);
    bool initAOEGunWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg);
    
    void fire();
    AOEGun* clone();
    
};

class LazerGunAOE : public AOEGun
{
public:
    static LazerGunAOE*     createLazerGunAOEWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg);
    bool initLazerGunAOEWithShootingPerAngle(float _perAngle, BulletType _bulletType, float _dmg);
    
    void fire(Vec2 _attackerPos, float _angle);
};

class GunHarasBot : public Gun
{
public:
    int count;
    float addAngle;
    
    static GunHarasBot* createGunHarasBotWithNDirection(int _count, float _addAngle,BulletType _bulletType, float _dmg);
    bool initGunHarasBotWithNDirection(int _count, float _addAngle, BulletType _bulletType, float _dmg);
    
    void fire();
};

class GunLightning : public Gun{
public:
    typedef std::function<void(Sprite* enemy,float _dmg)> ccOnDamageEnemy;
    
    ccOnDamageEnemy onDamageEnemy;
    
    int countDirection;
    LightningColor color;

    static GunLightning* createGunLightning(int _countDirection, float _dmg);
    static GunLightning* createGunLightning(int _countDirection, float _dmg, int _color);

    
    bool initGunLightning(int _countDirection, float _dmg, LightningColor _color);
    virtual void fire();
    void activeDOT();

    GunLightning* clone();
    
    void addUnit(Sprite* unit);
    void removeUnit(Sprite* unit);
    void clear();
private:
    Vector<Sprite*> arrUnit;
    
    void updateDamageOverTime(float dt);
};
#endif
