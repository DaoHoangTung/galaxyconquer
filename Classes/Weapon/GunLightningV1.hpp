
#ifndef GunLightningV1_hpp
#define GunLightningV1_hpp

#include "cocos2d.h"
#include "Gun.h"
#include "EnumManager.h"
USING_NS_CC;


class GunLightningV1 : public GunLightning{
public:
    static GunLightningV1* createGunLightningV1(int _count, int _dmg, Direction _direct);
    bool initGunLightningV1(int _countDirection, int _dmg, Direction _direct);
    
    void fire() override;
    
    GunLightningV1* clone() override;
    
    static void updateSeed();
    
private:
    float angle;
    Direction direct;
};

#endif /* GunLightningV1_hpp */
