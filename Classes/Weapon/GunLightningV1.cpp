
#include "GunLightningV1.hpp"


static float SEED_1 = 0;
static float SEED_2 = 0;

void GunLightningV1::updateSeed(){
    SEED_1 = random(-30, 30);
    SEED_2 = random(-90, 90);
}

GunLightningV1* GunLightningV1::createGunLightningV1(int _count, int _dmg, Direction _direct){
    auto gunLightning = new (std::nothrow) GunLightningV1();
    
    if (gunLightning){
        if (gunLightning->initGunLightningV1(_count,_dmg, _direct)){;
            //gunHarasBot->autorelease();
        }
        else{
            delete gunLightning;
            gunLightning = nullptr;
        }
    }
    return gunLightning;
}

bool GunLightningV1::initGunLightningV1(int _countDirection, int _dmg, Direction _direct){
    if (!Gun::initGun(BulletType::LIGHTNING,_dmg)){
        return false;
    }
    
    this->countDirection = _countDirection;
    this->direct = _direct;
    this->onDamageEnemy= NULL;
    return true;
}


void GunLightningV1::fire(){
    switch (direct) {
        case D_RIGHT:
            angle = 90;
            break;
            
        case D_TOP:
            angle = 0;
            break;
            
        case D_BOTTOM:
            angle = 180;
            break;
            
            case D_LEFT:
            angle = 270;
            break;
        default:
            break;
    }
    
    auto rand1 = random(10, 30);
    auto rand2 = random(-60, 60);
    this->vecLightningBullet.pushBack(LightningSprite::CreateLightningEffect(this, angle+SEED_1, angle + SEED_2, 2,LightningClass::forEnemy,this->dmg));
    
    this->vecLightningBullet.pushBack(LightningSprite::CreateLightningEffect(this, angle-SEED_1, angle -SEED_2, 2,LightningClass::forEnemy,this->dmg));
    
    this->activeDOT();
}

GunLightningV1* GunLightningV1::clone(){
    auto gun = createGunLightningV1(this->countDirection,this->dmg,this->direct);
    gun->onDamageEnemy = this->onDamageEnemy;
    return gun;
}

