#include "Mission.h"
#include "Boss.hpp"
bool Mission::initLevel()
{
    if (!Layer::init()){
        return false;
    }
    
    this->currentPriorityWave = -1;

    return true;
}

void Mission::start()
{
    this->scheduleUpdate();
}

void Mission::RestartLeaderboardPropperties(){
    auto global = GLOBAL::getInstance();
    global->totalEnemy = getTotalEnemy();
    global->totalWave = (int)arrWave.size();
    global->totalGameSec = 0;
    global->totalEnemyKilled = 0;
    global->totalWaveDestroyed = 0;
    global->isWin = false;
}

void Mission::UpdateLeaderboardPropperties(){
    auto global = GLOBAL::getInstance();
    global->totalEnemy += getTotalEnemy();
    global->totalWave += (int)arrWave.size();
}

void Mission::stop()
{
    this->unscheduleUpdate();
    
    
    
    arrWaveRunning.clear();
    arrWaveDidFinish.clear();
    
    this->currentPriorityWave = -1;
}

void Mission::clear()
{
    
//    Boss::clearCurrentBoss();
    
    this->unscheduleUpdate();
    
    for (auto wave : arrWaveRunning){
        wave->clear();
        wave = nullptr;
    }
    
    for (auto wave : arrWave){
        wave->clear();
        wave = nullptr;
    }
    
    arrWaveRunning.clear();
    arrWave.clear();
    arrWaveDidFinish.clear();
    
    this->removeAllChildren();
    this->removeFromParentAndCleanup(true);
    
}

void Mission::update(float delta)
{
    Vector<Wave*> arrTemp;
    
    if (arrWaveRunning.size() <= 0){
        this->currentPriorityWave++;
        if (arrWave.size() > 0){
            for (auto wave : arrWave){
                if (wave->getPriority() == this->currentPriorityWave){
                    wave->start();
                    
                    this->arrWaveRunning.pushBack(wave);
                    arrTemp.pushBack(wave);
                }
                
            }
        }
        else{
            if (GLOBAL::GET_CURRENT_MISSION_INDEX() == GLOBAL::GET_DEVIL_SQUARE_MAP_INDEX()){ //14 is Devil Square Map
            }else{
                auto boss = Boss::getBossAtTag(GLOBAL::GET_CURRENT_MISSION_INDEX());
                boss->setName("Boss");
                this->addChild(boss);
                boss->start();
                Boss::setCurrentBoss(boss);
                if(onBossAppearCallback != NULL){
                    this->onBossAppearCallback();
                }
            }
            this->unscheduleUpdate();

        }
    }
    
    if (arrTemp.size() > 0){
        for (auto wave : arrTemp){
            this->arrWave.eraseObject(wave, false);
        }
        arrTemp.clear();
    }
    
    if (this->arrWaveRunning.size() > 0){
        for (auto wave : this->arrWaveRunning){
            if (wave->getIsDone()){
                this->arrWaveDidFinish.pushBack(wave);
                arrTemp.pushBack(wave);
                        
                CCLOG("Unit alive: %d",wave->countRemainUnitAlive);
            }
        }
    }
    
    if (arrTemp.size() > 0){
        for (auto wave : arrTemp){
            this->arrWaveRunning.eraseObject(wave, false);
        }
        arrTemp.clear();
    }
    
    if (this->arrWaveDidFinish.size() > 0){
        for (auto wave : this->arrWaveDidFinish){
            wave->clear();
        }
        arrWaveDidFinish.clear();
    }
    
    
}


int Mission::getTotalEnemy(){
    int totalEnemy = 0;
    for(Wave* wave : arrWave){
        totalEnemy+= wave->getTotalUnit();
    }
    
    return totalEnemy;
}





