#ifndef _LEVEL_H_
#define _LEVEL_H_

#include "cocos2d.h"
#include "Wave.h"
#include "Path.h"
#include "Explosion.h"
USING_NS_CC;

class Mission : public Layer
{
public:
    Vector<Wave*> arrWave, arrWaveRunning, arrWaveDidFinish;
    
    int currentPriorityWave;
    
    bool initLevel();
    void start();
    void stop();
    void clear();
    void RestartLeaderboardPropperties();
    void UpdateLeaderboardPropperties();
    void update(float delta);
    
    typedef std::function<void()> ccBossAppearCallback;
    ccBossAppearCallback onBossAppearCallback;
    
    CREATE_FUNC(Mission);

protected:
private:
    int getTotalEnemy();
};

#endif
