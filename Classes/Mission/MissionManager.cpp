
#include "MissionManager.hpp"
#include "Path.h"
#include "Wave.h"

static MissionManager* instance = NULL;

MissionManager* MissionManager::GetInstance()
{
    if (instance == NULL)
    {
        instance = new (std::nothrow) MissionManager();
        instance->retain();
    }
    
    return instance;
}


Mission* MissionManager::GetMission(int index)
{
    Mission* mission = NULL;
    
    switch (index) {
        case 1:
            mission = GetInstance()->Map1();
            break;
            
        default:
            break;
    }
    
    return mission;
}

Mission* MissionManager::Map1()
{
    Mission* mission = Mission::create();
    
   Path* path1 = PathReturn::createAt_Y(0.60, 0.80, 50.00, 320.00, (PathDirection)1);
    auto gun1 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave1 = NonAnimateWave::createNonAnimateWave("FleaWing", 20.00, 10.00, gun1, 2.00, 0.00, 1.00, path1, 200.00, 0.40);
    mission->arrWave.pushBack(wave1);
    Path* path2 = PathReturn::createAt_Y(0.40, 0.20, 50.00, 320.00, (PathDirection)1);
    auto gun2 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)5,0);
    auto wave2 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun2, 2.00, 3.00, 1.00, path2, 200.00, 0.40);
    mission->arrWave.pushBack(wave2);
    Path* path3 = PathReturn::createAt_Y(0.80, 0.60, 50.00, 320.00, (PathDirection)-1);
    auto gun3 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave3 = NonAnimateWave::createNonAnimateWave("FleaWing", 20.00, 10.00, gun3, 2.00, 6.00, 1.00, path3, 200.00, 0.40);
    mission->arrWave.pushBack(wave3);
    Path* path4 = PathReturn::createAt_Y(0.40, 0.20, 50.00, 320.00, (PathDirection)-1);
    auto gun4 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)5,0);
    auto wave4 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun4, 2.00, 9.00, 1.00, path4, 200.00, 0.40);
    mission->arrWave.pushBack(wave4);
    Path* path5 = PathAcross::createAt_Y(0.80, 0.80, (PathDirection)1);
    auto gun5 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave5 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun5, 2.00, 0.00, 2.00, path5, 200.00, 0.40);
    mission->arrWave.pushBack(wave5);
    Path* path6 = PathAcross::createAt_Y(0.60, 0.60, (PathDirection)-1);
    auto gun6 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)5,0);
    auto wave6 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun6, 2.00, 2.00, 2.00, path6, 200.00, 0.40);
    mission->arrWave.pushBack(wave6);
    Path* path7 = PathAcross::createAt_Y(0.40, 0.40, (PathDirection)1);
    auto gun7 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave7 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun7, 2.00, 4.00, 2.00, path7, 200.00, 0.40);
    mission->arrWave.pushBack(wave7);
    Path* path8 = PathAcross::createAt_Y(0.20, 0.20, (PathDirection)-1);
    auto gun8 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)5,0);
    auto wave8 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun8, 2.00, 6.00, 2.00, path8, 200.00, 0.40);
    mission->arrWave.pushBack(wave8);
    Path* path9 = PathZicZac::createAt_X_With_Height(0.20, 50.00, 10.00, (PathDirection)-1);
    auto gun9 = NormalGun::createNormalGunWithShootingPerAngle(15.00,(BulletType)1,0);
    auto wave9 = AnimateWave::createAnimateWave("TennisOrb", 20.00, 10.00, gun9, 2.00, 0.00, 3.00, path9, 50.00, 0.40);
    mission->arrWave.pushBack(wave9);
    Path* path10 = PathZicZac::createAt_X_With_Height(0.80, -50.00, 10.00, (PathDirection)-1);
    auto gun10 = NormalGun::createNormalGunWithShootingPerAngle(15.00,(BulletType)1,0);
    auto wave10 = AnimateWave::createAnimateWave("TennisOrb", 20.00, 10.00, gun10, 2.00, 0.00, 3.00, path10, 50.00, 0.40);
    mission->arrWave.pushBack(wave10);
    Path* path11 = PathAcross::createAt_X(0.50, 0.50, (PathDirection)-1);
    auto gun11 = GunLightning::createGunLightning(1,2.50,0);
    auto wave11 = SpecialWave::createSpecialWave("EnergiserEA", 200.00, 1.00, gun11, 3.00, 0.00, 3.00, path11, 20.00, 0.40, 5.00, (TypeCoin)0);
    mission->arrWave.pushBack(wave11);
    Path* path12 = PathParabon::createAt_Y_With_Height(0.50, 100.00, (PathDirection)1);
    auto gun12 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave12 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun12, 2.00, 3.00, 3.00, path12, 200.00, 0.40);
    mission->arrWave.pushBack(wave12);
    Path* path13 = PathParabon::createAt_Y_With_Height(0.40, 100.00, (PathDirection)-1);
    auto gun13 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave13 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun13, 2.00, 3.00, 3.00, path13, 200.00, 0.40);
    mission->arrWave.pushBack(wave13);
    Path* path14 = PathParabon::createAt_Y_With_Height(0.30, 100.00, (PathDirection)-1);
    auto gun14 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave14 = NonAnimateWave::createNonAnimateWave("FleaWing", 20.00, 10.00, gun14, 2.00, 3.00, 3.00, path14, 200.00, 0.40);
    mission->arrWave.pushBack(wave14);
    Path* path15 = PathParabon::createAt_Y_With_Height(0.20, 100.00, (PathDirection)1);
    auto gun15 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave15 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun15, 2.00, 3.00, 3.00, path15, 200.00, 0.40);
    mission->arrWave.pushBack(wave15);
    Path* path16 = PathReturn::createAt_Y(0.30, 0.70, 100.00, 270.00, (PathDirection)1);
    auto gun16 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave16 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun16, 2.00, 0.00, 4.00, path16, 150.00, 0.70);
    mission->arrWave.pushBack(wave16);
    Path* path17 = PathReturn::createAt_Y(0.40, 0.60, 50.00, 250.00, (PathDirection)1);
    auto gun17 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave17 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun17, 2.00, 0.00, 4.00, path17, 150.00, 0.70);
    mission->arrWave.pushBack(wave17);
    Path* path18 = PathReturn::createAt_Y(0.30, 0.70, 100.00, 270.00, (PathDirection)-1);
    auto gun18 = NormalGun::createNormalGunWithShootingPerAngle(30.00,(BulletType)1,0);
    auto wave18 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun18, 2.00, 0.00, 4.00, path18, 150.00, 0.70);
    mission->arrWave.pushBack(wave18);
    Path* path19 = PathReturn::createAt_Y(0.40, 0.60, 50.00, 250.00, (PathDirection)-1);
    auto gun19 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave19 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun19, 2.00, 0.00, 4.00, path19, 150.00, 0.70);
    mission->arrWave.pushBack(wave19);

    Path* path20 = new (std::nothrow) Path();
    path20->arrVector.pushBack(new KKPoint(Vec2(-32.85,733.74)));
    path20->arrVector.pushBack(new KKPoint(Vec2(-5.98,748.07)));
    path20->arrVector.pushBack(new KKPoint(Vec2(31.52,776.67)));
    path20->arrVector.pushBack(new KKPoint(Vec2(63.91,811.66)));
    path20->arrVector.pushBack(new KKPoint(Vec2(129.27,841.55)));
    path20->arrVector.pushBack(new KKPoint(Vec2(182.99,864.59)));
    path20->arrVector.pushBack(new KKPoint(Vec2(261.31,882.90)));
    path20->arrVector.pushBack(new KKPoint(Vec2(376.58,889.37)));
    path20->arrVector.pushBack(new KKPoint(Vec2(468.25,884.33)));
    path20->arrVector.pushBack(new KKPoint(Vec2(526.64,865.43)));
    path20->arrVector.pushBack(new KKPoint(Vec2(584.58,823.58)));
    path20->arrVector.pushBack(new KKPoint(Vec2(608.21,771.22)));
    path20->arrVector.pushBack(new KKPoint(Vec2(595.09,745.26)));
    path20->arrVector.pushBack(new KKPoint(Vec2(536.37,730.16)));
    path20->arrVector.pushBack(new KKPoint(Vec2(476.88,743.05)));
    path20->arrVector.pushBack(new KKPoint(Vec2(441.74,760.27)));
    path20->arrVector.pushBack(new KKPoint(Vec2(363.59,775.12)));
    path20->arrVector.pushBack(new KKPoint(Vec2(276.64,764.50)));
    path20->arrVector.pushBack(new KKPoint(Vec2(203.66,739.69)));
    path20->arrVector.pushBack(new KKPoint(Vec2(122.14,705.97)));
    path20->arrVector.pushBack(new KKPoint(Vec2(68.47,658.52)));
    path20->arrVector.pushBack(new KKPoint(Vec2(54.14,590.81)));
    path20->arrVector.pushBack(new KKPoint(Vec2(54.55,530.49)));
    path20->arrVector.pushBack(new KKPoint(Vec2(57.24,445.70)));
    path20->arrVector.pushBack(new KKPoint(Vec2(71.04,348.88)));
    path20->arrVector.pushBack(new KKPoint(Vec2(104.41,289.34)));
    path20->arrVector.pushBack(new KKPoint(Vec2(220.61,232.43)));
    path20->arrVector.pushBack(new KKPoint(Vec2(345.39,205.31)));
    path20->arrVector.pushBack(new KKPoint(Vec2(443.18,226.58)));
    path20->arrVector.pushBack(new KKPoint(Vec2(533.67,265.42)));
    path20->arrVector.pushBack(new KKPoint(Vec2(591.40,333.23)));
    path20->arrVector.pushBack(new KKPoint(Vec2(618.18,471.83)));
    path20->arrVector.pushBack(new KKPoint(Vec2(591.50,572.53)));
    path20->arrVector.pushBack(new KKPoint(Vec2(503.57,585.31)));
    path20->arrVector.pushBack(new KKPoint(Vec2(412.61,508.62)));
    path20->arrVector.pushBack(new KKPoint(Vec2(365.79,458.12)));
    path20->arrVector.pushBack(new KKPoint(Vec2(239.39,407.62)));
    path20->arrVector.pushBack(new KKPoint(Vec2(158.31,444.22)));
    path20->arrVector.pushBack(new KKPoint(Vec2(121.86,536.46)));
    path20->arrVector.pushBack(new KKPoint(Vec2(96.43,614.05)));
    path20->arrVector.pushBack(new KKPoint(Vec2(41.86,675.26)));
    path20->arrVector.pushBack(new KKPoint(Vec2(-44.11,690.74)));

    auto gun20 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave20 = NonAnimateWave::createNonAnimateWave("FleaTieB", 20.00, 10.00, gun20, 2.00, 13.00, 4.00, path20, 150.00, 0.70);
    mission->arrWave.pushBack(wave20);

    Path* path21 = new (std::nothrow) PathFreeClone();
    path21->arrVector.pushBack(new KKPoint(Vec2(-32.85,663.74)));
    path21->arrVector.pushBack(new KKPoint(Vec2(-5.98,678.07)));
    path21->arrVector.pushBack(new KKPoint(Vec2(31.52,706.67)));
    path21->arrVector.pushBack(new KKPoint(Vec2(63.91,741.66)));
    path21->arrVector.pushBack(new KKPoint(Vec2(129.27,771.55)));
    path21->arrVector.pushBack(new KKPoint(Vec2(182.99,794.59)));
    path21->arrVector.pushBack(new KKPoint(Vec2(261.31,812.90)));
    path21->arrVector.pushBack(new KKPoint(Vec2(376.58,819.37)));
    path21->arrVector.pushBack(new KKPoint(Vec2(468.25,814.33)));
    path21->arrVector.pushBack(new KKPoint(Vec2(526.64,795.43)));
    path21->arrVector.pushBack(new KKPoint(Vec2(584.58,753.58)));
    path21->arrVector.pushBack(new KKPoint(Vec2(608.21,701.22)));
    path21->arrVector.pushBack(new KKPoint(Vec2(595.09,675.26)));
    path21->arrVector.pushBack(new KKPoint(Vec2(536.37,660.16)));
    path21->arrVector.pushBack(new KKPoint(Vec2(476.88,673.05)));
    path21->arrVector.pushBack(new KKPoint(Vec2(441.74,690.27)));
    path21->arrVector.pushBack(new KKPoint(Vec2(363.59,705.12)));
    path21->arrVector.pushBack(new KKPoint(Vec2(276.64,694.50)));
    path21->arrVector.pushBack(new KKPoint(Vec2(203.66,669.69)));
    path21->arrVector.pushBack(new KKPoint(Vec2(122.14,635.97)));
    path21->arrVector.pushBack(new KKPoint(Vec2(68.47,588.52)));
    path21->arrVector.pushBack(new KKPoint(Vec2(54.14,520.81)));
    path21->arrVector.pushBack(new KKPoint(Vec2(54.55,460.49)));
    path21->arrVector.pushBack(new KKPoint(Vec2(57.24,375.70)));
    path21->arrVector.pushBack(new KKPoint(Vec2(71.04,278.88)));
    path21->arrVector.pushBack(new KKPoint(Vec2(104.41,219.34)));
    path21->arrVector.pushBack(new KKPoint(Vec2(220.61,162.43)));
    path21->arrVector.pushBack(new KKPoint(Vec2(345.39,135.31)));
    path21->arrVector.pushBack(new KKPoint(Vec2(443.18,156.58)));
    path21->arrVector.pushBack(new KKPoint(Vec2(533.67,195.42)));
    path21->arrVector.pushBack(new KKPoint(Vec2(591.40,263.23)));
    path21->arrVector.pushBack(new KKPoint(Vec2(618.18,401.83)));
    path21->arrVector.pushBack(new KKPoint(Vec2(591.50,502.53)));
    path21->arrVector.pushBack(new KKPoint(Vec2(503.57,515.31)));
    path21->arrVector.pushBack(new KKPoint(Vec2(412.61,438.62)));
    path21->arrVector.pushBack(new KKPoint(Vec2(365.79,388.12)));
    path21->arrVector.pushBack(new KKPoint(Vec2(239.39,337.62)));
    path21->arrVector.pushBack(new KKPoint(Vec2(158.31,374.22)));
    path21->arrVector.pushBack(new KKPoint(Vec2(121.86,466.46)));
    path21->arrVector.pushBack(new KKPoint(Vec2(96.43,544.05)));
    path21->arrVector.pushBack(new KKPoint(Vec2(41.86,605.26)));
    path21->arrVector.pushBack(new KKPoint(Vec2(-44.11,620.74)));

    auto gun21 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave21 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun21, 2.00, 13.00, 4.00, path21, 150.00, 0.70);
    mission->arrWave.pushBack(wave21);

    Path* path22 = new (std::nothrow) Path();
    path22->arrVector.pushBack(new KKPoint(Vec2(659.89,208.24)));
    path22->arrVector.pushBack(new KKPoint(Vec2(619.59,171.51)));
    path22->arrVector.pushBack(new KKPoint(Vec2(553.37,132.21)));
    path22->arrVector.pushBack(new KKPoint(Vec2(479.95,87.08)));
    path22->arrVector.pushBack(new KKPoint(Vec2(377.96,59.47)));
    path22->arrVector.pushBack(new KKPoint(Vec2(321.46,60.32)));
    path22->arrVector.pushBack(new KKPoint(Vec2(219.13,42.60)));
    path22->arrVector.pushBack(new KKPoint(Vec2(121.08,51.00)));
    path22->arrVector.pushBack(new KKPoint(Vec2(37.08,79.14)));
    path22->arrVector.pushBack(new KKPoint(Vec2(-21.71,149.99)));
    path22->arrVector.pushBack(new KKPoint(Vec2(-3.67,209.02)));
    path22->arrVector.pushBack(new KKPoint(Vec2(45.04,194.38)));
    path22->arrVector.pushBack(new KKPoint(Vec2(111.50,162.73)));
    path22->arrVector.pushBack(new KKPoint(Vec2(203.21,136.51)));
    path22->arrVector.pushBack(new KKPoint(Vec2(283.52,137.37)));
    path22->arrVector.pushBack(new KKPoint(Vec2(428.28,194.20)));
    path22->arrVector.pushBack(new KKPoint(Vec2(518.60,240.22)));
    path22->arrVector.pushBack(new KKPoint(Vec2(585.19,294.34)));
    path22->arrVector.pushBack(new KKPoint(Vec2(511.97,324.20)));
    path22->arrVector.pushBack(new KKPoint(Vec2(406.20,295.18)));
    path22->arrVector.pushBack(new KKPoint(Vec2(326.49,256.68)));
    path22->arrVector.pushBack(new KKPoint(Vec2(191.63,264.65)));
    path22->arrVector.pushBack(new KKPoint(Vec2(89.95,326.81)));
    path22->arrVector.pushBack(new KKPoint(Vec2(75.73,392.79)));
    path22->arrVector.pushBack(new KKPoint(Vec2(67.55,469.16)));
    path22->arrVector.pushBack(new KKPoint(Vec2(52.22,590.08)));
    path22->arrVector.pushBack(new KKPoint(Vec2(77.41,711.82)));
    path22->arrVector.pushBack(new KKPoint(Vec2(93.52,801.20)));
    path22->arrVector.pushBack(new KKPoint(Vec2(156.60,867.39)));
    path22->arrVector.pushBack(new KKPoint(Vec2(299.31,915.66)));
    path22->arrVector.pushBack(new KKPoint(Vec2(449.04,925.61)));
    path22->arrVector.pushBack(new KKPoint(Vec2(550.47,877.20)));
    path22->arrVector.pushBack(new KKPoint(Vec2(596.76,765.68)));
    path22->arrVector.pushBack(new KKPoint(Vec2(675.67,686.16)));

    auto gun22 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave22 = NonAnimateWave::createNonAnimateWave("FleaWing", 20.00, 10.00, gun22, 2.00, 13.00, 4.00, path22, 150.00, 0.70);
    mission->arrWave.pushBack(wave22);

    Path* path23 = new (std::nothrow) PathFreeClone();
    path23->arrVector.pushBack(new KKPoint(Vec2(659.89,278.24)));
    path23->arrVector.pushBack(new KKPoint(Vec2(619.59,241.51)));
    path23->arrVector.pushBack(new KKPoint(Vec2(553.37,202.21)));
    path23->arrVector.pushBack(new KKPoint(Vec2(479.95,157.08)));
    path23->arrVector.pushBack(new KKPoint(Vec2(377.96,129.47)));
    path23->arrVector.pushBack(new KKPoint(Vec2(321.46,130.32)));
    path23->arrVector.pushBack(new KKPoint(Vec2(219.13,112.60)));
    path23->arrVector.pushBack(new KKPoint(Vec2(121.08,121.00)));
    path23->arrVector.pushBack(new KKPoint(Vec2(37.08,149.14)));
    path23->arrVector.pushBack(new KKPoint(Vec2(-21.71,219.99)));
    path23->arrVector.pushBack(new KKPoint(Vec2(-3.67,279.02)));
    path23->arrVector.pushBack(new KKPoint(Vec2(45.04,264.38)));
    path23->arrVector.pushBack(new KKPoint(Vec2(111.50,232.73)));
    path23->arrVector.pushBack(new KKPoint(Vec2(203.21,206.51)));
    path23->arrVector.pushBack(new KKPoint(Vec2(283.52,207.37)));
    path23->arrVector.pushBack(new KKPoint(Vec2(428.28,264.20)));
    path23->arrVector.pushBack(new KKPoint(Vec2(518.60,310.22)));
    path23->arrVector.pushBack(new KKPoint(Vec2(585.19,364.34)));
    path23->arrVector.pushBack(new KKPoint(Vec2(511.97,394.20)));
    path23->arrVector.pushBack(new KKPoint(Vec2(406.20,365.18)));
    path23->arrVector.pushBack(new KKPoint(Vec2(326.49,326.68)));
    path23->arrVector.pushBack(new KKPoint(Vec2(191.63,334.65)));
    path23->arrVector.pushBack(new KKPoint(Vec2(89.95,396.81)));
    path23->arrVector.pushBack(new KKPoint(Vec2(75.73,462.79)));
    path23->arrVector.pushBack(new KKPoint(Vec2(67.55,539.16)));
    path23->arrVector.pushBack(new KKPoint(Vec2(52.22,660.08)));
    path23->arrVector.pushBack(new KKPoint(Vec2(77.41,781.82)));
    path23->arrVector.pushBack(new KKPoint(Vec2(93.52,871.20)));
    path23->arrVector.pushBack(new KKPoint(Vec2(156.60,937.39)));
    path23->arrVector.pushBack(new KKPoint(Vec2(299.31,985.66)));
    path23->arrVector.pushBack(new KKPoint(Vec2(449.04,995.61)));
    path23->arrVector.pushBack(new KKPoint(Vec2(550.47,947.20)));
    path23->arrVector.pushBack(new KKPoint(Vec2(596.76,835.68)));
    path23->arrVector.pushBack(new KKPoint(Vec2(675.67,756.16)));

    auto gun23 = NormalGun::createNormalGunWithShootingPerAngle(45.00,(BulletType)1,0);
    auto wave23 = NonAnimateWave::createNonAnimateWave("FleaTie", 20.00, 10.00, gun23, 2.00, 13.00, 4.00, path23, 150.00, 0.70);
    mission->arrWave.pushBack(wave23);
    Path* path24 = PathZicZac::createAt_X_With_Height(0.50, 50.00, 10.00, (PathDirection)-1);
    auto gun24 = GunLightning::createGunLightning(1,2.50,0);
    auto wave24 = SpecialWave::createSpecialWave("EnergiserEA", 200.00, 1.00, gun24, 3.00, 0.00, 4.00, path24, 30.00, 6.00, 13.00, (TypeCoin)0);
    mission->arrWave.pushBack(wave24);


    for(auto item : mission->arrWave){
        mission->addChild(item);
    }
    
    mission->retain();

    return mission;

}
