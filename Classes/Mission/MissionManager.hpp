
#ifndef MissionManager_hpp
#define MissionManager_hpp

#include "cocos2d.h"
#include "Mission.h"
#include "ui/CocosGUI.h"
USING_NS_CC;

class MissionManager : public Ref
{
public:
    static MissionManager* GetInstance();
    
    static Mission* GetMission(int index);
    
    Mission* Map1();
};

#endif /* MissionManager_hpp */
