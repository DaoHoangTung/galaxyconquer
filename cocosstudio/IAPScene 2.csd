<GameFile>
  <PropertyGroup Name="IAPScene" Type="Scene" ID="a22e2db4-bdf3-44c5-acbf-68370ea3035f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="161" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="330405948" Tag="426" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-194.7520" RightMargin="-189.2480" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="317.2480" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4957" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0" ActionTag="1165692149" Tag="425" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4_0" ActionTag="1411229523" Tag="356" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-65.0000" RightMargin="-65.0000" TopMargin="944.6596" BottomMargin="-134.6596" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="770.0000" Y="150.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="-59.6596" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="-0.0621" />
            <PreSize X="1.2031" Y="0.1563" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="-482546038" Tag="1" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="323.7760" RightMargin="316.2240" TopMargin="345.6000" BottomMargin="614.4000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Button_1" ActionTag="-1544595919" Tag="1" IconVisible="False" LeftMargin="-290.0000" RightMargin="110.0000" TopMargin="-139.0000" BottomMargin="-139.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="256" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="278.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2_0" ActionTag="19380654" Tag="178" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="65.5000" BottomMargin="65.5000" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="51" Scale9Height="51" ctype="ImageViewObjectData">
                    <Size X="147.0000" Y="147.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="139.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8167" Y="0.5288" />
                    <FileData Type="Normal" Path="NewIAPScene/0.99.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0" ActionTag="1834274482" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="62.5000" RightMargin="62.5000" TopMargin="226.0812" BottomMargin="22.9188" FontSize="20" LabelText="1,49$" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="55.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="37.4188" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1346" />
                    <PreSize X="0.3056" Y="0.1043" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_2_0_0" ActionTag="60267693" Tag="339" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-68.0000" RightMargin="-68.0000" TopMargin="-29.5002" BottomMargin="179.5002" Scale9Enable="True" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="220" Scale9Height="32" ctype="ImageViewObjectData">
                    <Size X="316.0000" Y="128.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0" ActionTag="-1007783559" Tag="340" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="159.8472" RightMargin="97.1528" TopMargin="31.8248" BottomMargin="37.1752" FontSize="50" LabelText="49" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="59.0000" Y="59.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="189.3472" Y="66.6752" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5992" Y="0.5209" />
                        <PreSize X="0.1867" Y="0.4609" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="243.5002" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8759" />
                    <PreSize X="1.7556" Y="0.4604" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewIAPScene/sms_itemFrame.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewIAPScene/sms_itemFrame_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewIAPScene/sms_itemFrame.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0" ActionTag="492191740" Tag="2" IconVisible="False" LeftMargin="-90.0000" RightMargin="-90.0000" TopMargin="-139.0000" BottomMargin="-139.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="256" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="278.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2_0" ActionTag="-1733317322" Tag="183" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="65.5000" BottomMargin="65.5000" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="51" Scale9Height="51" ctype="ImageViewObjectData">
                    <Size X="147.0000" Y="147.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="139.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8167" Y="0.5288" />
                    <FileData Type="Normal" Path="NewIAPScene/2.99.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0" ActionTag="-970574197" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="62.5000" RightMargin="62.5000" TopMargin="226.0812" BottomMargin="22.9188" FontSize="20" LabelText="2,99$" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="55.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="37.4188" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1346" />
                    <PreSize X="0.3056" Y="0.1043" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_2_0_0_0" ActionTag="-195032504" Tag="343" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-68.0000" RightMargin="-68.0000" TopMargin="-29.5002" BottomMargin="179.5002" Scale9Enable="True" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="220" Scale9Height="32" ctype="ImageViewObjectData">
                    <Size X="316.0000" Y="128.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0" ActionTag="1907860385" Tag="344" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="159.8472" RightMargin="97.1528" TopMargin="31.8248" BottomMargin="37.1752" FontSize="50" LabelText="99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="59.0000" Y="59.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="189.3472" Y="66.6752" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5992" Y="0.5209" />
                        <PreSize X="0.1867" Y="0.4609" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="243.5002" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8759" />
                    <PreSize X="1.7556" Y="0.4604" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewIAPScene/sms_itemFrame.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewIAPScene/sms_itemFrame_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewIAPScene/sms_itemFrame.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0_0" ActionTag="-1068972455" Tag="3" IconVisible="False" LeftMargin="110.0000" RightMargin="-290.0000" TopMargin="-139.0000" BottomMargin="-139.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="256" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="278.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2_0" ActionTag="1380343352" Tag="188" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="65.5000" BottomMargin="65.5000" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="51" Scale9Height="51" ctype="ImageViewObjectData">
                    <Size X="147.0000" Y="147.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="139.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8167" Y="0.5288" />
                    <FileData Type="Normal" Path="NewIAPScene/4.99.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0" ActionTag="-1213310045" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="62.4590" RightMargin="62.5410" TopMargin="225.9670" BottomMargin="23.0330" FontSize="20" LabelText="4,49$" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="55.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5122" ScaleY="0.4836" />
                    <Position X="90.6300" Y="37.0574" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5035" Y="0.1333" />
                    <PreSize X="0.3056" Y="0.1043" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_2_0_0_0_0" ActionTag="939803031" Tag="345" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-68.0000" RightMargin="-68.0000" TopMargin="-29.5002" BottomMargin="179.5002" Scale9Enable="True" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="220" Scale9Height="32" ctype="ImageViewObjectData">
                    <Size X="316.0000" Y="128.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0" ActionTag="-2144550066" Tag="346" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="145.3472" RightMargin="82.6528" TopMargin="31.8248" BottomMargin="37.1752" FontSize="50" LabelText="199" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="88.0000" Y="59.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="189.3472" Y="66.6752" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5992" Y="0.5209" />
                        <PreSize X="0.2785" Y="0.4609" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="243.5002" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8759" />
                    <PreSize X="1.7556" Y="0.4604" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewIAPScene/sms_itemFrame.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewIAPScene/sms_itemFrame_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewIAPScene/sms_itemFrame.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1" ActionTag="-733510832" Tag="4" IconVisible="False" LeftMargin="-190.5001" RightMargin="9.5001" TopMargin="184.4454" BottomMargin="-462.4454" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="151" Scale9Height="256" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="181.0000" Y="278.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2_0" ActionTag="-1123479280" Tag="193" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="17.0000" RightMargin="17.0000" TopMargin="65.5000" BottomMargin="65.5000" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="51" Scale9Height="51" ctype="ImageViewObjectData">
                    <Size X="147.0000" Y="147.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.5000" Y="139.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8122" Y="0.5288" />
                    <FileData Type="Normal" Path="NewIAPScene/49.99.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0" ActionTag="1787054200" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="63.0000" TopMargin="226.0812" BottomMargin="22.9188" FontSize="20" LabelText="9,99$" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="55.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.5000" Y="37.4188" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1346" />
                    <PreSize X="0.3039" Y="0.1043" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_2_0_0_0_0_0" ActionTag="597622850" Tag="347" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-67.5000" RightMargin="-67.5000" TopMargin="-29.5002" BottomMargin="179.5002" Scale9Enable="True" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="220" Scale9Height="32" ctype="ImageViewObjectData">
                    <Size X="316.0000" Y="128.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0" ActionTag="-1834311550" Tag="348" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="145.3472" RightMargin="82.6528" TopMargin="31.8248" BottomMargin="37.1752" FontSize="50" LabelText="499" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="88.0000" Y="59.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="189.3472" Y="66.6752" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5992" Y="0.5209" />
                        <PreSize X="0.2785" Y="0.4609" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.5000" Y="243.5002" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8759" />
                    <PreSize X="1.7459" Y="0.4604" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0001" Y="-323.4454" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewIAPScene/inapp_itemFrame.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewIAPScene/inapp_itemFrame_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewIAPScene/inapp_itemFrame.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0" ActionTag="-118904827" Tag="5" IconVisible="False" LeftMargin="9.5003" RightMargin="-190.5003" TopMargin="184.4454" BottomMargin="-462.4454" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="151" Scale9Height="256" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="181.0000" Y="278.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2_0" ActionTag="937090043" Tag="205" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="17.0000" RightMargin="17.0000" TopMargin="65.5000" BottomMargin="65.5000" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="51" Scale9Height="51" ctype="ImageViewObjectData">
                    <Size X="147.0000" Y="147.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.5000" Y="139.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8122" Y="0.5288" />
                    <FileData Type="Normal" Path="NewIAPScene/99.99.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0" ActionTag="1246701412" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="226.0812" BottomMargin="22.9188" FontSize="20" LabelText="10,99$" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="66.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.5000" Y="37.4188" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1346" />
                    <PreSize X="0.3646" Y="0.1043" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_2_0_0_0_0_0_0" ActionTag="-667648730" Tag="349" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-67.5000" RightMargin="-67.5000" TopMargin="-29.5002" BottomMargin="179.5002" Scale9Enable="True" LeftEage="48" RightEage="48" TopEage="48" BottomEage="48" Scale9OriginX="48" Scale9OriginY="48" Scale9Width="220" Scale9Height="32" ctype="ImageViewObjectData">
                    <Size X="316.0000" Y="128.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0" ActionTag="-970376044" Tag="350" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="145.3472" RightMargin="82.6528" TopMargin="31.8248" BottomMargin="37.1752" FontSize="50" LabelText="599" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="88.0000" Y="59.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="189.3472" Y="66.6752" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5992" Y="0.5209" />
                        <PreSize X="0.2785" Y="0.4609" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="90.5000" Y="243.5002" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8759" />
                    <PreSize X="1.7459" Y="0.4604" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0003" Y="-323.4454" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewIAPScene/inapp_itemFrame.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewIAPScene/inapp_itemFrame_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewIAPScene/inapp_itemFrame.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="323.7760" Y="614.4000" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5059" Y="0.6400" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1_0" ActionTag="-1145659748" Tag="2" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="894.3361" BottomMargin="65.6639" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Button_1_1_0" ActionTag="-1810720948" Tag="7" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-74.9979" RightMargin="-75.0021" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="0.0021" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0_0" ActionTag="490852745" Tag="8" RotationSkewX="20.0000" IconVisible="False" LeftMargin="214.4095" RightMargin="-364.4095" TopMargin="-78.5303" BottomMargin="28.5303" TouchEnable="True" FontSize="25" ButtonText="Shop        " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Image_12_0" ActionTag="-1072023546" Tag="898" RotationSkewX="-0.9268" RotationSkewY="-0.9230" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="39.5371" RightMargin="-20.5371" TopMargin="-39.5000" BottomMargin="-39.5000" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                    <Size X="131.0000" Y="129.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="105.0371" Y="25.0000" />
                    <Scale ScaleX="0.2500" ScaleY="0.2500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7002" Y="0.5000" />
                    <PreSize X="0.8733" Y="2.5800" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="289.4095" Y="53.5303" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_1" ActionTag="1170193557" Tag="9" RotationSkewX="-20.0000" IconVisible="False" LeftMargin="-352.9175" RightMargin="202.9175" TopMargin="-78.5278" BottomMargin="28.5278" TouchEnable="True" FontSize="25" ButtonText="Shop      " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2_0" ActionTag="-1583835503" Tag="897" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="95.7998" RightMargin="27.2002" TopMargin="10.3900" BottomMargin="13.6100" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="109.2998" Y="26.6100" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7287" Y="0.5322" />
                    <PreSize X="0.1800" Y="0.5200" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-277.9175" Y="53.5278" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="65.6639" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0684" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="-772797903" Tag="3" IconVisible="False" LeftMargin="-69.7740" RightMargin="-60.2260" TopMargin="-43.6107" BottomMargin="853.6107" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="770.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_15" ActionTag="-1745805742" Tag="246" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="68.3931" BottomMargin="44.6069" LabelText="Are you ready to make some deals !" ctype="TextBMFontObjectData">
                <Size X="450.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="385.0000" Y="63.1069" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4207" />
                <PreSize X="0.5844" Y="0.2467" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="315.2260" Y="928.6107" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4925" Y="0.9673" />
            <PreSize X="1.2031" Y="0.1563" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="2099140030" Tag="4" IconVisible="False" LeftMargin="-98.1196" RightMargin="418.1196" TopMargin="122.2079" BottomMargin="777.7921" Scale9Enable="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="2111752423" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="51.0240" RightMargin="38.9760" TopMargin="1.2480" BottomMargin="-7.2480" LabelText="1,000,000" ctype="TextBMFontObjectData">
                <Size X="230.0000" Y="66.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="-841554253" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-49.4641" RightMargin="252.4641" TopMargin="12.9974" BottomMargin="27.0026" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-35.9641" Y="40.0026" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1564" Y="0.6061" />
                    <PreSize X="0.1174" Y="0.3939" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="281.0240" Y="25.7520" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8782" Y="0.4292" />
                <PreSize X="0.7188" Y="1.1000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_1" ActionTag="-46849921" Tag="2" IconVisible="True" LeftMargin="151.4053" RightMargin="168.5947" TopMargin="24.7902" BottomMargin="35.2098" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="151.4053" Y="35.2098" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4731" Y="0.5868" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/IAPeffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="61.8804" Y="807.7921" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0967" Y="0.8415" />
            <PreSize X="0.5000" Y="0.0625" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1_0" ActionTag="-1259683970" Tag="5" IconVisible="False" LeftMargin="417.9999" RightMargin="-97.9999" TopMargin="123.0000" BottomMargin="777.0000" Scale9Enable="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="-1452315759" Tag="1" IconVisible="False" LeftMargin="81.3086" RightMargin="185.6914" TopMargin="11.9108" BottomMargin="13.0892" FontSize="30" LabelText="100" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="53.0000" Y="35.0000" />
                <Children>
                  <AbstractNodeData Name="Image_12" ActionTag="-218878383" Tag="1" RotationSkewX="-0.9268" RotationSkewY="-0.9232" IconVisible="False" LeftMargin="-87.3355" RightMargin="9.3355" TopMargin="-46.5112" BottomMargin="-47.4888" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                    <Size X="131.0000" Y="129.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-21.8355" Y="17.0112" />
                    <Scale ScaleX="0.3500" ScaleY="0.3500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.4120" Y="0.4860" />
                    <PreSize X="2.4717" Y="3.6857" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="81.3086" Y="30.5892" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2541" Y="0.5098" />
                <PreSize X="0.1656" Y="0.5833" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_1" ActionTag="-1197969975" Tag="2" IconVisible="True" LeftMargin="120.4056" RightMargin="199.5944" TopMargin="24.7901" BottomMargin="35.2099" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="120.4056" Y="35.2099" />
                <Scale ScaleX="0.5559" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3763" Y="0.5868" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/starPurchaseEffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="577.9999" Y="807.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9031" Y="0.8406" />
            <PreSize X="0.5000" Y="0.0625" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>