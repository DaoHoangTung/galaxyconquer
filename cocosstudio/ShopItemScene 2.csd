<GameFile>
  <PropertyGroup Name="ShopItemScene" Type="Scene" ID="d6fe73fc-8285-4f56-9ab5-a4cbf7c205bd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="102" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Image_3" ActionTag="1323743096" Alpha="153" Tag="200" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftEage="165" RightEage="165" TopEage="198" BottomEage="198" Scale9OriginX="165" Scale9OriginY="198" Scale9Width="470" Scale9Height="204" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="NewShopItemScene/bg_1_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_3_0" ActionTag="101172967" Alpha="153" Tag="300" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-640.0000" RightMargin="640.0000" LeftEage="165" RightEage="165" TopEage="198" BottomEage="198" Scale9OriginX="165" Scale9OriginY="198" Scale9Width="470" Scale9Height="204" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="NewShopItemScene/bg_1_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayer" ActionTag="-605266962" Tag="1" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="528.0000" BottomMargin="432.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="BTNUpgradeGunE0_6" ActionTag="-1409094411" Tag="3" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="118.0000" RightMargin="-182.0000" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunE0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunN0_7" ActionTag="-902506271" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="-182.0001" BottomMargin="118.0001" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="150.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunN0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunNE0_8" ActionTag="-800388627" Tag="2" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="-132.0000" BottomMargin="68.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunNE0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunNW0_9" ActionTag="1549406443" Tag="8" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="-132.0000" BottomMargin="68.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunNW0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunS0_10" ActionTag="730523351" Tag="5" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="118.0000" BottomMargin="-182.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunS0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunSE0_11" ActionTag="-788897509" Tag="4" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="68.0000" BottomMargin="-132.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="-100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunSE0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunSW0_12" ActionTag="470905492" Tag="6" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="68.0000" BottomMargin="-132.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="-100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunSW0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunW0_13" ActionTag="-1224409210" Tag="7" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-182.0000" RightMargin="118.0000" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeGunW0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeMagnet0_14" ActionTag="-783989113" Tag="9" IconVisible="False" LeftMargin="-231.9998" RightMargin="167.9998" TopMargin="118.0000" BottomMargin="-182.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-199.9998" Y="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeMagnet0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeRocket0_15" ActionTag="1293395984" Tag="10" IconVisible="False" LeftMargin="168.0000" RightMargin="-232.0000" TopMargin="118.0000" BottomMargin="-182.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0000" Y="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeRocket0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeROF1_16" ActionTag="-425949630" Tag="13" IconVisible="False" LeftMargin="-232.0000" RightMargin="168.0000" TopMargin="-182.0000" BottomMargin="118.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-200.0000" Y="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNaborbShieldDamage0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeSpeed1_17" ActionTag="-1177154155" Tag="16" IconVisible="False" LeftMargin="168.0000" RightMargin="-232.0000" TopMargin="-181.9974" BottomMargin="117.9974" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0000" Y="149.9974" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNaborbShieldRecharge0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeMagnet0_14_0" ActionTag="-2121173369" Tag="14" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="-257.0000" BottomMargin="193.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNaborbShieldDuration0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeMagnet0_14_0_0" ActionTag="-1094633553" Tag="15" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="-257.0000" BottomMargin="193.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNaborbShieldRange0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeMagnet0_14_0_0_0" ActionTag="1033539349" Tag="12" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="193.0000" BottomMargin="-257.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="-225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeSpeed0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeMagnet0_14_0_0_0_0" ActionTag="1581796422" Tag="11" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="193.0000" BottomMargin="-257.0000" ctype="SpriteObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="-225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="BTNUpgradeROF0.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="432.0000" />
            <Scale ScaleX="1.1000" ScaleY="1.1000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4500" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="-1061495159" Tag="2" IconVisible="False" LeftMargin="-68.0726" RightMargin="-61.9274" TopMargin="-43.1713" BottomMargin="853.1713" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="770.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="txtInfo" ActionTag="1584143654" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="147.0000" RightMargin="147.0000" TopMargin="68.0050" BottomMargin="41.9950" FontSize="34" LabelText="fire large bullets from front gun" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="476.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="385.0000" Y="61.9950" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4133" />
                <PreSize X="0.6182" Y="0.2667" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="316.9274" Y="928.1713" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4952" Y="0.9668" />
            <PreSize X="1.2031" Y="0.1563" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="1636897301" Tag="3" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="528.0000" BottomMargin="432.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="320.0000" Y="432.0000" />
            <Scale ScaleX="2.0919" ScaleY="2.0243" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4500" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="870444571" Tag="4" IconVisible="False" LeftMargin="-100.0000" RightMargin="420.0000" TopMargin="110.0000" BottomMargin="790.0000" Scale9Enable="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1462539616" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="51.0239" RightMargin="38.9761" TopMargin="0.2480" BottomMargin="-6.2480" LabelText="1,000,000" ctype="TextBMFontObjectData">
                <Size X="230.0000" Y="66.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="444435602" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-49.4641" RightMargin="252.4641" TopMargin="12.9974" BottomMargin="27.0026" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-35.9641" Y="40.0026" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1564" Y="0.6061" />
                    <PreSize X="0.1174" Y="0.3939" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="281.0239" Y="26.7520" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8782" Y="0.4459" />
                <PreSize X="0.7188" Y="1.1000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.0000" Y="820.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.8542" />
            <PreSize X="0.5000" Y="0.0625" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1_0" ActionTag="1260910505" Tag="5" IconVisible="False" LeftMargin="430.0000" RightMargin="-110.0000" TopMargin="113.2819" BottomMargin="786.7181" Scale9Enable="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1703935810" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="36.6720" RightMargin="137.3280" TopMargin="12.7480" BottomMargin="10.2520" LabelText="$1,000,000" ctype="TextBMFontObjectData">
                <Size X="146.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="36.6720" Y="28.7520" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1146" Y="0.4792" />
                <PreSize X="0.4563" Y="0.6167" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="590.0000" Y="816.7181" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9219" Y="0.8507" />
            <PreSize X="0.5000" Y="0.0625" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0" ActionTag="-1852109770" Tag="99" RotationSkewX="-20.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="441.8099" RightMargin="48.1901" TopMargin="880.6400" BottomMargin="29.3600" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="516.8099" Y="54.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8075" Y="0.0566" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0_0" ActionTag="1157025817" Tag="1489" RotationSkewX="20.0003" RotationSkewY="0.0013" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="48.2000" RightMargin="441.8000" TopMargin="880.6400" BottomMargin="29.3600" TouchEnable="True" FontSize="25" ButtonText="Extra" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="123.2000" Y="54.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1925" Y="0.0566" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>