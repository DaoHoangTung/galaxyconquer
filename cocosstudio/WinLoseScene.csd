<GameFile>
  <PropertyGroup Name="WinLoseScene" Type="Scene" ID="fd35c520-41a7-4a95-a6e2-561092ecfca7" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="696" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="726595004" Tag="740" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-193.4080" RightMargin="-190.5920" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.5920" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4978" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0" ActionTag="-1544761814" Tag="741" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="1634615906" Tag="738" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-5.0000" RightMargin="-5.0000" TopMargin="-40.6074" BottomMargin="800.6074" LeftEage="55" RightEage="55" TopEage="11" BottomEage="11" Scale9OriginX="55" Scale9OriginY="11" Scale9Width="168" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="650.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="WIN" ActionTag="-103686786" VisibleForFrame="False" Tag="739" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="170.0000" RightMargin="170.0000" TopMargin="89.9978" BottomMargin="60.0022" LabelText="Mission Complete" ctype="TextBMFontObjectData">
                <Size X="310.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="325.0000" Y="85.0022" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4250" />
                <PreSize X="0.4769" Y="0.2500" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="LOSE" ActionTag="816694941" Tag="742" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="193.5000" RightMargin="193.5000" TopMargin="88.5000" BottomMargin="58.5000" LabelText="Mission Failed" ctype="TextBMFontObjectData">
                <Size X="263.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="325.0000" Y="85.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4250" />
                <PreSize X="0.4046" Y="0.2650" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinLose.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="900.6074" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9381" />
            <PreSize X="1.0156" Y="0.2083" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_2" ActionTag="-1871224962" Tag="995" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="288.0000" BottomMargin="672.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_3" ActionTag="1529476121" Tag="996" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-458.9998" RightMargin="180.9998" TopMargin="146.0001" BottomMargin="-228.0001" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-319.9998" Y="-187.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3_0" ActionTag="385341325" Tag="997" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="181.0000" RightMargin="-459.0000" TopMargin="146.0000" BottomMargin="-228.0000" FlipY="True" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="-187.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="672.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_4" ActionTag="258147068" Tag="999" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="95.0000" RightMargin="95.0000" TopMargin="205.0000" BottomMargin="205.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="450.0000" Y="550.0000" />
            <Children>
              <AbstractNodeData Name="Image_8" ActionTag="-571217347" Tag="1000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="66.0000" RightMargin="66.0000" TopMargin="-59.2999" BottomMargin="349.2999" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="252" Scale9Height="194" ctype="ImageViewObjectData">
                <Size X="318.0000" Y="260.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="479.2999" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8715" />
                <PreSize X="0.7067" Y="0.4727" />
                <FileData Type="PlistSubImage" Path="Boss13.png" Plist="NewLobbyScene/BossActive.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_12" ActionTag="-1167989134" Tag="1011" RotationSkewX="15.0002" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="101.0000" RightMargin="101.0000" TopMargin="193.6919" BottomMargin="321.3081" FontSize="28" LabelText="-Enemy Destroyed-" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="248.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="338.8081" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6160" />
                <PreSize X="0.5511" Y="0.0636" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="128" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_12_0" ActionTag="-135397735" Tag="1013" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="125.5000" RightMargin="125.5000" TopMargin="294.8654" BottomMargin="220.1346" FontSize="28" LabelText="-Wave Cleared-" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="199.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="237.6346" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4321" />
                <PreSize X="0.4422" Y="0.0636" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="128" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_12_0_0" ActionTag="-2054389929" Tag="1015" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="170.5000" RightMargin="170.5000" TopMargin="392.3279" BottomMargin="114.6721" FontSize="35" LabelText="-Time-" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="109.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="136.1721" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2476" />
                <PreSize X="0.2422" Y="0.0782" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_39_0_0" ActionTag="786929537" Tag="1016" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="159.5000" RightMargin="159.5000" TopMargin="436.5225" BottomMargin="47.4775" LabelText="05:00" ctype="TextBMFontObjectData">
                <Size X="131.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="80.4775" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1463" />
                <PreSize X="0.2911" Y="0.1200" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Red.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_19_0" ActionTag="-994009289" Tag="1023" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="125.0000" RightMargin="125.0000" TopMargin="336.9970" BottomMargin="173.0030" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="13" BottomEage="13" Scale9OriginX="33" Scale9OriginY="13" Scale9Width="224" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="200.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="LoadingBar_1" ActionTag="-683647909" Tag="1024" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="6.5000" BottomMargin="8.5000" ProgressInfo="0" ctype="LoadingBarObjectData">
                    <Size X="140.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="21.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="115" G="255" B="0" />
                    <PrePosition X="0.5000" Y="0.5250" />
                    <PreSize X="0.7000" Y="0.6250" />
                    <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_9" ActionTag="-1882027530" Tag="1026" RotationSkewX="20.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="81.5000" RightMargin="81.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="15" LabelText="0/13" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="37.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1850" Y="0.5000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="128" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="193.0030" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3509" />
                <PreSize X="0.4444" Y="0.0727" />
                <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_19" ActionTag="2113575532" Tag="1017" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="237.1173" BottomMargin="272.8827" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="13" BottomEage="13" Scale9OriginX="33" Scale9OriginY="13" Scale9Width="224" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="290.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="LoadingBar_1" ActionTag="1253384671" Tag="1018" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="6.5000" BottomMargin="8.5000" ProgressInfo="0" ctype="LoadingBarObjectData">
                    <Size X="230.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="145.0000" Y="21.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="115" G="255" B="0" />
                    <PrePosition X="0.5000" Y="0.5250" />
                    <PreSize X="0.7931" Y="0.6250" />
                    <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_9" ActionTag="774742827" Tag="1021" RotationSkewX="20.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="122.5000" RightMargin="122.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="15" LabelText="0/100" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="45.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="145.0000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1552" Y="0.5000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="128" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="292.8827" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5325" />
                <PreSize X="0.6444" Y="0.0727" />
                <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1" ActionTag="1402691582" Tag="1032" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="112.0000" RightMargin="112.0000" TopMargin="457.0941" BottomMargin="-127.0941" LeftEage="74" RightEage="74" TopEage="72" BottomEage="72" Scale9OriginX="74" Scale9OriginY="72" Scale9Width="78" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="226.0000" Y="220.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="-17.0941" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.0311" />
                <PreSize X="0.5022" Y="0.4000" />
                <FileData Type="Normal" Path="NewRank/dautruong_img_flare.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_39_0_0_0" ActionTag="-508778457" Tag="1034" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="178.5000" RightMargin="178.5000" TopMargin="540.4754" BottomMargin="-25.4754" LabelText="5.5/10" ctype="TextBMFontObjectData">
                <Size X="93.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="-7.9754" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.0145" />
                <PreSize X="0.2067" Y="0.0636" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Sharnay_text.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7031" Y="0.5729" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0" ActionTag="441317892" Tag="1101" IconVisible="False" LeftMargin="237.9000" RightMargin="252.1000" TopMargin="900.0000" BottomMargin="10.0000" TouchEnable="True" FontSize="25" ButtonText="Lobby" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="312.9000" Y="35.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4889" Y="0.0365" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0_0" ActionTag="1179091246" Tag="1102" RotationSkewX="-20.0000" IconVisible="False" LeftMargin="479.0500" RightMargin="10.9500" TopMargin="862.0000" BottomMargin="48.0000" TouchEnable="True" FontSize="25" ButtonText="Next" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="554.0500" Y="73.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8657" Y="0.0760" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0_0_0" ActionTag="98269296" Tag="1103" RotationSkewX="20.0000" IconVisible="False" LeftMargin="11.0200" RightMargin="478.9800" TopMargin="862.0000" BottomMargin="48.0000" TouchEnable="True" FontSize="25" ButtonText="Again" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="86.0200" Y="73.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1344" Y="0.0760" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>