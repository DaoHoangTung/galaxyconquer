<GameFile>
  <PropertyGroup Name="LoadingScene" Type="Scene" ID="253ac503-7d90-4a56-bdde-0060305ca73e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="276" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1244287527" Tag="277" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="263.5000" RightMargin="263.5000" TopMargin="464.0000" BottomMargin="464.0000" LabelText="Loading..." ctype="TextBMFontObjectData">
            <Size X="113.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.1766" Y="0.0333" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-264772226" Tag="1" IconVisible="False" LeftMargin="-125.5859" RightMargin="657.5859" TopMargin="0.6373" BottomMargin="924.3627" FontSize="30" LabelText="Dummy" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-71.5859" Y="941.8627" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1119" Y="0.9811" />
            <PreSize X="0.1688" Y="0.0365" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1_0" ActionTag="2057319460" VisibleForFrame="False" Tag="1434" IconVisible="False" LeftMargin="-216.5103" RightMargin="649.5103" TopMargin="35.9426" BottomMargin="323.0574" LeftEage="68" RightEage="68" TopEage="198" BottomEage="198" Scale9OriginX="68" Scale9OriginY="198" Scale9Width="71" Scale9Height="205" ctype="ImageViewObjectData">
            <Size X="207.0000" Y="601.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="-113.0103" Y="323.0574" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1766" Y="0.3365" />
            <PreSize X="0.3234" Y="0.6260" />
            <FileData Type="PlistSubImage" Path="zap_1_blue_1.png" Plist="NewPlist/zap_1_blue.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="loadScoreSkill" ActionTag="134232184" Tag="2356" IconVisible="False" LeftMargin="-100.5013" RightMargin="676.5013" TopMargin="450.8661" BottomMargin="445.1339" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="64.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-68.5013" Y="477.1339" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1070" Y="0.4970" />
            <PreSize X="0.1000" Y="0.0667" />
            <FileData Type="PlistSubImage" Path="ScoreSkillActive1.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>