<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="3eb784ac-5e62-4e40-8357-a585f548e6c5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="6" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="test" ActionTag="1891837369" Tag="8" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="170.0000" RightMargin="170.0000" TopMargin="206.0000" BottomMargin="206.0000" FontSize="50" LabelText="IDMP Project&#xA;Galaxy Conquer&#xA;&#xA;Presented by Project Cocos" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="620.0000" Y="228.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6458" Y="0.3562" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="255" B="255" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>