<GameFile>
  <PropertyGroup Name="LobbyScene" Type="Scene" ID="cd5c7e17-372d-468e-8342-a0d63346dc1f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="941" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="1335248806" Tag="999" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-194.6240" RightMargin="-189.3760" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="317.3760" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4959" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="-428615049" Tag="36" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView_1" ActionTag="264038208" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.0240" RightMargin="1.0240" TopMargin="573.0000" BottomMargin="187.0000" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="-6" Scale9OriginY="-6" Scale9Width="12" Scale9Height="12" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="50" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
            <Size X="640.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="HeadDummy" ActionTag="196364359" Alpha="0" Tag="99" IconVisible="False" RightMargin="2220.0000" TopMargin="50.0000" BottomMargin="50.0000" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
                <Size X="220.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="110.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0451" Y="0.5000" />
                <PreSize X="0.0902" Y="0.5000" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss1" ActionTag="1480105296" ZOrder="1" Tag="1" IconVisible="False" LeftMargin="270.0000" RightMargin="2070.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1311" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription1.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss2" ActionTag="950281857" ZOrder="2" Tag="2" IconVisible="False" LeftMargin="420.0000" RightMargin="1920.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="470.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1926" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription2.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss3" ActionTag="4569787" ZOrder="3" Tag="3" IconVisible="False" LeftMargin="570.0000" RightMargin="1770.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="620.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2541" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescriptionDisable3.jpg" Plist="NewLobbyScene/BossDescriptionDisable.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss9" ActionTag="338647511" ZOrder="4" Tag="4" IconVisible="False" LeftMargin="720.0000" RightMargin="1620.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="770.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3156" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription4.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss10" ActionTag="1088134777" ZOrder="5" Tag="5" IconVisible="False" LeftMargin="870.0000" RightMargin="1470.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="920.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3770" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription5.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss11" ActionTag="-10127979" ZOrder="6" Tag="6" IconVisible="False" LeftMargin="1020.0000" RightMargin="1320.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1070.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4385" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription6.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss13" ActionTag="-96051046" ZOrder="7" Tag="7" IconVisible="False" LeftMargin="1170.0000" RightMargin="1170.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1220.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription7.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss4" ActionTag="-318267741" ZOrder="8" Tag="8" IconVisible="False" LeftMargin="1320.0000" RightMargin="1020.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1370.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5615" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription8.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss6" ActionTag="360509103" ZOrder="9" Tag="9" IconVisible="False" LeftMargin="1470.0000" RightMargin="870.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1520.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6230" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription9.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss12" ActionTag="-673930065" ZOrder="10" Tag="10" IconVisible="False" LeftMargin="1620.0000" RightMargin="720.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1670.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6844" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription10.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss5" ActionTag="-310253990" ZOrder="11" Tag="11" IconVisible="False" LeftMargin="1770.0000" RightMargin="570.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1820.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7459" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription11.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss7" ActionTag="-2065124496" ZOrder="12" Tag="12" IconVisible="False" LeftMargin="1920.0000" RightMargin="420.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1970.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8074" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription12.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Boss8" ActionTag="282817025" ZOrder="13" Tag="13" IconVisible="False" LeftMargin="2070.0000" RightMargin="270.0000" TopMargin="50.0000" BottomMargin="50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="2120.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8689" Y="0.5000" />
                <PreSize X="0.0410" Y="0.5000" />
                <FileData Type="PlistSubImage" Path="BossDescription13.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="EndDummy" ActionTag="907723432" ZOrder="14" Alpha="0" Tag="100" IconVisible="False" LeftMargin="2220.0000" TopMargin="50.0000" BottomMargin="50.0000" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
                <Size X="220.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="2330.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9549" Y="0.5000" />
                <PreSize X="0.0902" Y="0.5000" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.9760" Y="287.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4984" Y="0.2990" />
            <PreSize X="1.0000" Y="0.2083" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_18" ActionTag="1968659151" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-10.0000" RightMargin="-10.0000" TopMargin="562.0000" BottomMargin="378.0000" FlipX="True" FlipY="True" LeftEage="122" RightEage="122" TopEage="6" BottomEage="6" Scale9OriginX="122" Scale9OriginY="6" Scale9Width="126" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="660.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="388.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4042" />
            <PreSize X="1.0313" Y="0.0208" />
            <FileData Type="Normal" Path="NewLobbyScene/155_TabDialogBorder_t_TabDialogBorder_t.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_18_0" ActionTag="1517821857" Tag="3" IconVisible="False" PositionPercentXEnabled="True" TopMargin="762.0000" BottomMargin="178.0000" Scale9Enable="True" LeftEage="34" RightEage="34" TopEage="6" BottomEage="6" Scale9OriginX="34" Scale9OriginY="6" Scale9Width="38" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="188.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1958" />
            <PreSize X="1.0000" Y="0.0208" />
            <FileData Type="Normal" Path="NewLobbyScene/152_TabDialogBorder_b_TabDialogBorder_b.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_20" ActionTag="-397893532" Tag="4" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-3.6000" RightMargin="623.6000" TopMargin="572.0000" BottomMargin="188.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="34" BottomEage="34" Scale9OriginX="6" Scale9OriginY="34" Scale9Width="8" Scale9Height="38" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="6.4000" Y="288.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0100" Y="0.3000" />
            <PreSize X="0.0313" Y="0.2083" />
            <FileData Type="Normal" Path="NewLobbyScene/154_TabDialogBorder_r_TabDialogBorder_r.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_20_0" ActionTag="927192195" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="623.6000" RightMargin="-3.6000" TopMargin="575.4560" BottomMargin="184.5440" FlipX="True" FlipY="True" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="34" BottomEage="34" Scale9OriginX="6" Scale9OriginY="34" Scale9Width="8" Scale9Height="38" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="633.6000" Y="284.5440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9900" Y="0.2964" />
            <PreSize X="0.0313" Y="0.2083" />
            <FileData Type="Normal" Path="NewLobbyScene/154_TabDialogBorder_r_TabDialogBorder_r.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="-855920886" Tag="6" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="832.0000" BottomMargin="128.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Button_1" ActionTag="421451411" Tag="1" RotationSkewX="20.0000" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-295.0010" RightMargin="115.0010" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" FontSize="25" ButtonText="Upgrade" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-205.0010" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0" ActionTag="-165435301" VisibleForFrame="False" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-75.0015" RightMargin="-74.9985" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" FontSize="25" ButtonText="Quest" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-0.0015" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0_0" ActionTag="-1553465853" Tag="3" RotationSkewX="-20.0003" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="114.9985" RightMargin="-294.9985" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" FontSize="25" ButtonText="Skill" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="204.9985" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1" ActionTag="-1219696474" VisibleForFrame="False" Tag="4" RotationSkewX="19.9994" IconVisible="False" LeftMargin="-308.3546" RightMargin="128.3546" TopMargin="53.5857" BottomMargin="-103.5857" TouchEnable="True" FontSize="25" ButtonText="Shopz" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-218.3546" Y="-78.5857" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0" ActionTag="-1477364217" Tag="5" IconVisible="False" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="53.5855" BottomMargin="-103.5855" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="250.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-78.5855" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0_0" ActionTag="1122286725" Tag="6" IconVisible="False" LeftMargin="-75.0000" RightMargin="-75.0000" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" FontSize="25" ButtonText="Rank" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="128.0000" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1333" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_2" ActionTag="1282514217" Tag="7" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="288.0000" BottomMargin="672.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_2" ActionTag="-1814258541" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-88.0000" RightMargin="-88.0000" TopMargin="2.4086" BottomMargin="-117.4086" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="37" BottomEage="37" Scale9OriginX="33" Scale9OriginY="37" Scale9Width="110" Scale9Height="41" ctype="ImageViewObjectData">
                <Size X="176.0000" Y="115.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-59.9086" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="Boss1.png" Plist="NewLobbyScene/BossActive.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="290233659" Tag="2" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-459.0000" RightMargin="181.0000" TopMargin="79.0000" BottomMargin="-161.0000" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-320.0000" Y="-120.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3_0" ActionTag="1637210262" Tag="3" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="181.0000" RightMargin="-459.0000" TopMargin="79.0000" BottomMargin="-161.0000" FlipY="True" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="-120.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="672.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_4" ActionTag="1210278965" Tag="8" RotationSkewX="19.9994" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="257.0000" RightMargin="257.0000" TopMargin="60.0000" BottomMargin="868.0000" LabelText="Description" ctype="TextBMFontObjectData">
            <Size X="126.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="320.0000" Y="900.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9375" />
            <PreSize X="0.1969" Y="0.0333" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_4_0" ActionTag="1728736155" Tag="9" RotationSkewX="19.9994" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="281.0000" RightMargin="281.0000" TopMargin="21.6445" BottomMargin="906.3555" LabelText="Author" ctype="TextBMFontObjectData">
            <Size X="78.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="320.0000" Y="938.3555" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9775" />
            <PreSize X="0.1219" Y="0.0333" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="679905513" Tag="10" IconVisible="False" LeftMargin="-421.5672" RightMargin="885.5672" TopMargin="629.0631" BottomMargin="215.9369" LeftEage="58" RightEage="58" TopEage="37" BottomEage="37" Scale9OriginX="58" Scale9OriginY="37" Scale9Width="60" Scale9Height="41" ctype="ImageViewObjectData">
            <Size X="176.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-333.5672" Y="273.4369" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.5212" Y="0.2848" />
            <PreSize X="0.2750" Y="0.1198" />
            <FileData Type="PlistSubImage" Path="Boss1Disable.png" Plist="NewLobbyScene/BossDisable.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1700912432" Tag="11" RotationSkewX="19.9977" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="85.5000" RightMargin="85.5000" TopMargin="518.8723" BottomMargin="391.1277" LabelText="Hold for 3 seconds to start !" ctype="TextBMFontObjectData">
            <Size X="469.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="416.1277" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4335" />
            <PreSize X="0.7328" Y="0.0521" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_wheel.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="455483058" Tag="12" RotationSkewX="20.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="298.5000" RightMargin="298.5000" TopMargin="472.0262" BottomMargin="421.9738" LabelText="3 !" ctype="TextBMFontObjectData">
            <Size X="43.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="454.9738" />
            <Scale ScaleX="2.0000" ScaleY="2.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4739" />
            <PreSize X="0.0672" Y="0.0688" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_17" ActionTag="-1921365812" Tag="325" IconVisible="False" LeftMargin="-103.2400" RightMargin="697.2400" TopMargin="-93.8313" BottomMargin="1007.8313" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-80.2400" Y="1030.8313" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1254" Y="1.0738" />
            <PreSize X="0.0719" Y="0.0479" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1_0" ActionTag="280909529" VisibleForFrame="False" Tag="718" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-4.9680" RightMargin="418.9680" TopMargin="408.5652" BottomMargin="331.4348" LeftEage="74" RightEage="74" TopEage="72" BottomEage="72" Scale9OriginX="74" Scale9OriginY="72" Scale9Width="78" Scale9Height="76" ctype="ImageViewObjectData">
            <Size X="226.0000" Y="220.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="108.0320" Y="441.4348" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1688" Y="0.4598" />
            <PreSize X="0.3531" Y="0.2292" />
            <FileData Type="Normal" Path="NewRank/dautruong_img_flare.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_2" ActionTag="1216251040" VisibleForFrame="False" Tag="1662" RotationSkewY="-0.0106" IconVisible="False" LeftMargin="70.7464" RightMargin="500.2536" TopMargin="488.0000" BottomMargin="410.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="39" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="69.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="105.2464" Y="441.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1644" Y="0.4594" />
            <PreSize X="0.1078" Y="0.0646" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="NewDailyReward/freencoin_menu_achievement.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewDailyReward/freencoin_menu_achievement.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewDailyReward/freencoin_menu_achievement.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1_0_0" ActionTag="1259870489" VisibleForFrame="False" Tag="2873" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="421.7200" RightMargin="-7.7200" TopMargin="412.5057" BottomMargin="327.4943" LeftEage="74" RightEage="74" TopEage="72" BottomEage="72" Scale9OriginX="74" Scale9OriginY="72" Scale9Width="78" Scale9Height="76" ctype="ImageViewObjectData">
            <Size X="226.0000" Y="220.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="534.7200" Y="437.4943" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8355" Y="0.4557" />
            <PreSize X="0.3531" Y="0.2292" />
            <FileData Type="Normal" Path="NewRank/dautruong_img_flare.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_29" ActionTag="1501207075" VisibleForFrame="False" Tag="888" IconVisible="False" LeftMargin="469.0000" RightMargin="39.0000" TopMargin="480.7254" BottomMargin="401.2746" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="102" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="132.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="535.0000" Y="440.2746" />
            <Scale ScaleX="0.5000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8359" Y="0.4586" />
            <PreSize X="0.2062" Y="0.0812" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="NewRank/dautruong_img_swords.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewRank/dautruong_img_swords.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewRank/dautruong_img_swords.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_1" ActionTag="803184827" VisibleForFrame="False" Tag="2869" RotationSkewX="-19.9995" IconVisible="False" LeftMargin="25.0000" RightMargin="465.0000" TopMargin="173.3287" BottomMargin="736.6713" TouchEnable="True" FontSize="25" ButtonText="Shop      " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_2_0" ActionTag="-2063315973" Tag="2870" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="95.7998" RightMargin="27.2002" TopMargin="10.3900" BottomMargin="13.6100" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                <Size X="27.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="109.2998" Y="26.6100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7287" Y="0.5322" />
                <PreSize X="0.1800" Y="0.5200" />
                <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="100.0000" Y="761.6713" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1563" Y="0.7934" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0_0" ActionTag="-916645746" VisibleForFrame="False" Tag="2871" RotationSkewX="20.0010" IconVisible="False" LeftMargin="465.0000" RightMargin="25.0000" TopMargin="173.3365" BottomMargin="736.6635" TouchEnable="True" FontSize="25" ButtonText="Shop        " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_12_0" ActionTag="1216477674" Tag="2872" RotationSkewX="-0.9268" RotationSkewY="-0.9230" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="39.5371" RightMargin="-20.5371" TopMargin="-39.5000" BottomMargin="-39.5000" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                <Size X="131.0000" Y="129.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="105.0371" Y="25.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7002" Y="0.5000" />
                <PreSize X="0.8733" Y="2.5800" />
                <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="540.0000" Y="761.6635" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8438" Y="0.7934" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>