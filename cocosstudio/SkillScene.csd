<GameFile>
  <PropertyGroup Name="SkillScene" Type="Scene" ID="7832a36f-2cea-4762-a771-28af0951da18" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="667" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="340509215" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-192.0000" RightMargin="-192.0000" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="-1891716083" Tag="2" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="dummyLevel" ActionTag="1977054077" Tag="911" RotationSkewX="15.0074" RotationSkewY="-0.0058" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-289.7585" RightMargin="841.7585" TopMargin="91.8353" BottomMargin="836.1647" LabelText="level 10" ctype="TextBMFontObjectData">
            <Size X="88.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-245.7585" Y="852.1647" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3840" Y="0.8877" />
            <PreSize X="0.1375" Y="0.0333" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_2" ActionTag="-794539431" Tag="3" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="288.0000" BottomMargin="672.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="1814596831" Tag="3" IconVisible="False" LeftMargin="-300.0000" RightMargin="-300.0000" TopMargin="-273.9995" BottomMargin="153.9995" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="600.0000" Y="120.0000" />
                <Children>
                  <AbstractNodeData Name="Text_4" ActionTag="-1685372378" Tag="1" IconVisible="False" LeftMargin="-237.3600" RightMargin="822.3600" TopMargin="42.9575" BottomMargin="51.0425" FontSize="22" LabelText="7" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="15.0000" Y="26.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_13" ActionTag="1310375770" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-64.5734" RightMargin="-24.4266" TopMargin="-39.0000" BottomMargin="-39.0000" LeftEage="43" RightEage="43" TopEage="42" BottomEage="42" Scale9OriginX="43" Scale9OriginY="42" Scale9Width="18" Scale9Height="20" ctype="ImageViewObjectData">
                        <Size X="104.0000" Y="104.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="-12.5734" Y="13.0000" />
                        <Scale ScaleX="0.2500" ScaleY="0.2500" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="-0.8382" Y="0.5000" />
                        <PreSize X="6.9333" Y="4.0000" />
                        <FileData Type="Normal" Path="NewSkillShop/Icon_Skill_Point.png" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-237.3600" Y="64.0425" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="-0.3956" Y="0.5337" />
                    <PreSize X="0.0250" Y="0.2167" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="213.9995" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewSkillShop/glassPanel_cornerBR.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="863780490" Tag="1" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-458.9998" RightMargin="180.9998" TopMargin="79.0001" BottomMargin="-161.0001" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-319.9998" Y="-120.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3_0" ActionTag="2117808048" Tag="2" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="181.0000" RightMargin="-459.0000" TopMargin="79.0000" BottomMargin="-161.0000" FlipY="True" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="-120.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="672.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView_2" ActionTag="-704733509" Tag="4" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="192.4000" RightMargin="7.6000" TopMargin="621.2406" BottomMargin="258.7594" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ItemMargin="10" HorizontalType="Align_HorizontalCenter" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
            <Size X="440.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.2900" ScaleY="0.5000" />
            <Position X="320.0000" Y="298.7594" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3112" />
            <PreSize X="0.6875" Y="0.0833" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_47" ActionTag="1800351863" Tag="743" IconVisible="False" LeftMargin="-6.7766" RightMargin="-13.2234" TopMargin="727.3918" BottomMargin="120.6082" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="36" BottomEage="36" Scale9OriginX="33" Scale9OriginY="36" Scale9Width="34" Scale9Height="40" ctype="ImageViewObjectData">
            <Size X="660.0000" Y="112.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="323.2234" Y="176.6082" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5050" Y="0.1840" />
            <PreSize X="1.0313" Y="0.1167" />
            <FileData Type="Normal" Path="NewSkillShop/glassPanel_tab.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView_1" ActionTag="2063221482" Tag="5" IconVisible="False" LeftMargin="9.9997" RightMargin="10.0003" TopMargin="685.0210" BottomMargin="74.9790" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="-6" Scale9OriginY="-6" Scale9Width="12" Scale9Height="12" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="50" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
            <Size X="620.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="HeadDummy" ActionTag="-307444468" Alpha="0" Tag="100" IconVisible="False" RightMargin="1830.0000" TopMargin="50.0000" BottomMargin="50.0000" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
                <Size X="220.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="110.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0537" Y="0.5000" />
                <PreSize X="0.1073" Y="0.5000" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="A" ActionTag="-970130680" ZOrder="1" IconVisible="False" LeftMargin="270.0000" RightMargin="1700.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="1992838520" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable1.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive1.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive1.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="310.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1512" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="B" ActionTag="1264990484" ZOrder="2" Tag="1" IconVisible="False" LeftMargin="400.0000" RightMargin="1570.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-907171663" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable2.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive2.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive2.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="440.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2146" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="C" ActionTag="4432124" ZOrder="3" Tag="2" IconVisible="False" LeftMargin="530.0000" RightMargin="1440.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-64046810" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable3.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive3.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive3.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="570.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2780" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="D" ActionTag="68451656" ZOrder="4" Tag="3" IconVisible="False" LeftMargin="660.0000" RightMargin="1310.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-1203395680" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable4.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive4.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive4.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="700.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3415" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="E" ActionTag="-58563986" ZOrder="5" Tag="4" IconVisible="False" LeftMargin="790.0000" RightMargin="1180.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-1381555816" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable5.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive5.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive5.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="830.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4049" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="F" ActionTag="1166931053" ZOrder="6" Tag="5" IconVisible="False" LeftMargin="920.0000" RightMargin="1050.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-1132258479" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable6.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive6.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive6.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="960.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4683" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="G" ActionTag="1462126673" ZOrder="7" Tag="6" IconVisible="False" LeftMargin="1050.0000" RightMargin="920.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-1819223870" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable7.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive7.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive7.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1090.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5317" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="H" ActionTag="618502465" ZOrder="8" Tag="7" IconVisible="False" LeftMargin="1180.0000" RightMargin="790.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="2046390345" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable8.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive8.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive8.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1220.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5951" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="I" ActionTag="1755283384" ZOrder="9" Tag="8" IconVisible="False" LeftMargin="1310.0000" RightMargin="660.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-2020013957" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable9.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive9.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive9.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1350.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6585" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="J" ActionTag="-790332086" ZOrder="10" Tag="9" IconVisible="False" LeftMargin="1440.0000" RightMargin="530.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="-1576476781" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="ScoreSkillDisable10.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <PressedFileData Type="PlistSubImage" Path="ScoreSkillActive10.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <NormalFileData Type="PlistSubImage" Path="ScoreSkillActive10.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1480.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7220" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="K" ActionTag="-1019307174" ZOrder="11" Tag="10" IconVisible="False" LeftMargin="1570.0000" RightMargin="400.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="925643347" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="DisableSkillD.jpg" Plist="NewSkillShop/SkillCast.plist" />
                    <PressedFileData Type="PlistSubImage" Path="SkillD.jpg" Plist="NewSkillShop/SkillCast.plist" />
                    <NormalFileData Type="PlistSubImage" Path="SkillD.jpg" Plist="NewSkillShop/SkillCast.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1610.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7854" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="L" ActionTag="-261513722" ZOrder="12" Tag="11" IconVisible="False" LeftMargin="1700.0000" RightMargin="270.0000" TopMargin="60.0000" BottomMargin="60.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="Button_4" ActionTag="335209730" Tag="1" IconVisible="False" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="8.0000" BottomMargin="8.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.8000" Y="0.8000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="DisableSkillF.jpg" Plist="NewSkillShop/SkillCast.plist" />
                    <PressedFileData Type="PlistSubImage" Path="SkillF.jpg" Plist="NewSkillShop/SkillCast.plist" />
                    <NormalFileData Type="PlistSubImage" Path="SkillF.jpg" Plist="NewSkillShop/SkillCast.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1740.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8488" Y="0.5000" />
                <PreSize X="0.0390" Y="0.4000" />
                <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="EndDummy" ActionTag="1720161042" ZOrder="13" Alpha="0" Tag="99" IconVisible="False" LeftMargin="1830.0000" TopMargin="50.0000" BottomMargin="50.0000" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
                <Size X="220.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1940.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9463" Y="0.5000" />
                <PreSize X="0.1073" Y="0.5000" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="319.9997" Y="174.9790" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1823" />
            <PreSize X="0.9688" Y="0.2083" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="241434528" Tag="6" RotationSkewX="14.9981" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="140.0000" RightMargin="140.0000" TopMargin="563.3468" BottomMargin="343.6532" LabelText="Click to active skill !" ctype="TextBMFontObjectData">
            <Size X="360.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="370.1532" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3856" />
            <PreSize X="0.5625" Y="0.0552" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1_0" ActionTag="249548494" Tag="200" RotationSkewX="14.9982" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="181.0000" RightMargin="181.0000" TopMargin="848.1484" BottomMargin="87.8516" LabelText="Scroll to display upgradable skills !" ctype="TextBMFontObjectData">
            <Size X="278.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="99.8516" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1040" />
            <PreSize X="0.4344" Y="0.0250" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Name.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView_2_0" ActionTag="646902694" Tag="7" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="94.3520" RightMargin="105.6479" TopMargin="175.2267" BottomMargin="704.7733" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ItemMargin="50" HorizontalType="Align_HorizontalCenter" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
            <Size X="440.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.2900" ScaleY="0.5000" />
            <Position X="221.9520" Y="744.7733" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3468" Y="0.7758" />
            <PreSize X="0.6875" Y="0.0833" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="dummy" ActionTag="359914762" ZOrder="12" Tag="8" IconVisible="False" LeftMargin="-282.5892" RightMargin="842.5892" TopMargin="-31.8835" BottomMargin="911.8835" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-242.5892" Y="951.8835" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3790" Y="0.9915" />
            <PreSize X="0.1250" Y="0.0833" />
            <FileData Type="Normal" Path="NewSkillShop/metalPanel.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="-1970865392" Tag="9" IconVisible="True" LeftMargin="320.0001" RightMargin="319.9999" TopMargin="316.0134" BottomMargin="643.9866" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Text_2" ActionTag="1307341280" Tag="104" IconVisible="False" LeftMargin="-95.4997" RightMargin="-95.5003" TopMargin="25.9941" BottomMargin="-60.9941" FontSize="30" LabelText="Extra Options:" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="191.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="0.0003" Y="-43.4941" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView_3" ActionTag="-138614425" Tag="1" IconVisible="False" LeftMargin="-259.9999" RightMargin="-260.0001" TopMargin="69.9514" BottomMargin="-269.9514" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ScrollDirectionType="0" ItemMargin="5" DirectionType="Vertical" HorizontalType="Align_HorizontalCenter" ctype="ListViewObjectData">
                <Size X="520.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="Text_2_0" ActionTag="871495495" Tag="105" IconVisible="False" LeftMargin="121.0000" RightMargin="121.0000" BottomMargin="171.0000" FontSize="25" LabelText="Increase bonus hp by 2%" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="278.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="260.0000" Y="185.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="191" G="191" B="191" />
                    <PrePosition X="0.5000" Y="0.9275" />
                    <PreSize X="0.5346" Y="0.1450" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_2_0_0" ActionTag="480277218" ZOrder="1" Tag="106" IconVisible="False" LeftMargin="61.0000" RightMargin="61.0000" TopMargin="34.0000" BottomMargin="137.0000" FontSize="25" LabelText="Reduce recovery time by 2 seconds" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="398.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="260.0000" Y="151.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="191" G="191" B="191" />
                    <PrePosition X="0.5000" Y="0.7575" />
                    <PreSize X="0.7654" Y="0.1450" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_2_0_0_0" ActionTag="-1823152666" ZOrder="2" Tag="107" IconVisible="False" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="68.0000" BottomMargin="52.0000" IsCustomSize="True" FontSize="25" LabelText="Every 5 times activating, has 10% to recover 100% life" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="500.0000" Y="80.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                    <Position X="260.0000" Y="132.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="191" G="191" B="191" />
                    <PrePosition X="0.5000" Y="0.6600" />
                    <PreSize X="0.9615" Y="0.4000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="0.0001" Y="-169.9514" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0001" Y="643.9866" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6708" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_12" ActionTag="1371268220" Tag="10" RotationSkewX="-0.9268" RotationSkewY="-0.9232" IconVisible="False" LeftMargin="-16.0808" RightMargin="552.0808" TopMargin="138.0000" BottomMargin="718.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="5" BottomEage="5" Scale9OriginX="6" Scale9OriginY="5" Scale9Width="92" Scale9Height="94" ctype="ImageViewObjectData">
            <Size X="104.0000" Y="104.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="35.9192" Y="770.0000" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0561" Y="0.8021" />
            <PreSize X="0.1625" Y="0.1083" />
            <FileData Type="Normal" Path="NewSkillShop/Icon_Skill_Point.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="-1095375640" Tag="11" IconVisible="False" LeftMargin="57.9615" RightMargin="568.0385" TopMargin="178.4998" BottomMargin="758.5002" FontSize="20" LabelText="3" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="14.0000" Y="23.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="57.9615" Y="770.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="242" B="0" />
            <PrePosition X="0.0906" Y="0.8021" />
            <PreSize X="0.0219" Y="0.0240" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0" ActionTag="-759062660" Tag="999" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="245.0000" RightMargin="245.0000" TopMargin="888.6401" BottomMargin="21.3599" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="46.3599" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0483" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>