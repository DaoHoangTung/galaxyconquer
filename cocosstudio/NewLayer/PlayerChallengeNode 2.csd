<GameFile>
  <PropertyGroup Name="PlayerChallengeNode" Type="Node" ID="e1eb7c71-6faa-4f42-8344-05c5d91b0cfa" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1531" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-1067898835" Tag="1" IconVisible="False" LeftMargin="-250.0000" RightMargin="-250.0000" TopMargin="-50.0000" BottomMargin="-50.0000" ClipAble="False" BackColorAlpha="54" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="500.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Image_9_0_0" ActionTag="1908870192" VisibleForFrame="False" Tag="7" IconVisible="False" LeftMargin="404.0000" RightMargin="66.0000" TopMargin="27.0000" BottomMargin="25.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="12" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="30.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="419.0000" Y="49.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8380" Y="0.4900" />
                <PreSize X="0.0600" Y="0.4800" />
                <FileData Type="Normal" Path="NewRank/dautruong_icon_rank3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_9_0" ActionTag="1092964851" VisibleForFrame="False" Tag="6" IconVisible="False" LeftMargin="404.0000" RightMargin="66.0000" TopMargin="27.0000" BottomMargin="25.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="12" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="30.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="419.0000" Y="49.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8380" Y="0.4900" />
                <PreSize X="0.0600" Y="0.4800" />
                <FileData Type="Normal" Path="NewRank/dautruong_icon_rank2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_9" ActionTag="-1156585186" Tag="5" IconVisible="False" LeftMargin="404.0000" RightMargin="66.0000" TopMargin="27.0000" BottomMargin="25.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="12" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="30.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="419.0000" Y="49.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8380" Y="0.4900" />
                <PreSize X="0.0600" Y="0.4800" />
                <FileData Type="Normal" Path="NewRank/dautruong_icon_rank1.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-1534571428" Tag="4" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="29.2366" RightMargin="435.7634" TopMargin="33.5000" BottomMargin="31.5000" LabelText="#1" ctype="TextBMFontObjectData">
                <Size X="35.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.2366" Y="49.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0585" Y="0.4900" />
                <PreSize X="0.0700" Y="0.3500" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Sharnay_text.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="719362841" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="124.0000" RightMargin="225.0000" TopMargin="5.6147" BottomMargin="55.3853" FontSize="30" LabelText="user_name" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="151.0000" Y="39.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="124.0000" Y="74.8853" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2480" Y="0.7489" />
                <PreSize X="0.3020" Y="0.3900" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="255" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1_0_0" ActionTag="-1394609929" Tag="2" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="124.0000" RightMargin="149.0000" TopMargin="43.4726" BottomMargin="3.5274" LabelText="343,402,999" ctype="TextBMFontObjectData">
                <Size X="227.0000" Y="53.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="124.0000" Y="30.0274" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2480" Y="0.3003" />
                <PreSize X="0.4540" Y="0.5300" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_5" ActionTag="-489333067" Tag="1564" IconVisible="False" LeftMargin="-244.0005" RightMargin="-125.9995" TopMargin="51.9554" BottomMargin="-71.9554" ctype="SpriteObjectData">
            <Size X="370.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-244.0005" Y="-61.9554" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>