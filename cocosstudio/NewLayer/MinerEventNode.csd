<GameFile>
  <PropertyGroup Name="MinerEventNode" Type="Node" ID="baed7b1a-155e-40a0-b316-d5213bf1ecdc" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="973" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_19" ActionTag="-1484999912" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="-85.4060" BottomMargin="45.4060" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="13" BottomEage="13" Scale9OriginX="33" Scale9OriginY="13" Scale9Width="224" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="250.0000" Y="40.0000" />
            <Children>
              <AbstractNodeData Name="LoadingBar_1" ActionTag="2032695156" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="6.5000" BottomMargin="8.5000" ProgressInfo="9" ctype="LoadingBarObjectData">
                <Size X="190.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="125.0000" Y="21.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="144" G="238" B="144" />
                <PrePosition X="0.5000" Y="0.5250" />
                <PreSize X="0.7600" Y="0.6250" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_6" ActionTag="-1362707611" Tag="2" RotationSkewX="19.9996" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="64.5000" RightMargin="64.5000" TopMargin="33.9376" BottomMargin="-46.9376" LabelText="Level 1" ctype="TextBMFontObjectData">
                <Size X="121.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="125.0000" Y="-20.4376" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.5109" />
                <PreSize X="0.4840" Y="1.3250" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_2" ActionTag="860774091" Tag="3" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="44.6000" RightMargin="52.4000" TopMargin="-117.4840" BottomMargin="42.4840" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="123" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="153.0000" Y="115.0000" />
                <Children>
                  <AbstractNodeData Name="Particle_3" ActionTag="1449742605" Tag="1" IconVisible="True" LeftMargin="83.2480" RightMargin="69.7520" TopMargin="63.4193" BottomMargin="51.5807" ctype="ParticleObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="83.2480" Y="51.5807" />
                    <Scale ScaleX="0.4221" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5441" Y="0.4485" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewParticle/donateRewardEffect.plist" Plist="" />
                    <BlendFunc Src="770" Dst="1" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_23" ActionTag="-1803951465" Tag="2" IconVisible="False" LeftMargin="123.1453" RightMargin="12.8547" TopMargin="9.8797" BottomMargin="67.1203" LeftEage="5" RightEage="5" TopEage="12" BottomEage="12" Scale9OriginX="5" Scale9OriginY="12" Scale9Width="7" Scale9Height="14" ctype="ImageViewObjectData">
                    <Size X="17.0000" Y="38.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.6453" Y="86.1203" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8604" Y="0.7489" />
                    <PreSize X="0.1111" Y="0.3304" />
                    <FileData Type="Normal" Path="NewIAPScene/SkTet_Thamgia_Item_Hopquato_Noti.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.1000" Y="99.9840" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4844" Y="2.4996" />
                <PreSize X="0.6120" Y="2.8750" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Ruongnho.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Ruongnho.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Ruongnho.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_2" ActionTag="1030956084" Tag="4" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="125.0000" RightMargin="125.0000" TopMargin="20.2640" BottomMargin="19.7360" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="125.0000" Y="19.7360" />
                <Scale ScaleX="0.7000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4934" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/donateLevelUp.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_9" ActionTag="1388901474" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="104.0000" RightMargin="104.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="15" LabelText="0/100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="125.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.1680" Y="0.5000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Italic.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dumy" ActionTag="-410067398" Tag="6" IconVisible="False" LeftMargin="-700.4999" RightMargin="775.4999" TopMargin="5.5037" BottomMargin="-18.5037" LabelText="+305 EXP" ctype="TextBMFontObjectData">
                <Size X="175.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-612.9999" Y="7.9963" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-2.4520" Y="0.1999" />
                <PreSize X="0.7000" Y="1.3250" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="65.4060" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>