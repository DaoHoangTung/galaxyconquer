<GameFile>
  <PropertyGroup Name="NotifyRewardLayer" Type="Node" ID="6491de0e-5bde-4c82-ae0f-00f5d398263d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="843" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="375110377" Tag="951" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-320.0000" RightMargin="-320.0000" TopMargin="-480.0000" BottomMargin="-480.0000" TouchEnable="True" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="-468959589" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-320.0000" RightMargin="-320.0000" TopMargin="-250.0000" BottomMargin="-250.0000" Scale9Enable="True" LeftEage="55" RightEage="45" TopEage="45" BottomEage="55" Scale9OriginX="55" Scale9OriginY="45" Scale9Width="700" Scale9Height="500" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="500.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_2_0_0" ActionTag="1199023012" Tag="870" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="228.6691" RightMargin="198.3309" TopMargin="271.7559" BottomMargin="175.2441" LabelText="+ 5,000,000" ctype="TextBMFontObjectData">
                <Size X="213.0000" Y="53.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="-1340352513" Tag="892" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-40.9900" RightMargin="226.9900" TopMargin="15.7551" BottomMargin="11.2449" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-27.4900" Y="24.2449" />
                    <Scale ScaleX="1.2000" ScaleY="1.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1291" Y="0.4575" />
                    <PreSize X="0.1268" Y="0.4906" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="441.6691" Y="201.7441" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6901" Y="0.4035" />
                <PreSize X="0.3328" Y="0.1060" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="1575698648" Tag="871" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="145.0000" RightMargin="145.0000" TopMargin="-34.3405" BottomMargin="467.3405" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="350.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="891223436" Tag="872" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="89.0000" RightMargin="89.0000" TopMargin="2.7246" BottomMargin="14.2754" LabelText="Top Event" ctype="TextBMFontObjectData">
                    <Size X="172.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="39.2754" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5862" />
                    <PreSize X="0.4914" Y="0.7463" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="500.8405" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0017" />
                <PreSize X="0.5469" Y="0.1340" />
                <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="-219157817" Tag="873" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="220.0000" RightMargin="220.0000" TopMargin="414.6384" BottomMargin="20.3616" TouchEnable="True" FontSize="25" ButtonText="Close" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="200.0000" Y="65.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="52.8616" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1057" />
                <PreSize X="0.3125" Y="0.1300" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-1264309007" Tag="889" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="168.0003" RightMargin="167.9997" TopMargin="48.8634" BottomMargin="340.1366" FontSize="30" LabelText="Congratulation !&#xA;You have just recieved&#xA;a reward !" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="304.0000" Y="111.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0003" Y="395.6366" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7913" />
                <PreSize X="0.4750" Y="0.2220" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="255" B="255" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2_0_0_0" ActionTag="-941282000" Tag="890" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="295.9094" RightMargin="286.0906" TopMargin="352.4730" BottomMargin="110.5271" LabelText="+ 50" ctype="TextBMFontObjectData">
                <Size X="58.0000" Y="37.0000" />
                <Children>
                  <AbstractNodeData Name="Image_12" ActionTag="1236435805" Tag="891" RotationSkewX="-0.9233" RotationSkewY="-0.9294" IconVisible="False" LeftMargin="-84.4151" RightMargin="11.4151" TopMargin="-48.7108" BottomMargin="-43.2892" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                    <Size X="131.0000" Y="129.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-18.9151" Y="21.2108" />
                    <Scale ScaleX="0.2500" ScaleY="0.2500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.3261" Y="0.5733" />
                    <PreSize X="2.2586" Y="3.4865" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="353.9094" Y="129.0271" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5530" Y="0.2581" />
                <PreSize X="0.0906" Y="0.0740" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_2" ActionTag="554169528" Tag="932" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="242.2607" RightMargin="250.7393" TopMargin="139.1843" BottomMargin="213.8157" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="105" Scale9Height="105" ctype="ImageViewObjectData">
                <Size X="147.0000" Y="147.0000" />
                <AnchorPoint ScaleX="0.5397" ScaleY="0.4799" />
                <Position X="321.5952" Y="284.3636" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5025" Y="0.5687" />
                <PreSize X="0.2297" Y="0.2940" />
                <FileData Type="Normal" Path="NewIAPScene/19.99.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_3" ActionTag="1160691144" Tag="950" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="311.0640" RightMargin="328.9360" TopMargin="231.1897" BottomMargin="268.8103" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="311.0640" Y="268.8103" />
                <Scale ScaleX="0.4221" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4860" Y="0.5376" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/donateRewardEffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_2" ActionTag="-1799952350" Tag="972" IconVisible="True" LeftMargin="303.8500" RightMargin="336.1500" TopMargin="331.8631" BottomMargin="168.1369" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="303.8500" Y="168.1369" />
                <Scale ScaleX="1.0439" ScaleY="0.6907" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4748" Y="0.3363" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/IAPeffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewShopItemScene/bg_1_1.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>