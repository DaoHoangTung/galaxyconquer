<GameFile>
  <PropertyGroup Name="GoldShopNode" Type="Node" ID="c4bb444b-8384-4d73-a18f-dd85c3f6d157" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="587" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-1134213782" Tag="1" IconVisible="False" LeftMargin="-275.0000" RightMargin="-275.0000" TopMargin="-325.0000" BottomMargin="-325.0000" LeftEage="316" RightEage="316" TopEage="211" BottomEage="211" Scale9OriginX="316" Scale9OriginY="211" Scale9Width="328" Scale9Height="218" ctype="ImageViewObjectData">
            <Size X="550.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="283466563" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="318.0000" RightMargin="55.0000" TopMargin="12.1150" BottomMargin="587.8850" LabelText="Gold Shop" ctype="TextBMFontObjectData">
                <Size X="177.0000" Y="50.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="495.0000" Y="612.8850" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9000" Y="0.9429" />
                <PreSize X="0.3218" Y="0.0769" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="1110311116" Tag="2" IconVisible="False" LeftMargin="-16.7560" RightMargin="534.7560" TopMargin="635.0538" BottomMargin="-16.0538" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-0.7560" Y="-0.5538" />
                <Scale ScaleX="1.3000" ScaleY="1.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.0014" Y="-0.0009" />
                <PreSize X="0.0582" Y="0.0477" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView_1" ActionTag="-1376544199" Tag="3" IconVisible="False" PositionPercentXEnabled="True" TopMargin="101.0768" BottomMargin="98.9232" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="20" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="550.0000" Y="450.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_4" ActionTag="-839522031" Tag="621" IconVisible="False" BottomMargin="325.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="550.0000" Y="125.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="1652921554" Tag="1634" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="275.0000" RightMargin="275.0000" TopMargin="62.5000" BottomMargin="62.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="275.0000" Y="62.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/GoldItemNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="325.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.7222" />
                    <PreSize X="1.0000" Y="0.2778" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="275.0000" Y="548.9232" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8445" />
                <PreSize X="1.0000" Y="0.6923" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_2" ActionTag="-41994746" Tag="4" IconVisible="False" LeftMargin="39.5982" RightMargin="473.4018" TopMargin="25.1225" BottomMargin="588.8775" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                <Size X="37.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-1910240987" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="47.9954" RightMargin="-158.9954" TopMargin="-8.5002" BottomMargin="-8.4998" LabelText="355,000" ctype="TextBMFontObjectData">
                    <Size X="148.0000" Y="53.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="47.9954" Y="18.0002" />
                    <Scale ScaleX="0.6000" ScaleY="0.6000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.2972" Y="0.5000" />
                    <PreSize X="4.0000" Y="1.4722" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.0982" Y="606.8775" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1056" Y="0.9337" />
                <PreSize X="0.0673" Y="0.0554" />
                <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-876176123" Tag="1928" RotationSkewX="15.0016" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-88.3949" RightMargin="87.3949" TopMargin="569.2582" BottomMargin="27.7418" LabelText="*Join Miner Event to re-stock !" ctype="TextBMFontObjectData">
                <Size X="551.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="187.1051" Y="54.2418" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3402" Y="0.0834" />
                <PreSize X="1.0018" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0" ActionTag="648800536" Tag="1940" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="373.6728" RightMargin="26.3271" TopMargin="572.8162" BottomMargin="27.1838" TouchEnable="True" FontSize="25" ButtonText="Go now !" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="448.6728" Y="52.1838" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8158" Y="0.0803" />
                <PreSize X="0.2727" Y="0.0769" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewGoldShop/newboard_BG_green.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>