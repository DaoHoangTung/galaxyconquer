<GameFile>
  <PropertyGroup Name="UserPanelNode" Type="Node" ID="651040b2-6afe-4277-987a-468d54278a18" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="385" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-465972248" Tag="1" IconVisible="False" LeftMargin="-300.0000" RightMargin="-300.0000" TopMargin="-300.0000" BottomMargin="-300.0000" Scale9Enable="True" LeftEage="88" RightEage="88" TopEage="13" BottomEage="13" Scale9OriginX="88" Scale9OriginY="13" Scale9Width="114" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="600.0000" Y="600.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="-1630155034" Tag="2" IconVisible="True" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLayer/UserPanelInfoNode.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_8_0" ActionTag="759384137" Tag="3" IconVisible="False" LeftMargin="-310.2543" RightMargin="278.2543" TopMargin="282.1537" BottomMargin="-313.1537" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="32.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-294.2543" Y="-297.6537" />
            <Scale ScaleX="1.3000" ScaleY="1.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_17" ActionTag="-1538149945" Tag="4" IconVisible="False" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="239.4596" BottomMargin="-286.4596" Scale9Enable="True" LeftEage="105" RightEage="105" TopEage="15" BottomEage="15" Scale9OriginX="105" Scale9OriginY="15" Scale9Width="110" Scale9Height="17" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="47.0000" />
            <Children>
              <AbstractNodeData Name="Node_1" ActionTag="-1432583758" Tag="1" IconVisible="True" RightMargin="320.0000" TopMargin="47.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="Image_19" ActionTag="472880573" Tag="1" IconVisible="False" LeftMargin="0.1999" RightMargin="-78.1999" TopMargin="-47.0000" BottomMargin="1.0000" LeftEage="25" RightEage="25" TopEage="15" BottomEage="15" Scale9OriginX="25" Scale9OriginY="15" Scale9Width="28" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="78.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="39.1999" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewUserPanel/dialog_TabBar4_selected_left.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Button_5" ActionTag="-687287265" Tag="10" IconVisible="False" LeftMargin="8.8619" RightMargin="-68.8619" TopMargin="-43.7468" BottomMargin="3.7468" TouchEnable="True" FontSize="13" ButtonText="Ganeral" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="260" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="60.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="38.8619" Y="23.7468" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_1_0" ActionTag="467192931" Tag="2" IconVisible="True" LeftMargin="79.0000" RightMargin="241.0000" TopMargin="47.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="Image_19" ActionTag="-751294210" VisibleForFrame="False" Tag="1" IconVisible="False" RightMargin="-80.0000" TopMargin="-47.0000" BottomMargin="1.0000" LeftEage="25" RightEage="25" TopEage="15" BottomEage="15" Scale9OriginX="25" Scale9OriginY="15" Scale9Width="30" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="80.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewUserPanel/dialog_TabBar4_selected_mid.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Button_5" ActionTag="-885936394" Tag="11" IconVisible="False" LeftMargin="8.8619" RightMargin="-68.8619" TopMargin="-43.7468" BottomMargin="3.7468" TouchEnable="True" FontSize="13" ButtonText="Setting" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="15" Scale9Height="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="60.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="38.8619" Y="23.7468" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="79.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2469" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_1_0_0" ActionTag="1157966657" Tag="3" IconVisible="True" LeftMargin="160.2978" RightMargin="159.7022" TopMargin="47.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="Image_19" ActionTag="287264169" VisibleForFrame="False" Tag="1" IconVisible="False" RightMargin="-80.0000" TopMargin="-47.0000" BottomMargin="1.0000" LeftEage="25" RightEage="25" TopEage="15" BottomEage="15" Scale9OriginX="25" Scale9OriginY="15" Scale9Width="30" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="80.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewUserPanel/dialog_TabBar4_selected_mid.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Button_5" ActionTag="844474591" Tag="12" IconVisible="False" LeftMargin="8.8619" RightMargin="-68.8619" TopMargin="-43.7468" BottomMargin="3.7468" TouchEnable="True" FontSize="13" ButtonText="Mail" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="12" Scale9OriginY="6" Scale9Width="3" Scale9Height="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="60.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="38.8619" Y="23.7468" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="160.2978" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5009" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_1_0_0_0" ActionTag="-477370588" Tag="4" IconVisible="True" LeftMargin="240.7139" RightMargin="79.2861" TopMargin="47.0968" BottomMargin="-0.0968" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="Image_19" ActionTag="1401788072" VisibleForFrame="False" Tag="1" IconVisible="False" LeftMargin="0.5000" RightMargin="-79.5000" TopMargin="-47.0000" BottomMargin="1.0000" LeftEage="25" RightEage="25" TopEage="15" BottomEage="15" Scale9OriginX="25" Scale9OriginY="15" Scale9Width="29" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="79.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewUserPanel/dialog_TabBar4_selected_right.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Button_5" ActionTag="749871316" Tag="13" IconVisible="False" LeftMargin="8.8619" RightMargin="-68.8619" TopMargin="-43.7468" BottomMargin="3.7468" TouchEnable="True" FontSize="13" ButtonText="Report" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="12" Scale9Height="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="60.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="38.8619" Y="23.7468" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="240.7139" Y="-0.0968" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7522" Y="-0.0021" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-262.9596" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/dialog_TabBar_4.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>