<GameFile>
  <PropertyGroup Name="ConfirmShoppingItem" Type="Layer" ID="57b2fe3c-2876-408a-a1e8-a3b662802725" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="164" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1270394456" Tag="1" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="222" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-486867343" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="95.0000" RightMargin="95.0000" TopMargin="330.0000" BottomMargin="330.0000" Scale9Enable="True" LeftEage="55" RightEage="45" TopEage="45" BottomEage="55" Scale9OriginX="55" Scale9OriginY="45" Scale9Width="1" Scale9Height="1" ctype="ImageViewObjectData">
                <Size X="450.0000" Y="300.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="480.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7031" Y="0.3125" />
                <FileData Type="Normal" Path="NewSkillShop/glassPanel_corners.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="1415468733" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="152.5002" RightMargin="152.4998" TopMargin="365.3759" BottomMargin="524.6241" FontSize="30" LabelText="Are you sure to purchase &#xA;this item ?" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="335.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="320.0002" Y="594.6241" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6194" />
                <PreSize X="0.5234" Y="0.0729" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_2" ActionTag="-1491326211" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="288.0000" RightMargin="288.0000" TopMargin="455.4659" BottomMargin="440.5341" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="472.5341" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4922" />
                <PreSize X="0.1000" Y="0.0667" />
                <FileData Type="Normal" Path="NewRewardLayer/reward0.jpg" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="134466476" Tag="4" IconVisible="False" LeftMargin="524.2547" RightMargin="83.7453" TopMargin="321.9397" BottomMargin="607.0603" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.2547" Y="622.5603" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8441" Y="0.6485" />
                <PreSize X="0.0500" Y="0.0323" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="1932963562" Tag="5" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="181.0000" RightMargin="181.0000" TopMargin="532.5221" BottomMargin="345.4779" TouchEnable="True" FontSize="40" ButtonText="OK" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="386.4779" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4026" />
                <PreSize X="0.4344" Y="0.0854" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>