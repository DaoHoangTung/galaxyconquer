<GameFile>
  <PropertyGroup Name="SpecialEventLayer" Type="Node" ID="d50e3990-9cde-4205-932d-1cd14673c0c0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="884" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="451533652" Tag="1" IconVisible="False" LeftMargin="-275.0000" RightMargin="-275.0000" TopMargin="-325.0000" BottomMargin="-325.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="95" RightEage="95" TopEage="13" BottomEage="13" Scale9OriginX="95" Scale9OriginY="13" Scale9Width="100" Scale9Height="14" ctype="PanelObjectData">
            <Size X="550.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="ListView_1" ActionTag="974758065" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="115.1002" BottomMargin="134.8998" TouchEnable="True" ClipAble="True" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" HorizontalType="Align_HorizontalCenter" ctype="ListViewObjectData">
                <Size X="500.0000" Y="400.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_3" ActionTag="-866262436" Tag="1141" IconVisible="False" BottomMargin="230.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="500.0000" Y="170.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="-1559880994" Tag="1168" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="250.0000" RightMargin="250.0000" TopMargin="85.0000" BottomMargin="85.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="250.0000" Y="85.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/SpecialEventNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="230.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.5750" />
                    <PreSize X="1.0000" Y="0.4250" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="334.8998" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5152" />
                <PreSize X="0.9091" Y="0.6154" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="77492269" Tag="2" RotationSkewX="14.9986" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-34.5000" RightMargin="-34.5000" TopMargin="42.3055" BottomMargin="554.6945" LabelText="*Join events to get more rewards !" ctype="TextBMFontObjectData">
                <Size X="619.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="581.1945" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8941" />
                <PreSize X="1.1255" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0_0" ActionTag="-1234202170" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="200.0000" RightMargin="200.0000" TopMargin="571.5332" BottomMargin="28.4668" TouchEnable="True" FontSize="25" ButtonText="Exit" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="53.4668" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0823" />
                <PreSize X="0.2727" Y="0.0769" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="freencoin_rollcall_highlight_5" ActionTag="863664459" Tag="1491" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="273.5000" RightMargin="273.5000" TopMargin="424.9026" BottomMargin="30.0974" ctype="SpriteObjectData">
                <Size X="3.0000" Y="195.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="127.5974" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1963" />
                <PreSize X="0.0055" Y="0.3000" />
                <FileData Type="PlistSubImage" Path="freencoin_rollcall_highlight.png" Plist="NewDailyReward/FreeCoinRollcall.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="freencoin_rollcall_highlight_5_0" ActionTag="-614332258" Tag="1492" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="250.2350" RightMargin="296.7650" TopMargin="16.0125" BottomMargin="438.9875" ctype="SpriteObjectData">
                <Size X="3.0000" Y="195.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="251.7350" Y="536.4875" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4577" Y="0.8254" />
                <PreSize X="0.0055" Y="0.3000" />
                <FileData Type="PlistSubImage" Path="freencoin_rollcall_highlight.png" Plist="NewDailyReward/FreeCoinRollcall.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_3" ActionTag="-1534642239" Tag="1118" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-175.0000" RightMargin="-175.0000" TopMargin="-363.0117" BottomMargin="296.0117" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
            <Size X="350.0000" Y="67.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-272689742" Tag="1119" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.5000" RightMargin="58.5000" TopMargin="1.7263" BottomMargin="15.2737" LabelText="Special Event" ctype="TextBMFontObjectData">
                <Size X="233.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="175.0000" Y="40.2737" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6011" />
                <PreSize X="0.6657" Y="0.7463" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="329.5117" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_2" ActionTag="113841696" Tag="751" IconVisible="True" LeftMargin="5.0437" RightMargin="-5.0437" TopMargin="-249.4074" BottomMargin="249.4074" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="5.0437" Y="249.4074" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewDailyReward/missionRewardEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>