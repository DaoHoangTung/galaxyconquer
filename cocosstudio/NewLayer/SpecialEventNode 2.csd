<GameFile>
  <PropertyGroup Name="SpecialEventNode" Type="Node" ID="a74742ca-e76a-4f5f-9300-46bfbcb200b6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1120" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="2125854478" Tag="1" IconVisible="False" LeftMargin="-250.0000" RightMargin="-250.0000" TopMargin="-85.0000" BottomMargin="-85.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="500.0000" Y="170.0000" />
            <Children>
              <AbstractNodeData Name="Button_4" ActionTag="-1872283033" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="42.2500" RightMargin="393.7500" TopMargin="40.0970" BottomMargin="65.9030" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.2500" Y="97.9030" />
                <Scale ScaleX="1.3500" ScaleY="1.3500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1485" Y="0.5759" />
                <PreSize X="0.1280" Y="0.3765" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="event1.jpg" Plist="SpecialEvent/eventIcon.plist" />
                <PressedFileData Type="PlistSubImage" Path="event1.jpg" Plist="SpecialEvent/eventIcon.plist" />
                <NormalFileData Type="PlistSubImage" Path="event1.jpg" Plist="SpecialEvent/eventIcon.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_5" ActionTag="-270347450" Tag="1335" IconVisible="False" PositionPercentXEnabled="True" RightMargin="130.0000" TopMargin="140.6384" BottomMargin="9.3616" ctype="SpriteObjectData">
                <Size X="370.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position Y="19.3616" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.1139" />
                <PreSize X="0.7400" Y="0.1176" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-590100238" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="142.3000" RightMargin="171.7000" TopMargin="32.1814" BottomMargin="109.8186" LabelText="Devil Space" ctype="TextBMFontObjectData">
                <Size X="186.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="142.3000" Y="123.8186" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2846" Y="0.7283" />
                <PreSize X="0.3720" Y="0.1647" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_33" ActionTag="1269250935" Tag="3" IconVisible="False" LeftMargin="142.1152" RightMargin="241.8848" TopMargin="77.8643" BottomMargin="55.1357" LabelText="00:00:00" ctype="TextBMFontObjectData">
                <Size X="116.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="142.1152" Y="73.6357" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2842" Y="0.4332" />
                <PreSize X="0.2320" Y="0.2176" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_2" ActionTag="1203919709" Tag="4" IconVisible="False" LeftMargin="381.5162" RightMargin="18.4838" TopMargin="55.9838" BottomMargin="69.0162" TouchEnable="True" FontSize="20" ButtonText="Join" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="169" Scale9Height="32" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="100.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="431.5162" Y="91.5162" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8630" Y="0.5383" />
                <PreSize X="0.2000" Y="0.2647" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="SpecialEvent/btnJoin.png" Plist="" />
                <PressedFileData Type="Normal" Path="SpecialEvent/btnJoin.png" Plist="" />
                <NormalFileData Type="Normal" Path="SpecialEvent/btnJoin.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="button_icon_support_5" ActionTag="-1792029830" Tag="1362" IconVisible="False" LeftMargin="-225.5060" RightMargin="200.5060" TopMargin="15.7513" BottomMargin="-40.7513" ctype="SpriteObjectData">
            <Size X="25.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-213.0060" Y="-28.2513" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="SpecialEvent/button_icon_support.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>