<GameFile>
  <PropertyGroup Name="UserPanelMailNode" Type="Node" ID="cc9a8290-4180-4a08-966d-b42a43baf68c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="683" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="1810254915" VisibleForFrame="False" Tag="690" IconVisible="False" LeftMargin="-274.9999" RightMargin="-275.0001" TopMargin="-300.0004" BottomMargin="-299.9996" Scale9Enable="True" LeftEage="147" RightEage="147" TopEage="77" BottomEage="77" Scale9OriginX="147" Scale9OriginY="77" Scale9Width="48" Scale9Height="31" ctype="ImageViewObjectData">
            <Size X="550.0000" Y="600.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0001" Y="0.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/report_dropdownMenu.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2" ActionTag="24715021" Tag="689" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-223.0000" RightMargin="-223.0000" TopMargin="-188.4339" BottomMargin="186.4339" LeftEage="147" RightEage="147" Scale9OriginX="147" Scale9Width="152" Scale9Height="2" ctype="ImageViewObjectData">
            <Size X="446.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="187.4339" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/dialog_popup_line.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="646014152" Tag="687" IconVisible="True" TopMargin="-228.9824" BottomMargin="228.9824" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="228.9824" />
            <Scale ScaleX="0.3322" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/UserPanelEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="-1075634725" Tag="686" IconVisible="False" LeftMargin="-38.0000" RightMargin="-38.0000" TopMargin="-272.4640" BottomMargin="219.4640" LabelText="Mail" ctype="TextBMFontObjectData">
            <Size X="76.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="245.9640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_12_0" ActionTag="1492982934" Tag="2553" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-103.5000" RightMargin="-103.5000" TopMargin="-22.1336" BottomMargin="-12.8664" FontSize="28" LabelText="Comming soon !" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="207.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="4.6336" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="128" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>