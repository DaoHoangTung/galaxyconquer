<GameFile>
  <PropertyGroup Name="SPShopNode" Type="Node" ID="3e12834e-b7d4-4ba0-8825-104752e674d5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="587" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-1134213782" Tag="1" IconVisible="False" LeftMargin="-275.0000" RightMargin="-275.0000" TopMargin="-325.0000" BottomMargin="-325.0000" LeftEage="316" RightEage="316" TopEage="211" BottomEage="211" Scale9OriginX="316" Scale9OriginY="211" Scale9Width="328" Scale9Height="218" ctype="ImageViewObjectData">
            <Size X="550.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="283466563" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="346.0000" RightMargin="55.0000" TopMargin="12.1150" BottomMargin="587.8850" LabelText="SP Shop" ctype="TextBMFontObjectData">
                <Size X="149.0000" Y="50.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="495.0000" Y="612.8850" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9000" Y="0.9429" />
                <PreSize X="0.2709" Y="0.0769" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="1110311116" Tag="2" IconVisible="False" LeftMargin="-14.2421" RightMargin="532.2421" TopMargin="635.1586" BottomMargin="-16.1586" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1.7579" Y="-0.6586" />
                <Scale ScaleX="1.3000" ScaleY="1.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0032" Y="-0.0010" />
                <PreSize X="0.0582" Y="0.0477" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView_1" ActionTag="-1376544199" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-0.0002" RightMargin="0.0002" TopMargin="99.4428" BottomMargin="100.5572" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="20" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="550.0000" Y="450.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_4" ActionTag="-839522031" Tag="621" IconVisible="False" BottomMargin="370.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="550.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="-586258814" Tag="706" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="275.0000" RightMargin="275.0000" TopMargin="40.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="275.0000" Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/SPItemNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="370.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.8222" />
                    <PreSize X="1.0000" Y="0.1778" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Panel_4_0" ActionTag="-815066346" ZOrder="1" Tag="1125" IconVisible="False" TopMargin="100.0000" BottomMargin="270.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="550.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="1930707304" Tag="1126" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="275.0000" RightMargin="275.0000" TopMargin="40.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="275.0000" Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/SPItemNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="270.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.6000" />
                    <PreSize X="1.0000" Y="0.1778" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Panel_4_1" ActionTag="-1775858441" ZOrder="2" Tag="1138" IconVisible="False" TopMargin="200.0000" BottomMargin="170.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="550.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="283443594" Tag="1139" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="275.0000" RightMargin="275.0000" TopMargin="40.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="275.0000" Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/SPItemNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="170.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.3778" />
                    <PreSize X="1.0000" Y="0.1778" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="274.9998" Y="550.5572" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8470" />
                <PreSize X="1.0000" Y="0.6923" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_46" ActionTag="755153504" Tag="4" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-62.5953" RightMargin="296.5953" TopMargin="-14.9900" BottomMargin="536.9900" LeftEage="104" RightEage="104" TopEage="42" BottomEage="42" Scale9OriginX="104" Scale9OriginY="42" Scale9Width="108" Scale9Height="44" ctype="ImageViewObjectData">
                <Size X="316.0000" Y="128.0000" />
                <Children>
                  <AbstractNodeData Name="Text_20" ActionTag="798615253" Tag="1" IconVisible="False" LeftMargin="131.2852" RightMargin="41.7148" TopMargin="35.7773" BottomMargin="39.2227" FontSize="45" LabelText="10,200" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="143.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="202.7852" Y="65.7227" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6417" Y="0.5135" />
                    <PreSize X="0.4525" Y="0.4141" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="95.4047" Y="600.9900" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1735" Y="0.9246" />
                <PreSize X="0.5745" Y="0.1969" />
                <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1854751135" Tag="2285" RotationSkewX="15.0016" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-62.4966" RightMargin="61.4966" TopMargin="562.2578" BottomMargin="34.7422" LabelText="*Join Miner Event to re-stock !" ctype="TextBMFontObjectData">
                <Size X="551.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="213.0034" Y="61.2422" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3873" Y="0.0942" />
                <PreSize X="1.0018" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0" ActionTag="-924617087" Tag="2284" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="380.9996" RightMargin="19.0004" TopMargin="563.8162" BottomMargin="36.1839" TouchEnable="True" FontSize="25" ButtonText="Go now !" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="455.9996" Y="61.1839" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8291" Y="0.0941" />
                <PreSize X="0.2727" Y="0.0769" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewSPShop/newboard_BG_blue.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>