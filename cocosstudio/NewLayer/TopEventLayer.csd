<GameFile>
  <PropertyGroup Name="TopEventLayer" Type="Layer" ID="a044046f-a78d-4474-9890-fd64b17c9f0d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="164" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1270394456" Tag="1" IconVisible="False" LeftMargin="-0.0001" RightMargin="0.0001" TopMargin="0.0004" BottomMargin="-0.0004" TouchEnable="True" ClipAble="False" BackColorAlpha="222" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-486867343" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="180.0000" BottomMargin="180.0000" Scale9Enable="True" LeftEage="55" RightEage="45" TopEage="45" BottomEage="55" Scale9OriginX="55" Scale9OriginY="45" Scale9Width="1" Scale9Height="1" ctype="ImageViewObjectData">
                <Size X="525.0000" Y="600.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="480.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.8203" Y="0.6250" />
                <FileData Type="Normal" Path="NewSkillShop/glassPanel_corners.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_2" ActionTag="-1491326211" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-3.6095" RightMargin="496.6095" TopMargin="98.5000" BottomMargin="714.5000" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="105" Scale9Height="105" ctype="ImageViewObjectData">
                <Size X="147.0000" Y="147.0000" />
                <Children>
                  <AbstractNodeData Name="Particle_3" ActionTag="-316081204" Tag="1459" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="73.5000" RightMargin="73.5000" TopMargin="92.4193" BottomMargin="54.5807" ctype="ParticleObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="73.5000" Y="54.5807" />
                    <Scale ScaleX="0.4221" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3713" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewParticle/donateRewardEffect.plist" Plist="" />
                    <BlendFunc Src="770" Dst="1" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="69.8905" Y="788.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1092" Y="0.8208" />
                <PreSize X="0.2297" Y="0.1531" />
                <FileData Type="Normal" Path="NewIAPScene/99.99.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="134466476" Tag="2" IconVisible="False" LeftMargin="47.5012" RightMargin="560.4988" TopMargin="758.7958" BottomMargin="170.2042" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.5012" Y="185.7042" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0992" Y="0.1934" />
                <PreSize X="0.0500" Y="0.0323" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="2126566098" Tag="1430" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="145.0000" RightMargin="145.0000" TopMargin="148.2875" BottomMargin="744.7125" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="350.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-454095968" Tag="1428" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="89.0000" RightMargin="89.0000" TopMargin="2.7246" BottomMargin="14.2754" LabelText="Top Event" ctype="TextBMFontObjectData">
                    <Size X="172.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="39.2754" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5862" />
                    <PreSize X="0.4914" Y="0.7463" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="778.2125" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8106" />
                <PreSize X="0.5469" Y="0.0698" />
                <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0" ActionTag="-2144811614" Tag="1460" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="113.0000" RightMargin="113.0000" TopMargin="340.3247" BottomMargin="576.6753" FontSize="35" LabelText="- Reward for this Season -" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="414.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="598.1753" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6231" />
                <PreSize X="0.6469" Y="0.0448" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="30" G="144" B="255" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1_0" ActionTag="1892677098" Tag="1462" RotationSkewX="14.9998" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="109.5000" RightMargin="109.5000" TopMargin="227.7014" BottomMargin="642.2986" FontSize="24" LabelText="When event ends, top players have the &#xA;highest score will be rewarded&#xA; as their hard work !" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="421.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="687.2986" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7159" />
                <PreSize X="0.6578" Y="0.0938" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="127" G="127" B="127" />
                <ShadowColor A="255" R="255" G="165" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_1" ActionTag="-68783479" Tag="231" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="270.2277" RightMargin="369.7723" TopMargin="469.9724" BottomMargin="490.0276" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_2_0_0" ActionTag="-38794942" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-112.0000" TopMargin="-71.5000" BottomMargin="18.5000" LabelText="Top 1:" ctype="TextBMFontObjectData">
                    <Size X="112.0000" Y="53.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="-978574362" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="171.4831" RightMargin="-220.4831" TopMargin="8.0000" BottomMargin="8.0000" FontSize="30" LabelText="+ 3,000,000" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="161.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_2" ActionTag="864433082" Tag="282" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-34.5759" RightMargin="168.5759" TopMargin="6.5667" BottomMargin="4.4333" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="27.0000" Y="26.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0759" Y="17.4333" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.1309" Y="0.4712" />
                            <PreSize X="0.1677" Y="0.7027" />
                            <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="171.4831" Y="26.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.5311" Y="0.5000" />
                        <PreSize X="1.4375" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="173" G="216" B="230" />
                        <ShadowColor A="255" R="30" G="144" B="255" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_1_0_0_0_0_0" ActionTag="-515301016" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.9822" RightMargin="-118.9822" TopMargin="47.7500" BottomMargin="-31.7500" FontSize="30" LabelText="+ 50 " VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="60.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_12" ActionTag="-215383788" Tag="1253" RotationSkewX="-0.9233" RotationSkewY="-0.9294" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-86.5000" RightMargin="15.5000" TopMargin="-43.1019" BottomMargin="-48.8981" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                            <Size X="131.0000" Y="129.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0000" Y="15.6019" />
                            <Scale ScaleX="0.2500" ScaleY="0.2500" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.3500" Y="0.4217" />
                            <PreSize X="2.1833" Y="3.4865" />
                            <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="170.9822" Y="-13.2500" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.5266" Y="-0.2500" />
                        <PreSize X="0.5357" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="128" G="0" B="128" />
                        <ShadowColor A="255" R="128" G="0" B="128" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position Y="45.0000" />
                    <Scale ScaleX="0.8000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_2_0_0_0_0_0" ActionTag="-89532229" Tag="4" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-174.0000" TopMargin="198.5000" BottomMargin="-251.5000" LabelText="Top 4-10:" ctype="TextBMFontObjectData">
                    <Size X="174.0000" Y="53.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="-1383181277" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="233.0000" RightMargin="-195.0000" TopMargin="8.0000" BottomMargin="8.0000" FontSize="30" LabelText="+ 800,000" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="136.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_2" ActionTag="33771731" Tag="1266" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-34.5759" RightMargin="143.5759" TopMargin="6.5667" BottomMargin="4.4333" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="27.0000" Y="26.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0759" Y="17.4333" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.1550" Y="0.4712" />
                            <PreSize X="0.1985" Y="0.7027" />
                            <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="233.0000" Y="26.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.3391" Y="0.5000" />
                        <PreSize X="0.7816" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="173" G="216" B="230" />
                        <ShadowColor A="255" R="30" G="144" B="255" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_1_0_0_0_0_0_0" ActionTag="357238441" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="233.0000" RightMargin="-102.0000" TopMargin="47.7500" BottomMargin="-31.7500" FontSize="30" LabelText="+ 5" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="43.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_12" ActionTag="-106861808" Tag="1297" RotationSkewX="-0.9233" RotationSkewY="-0.9294" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-86.5000" RightMargin="-1.5000" TopMargin="-43.1019" BottomMargin="-48.8981" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                            <Size X="131.0000" Y="129.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0000" Y="15.6019" />
                            <Scale ScaleX="0.2500" ScaleY="0.2500" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.4884" Y="0.4217" />
                            <PreSize X="3.0465" Y="3.4865" />
                            <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="233.0000" Y="-13.2500" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.3391" Y="-0.2500" />
                        <PreSize X="0.2471" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="128" G="0" B="128" />
                        <ShadowColor A="255" R="128" G="0" B="128" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position Y="-225.0000" />
                    <Scale ScaleX="0.8000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_2_0_0_0" ActionTag="1856239672" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-112.0000" TopMargin="18.5000" BottomMargin="-71.5000" LabelText="Top 2:" ctype="TextBMFontObjectData">
                    <Size X="112.0000" Y="53.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="1185118983" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="171.4831" RightMargin="-220.4831" TopMargin="8.0000" BottomMargin="8.0000" FontSize="30" LabelText="+ 2,000,000" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="161.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_2" ActionTag="-1803628895" Tag="1288" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-34.5759" RightMargin="168.5759" TopMargin="6.5667" BottomMargin="4.4333" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="27.0000" Y="26.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0759" Y="17.4333" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.1309" Y="0.4712" />
                            <PreSize X="0.1677" Y="0.7027" />
                            <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="171.4831" Y="26.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.5311" Y="0.5000" />
                        <PreSize X="1.4375" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="173" G="216" B="230" />
                        <ShadowColor A="255" R="30" G="144" B="255" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_1_0_0_0_0_0" ActionTag="-2054555767" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.9822" RightMargin="-118.9822" TopMargin="47.7500" BottomMargin="-31.7500" FontSize="30" LabelText="+ 25 " VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="60.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_12" ActionTag="-203267081" Tag="1290" RotationSkewX="-0.9233" RotationSkewY="-0.9294" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-86.5000" RightMargin="15.5000" TopMargin="-43.1019" BottomMargin="-48.8981" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                            <Size X="131.0000" Y="129.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0000" Y="15.6019" />
                            <Scale ScaleX="0.2500" ScaleY="0.2500" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.3500" Y="0.4217" />
                            <PreSize X="2.1833" Y="3.4865" />
                            <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="170.9822" Y="-13.2500" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.5266" Y="-0.2500" />
                        <PreSize X="0.5357" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="128" G="0" B="128" />
                        <ShadowColor A="255" R="128" G="0" B="128" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position Y="-45.0000" />
                    <Scale ScaleX="0.8000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_2_0_0_0_0" ActionTag="-203101474" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-112.0000" TopMargin="108.5000" BottomMargin="-161.5000" LabelText="Top 3:" ctype="TextBMFontObjectData">
                    <Size X="112.0000" Y="53.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="-9058980" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="171.4831" RightMargin="-220.4831" TopMargin="8.0000" BottomMargin="8.0000" FontSize="30" LabelText="+ 1,000,000" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="161.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_2" ActionTag="-1180460751" Tag="1293" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-34.5759" RightMargin="168.5759" TopMargin="6.5667" BottomMargin="4.4333" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="27.0000" Y="26.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0759" Y="17.4333" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.1309" Y="0.4712" />
                            <PreSize X="0.1677" Y="0.7027" />
                            <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="171.4831" Y="26.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.5311" Y="0.5000" />
                        <PreSize X="1.4375" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="173" G="216" B="230" />
                        <ShadowColor A="255" R="30" G="144" B="255" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_1_0_0_0_0_0" ActionTag="103502538" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.9822" RightMargin="-118.9822" TopMargin="47.7500" BottomMargin="-31.7500" FontSize="30" LabelText="+ 10 " VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="60.0000" Y="37.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_12" ActionTag="413508804" Tag="1295" RotationSkewX="-0.9233" RotationSkewY="-0.9294" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-86.5000" RightMargin="15.5000" TopMargin="-43.1019" BottomMargin="-48.8981" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                            <Size X="131.0000" Y="129.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="-21.0000" Y="15.6019" />
                            <Scale ScaleX="0.2500" ScaleY="0.2500" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.3500" Y="0.4217" />
                            <PreSize X="2.1833" Y="3.4865" />
                            <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="170.9822" Y="-13.2500" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.5266" Y="-0.2500" />
                        <PreSize X="0.5357" Y="0.6981" />
                        <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="128" G="0" B="128" />
                        <ShadowColor A="255" R="128" G="0" B="128" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position Y="-135.0000" />
                    <Scale ScaleX="0.8000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="270.2277" Y="490.0276" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4222" Y="0.5104" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_2_0" ActionTag="1334270005" Tag="1249" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="497.6635" RightMargin="-4.6635" TopMargin="98.1501" BottomMargin="714.8499" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="105" Scale9Height="105" ctype="ImageViewObjectData">
                <Size X="147.0000" Y="147.0000" />
                <Children>
                  <AbstractNodeData Name="Particle_3" ActionTag="-798598408" Tag="1250" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="73.5000" RightMargin="73.5000" TopMargin="92.4193" BottomMargin="54.5807" ctype="ParticleObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="73.5000" Y="54.5807" />
                    <Scale ScaleX="0.4221" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3713" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewParticle/donateRewardEffect.plist" Plist="" />
                    <BlendFunc Src="770" Dst="1" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="571.1635" Y="788.3499" />
                <Scale ScaleX="-0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8924" Y="0.8212" />
                <PreSize X="0.2297" Y="0.1531" />
                <FileData Type="Normal" Path="NewIAPScene/99.99.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="319.9999" Y="479.9996" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>