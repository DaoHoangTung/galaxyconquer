<GameFile>
  <PropertyGroup Name="VersionInfoLayer" Type="Layer" ID="7c979752-1942-49d8-9668-2016d1b14dd2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="164" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1270394456" Tag="1" IconVisible="False" TopMargin="0.0002" BottomMargin="-0.0002" TouchEnable="True" ClipAble="False" BackColorAlpha="222" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-486867343" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="180.0000" BottomMargin="180.0000" Scale9Enable="True" LeftEage="55" RightEage="45" TopEage="45" BottomEage="55" Scale9OriginX="55" Scale9OriginY="45" Scale9Width="1" Scale9Height="1" ctype="ImageViewObjectData">
                <Size X="525.0000" Y="600.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="480.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.8203" Y="0.6250" />
                <FileData Type="Normal" Path="NewSkillShop/glassPanel_corners.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="134466476" Tag="2" IconVisible="False" LeftMargin="46.5081" RightMargin="561.4919" TopMargin="754.7991" BottomMargin="174.2009" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.5081" Y="189.7009" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0977" Y="0.1976" />
                <PreSize X="0.0500" Y="0.0323" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="1932963562" VisibleForFrame="False" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="132.5000" RightMargin="132.5000" TopMargin="627.9988" BottomMargin="232.0012" TouchEnable="True" FontSize="40" ButtonText="Enter Now !" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="375.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="282.0012" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2938" />
                <PreSize X="0.5859" Y="0.1042" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="2126566098" Tag="1430" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="145.0000" RightMargin="145.0000" TopMargin="148.2875" BottomMargin="744.7125" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="350.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-454095968" Tag="1428" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="76.5000" RightMargin="76.5000" TopMargin="2.7246" BottomMargin="14.2754" LabelText="Version 3.0" ctype="TextBMFontObjectData">
                    <Size X="197.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="39.2754" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5862" />
                    <PreSize X="0.5629" Y="0.7463" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="778.2125" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8106" />
                <PreSize X="0.5469" Y="0.0698" />
                <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1_1" ActionTag="498591257" Tag="1780" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="125.0000" RightMargin="125.0000" TopMargin="237.4213" BottomMargin="693.5787" FontSize="25" LabelText="Reward for Top players in Season 1" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="390.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="708.0787" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7376" />
                <PreSize X="0.6094" Y="0.0302" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2_0_0_0" ActionTag="-1381885939" Tag="1781" RotationSkewX="19.9994" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="231.5002" RightMargin="231.4998" TopMargin="361.2776" BottomMargin="545.7224" LabelText="New rule !" ctype="TextBMFontObjectData">
                <Size X="177.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0002" Y="572.2224" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5961" />
                <PreSize X="0.2766" Y="0.0552" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1_1_0" ActionTag="-1366650809" Tag="1782" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="170.5005" RightMargin="170.4995" TopMargin="430.1165" BottomMargin="421.8835" FontSize="21" LabelText="Top 1: Limit 10% score gaining.&#xA;Top 2: Limit 20% score gaining.&#xA;....&#xA; Top 10: No limit score gaining." VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="299.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0005" Y="475.8835" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4957" />
                <PreSize X="0.4672" Y="0.1125" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="30" G="144" B="255" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_33_0" ActionTag="-674812851" Tag="1783" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="114.0000" RightMargin="114.0000" TopMargin="303.3734" BottomMargin="619.6266" LabelText="Restart Top Event for SEASON 2" ctype="TextBMFontObjectData">
                <Size X="412.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="638.1266" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6647" />
                <PreSize X="0.6438" Y="0.0385" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2_0_0_0_0" ActionTag="382369245" Tag="1787" RotationSkewX="19.9996" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="67.9973" RightMargin="42.0027" TopMargin="552.4989" BottomMargin="354.5011" LabelText="New auto skill casting mode !" ctype="TextBMFontObjectData">
                <Size X="530.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="332.9973" Y="381.0011" />
                <Scale ScaleX="0.8500" ScaleY="0.8500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5203" Y="0.3969" />
                <PreSize X="0.8281" Y="0.0552" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1_1_1" ActionTag="173501474" Tag="1789" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="209.0610" RightMargin="266.9390" TopMargin="634.3518" BottomMargin="304.6482" FontSize="18" LabelText="(by going to Setting)" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="164.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="291.0610" Y="315.1482" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4548" Y="0.3283" />
                <PreSize X="0.2562" Y="0.0219" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1_1_2" ActionTag="781503265" Tag="1791" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="221.9998" RightMargin="222.0002" TopMargin="705.2291" BottomMargin="223.7708" FontSize="25" LabelText="- Fix other bugs -" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="196.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="319.9998" Y="239.2708" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2492" />
                <PreSize X="0.3063" Y="0.0323" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="479.9998" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Power_1" ActionTag="910322248" Tag="1790" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="319.3965" RightMargin="134.6035" TopMargin="549.4861" BottomMargin="204.5139" ctype="SpriteObjectData">
            <Size X="186.0000" Y="206.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="412.3965" Y="307.5139" />
            <Scale ScaleX="0.3500" ScaleY="0.3500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6444" Y="0.3203" />
            <PreSize X="0.2906" Y="0.2146" />
            <FileData Type="Normal" Path="NewSkillShop/Power.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>