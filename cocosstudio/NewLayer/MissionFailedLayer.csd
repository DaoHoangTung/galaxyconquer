<GameFile>
  <PropertyGroup Name="MissionFailedLayer" Type="Node" ID="bc43110c-f404-4df5-acfb-db22d21563ad" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1039" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="887889483" Alpha="240" Tag="1" IconVisible="False" LeftMargin="-320.0000" RightMargin="-320.0000" TopMargin="-480.0000" BottomMargin="-480.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_8" ActionTag="982688084" Tag="2" IconVisible="False" LeftMargin="-131.4966" RightMargin="-131.5034" TopMargin="-176.5000" BottomMargin="123.5000" LabelText="Mission Failed" ctype="TextBMFontObjectData">
            <Size X="263.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0034" Y="150.0000" />
            <Scale ScaleX="1.3000" ScaleY="1.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinLose.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_4" ActionTag="-613265609" Tag="3" IconVisible="False" LeftMargin="-82.5000" RightMargin="-82.5000" TopMargin="-17.5000" BottomMargin="-17.5000" LabelText="End in . . . 3 !" ctype="TextBMFontObjectData">
            <Size X="165.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Sharnay_text.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>