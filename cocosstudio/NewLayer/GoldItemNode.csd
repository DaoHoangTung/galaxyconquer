<GameFile>
  <PropertyGroup Name="GoldItemNode" Type="Node" ID="0a4ecf26-9c1a-4e6a-b51c-8af6e02064b6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="658" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="14115258" Tag="1" IconVisible="False" LeftMargin="-275.0000" RightMargin="-275.0000" TopMargin="-40.0000" BottomMargin="-40.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="126" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="550.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="Image_4" ActionTag="1263011360" Alpha="226" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="16.7066" RightMargin="469.2934" TopMargin="8.0000" BottomMargin="8.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                <Size X="64.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_8" ActionTag="-1957006818" Tag="1" RotationSkewX="20.0004" IconVisible="False" LeftMargin="1.8765" RightMargin="7.1235" TopMargin="46.6780" BottomMargin="-35.6780" LabelText="1/1" ctype="TextBMFontObjectData">
                    <Size X="55.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="29.3765" Y="-9.1780" />
                    <Scale ScaleX="0.4000" ScaleY="0.4000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4590" Y="-0.1434" />
                    <PreSize X="0.8594" Y="0.8281" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="48.7066" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0886" Y="0.5000" />
                <PreSize X="0.1164" Y="0.8000" />
                <FileData Type="PlistSubImage" Path="goldItem1.jpg" Plist="NewGoldShop/goldItem.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="863848870" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="111.1240" RightMargin="323.8760" TopMargin="24.0000" BottomMargin="28.0000" FontSize="20" LabelText="+ 2500 Gold" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="115.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="111.1240" Y="42.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2020" Y="0.5250" />
                <PreSize X="0.2091" Y="0.3500" />
                <FontResource Type="Normal" Path="fonts/new/UTM_NEO_SANS INTELBOLD_ITALIC_0.TTF" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_5" ActionTag="1109549075" Tag="3" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="396.3006" RightMargin="13.6994" TopMargin="13.0000" BottomMargin="13.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="24" Scale9Height="32" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Text_2" ActionTag="-2054693515" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="52.5000" RightMargin="24.5000" TopMargin="15.0601" BottomMargin="15.9399" FontSize="20" LabelText="15,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="63.0000" Y="23.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_1" ActionTag="951680483" Tag="1574" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-28.7622" RightMargin="64.7622" TopMargin="-1.5000" BottomMargin="-1.5000" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="27.0000" Y="26.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="-15.2622" Y="11.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="-0.2423" Y="0.5000" />
                        <PreSize X="0.4286" Y="1.1304" />
                        <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="84.0000" Y="27.4399" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6000" Y="0.5081" />
                    <PreSize X="0.4500" Y="0.4259" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="466.3006" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8478" Y="0.5000" />
                <PreSize X="0.2545" Y="0.6750" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewGoldShop/button_square_orange.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewGoldShop/button_square_orange_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewGoldShop/button_square_orange.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_5" ActionTag="296184345" Tag="702" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="96.6771" RightMargin="452.3229" TopMargin="16.5000" BottomMargin="16.5000" Scale9Enable="True" TopEage="15" BottomEage="15" Scale9OriginY="15" Scale9Width="1" Scale9Height="17" ctype="ImageViewObjectData">
                <Size X="1.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="97.1771" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="128" G="0" B="128" />
                <PrePosition X="0.1767" Y="0.5000" />
                <PreSize X="0.0018" Y="0.5875" />
                <FileData Type="Normal" Path="NewRewardLayer/vip_tab_seperate.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_5_0" ActionTag="354766004" Tag="703" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="381.4801" RightMargin="167.5199" TopMargin="16.5000" BottomMargin="16.5000" Scale9Enable="True" TopEage="15" BottomEage="15" Scale9OriginY="15" Scale9Width="1" Scale9Height="17" ctype="ImageViewObjectData">
                <Size X="1.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="381.9801" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="128" G="0" B="128" />
                <PrePosition X="0.6945" Y="0.5000" />
                <PreSize X="0.0018" Y="0.5875" />
                <FileData Type="Normal" Path="NewRewardLayer/vip_tab_seperate.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_1" ActionTag="-444518858" Tag="4" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="275.0000" RightMargin="275.0000" TopMargin="40.0000" BottomMargin="40.0000" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="275.0000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/IAPeffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>