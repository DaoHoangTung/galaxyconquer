<GameFile>
  <PropertyGroup Name="MapRateRewardNode" Type="Node" ID="ee26bc2d-82a6-4a9e-a55e-b4a46718975e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1120" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="2125854478" Tag="1" IconVisible="False" LeftMargin="-250.0000" RightMargin="-250.0000" TopMargin="-85.0000" BottomMargin="-85.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="500.0000" Y="170.0000" />
            <Children>
              <AbstractNodeData Name="Image_7" ActionTag="-2040416505" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="31.3661" RightMargin="368.6339" TopMargin="9.2280" BottomMargin="60.7720" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-590100238" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="7.0000" RightMargin="7.0000" TopMargin="95.3183" BottomMargin="-30.3183" LabelText="10/10" ctype="TextBMFontObjectData">
                    <Size X="86.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="-12.8183" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.1282" />
                    <PreSize X="0.8600" Y="0.3500" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Sharnay_text.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="81.3661" Y="110.7720" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1627" Y="0.6516" />
                <PreSize X="0.2000" Y="0.5882" />
                <FileData Type="PlistSubImage" Path="BossDescription1.jpg" Plist="NewLobbyScene/BossDescription.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_3" ActionTag="-1918044222" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="150.0903" RightMargin="103.9097" TopMargin="54.5050" BottomMargin="62.4950" LabelText="+ 3,000,000 $" ctype="TextBMFontObjectData">
                <Size X="246.0000" Y="53.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="150.0903" Y="88.9950" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3002" Y="0.5235" />
                <PreSize X="0.4920" Y="0.3118" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_2" ActionTag="1200616432" Tag="3" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="413.8990" RightMargin="32.1010" TopMargin="58.0000" BottomMargin="58.0000" TouchEnable="True" FontSize="25" ButtonText="Get" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="24" Scale9Height="32" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="54.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="440.8990" Y="85.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8818" Y="0.5000" />
                <PreSize X="0.1080" Y="0.3176" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewRewardLayer/button_square_to4.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewRewardLayer/button_square_to4_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewRewardLayer/button_square_to4.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_5" ActionTag="-270347450" Tag="1335" IconVisible="False" LeftMargin="29.6791" RightMargin="100.3209" TopMargin="140.6384" BottomMargin="9.3616" ctype="SpriteObjectData">
                <Size X="370.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6791" Y="19.3616" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0594" Y="0.1139" />
                <PreSize X="0.7400" Y="0.1176" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_28" ActionTag="-410400021" Tag="4" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="422.8615" RightMargin="38.1385" TopMargin="71.5850" BottomMargin="71.4150" LeftEage="12" RightEage="12" TopEage="8" BottomEage="8" Scale9OriginX="12" Scale9OriginY="8" Scale9Width="15" Scale9Height="11" ctype="ImageViewObjectData">
                <Size X="39.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="442.3615" Y="84.9150" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8847" Y="0.4995" />
                <PreSize X="0.0780" Y="0.1588" />
                <FileData Type="Normal" Path="NewRewardLayer/tx_history_win.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>