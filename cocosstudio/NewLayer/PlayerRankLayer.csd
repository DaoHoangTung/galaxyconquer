<GameFile>
  <PropertyGroup Name="PlayerRankLayer" Type="Layer" ID="b574da39-b7b8-4f8d-9fdd-20622c5deb68" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="76" ctype="GameLayerObjectData">
        <Size X="500.0000" Y="120.0000" />
        <Children>
          <AbstractNodeData Name="BitmapFontLabel_1_0_0" ActionTag="46098368" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="123.6000" RightMargin="234.4000" TopMargin="64.9751" BottomMargin="23.0249" LabelText="343,402,999" ctype="TextBMFontObjectData">
            <Size X="142.0000" Y="32.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="123.6000" Y="39.0249" />
            <Scale ScaleX="1.3000" ScaleY="1.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2472" Y="0.3252" />
            <PreSize X="0.2840" Y="0.2667" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="601725786" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="125.5000" RightMargin="225.5000" TopMargin="18.6147" BottomMargin="66.3853" FontSize="30" LabelText="user_name" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="149.0000" Y="35.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="125.5000" Y="83.8853" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2510" Y="0.6990" />
            <PreSize X="0.2980" Y="0.2917" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-1655492068" Tag="4" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="35.2366" RightMargin="427.7634" TopMargin="42.0640" BottomMargin="37.9360" LabelText="#1" ctype="TextBMFontObjectData">
            <Size X="37.0000" Y="40.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="35.2366" Y="57.9360" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0705" Y="0.4828" />
            <PreSize X="0.0740" Y="0.3333" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Rank.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_9" ActionTag="881651926" Tag="5" IconVisible="False" LeftMargin="435.0000" RightMargin="35.0000" TopMargin="36.0000" BottomMargin="36.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="12" Scale9Height="18" ctype="ImageViewObjectData">
            <Size X="30.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="450.0000" Y="60.0000" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9000" Y="0.5000" />
            <PreSize X="0.0600" Y="0.4000" />
            <FileData Type="Normal" Path="NewRank/dautruong_icon_rank1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_9_0" ActionTag="-243771257" VisibleForFrame="False" Tag="6" IconVisible="False" LeftMargin="435.0000" RightMargin="35.0000" TopMargin="36.0000" BottomMargin="36.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="12" Scale9Height="18" ctype="ImageViewObjectData">
            <Size X="30.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="450.0000" Y="60.0000" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9000" Y="0.5000" />
            <PreSize X="0.0600" Y="0.4000" />
            <FileData Type="Normal" Path="NewRank/dautruong_icon_rank2.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_9_0_0" ActionTag="-1956034488" VisibleForFrame="False" Tag="7" IconVisible="False" LeftMargin="435.0000" RightMargin="35.0000" TopMargin="36.0000" BottomMargin="36.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="12" Scale9Height="18" ctype="ImageViewObjectData">
            <Size X="30.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="450.0000" Y="60.0000" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9000" Y="0.5000" />
            <PreSize X="0.0600" Y="0.4000" />
            <FileData Type="Normal" Path="NewRank/dautruong_icon_rank3.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>