<GameFile>
  <PropertyGroup Name="MultipointEffectLayer" Type="Node" ID="382f03d6-ed09-43e3-a800-396abb30a702" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="987" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Particle_2" ActionTag="741418872" Tag="1" IconVisible="True" LeftMargin="-6.8348" RightMargin="6.8348" TopMargin="6.8350" BottomMargin="-6.8350" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-6.8348" Y="-6.8350" />
            <Scale ScaleX="0.9093" ScaleY="0.6907" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewParticle/IAPeffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1178386655" Tag="2" IconVisible="False" LeftMargin="-103.5003" RightMargin="-103.4997" TopMargin="-18.3660" BottomMargin="-9.6340" LabelText="Coin Rate x2" ctype="TextBMFontObjectData">
            <Size X="207.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0003" Y="4.3660" />
            <Scale ScaleX="2.0000" ScaleY="2.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>