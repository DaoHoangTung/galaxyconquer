<GameFile>
  <PropertyGroup Name="StatusElement" Type="Node" ID="de39d2c0-56ed-4241-bf3d-9b5035269cb2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="340" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-372124134" Tag="1" IconVisible="False" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="-32.0000" BottomMargin="-32.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="250.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="Button_1" ActionTag="1546717287" Tag="1" IconVisible="False" LeftMargin="22.0000" RightMargin="164.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.0000" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2160" Y="0.5000" />
                <PreSize X="0.2560" Y="1.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="spItem1.jpg" Plist="NewSPShop/spItem.plist" />
                <PressedFileData Type="PlistSubImage" Path="spItem1.jpg" Plist="NewSPShop/spItem.plist" />
                <NormalFileData Type="PlistSubImage" Path="spItem1.jpg" Plist="NewSPShop/spItem.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1250572100" Tag="2" IconVisible="False" LeftMargin="105.3460" RightMargin="28.6540" TopMargin="14.6275" BottomMargin="12.3725" LabelText="00:02:00" ctype="TextBMFontObjectData">
                <Size X="116.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="105.3460" Y="30.8725" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4214" Y="0.4824" />
                <PreSize X="0.4640" Y="0.5781" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>