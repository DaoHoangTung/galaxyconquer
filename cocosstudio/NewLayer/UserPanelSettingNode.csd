<GameFile>
  <PropertyGroup Name="UserPanelSettingNode" Type="Node" ID="757dc075-67dc-4b83-9b9f-7f2125792a28" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="683" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="1810254915" VisibleForFrame="False" Tag="690" IconVisible="False" LeftMargin="-274.9999" RightMargin="-275.0001" TopMargin="-300.0004" BottomMargin="-299.9996" Scale9Enable="True" LeftEage="147" RightEage="147" TopEage="77" BottomEage="77" Scale9OriginX="147" Scale9OriginY="77" Scale9Width="48" Scale9Height="31" ctype="ImageViewObjectData">
            <Size X="550.0000" Y="600.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0001" Y="0.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/report_dropdownMenu.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2" ActionTag="24715021" Tag="689" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-223.0000" RightMargin="-223.0000" TopMargin="-188.4339" BottomMargin="186.4339" LeftEage="147" RightEage="147" Scale9OriginX="147" Scale9Width="152" Scale9Height="2" ctype="ImageViewObjectData">
            <Size X="446.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="187.4339" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/dialog_popup_line.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="646014152" Tag="687" IconVisible="True" TopMargin="-228.9824" BottomMargin="228.9824" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="228.9824" />
            <Scale ScaleX="0.5325" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/UserPanelEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="-1075634725" Tag="686" IconVisible="False" LeftMargin="-64.5000" RightMargin="-64.5000" TopMargin="-272.4640" BottomMargin="219.4640" LabelText="Setting" ctype="TextBMFontObjectData">
            <Size X="129.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="245.9640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-2019107656" Tag="1444" IconVisible="False" LeftMargin="-242.9529" RightMargin="86.9529" TopMargin="-150.2611" BottomMargin="121.2611" FontSize="25" LabelText="Auto cast kill:" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="156.0000" Y="29.0000" />
            <Children>
              <AbstractNodeData Name="CheckBox_1" ActionTag="207145768" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="170.6952" RightMargin="-54.6952" TopMargin="-5.5000" BottomMargin="-5.5000" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="40.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="170.6952" Y="14.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0942" Y="0.5000" />
                <PreSize X="0.2564" Y="1.3793" />
                <NormalBackFileData Type="Default" Path="Default/CheckBox_Normal.png" Plist="" />
                <PressedBackFileData Type="Default" Path="Default/CheckBox_Press.png" Plist="" />
                <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
                <NodeNormalFileData Type="Default" Path="Default/CheckBoxNode_Normal.png" Plist="" />
                <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="-86.9529" Y="135.7611" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>