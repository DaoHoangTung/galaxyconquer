<GameFile>
  <PropertyGroup Name="ActiveSkillNotifyLayer" Type="Node" ID="46001ad8-f60c-4fa5-bd57-3b04b927aa74" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="987" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-109318094" Tag="1" IconVisible="False" LeftMargin="-113.0000" RightMargin="-113.0000" TopMargin="-110.0000" BottomMargin="-110.0000" LeftEage="74" RightEage="74" TopEage="72" BottomEage="72" Scale9OriginX="74" Scale9OriginY="72" Scale9Width="78" Scale9Height="76" ctype="ImageViewObjectData">
            <Size X="226.0000" Y="220.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewRank/dautruong_img_flare.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2" ActionTag="-922225428" Tag="2" IconVisible="False" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="64.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-674776388" Tag="3" RotationSkewX="25.0000" IconVisible="False" LeftMargin="-187.5000" RightMargin="-187.5000" TopMargin="55.7100" BottomMargin="-108.7100" LabelText="New Passive Ability !" ctype="TextBMFontObjectData">
            <Size X="375.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-82.2100" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>