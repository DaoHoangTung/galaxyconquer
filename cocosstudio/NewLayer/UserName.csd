<GameFile>
  <PropertyGroup Name="UserName" Type="Layer" ID="6848cca8-40af-4674-8761-595ff58b1e26" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="137" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Panel_4" ActionTag="-135616246" VisibleForFrame="False" Tag="1715" IconVisible="False" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="backGround" ActionTag="1979137925" Tag="1510" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-192.0000" RightMargin="-192.0000" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="-1660003534" Tag="1509" IconVisible="True" LeftMargin="229.4712" RightMargin="410.5288" TopMargin="547.1556" BottomMargin="412.8444" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="229.4712" Y="412.8444" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3585" Y="0.4300" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_1" ActionTag="798660740" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="-178.7520" BottomMargin="178.7520" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="55" RightEage="55" TopEage="13" BottomEage="13" Scale9OriginX="-55" Scale9OriginY="-13" Scale9Width="110" Scale9Height="26" ctype="PanelObjectData">
            <Size X="480.0000" Y="960.0000" />
            <Children>
              <AbstractNodeData Name="Button_1" ActionTag="1068859399" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="115.0000" RightMargin="115.0000" TopMargin="745.7973" BottomMargin="132.2027" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="250.0000" Y="82.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_16" ActionTag="1815312172" Tag="1506" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="88.5000" RightMargin="88.5000" TopMargin="22.5000" BottomMargin="22.5000" LabelText="Login" ctype="TextBMFontObjectData">
                    <Size X="73.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="125.0000" Y="41.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.2920" Y="0.4512" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="240.0000" Y="173.2027" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1804" />
                <PreSize X="0.5208" Y="0.0854" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0" ActionTag="2109676016" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="835.8266" BottomMargin="42.1734" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="320.0000" Y="82.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_16_0" ActionTag="730893080" Tag="1507" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="104.0000" RightMargin="104.0000" TopMargin="22.5000" BottomMargin="22.5000" LabelText="Register" ctype="TextBMFontObjectData">
                    <Size X="112.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="41.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.3500" Y="0.4512" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="240.0000" Y="83.1734" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0866" />
                <PreSize X="0.6667" Y="0.0854" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_3" ActionTag="-42105284" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-420.8520" RightMargin="495.8520" TopMargin="716.1016" BottomMargin="157.8984" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="375" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="405.0000" Y="86.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-218.3520" Y="200.8984" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.4549" Y="0.2093" />
                <PreSize X="0.8438" Y="0.0896" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewMenu/facebook-login.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/facebook-login.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/facebook-login.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1" ActionTag="-238877806" Tag="4" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="40.0000" RightMargin="40.0000" TopMargin="419.6111" BottomMargin="440.3889" Scale9Enable="True" LeftEage="95" RightEage="95" TopEage="13" BottomEage="13" Scale9OriginX="95" Scale9OriginY="13" Scale9Width="316" Scale9Height="208" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="TextField_2" ActionTag="-644185009" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="48.2400" RightMargin="51.7600" TopMargin="36.1600" BottomMargin="18.8400" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="Player name" MaxLengthEnable="True" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="300.0000" Y="45.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="48.2400" Y="41.3400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.1206" Y="0.4134" />
                    <PreSize X="0.7500" Y="0.4500" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_2" ActionTag="-674238897" Tag="171" RotationSkewX="20.0004" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="94.5000" RightMargin="94.5000" TopMargin="-47.7000" BottomMargin="104.7000" FontSize="35" LabelText="Player Name:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="211.0000" Y="43.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="126.2000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.2620" />
                    <PreSize X="0.5275" Y="0.4300" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="30" G="144" B="255" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="240.0000" Y="490.3889" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5108" />
                <PreSize X="0.8333" Y="0.1042" />
                <FileData Type="Normal" Path="NewMissionSceneResource/panel-5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1_0" ActionTag="13913358" Tag="5" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="40.0000" RightMargin="40.0000" TopMargin="594.9361" BottomMargin="265.0639" Scale9Enable="True" LeftEage="95" RightEage="95" TopEage="13" BottomEage="13" Scale9OriginX="95" Scale9OriginY="13" Scale9Width="316" Scale9Height="208" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="TextField_2" ActionTag="-1518418667" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="52.9200" RightMargin="47.0800" TopMargin="38.8200" BottomMargin="16.1800" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="Password" MaxLengthEnable="True" MaxLengthText="16" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="300.0000" Y="45.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="52.9200" Y="38.6800" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.1323" Y="0.3868" />
                    <PreSize X="0.7500" Y="0.4500" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_2_0" ActionTag="-197494474" Tag="172" RotationSkewX="20.0004" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="116.5000" RightMargin="116.5000" TopMargin="-48.9200" BottomMargin="105.9200" FontSize="35" LabelText="Password:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="167.0000" Y="43.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="127.4200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.2742" />
                    <PreSize X="0.4175" Y="0.4300" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="30" G="144" B="255" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="240.0000" Y="315.0639" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3282" />
                <PreSize X="0.8333" Y="0.1042" />
                <FileData Type="Normal" Path="NewMissionSceneResource/panel-5.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="658.7520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6862" />
            <PreSize X="0.7500" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="-435532472" Tag="1592" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-5.0000" RightMargin="-5.0000" TopMargin="-49.0379" BottomMargin="809.0379" LeftEage="55" RightEage="55" TopEage="11" BottomEage="11" Scale9OriginX="55" Scale9OriginY="11" Scale9Width="168" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="650.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_19" ActionTag="351466989" Tag="1593" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="158.5000" RightMargin="158.5000" TopMargin="89.9978" BottomMargin="60.0022" LabelText="Galaxy Commander" ctype="TextBMFontObjectData">
                <Size X="333.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="325.0000" Y="85.0022" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4250" />
                <PreSize X="0.5123" Y="0.2500" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="909.0379" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9469" />
            <PreSize X="1.0156" Y="0.2083" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4_0" ActionTag="1623423840" Tag="1716" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="95.0000" RightMargin="95.0000" TopMargin="884.0023" BottomMargin="-124.0023" LeftEage="55" RightEage="55" TopEage="11" BottomEage="11" Scale9OriginX="55" Scale9OriginY="11" Scale9Width="168" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="450.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_2_0" ActionTag="1282993125" Tag="1719" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="130.5000" RightMargin="130.5000" TopMargin="27.4924" BottomMargin="144.5076" LabelText="Version 4.0" ctype="TextBMFontObjectData">
                <Size X="189.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="158.5076" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7925" />
                <PreSize X="0.4200" Y="0.1400" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="-24.0023" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="-0.0250" />
            <PreSize X="0.7031" Y="0.2083" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>