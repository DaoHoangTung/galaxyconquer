<GameFile>
  <PropertyGroup Name="GuideMinerEventLayer" Type="Layer" ID="e7b31be9-9928-44a3-b438-0e44031fea17" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="164" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1270394456" Tag="1" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="222" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-486867343" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.0000" RightMargin="70.0000" TopMargin="155.0000" BottomMargin="155.0000" Scale9Enable="True" LeftEage="55" RightEage="45" TopEage="45" BottomEage="55" Scale9OriginX="55" Scale9OriginY="45" Scale9Width="1" Scale9Height="1" ctype="ImageViewObjectData">
                <Size X="500.0000" Y="650.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="480.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7813" Y="0.6771" />
                <FileData Type="Normal" Path="NewSkillShop/glassPanel_corners.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="134466476" Tag="4" IconVisible="False" LeftMargin="59.3833" RightMargin="548.6167" TopMargin="781.8233" BottomMargin="147.1767" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.3833" Y="162.6767" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1178" Y="0.1695" />
                <PreSize X="0.0500" Y="0.0323" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0_0" ActionTag="-1629332980" Tag="5" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="245.5160" RightMargin="249.4840" TopMargin="714.2983" BottomMargin="195.7017" TouchEnable="True" FontSize="25" ButtonText="Mine" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="145.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="318.0160" Y="220.7017" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4969" Y="0.2299" />
                <PreSize X="0.2266" Y="0.0521" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1" ActionTag="1223120267" Tag="571" RotationSkewX="14.9998" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="113.0000" RightMargin="113.0000" TopMargin="252.6660" BottomMargin="655.3340" FontSize="22" LabelText="Get 100% EXP by watching Ad Video.&#xA;Get 200% EXP by spending SP in SP Shop." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="414.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="681.3340" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7097" />
                <PreSize X="0.6469" Y="0.0542" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0" ActionTag="-668379372" Tag="570" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="280.5418" RightMargin="276.4582" TopMargin="216.4779" BottomMargin="713.5221" FontSize="24" LabelText="- Rule -" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="83.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.4754" ScaleY="0.5002" />
                <Position X="320.0000" Y="728.5281" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7589" />
                <PreSize X="0.1297" Y="0.0313" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="128" G="0" B="128" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="778113507" Tag="573" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="262.0000" RightMargin="262.0000" TopMargin="328.3278" BottomMargin="601.6722" FontSize="24" LabelText="- Reward -" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="116.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="616.6722" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6424" />
                <PreSize X="0.1813" Y="0.0313" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="128" G="0" B="128" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_33" ActionTag="-648424065" Tag="946" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="218.5160" RightMargin="222.4840" TopMargin="664.8642" BottomMargin="258.1358" LabelText="Ready to mine !" ctype="TextBMFontObjectData">
                <Size X="199.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="318.0160" Y="276.6358" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4969" Y="0.2882" />
                <PreSize X="0.3109" Y="0.0385" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="FileNode_1" ActionTag="-1247650350" Tag="983" IconVisible="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="629.4179" BottomMargin="330.5821" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="320.0000" Y="330.5821" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3444" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewLayer/MinerEventNode.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="2126566098" Tag="1430" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="145.0000" RightMargin="145.0000" TopMargin="124.2222" BottomMargin="768.7778" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="350.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-454095968" Tag="1428" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="72.5000" RightMargin="72.5000" TopMargin="2.7246" BottomMargin="14.2754" LabelText="Miner Event" ctype="TextBMFontObjectData">
                    <Size X="205.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="39.2754" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5862" />
                    <PreSize X="0.5857" Y="0.7463" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="802.2778" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8357" />
                <PreSize X="0.5469" Y="0.0698" />
                <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_1_0" ActionTag="-2089358688" Tag="572" RotationSkewX="15.0015" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="194.0000" RightMargin="194.0000" TopMargin="363.1998" BottomMargin="570.8002" FontSize="22" LabelText="Coin, SP, skills and more !" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="252.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="583.8002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6081" />
                <PreSize X="0.3938" Y="0.0271" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>