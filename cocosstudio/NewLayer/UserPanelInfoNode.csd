<GameFile>
  <PropertyGroup Name="UserPanelInfoNode" Type="Node" ID="f51974bc-179a-4f15-8155-79386f86b4d9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="683" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="1810254915" VisibleForFrame="False" Tag="690" IconVisible="False" LeftMargin="-274.9999" RightMargin="-275.0001" TopMargin="-300.0004" BottomMargin="-299.9996" Scale9Enable="True" LeftEage="147" RightEage="147" TopEage="77" BottomEage="77" Scale9OriginX="147" Scale9OriginY="77" Scale9Width="48" Scale9Height="31" ctype="ImageViewObjectData">
            <Size X="550.0000" Y="600.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0001" Y="0.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/report_dropdownMenu.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2" ActionTag="24715021" Tag="689" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-223.0000" RightMargin="-223.0000" TopMargin="-188.4339" BottomMargin="186.4339" LeftEage="147" RightEage="147" Scale9OriginX="147" Scale9Width="152" Scale9Height="2" ctype="ImageViewObjectData">
            <Size X="446.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="187.4339" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/dialog_popup_line.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="646014152" Tag="687" IconVisible="True" TopMargin="-228.9824" BottomMargin="228.9824" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="228.9824" />
            <Scale ScaleX="0.8009" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewUserPanel/UserPanelEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="-1075634725" Tag="686" IconVisible="False" LeftMargin="-97.0000" RightMargin="-97.0000" TopMargin="-272.4640" BottomMargin="219.4640" LabelText="Player Info" ctype="TextBMFontObjectData">
            <Size X="194.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="245.9640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_5_0" ActionTag="-1935052573" Tag="336" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-1.0000" RightMargin="-1.0000" TopMargin="-175.0000" BottomMargin="-175.0000" Scale9Enable="True" TopEage="15" BottomEage="15" Scale9OriginY="15" Scale9Width="1" Scale9Height="17" ctype="ImageViewObjectData">
            <Size X="2.0000" Y="350.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewRewardLayer/vip_tab_seperate.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="-129164910" Tag="1" IconVisible="True" LeftMargin="-209.5497" RightMargin="209.5497" TopMargin="-115.1688" BottomMargin="115.1688" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Text_8" ActionTag="-1896499228" Tag="1" IconVisible="False" LeftMargin="-45.0000" RightMargin="-18.0000" TopMargin="-59.5000" BottomMargin="36.5000" FontSize="20" LabelText="Player:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="-45.0000" Y="48.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_21" ActionTag="1761912450" Tag="2" IconVisible="False" LeftMargin="-42.0000" RightMargin="-22.0000" TopMargin="-26.0000" BottomMargin="-38.0000" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-10.0000" Y="-6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="avatar0.jpg" Plist="NewUserPanel/UserAvatar.plist" />
                <PressedFileData Type="PlistSubImage" Path="avatar0.jpg" Plist="NewUserPanel/UserAvatar.plist" />
                <NormalFileData Type="PlistSubImage" Path="avatar0.jpg" Plist="NewUserPanel/UserAvatar.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_8" ActionTag="-1840319018" Tag="3" IconVisible="False" LeftMargin="31.7582" RightMargin="-209.7582" TopMargin="-34.4851" BottomMargin="-18.5149" LabelText="KuKulKan" ctype="TextBMFontObjectData">
                <Size X="178.0000" Y="53.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="31.7582" Y="7.9851" />
                <Scale ScaleX="0.6500" ScaleY="0.6500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_6" ActionTag="1351076664" Tag="4" RotationSkewX="20.0023" IconVisible="False" LeftMargin="-2.9176" RightMargin="-143.0824" TopMargin="1.0278" BottomMargin="-51.0278" LabelText="Level 13" ctype="TextBMFontObjectData">
                <Size X="146.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="70.0824" Y="-26.0278" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-1939119322" Tag="5" IconVisible="False" LeftMargin="33.3596" RightMargin="-87.3596" TopMargin="264.5333" BottomMargin="-304.5333" LabelText="#31" ctype="TextBMFontObjectData">
                <Size X="54.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="33.3596" Y="-284.5333" />
                <Scale ScaleX="1.3000" ScaleY="1.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Rank.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-434415477" Tag="6" IconVisible="False" LeftMargin="33.4262" RightMargin="-175.4262" TopMargin="227.3445" BottomMargin="-259.3445" LabelText="343,402,999" ctype="TextBMFontObjectData">
                <Size X="142.0000" Y="32.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="33.4262" Y="-243.3445" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_11" ActionTag="-1209288852" Tag="7" IconVisible="False" LeftMargin="-103.2546" RightMargin="-82.7454" TopMargin="156.4713" BottomMargin="-362.4713" Scale9Enable="True" LeftEage="61" RightEage="61" TopEage="67" BottomEage="67" Scale9OriginX="61" Scale9OriginY="67" Scale9Width="64" Scale9Height="72" ctype="ImageViewObjectData">
                <Size X="186.0000" Y="206.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-10.2546" Y="-259.4713" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewSkillShop/Power.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_8_0" ActionTag="623734359" Tag="8" IconVisible="False" LeftMargin="-45.0000" RightMargin="-77.0000" TopMargin="184.6566" BottomMargin="-207.6566" FontSize="20" LabelText="Achievement:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="122.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="-45.0000" Y="-196.1566" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1_0" ActionTag="1033505680" Tag="9" IconVisible="False" LeftMargin="-10.4620" RightMargin="-219.5380" TopMargin="92.9716" BottomMargin="-158.9716" LabelText="1,000,000" ctype="TextBMFontObjectData">
                <Size X="230.0000" Y="66.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="1469058041" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-49.4641" RightMargin="252.4641" TopMargin="12.9974" BottomMargin="27.0026" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-35.9641" Y="40.0026" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1564" Y="0.6061" />
                    <PreSize X="0.1174" Y="0.3939" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="-10.4620" Y="-125.9716" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0" ActionTag="-1577727601" Tag="10" IconVisible="False" LeftMargin="-12.3616" RightMargin="-40.6384" TopMargin="68.9244" BottomMargin="-103.9244" FontSize="30" LabelText="100" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="53.0000" Y="35.0000" />
                <Children>
                  <AbstractNodeData Name="Image_12" ActionTag="-1685239646" Tag="1" RotationSkewX="-0.9268" RotationSkewY="-0.9232" IconVisible="False" LeftMargin="-87.3355" RightMargin="9.3355" TopMargin="-46.5112" BottomMargin="-47.4888" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                    <Size X="131.0000" Y="129.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-21.8355" Y="17.0112" />
                    <Scale ScaleX="0.3500" ScaleY="0.3500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.4120" Y="0.4860" />
                    <PreSize X="2.4717" Y="3.6857" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="-12.3616" Y="-86.4244" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-209.5497" Y="115.1688" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_8" ActionTag="872803864" Tag="2" IconVisible="True" LeftMargin="137.5000" RightMargin="-137.5000" TopMargin="3.9844" BottomMargin="-3.9844" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Text_34" ActionTag="1852437053" Tag="338" IconVisible="False" LeftMargin="-33.0000" RightMargin="-33.0000" TopMargin="-174.5000" BottomMargin="151.5000" FontSize="20" LabelText="Status:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="163.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView_2" ActionTag="-1188041493" Tag="1" IconVisible="False" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="-137.0324" BottomMargin="-162.9676" TouchEnable="True" ClipAble="True" BackColorAlpha="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ItemMargin="5" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="250.0000" Y="300.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_5" ActionTag="2080948271" Tag="369" IconVisible="False" BottomMargin="236.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
                    <Size X="250.0000" Y="64.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_2" ActionTag="1158742586" Tag="370" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="125.0000" RightMargin="125.0000" TopMargin="32.0000" BottomMargin="32.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="125.0000" Y="32.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/StatusElement.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="236.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.7867" />
                    <PreSize X="1.0000" Y="0.2133" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position Y="137.0324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="137.5000" Y="-3.9844" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>