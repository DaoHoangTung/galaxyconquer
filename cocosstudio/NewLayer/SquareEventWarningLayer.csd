<GameFile>
  <PropertyGroup Name="SquareEventWarningLayer" Type="Layer" ID="2da9c31e-6854-4ae2-a8ba-3025aba3a929" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="164" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1270394456" Tag="1" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="222" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-486867343" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="95.0000" RightMargin="95.0000" TopMargin="305.0000" BottomMargin="305.0000" Scale9Enable="True" LeftEage="55" RightEage="45" TopEage="45" BottomEage="55" Scale9OriginX="55" Scale9OriginY="45" Scale9Width="1" Scale9Height="1" ctype="ImageViewObjectData">
                <Size X="450.0000" Y="350.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="480.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7031" Y="0.3646" />
                <FileData Type="Normal" Path="NewSkillShop/glassPanel_corners.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="1415468733" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="163.4512" RightMargin="370.5488" TopMargin="442.2286" BottomMargin="482.7714" FontSize="30" LabelText="Require" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="106.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="216.4512" Y="517.7714" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3382" Y="0.5393" />
                <PreSize X="0.1656" Y="0.0365" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="134466476" VisibleForFrame="False" Tag="2" IconVisible="False" LeftMargin="522.4683" RightMargin="85.5317" TopMargin="297.7040" BottomMargin="631.2960" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="538.4683" Y="646.7960" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8414" Y="0.6737" />
                <PreSize X="0.0500" Y="0.0323" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="1932963562" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="181.0000" RightMargin="181.0000" TopMargin="559.1235" BottomMargin="318.8765" TouchEnable="True" FontSize="40" ButtonText="Return" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="359.8765" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3749" />
                <PreSize X="0.4344" Y="0.0854" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-431277406" Tag="1432" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="283.8878" RightMargin="185.1122" TopMargin="429.4623" BottomMargin="477.5377" LabelText="$100,000" ctype="TextBMFontObjectData">
                <Size X="171.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="369.3878" Y="504.0377" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5772" Y="0.5250" />
                <PreSize X="0.2672" Y="0.0552" />
                <LabelBMFontFile_CNB Type="Normal" Path="NewCustomFont/fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="2126566098" Tag="1430" IconVisible="False" LeftMargin="149.0896" RightMargin="140.9104" TopMargin="335.9811" BottomMargin="557.0189" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="350.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-454095968" Tag="1428" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="61.5000" RightMargin="61.5000" TopMargin="2.7246" BottomMargin="14.2754" LabelText="Square Event" ctype="TextBMFontObjectData">
                    <Size X="227.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="39.2754" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5862" />
                    <PreSize X="0.6486" Y="0.7463" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="324.0896" Y="590.5189" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5064" Y="0.6151" />
                <PreSize X="0.5469" Y="0.0698" />
                <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0" ActionTag="-474003821" Tag="2355" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="232.5005" RightMargin="232.4995" TopMargin="499.3906" BottomMargin="425.6094" FontSize="30" LabelText="to join event." VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="175.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="320.0005" Y="460.6094" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4798" />
                <PreSize X="0.2734" Y="0.0365" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>