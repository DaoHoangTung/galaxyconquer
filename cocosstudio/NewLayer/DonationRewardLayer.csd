<GameFile>
  <PropertyGroup Name="DonationRewardLayer" Type="Layer" ID="bd6c065c-a277-4fe2-8f60-d5c8f4604d80" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="181" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Image_3" ActionTag="-848722930" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="45.0000" RightMargin="45.0000" TopMargin="155.0000" BottomMargin="155.0000" Scale9Enable="True" LeftEage="55" RightEage="55" TopEage="15" BottomEage="15" Scale9OriginX="55" Scale9OriginY="15" Scale9Width="180" Scale9Height="10" ctype="ImageViewObjectData">
            <Size X="550.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="Image_5" ActionTag="901669254" Tag="198" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="50.0000" RightMargin="50.0000" TopMargin="267.9384" BottomMargin="377.0616" Scale9Enable="True" LeftEage="270" RightEage="270" Scale9OriginX="270" Scale9Width="280" Scale9Height="2" ctype="ImageViewObjectData">
                <Size X="450.0000" Y="5.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="379.5616" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5839" />
                <PreSize X="0.8182" Y="0.0077" />
                <FileData Type="Normal" Path="NewRewardLayer/tx_dialog_history_line.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="2079911518" Tag="201" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="428.2875" RightMargin="70.7125" TopMargin="114.8100" BottomMargin="508.1900" FontSize="20" LabelText="Level" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="51.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="453.7875" Y="521.6900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8251" Y="0.8026" />
                <PreSize X="0.0927" Y="0.0415" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2_0" ActionTag="559762251" Tag="202" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="226.9681" RightMargin="218.0319" TopMargin="114.8100" BottomMargin="508.1900" FontSize="20" LabelText="Description" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="105.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="279.4681" Y="521.6900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5081" Y="0.8026" />
                <PreSize X="0.1909" Y="0.0415" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2_0_0" ActionTag="658323076" Tag="203" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="79.9275" RightMargin="424.0725" TopMargin="114.8100" BottomMargin="508.1900" FontSize="20" LabelText="Type" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="46.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.9275" Y="521.6900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1871" Y="0.8026" />
                <PreSize X="0.0836" Y="0.0415" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView_1" ActionTag="1748135547" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="51.4850" RightMargin="48.5150" TopMargin="159.9668" BottomMargin="115.0332" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="25" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="450.0000" Y="375.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_4" ActionTag="717229715" Tag="253" IconVisible="False" BottomMargin="295.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="450.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_3" ActionTag="1629027543" Tag="234" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="225.0000" RightMargin="225.0000" TopMargin="40.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="225.0000" Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/DonationRewardElement.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="295.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.7867" />
                    <PreSize X="1.0000" Y="0.2133" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Panel_4_0" ActionTag="1043736258" ZOrder="1" Tag="254" IconVisible="False" TopMargin="95.0000" BottomMargin="200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="450.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_3" ActionTag="-383912479" Tag="255" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="225.0000" RightMargin="225.0000" TopMargin="40.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="225.0000" Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/DonationRewardElement.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="190.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.5333" />
                    <PreSize X="1.0000" Y="0.2133" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Panel_4_1" ActionTag="-1152395277" ZOrder="2" Tag="264" IconVisible="False" TopMargin="190.0000" BottomMargin="105.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="450.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_3" ActionTag="577028921" Tag="265" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="225.0000" RightMargin="225.0000" TopMargin="40.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="225.0000" Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/DonationRewardElement.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="85.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.2800" />
                    <PreSize X="1.0000" Y="0.2133" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="276.4850" Y="490.0332" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5027" Y="0.7539" />
                <PreSize X="0.8182" Y="0.5769" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_1_0_0" ActionTag="-995046797" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="200.0000" RightMargin="200.0000" TopMargin="571.4572" BottomMargin="28.5428" TouchEnable="True" FontSize="25" ButtonText="Exit" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="53.5428" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0824" />
                <PreSize X="0.2727" Y="0.0769" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="200" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="freencoin_rollcall_highlight_5" ActionTag="1885469254" Tag="493" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="273.5000" RightMargin="273.5000" TopMargin="447.0853" BottomMargin="7.9147" ctype="SpriteObjectData">
                <Size X="3.0000" Y="195.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="105.4147" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1622" />
                <PreSize X="0.0055" Y="0.3000" />
                <FileData Type="PlistSubImage" Path="freencoin_rollcall_highlight.png" Plist="NewDailyReward/FreeCoinRollcall.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="39797247" Tag="502" RotationSkewX="14.9986" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-66.0000" RightMargin="-66.0000" TopMargin="40.8928" BottomMargin="556.1072" LabelText="*Have a lucky day with more rewards !" ctype="TextBMFontObjectData">
                <Size X="682.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="582.6072" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8963" />
                <PreSize X="1.2400" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3_0" ActionTag="-1087020748" Tag="405" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="100.0000" RightMargin="100.0000" TopMargin="-31.6703" BottomMargin="614.6703" Scale9Enable="True" LeftEage="75" RightEage="75" TopEage="18" BottomEage="18" Scale9OriginX="75" Scale9OriginY="18" Scale9Width="79" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="350.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1739357551" Tag="406" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.0000" RightMargin="57.0000" TopMargin="1.7263" BottomMargin="15.2737" LabelText="Miner Reward" ctype="TextBMFontObjectData">
                    <Size X="236.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="40.2737" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6011" />
                    <PreSize X="0.6743" Y="0.7463" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.0000" Y="648.1703" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9972" />
                <PreSize X="0.6364" Y="0.1031" />
                <FileData Type="Normal" Path="NewIAPScene/SkTet_Dialog_Phanthuong_Badge.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8594" Y="0.6771" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>