<GameFile>
  <PropertyGroup Name="DonationRewardElement" Type="Node" ID="fea9e30e-f60b-4f5a-a439-ef128033206a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="220" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-798510567" Tag="1" IconVisible="False" LeftMargin="-225.0000" RightMargin="-225.0000" TopMargin="-50.0000" BottomMargin="-50.0000" ClipAble="False" BackColorAlpha="54" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="450.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Image_4" ActionTag="-1981456985" Alpha="226" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.7066" RightMargin="367.2934" TopMargin="18.0000" BottomMargin="18.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.7066" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1127" Y="0.5000" />
                <PreSize X="0.1422" Y="0.6400" />
                <FileData Type="PlistSubImage" Path="reward0.jpg" Plist="NewPlist/rewardIcon.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-797895467" Tag="2" RotationSkewX="15.0000" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="111.1237" RightMargin="138.8763" TopMargin="22.5000" BottomMargin="22.5000" IsCustomSize="True" FontSize="20" LabelText="+ 2500 Gold " VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="200.0000" Y="55.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="111.1237" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2469" Y="0.5000" />
                <PreSize X="0.4444" Y="0.5500" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="30" G="144" B="255" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="2079070359" Tag="3" RotationSkewX="20.0013" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="309.8277" RightMargin="30.1723" TopMargin="20.4500" BottomMargin="26.5500" LabelText="level 1" ctype="TextBMFontObjectData">
                <Size X="110.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="364.8277" Y="53.0500" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8107" Y="0.5305" />
                <PreSize X="0.2444" Y="0.5300" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="-1139701272" VisibleForFrame="False" Tag="4" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="365.0000" RightMargin="15.0000" TopMargin="23.0000" BottomMargin="23.0000" TouchEnable="True" FontSize="25" ButtonText="Get" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="24" Scale9Height="32" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="70.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0000" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8889" Y="0.5000" />
                <PreSize X="0.1556" Y="0.5400" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewRewardLayer/button_square_to4.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewRewardLayer/button_square_to4_tap.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewRewardLayer/button_square_to4.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_28" ActionTag="-1007936804" Tag="5" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="31.7800" RightMargin="379.2200" TopMargin="36.2400" BottomMargin="36.7600" LeftEage="12" RightEage="12" TopEage="8" BottomEage="8" Scale9OriginX="12" Scale9OriginY="8" Scale9Width="15" Scale9Height="11" ctype="ImageViewObjectData">
                <Size X="39.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="51.2800" Y="50.2600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1140" Y="0.5026" />
                <PreSize X="0.0867" Y="0.2700" />
                <FileData Type="Normal" Path="NewRewardLayer/tx_history_win.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_1" ActionTag="-861613981" Tag="6" IconVisible="True" LeftMargin="187.5818" RightMargin="262.4182" TopMargin="60.3271" BottomMargin="39.6729" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="187.5818" Y="39.6729" />
                <Scale ScaleX="0.9000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4168" Y="0.3967" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewRewardLayer/rewardRecievedEffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_5" ActionTag="-1535728187" Tag="415" IconVisible="False" LeftMargin="-225.0003" RightMargin="-144.9997" TopMargin="42.9558" BottomMargin="-62.9558" ctype="SpriteObjectData">
            <Size X="370.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-225.0003" Y="-52.9558" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>