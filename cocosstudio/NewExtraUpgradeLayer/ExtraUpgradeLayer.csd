<GameFile>
  <PropertyGroup Name="ExtraUpgradeLayer" Type="Node" ID="386ad1c1-b87b-4026-9480-944def588753" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1156" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="-178677227" Tag="1228" IconVisible="False" LeftMargin="-320.0000" RightMargin="-320.0000" TopMargin="-480.0000" BottomMargin="-480.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="141" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_1" ActionTag="-1022660496" Tag="1" IconVisible="False" LeftMargin="-224.9997" RightMargin="-225.0003" TopMargin="-326.4290" BottomMargin="-323.5710" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="17" RightEage="17" TopEage="17" BottomEage="17" Scale9OriginX="17" Scale9OriginY="17" Scale9Width="926" Scale9Height="606" ctype="PanelObjectData">
            <Size X="450.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1193147082" Tag="1230" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="97.5000" RightMargin="97.5000" TopMargin="15.8043" BottomMargin="581.1957" LabelText="Extra Upgrade" ctype="TextBMFontObjectData">
                <Size X="255.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="607.6957" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9349" />
                <PreSize X="0.5667" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1" ActionTag="-992171924" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="193.0000" RightMargin="193.0000" TopMargin="178.0434" BottomMargin="407.9566" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="439.9566" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6769" />
                <PreSize X="0.1422" Y="0.0985" />
                <FileData Type="PlistSubImage" Path="BTNaborbShieldDamage2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="215716949" Tag="1261" RotationSkewX="15.0002" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="127.5000" RightMargin="127.5000" TopMargin="262.2993" BottomMargin="356.7007" FontSize="25" LabelText="--Training Cost--" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="195.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="372.2007" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5726" />
                <PreSize X="0.4333" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1_0" ActionTag="507209859" Tag="2" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="151.0000" RightMargin="151.0000" TopMargin="285.6300" BottomMargin="311.3700" LabelText="100,000" ctype="TextBMFontObjectData">
                <Size X="148.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="337.8700" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5198" />
                <PreSize X="0.3289" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinLose.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_19" ActionTag="1840330593" Tag="4" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="371.3656" BottomMargin="238.6344" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="13" BottomEage="13" Scale9OriginX="33" Scale9OriginY="13" Scale9Width="224" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="290.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="LoadingBar_1" ActionTag="-886761405" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="6.5000" BottomMargin="8.5000" ProgressInfo="13" ctype="LoadingBarObjectData">
                    <Size X="230.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="145.0000" Y="21.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="165" B="0" />
                    <PrePosition X="0.5000" Y="0.5250" />
                    <PreSize X="0.7931" Y="0.6250" />
                    <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_6" ActionTag="-2082412767" Tag="2" RotationSkewX="19.9994" IconVisible="False" LeftMargin="98.0000" RightMargin="98.0000" TopMargin="44.2065" BottomMargin="-39.2065" LabelText="Level 3" ctype="TextBMFontObjectData">
                    <Size X="94.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="145.0000" Y="-21.7065" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.5427" />
                    <PreSize X="0.3241" Y="0.8750" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Sharnay_text.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Particle_2" ActionTag="-1594124077" Tag="3" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="145.0000" RightMargin="145.0000" TopMargin="20.2640" BottomMargin="19.7360" ctype="ParticleObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="145.0000" Y="19.7360" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.4934" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="NewParticle/extraUpgradeEffect.plist" Plist="" />
                    <BlendFunc Src="770" Dst="1" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_9" ActionTag="-763965811" Tag="4" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="124.0000" RightMargin="124.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="15" LabelText="0/100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="42.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="145.0000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1448" Y="0.5000" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Italic.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="dumy" ActionTag="-386228081" Tag="5" IconVisible="False" LeftMargin="471.6395" RightMargin="-356.6395" TopMargin="-167.3578" BottomMargin="154.3578" LabelText="+305 EXP" ctype="TextBMFontObjectData">
                    <Size X="175.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="559.1395" Y="180.8578" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.9281" Y="4.5214" />
                    <PreSize X="0.6034" Y="1.3250" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="258.6344" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3979" />
                <PreSize X="0.6444" Y="0.0615" />
                <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="351618922" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.7500" RightMargin="211.2500" TopMargin="103.1100" BottomMargin="480.8900" LabelText="1,000,000" ctype="TextBMFontObjectData">
                <Size X="230.0000" Y="66.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="2032544601" Tag="1263" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-49.4641" RightMargin="252.4641" TopMargin="12.9974" BottomMargin="27.0026" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-35.9641" Y="40.0026" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1564" Y="0.6061" />
                    <PreSize X="0.1174" Y="0.3939" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.7500" Y="513.8900" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2750" Y="0.7906" />
                <PreSize X="0.5111" Y="0.1015" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="-1846189855" Tag="5" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="86.0000" RightMargin="86.0000" TopMargin="557.8412" BottomMargin="10.1588" TouchEnable="True" FontSize="40" ButtonText="Upgrade" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="51.1588" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0787" />
                <PreSize X="0.6178" Y="0.1262" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_8" ActionTag="151828814" Tag="6" IconVisible="False" LeftMargin="-9.3826" RightMargin="427.3826" TopMargin="630.0993" BottomMargin="-11.0993" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="6.6174" Y="4.4007" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0147" Y="0.0068" />
                <PreSize X="0.0711" Y="0.0477" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewSPShop/tx_history_lose.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_46" ActionTag="279203119" Tag="1536" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="203.1958" RightMargin="-69.1958" TopMargin="68.7300" BottomMargin="453.2700" LeftEage="104" RightEage="104" TopEage="42" BottomEage="42" Scale9OriginX="104" Scale9OriginY="42" Scale9Width="108" Scale9Height="44" ctype="ImageViewObjectData">
                <Size X="316.0000" Y="128.0000" />
                <Children>
                  <AbstractNodeData Name="Text_20" ActionTag="-557210127" Tag="1537" IconVisible="False" LeftMargin="131.2852" RightMargin="41.7148" TopMargin="35.7773" BottomMargin="39.2227" FontSize="45" LabelText="10,200" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="143.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="202.7852" Y="65.7227" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6417" Y="0.5135" />
                    <PreSize X="0.4525" Y="0.4141" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="361.1958" Y="517.2700" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8027" Y="0.7958" />
                <PreSize X="0.7022" Y="0.1969" />
                <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_0" ActionTag="1155331046" Tag="1538" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="527.7921" BottomMargin="99.2079" FontSize="18" LabelText="*Gain more 50% EXP by speding with 2 SP" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="335.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="110.7079" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1703" />
                <PreSize X="0.7444" Y="0.0354" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="165" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CheckBox_2" ActionTag="-1973429265" Tag="1541" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="203.9200" RightMargin="206.0800" TopMargin="474.4531" BottomMargin="135.5469" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="40.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="Text_3_1" ActionTag="269443404" Tag="1542" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="54.7560" RightMargin="-69.7560" TopMargin="4.5000" BottomMargin="4.5000" FontSize="25" LabelText="2 SP" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="55.0000" Y="31.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="82.2560" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="2.0564" Y="0.5000" />
                    <PreSize X="1.3750" Y="0.7750" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="223.9200" Y="155.5469" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4976" Y="0.2393" />
                <PreSize X="0.0889" Y="0.0615" />
                <NormalBackFileData Type="Default" Path="Default/CheckBox_Normal.png" Plist="" />
                <PressedBackFileData Type="Default" Path="Default/CheckBox_Press.png" Plist="" />
                <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
                <NodeNormalFileData Type="Default" Path="Default/CheckBoxNode_Normal.png" Plist="" />
                <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0003" Y="1.4290" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewSPShop/newboard_BG_blue.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>