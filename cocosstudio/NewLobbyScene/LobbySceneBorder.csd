<GameFile>
  <PropertyGroup Name="LobbySceneBorder" Type="Node" ID="7c7976c9-2023-480d-908f-c66e660cdf6b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="38" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_2_0" ActionTag="2012872950" Tag="51" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-10.0000" RightMargin="-10.0000" TopMargin="-10.0000" BottomMargin="-10.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_0" ActionTag="7824960" Tag="56" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="-30.0000" TopMargin="-10.0000" BottomMargin="-10.0000" FlipX="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="20.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_1" ActionTag="79739687" Tag="59" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="-50.0000" TopMargin="-10.0000" BottomMargin="-10.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_0_0" ActionTag="1141130802" Tag="60" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="50.0000" RightMargin="-70.0000" TopMargin="-10.0000" BottomMargin="-10.0000" FlipX="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_2" ActionTag="2003641079" Tag="61" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-90.0000" RightMargin="70.0000" TopMargin="-10.0000" BottomMargin="-10.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-80.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_0_1" ActionTag="-424778387" Tag="62" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-70.0000" RightMargin="50.0000" TopMargin="-10.0000" BottomMargin="-10.0000" FlipX="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-60.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_1_0" ActionTag="375381505" Tag="63" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-50.0000" RightMargin="30.0000" TopMargin="-10.0000" BottomMargin="-10.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_0_0_0" ActionTag="-1875974702" Tag="64" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="10.0000" TopMargin="-10.0000" BottomMargin="-10.0000" FlipX="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-20.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2_0_0_0_1" ActionTag="2121224567" Tag="65" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="70.0000" RightMargin="-90.0000" TopMargin="-10.0000" BottomMargin="-10.0000" FlipX="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="20.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="80.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/151_TabDialogBorder_tl_TabDialogBorder_tl.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>