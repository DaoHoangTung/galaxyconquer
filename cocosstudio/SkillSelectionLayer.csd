<GameFile>
  <PropertyGroup Name="SkillSelectionLayer" Type="Layer" ID="71ce8b92-36d0-4ddf-88a0-f2b136701442" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="88" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="120.0000" />
        <Children>
          <AbstractNodeData Name="Image_2" ActionTag="1418113527" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-15.0000" RightMargin="-15.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="36" BottomEage="36" Scale9OriginX="33" Scale9OriginY="36" Scale9Width="34" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="350.0000" Y="120.0000" />
            <Children>
              <AbstractNodeData Name="ListView_3" ActionTag="1834519898" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="15.0000" RightMargin="15.0000" TopMargin="20.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="110" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="10" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
                <Size X="320.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="175.0000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.9143" Y="0.6667" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="60.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0938" Y="1.0000" />
            <FileData Type="Normal" Path="NewSkillShop/glassPanel_projection.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>