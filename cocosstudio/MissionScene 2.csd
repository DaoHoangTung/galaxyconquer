<GameFile>
  <PropertyGroup Name="MissionScene" Type="Scene" ID="849cd73c-84fa-4eba-8bf7-050a31dc9737" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="24" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Node_5" ActionTag="-2048105380" Tag="161" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="480.0000" BottomMargin="480.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="backGround" ActionTag="2141876732" Tag="160" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-512.0000" RightMargin="-512.0000" TopMargin="-512.0000" BottomMargin="-512.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
                <Size X="1024.0000" Y="1024.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_1_0" ActionTag="-171674178" Tag="159" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="1900787666" VisibleForFrame="False" Tag="29" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="480.0000" BottomMargin="480.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-1603333069" Tag="27" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-320.0000" RightMargin="-320.0000" TopMargin="-480.0000" BottomMargin="-480.0000" LeftEage="165" RightEage="165" TopEage="198" BottomEage="198" Scale9OriginX="165" Scale9OriginY="198" Scale9Width="170" Scale9Height="204" ctype="ImageViewObjectData">
                <Size X="640.0000" Y="960.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/BackdropNebula.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1_0" ActionTag="-1447227844" Tag="28" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-320.0000" RightMargin="-320.0000" TopMargin="480.0000" BottomMargin="-1440.0000" LeftEage="165" RightEage="165" TopEage="198" BottomEage="198" Scale9OriginX="165" Scale9OriginY="198" Scale9Width="170" Scale9Height="204" ctype="ImageViewObjectData">
                <Size X="640.0000" Y="960.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-960.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/BackdropNebula.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="-282066051" Tag="46" IconVisible="False" LeftMargin="-69.7738" RightMargin="-60.2262" TopMargin="-67.8936" BottomMargin="877.8935" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="770.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="Node_3" ActionTag="346777412" Tag="34" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="385.0000" RightMargin="385.0000" TopMargin="546.8850" BottomMargin="-396.8850" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="1148659707" Tag="35" IconVisible="False" LeftMargin="142.5618" RightMargin="-300.5618" TopMargin="-459.8282" BottomMargin="419.8282" LabelText="$9,999,999" ctype="TextBMFontObjectData">
                    <Size X="158.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="300.5618" Y="439.8282" />
                    <Scale ScaleX="0.7000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Rank.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_2_0" ActionTag="137402289" Tag="36" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-71.0000" RightMargin="-71.0000" TopMargin="-461.3388" BottomMargin="429.3388" LabelText="999,000,000" ctype="TextBMFontObjectData">
                    <Size X="142.0000" Y="32.0000" />
                    <Children>
                      <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1889392828" Tag="1" IconVisible="False" LeftMargin="146.6796" RightMargin="-33.6796" TopMargin="-0.3224" BottomMargin="-4.6776" LabelText="x1" ctype="TextBMFontObjectData">
                        <Size X="29.0000" Y="37.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="146.6796" Y="13.8224" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.0330" Y="0.4320" />
                        <PreSize X="0.2042" Y="1.1563" />
                        <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position Y="445.3388" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ListView_1" ActionTag="1217612316" Tag="38" IconVisible="False" LeftMargin="-294.1365" RightMargin="144.1365" TopMargin="-459.3953" BottomMargin="429.3953" TouchEnable="True" ClipAble="False" BackColorAlpha="23" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ItemMargin="5" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
                    <Size X="150.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_7" ActionTag="-368858483" Tag="1025" IconVisible="False" RightMargin="120.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
                        <Size X="30.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="15.0000" Y="15.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1000" Y="0.5000" />
                        <PreSize X="0.2000" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="PowerupExtraLife0.png" Plist="BonusItem/Bonus.plist" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_7_0" ActionTag="-1177543145" ZOrder="1" Tag="1026" IconVisible="False" LeftMargin="35.0000" RightMargin="85.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
                        <Size X="30.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="15.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3333" Y="0.5000" />
                        <PreSize X="0.2000" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="PowerupExtraLife0.png" Plist="BonusItem/Bonus.plist" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_7_0_0" ActionTag="545091762" ZOrder="2" Tag="1027" IconVisible="False" LeftMargin="70.0000" RightMargin="50.0000" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
                        <Size X="30.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="85.0000" Y="15.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5667" Y="0.5000" />
                        <PreSize X="0.2000" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="PowerupExtraLife0.png" Plist="BonusItem/Bonus.plist" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-294.1365" Y="444.3953" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <SingleColor A="255" R="150" G="150" B="255" />
                    <FirstColor A="255" R="150" G="150" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="385.0000" Y="-396.8850" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-2.6459" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="315.2262" Y="952.8935" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4925" Y="0.9926" />
            <PreSize X="1.2031" Y="0.1563" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_2" ActionTag="1593534344" Tag="30" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="336.0000" BottomMargin="624.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="HudBarBorderHealth_5" ActionTag="-127764277" Tag="41" IconVisible="False" LeftMargin="-309.0000" RightMargin="291.0000" TopMargin="-187.0000" BottomMargin="13.0000" ctype="SpriteObjectData">
                <Size X="18.0000" Y="174.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-300.0000" Y="100.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/HudBarBorderHealth.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HudBarBorderShield_3" ActionTag="2046202149" Tag="32" IconVisible="False" LeftMargin="-309.0000" RightMargin="291.0000" TopMargin="213.0000" BottomMargin="-387.0000" ctype="SpriteObjectData">
                <Size X="18.0000" Y="174.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-300.0000" Y="-300.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/HudBarBorderShield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HudBarBorderEnemyHealth_4" ActionTag="-1821910305" Tag="33" IconVisible="False" LeftMargin="291.0000" RightMargin="-309.0000" TopMargin="-187.0000" BottomMargin="13.0000" ctype="SpriteObjectData">
                <Size X="18.0000" Y="174.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0000" Y="100.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/HudBarBorderEnemyHealth.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HudBarBorderShockwave_13" ActionTag="-475040735" Tag="48" IconVisible="False" LeftMargin="291.0000" RightMargin="-309.0000" TopMargin="213.0000" BottomMargin="-387.0000" ctype="SpriteObjectData">
                <Size X="18.0000" Y="174.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0000" Y="-300.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/HudBarBorderShockwave.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="LoadingBar_1" ActionTag="1998642581" Tag="39" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="-414.4998" RightMargin="184.4998" TopMargin="-113.5025" BottomMargin="100.5025" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="230.0000" Y="13.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-299.4998" Y="107.0025" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <ImageFileData Type="Normal" Path="NewMissionSceneResource/sliderProgress2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="LoadingBar_1_0" ActionTag="-894835620" Tag="42" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="185.5000" RightMargin="-415.5000" TopMargin="-113.5000" BottomMargin="100.5000" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="230.0000" Y="13.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.5000" Y="107.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <ImageFileData Type="Normal" Path="NewMissionSceneResource/sliderProgress2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="LoadingBar_1_1" ActionTag="-428160394" Tag="43" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="-414.5000" RightMargin="184.5000" TopMargin="285.9630" BottomMargin="-298.9630" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="230.0000" Y="13.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-299.5000" Y="-292.4630" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="128" G="0" B="128" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <ImageFileData Type="Normal" Path="NewMissionSceneResource/sliderProgress2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="LoadingBar_1_1_0" ActionTag="1652224672" Tag="49" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="185.5001" RightMargin="-415.5001" TopMargin="285.9599" BottomMargin="-298.9599" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="230.0000" Y="13.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.5001" Y="-292.4599" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="30" G="144" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <ImageFileData Type="Normal" Path="NewMissionSceneResource/sliderProgress2.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="624.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6500" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_4" ActionTag="1131852148" Tag="47" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="953.0000" BottomMargin="7.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="LoadingBar_4" ActionTag="-663574725" Tag="44" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-139.0000" RightMargin="-139.0000" TopMargin="-25.0000" BottomMargin="-25.0000" ProgressInfo="53" ctype="LoadingBarObjectData">
                <Size X="278.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <ImageFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="backtotoppressed_6" ActionTag="-1746287457" Tag="45" IconVisible="False" LeftMargin="-385.0000" RightMargin="107.0000" TopMargin="-3.5566" BottomMargin="-78.4434" ctype="SpriteObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-246.0000" Y="-37.4434" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="backtotoppressed_6_0" ActionTag="1463249625" Tag="48" IconVisible="False" LeftMargin="107.0000" RightMargin="-385.0000" TopMargin="-3.5600" BottomMargin="-78.4400" ctype="SpriteObjectData">
                <Size X="278.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="246.0000" Y="-37.4400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_6" ActionTag="-1247219613" Tag="49" IconVisible="False" LeftMargin="-59.5000" RightMargin="-59.5000" TopMargin="-22.4665" BottomMargin="-5.5335" LabelText="Level 6" ctype="TextBMFontObjectData">
                <Size X="119.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="8.4665" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="NewCustomFont/fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="7.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0073" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="-748279765" VisibleForFrame="False" Tag="98" IconVisible="True" LeftMargin="315.0400" RightMargin="324.9600" TopMargin="493.2646" BottomMargin="466.7354" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="315.0400" Y="466.7354" />
            <Scale ScaleX="1.9410" ScaleY="2.7589" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4923" Y="0.4862" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewParticle/shipGetHitEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="dumyLife" ActionTag="-1657005667" Tag="1029" IconVisible="False" LeftMargin="-112.3144" RightMargin="722.3144" TopMargin="-12.0842" BottomMargin="942.0842" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-97.3144" Y="957.0842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1521" Y="0.9970" />
            <PreSize X="0.0469" Y="0.0313" />
            <FileData Type="PlistSubImage" Path="PowerupExtraLife0.png" Plist="BonusItem/Bonus.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="-1010526908" Tag="1034" IconVisible="False" LeftMargin="-181.7932" RightMargin="668.7932" TopMargin="68.8631" BottomMargin="854.1369" LabelText="dummyText" ctype="TextBMFontObjectData">
            <Size X="153.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-105.2932" Y="872.6369" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1645" Y="0.9090" />
            <PreSize X="0.2391" Y="0.0385" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red2.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_2" ActionTag="44506410" Tag="1035" IconVisible="True" LeftMargin="-96.9596" RightMargin="736.9596" TopMargin="160.0723" BottomMargin="799.9277" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-96.9596" Y="799.9277" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1515" Y="0.8333" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewMissionSceneResource/shipExplosion.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="rocketAoEdumy" ActionTag="-2124295965" Tag="200" IconVisible="True" LeftMargin="-140.7785" RightMargin="780.7785" TopMargin="170.1362" BottomMargin="789.8638" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-140.7785" Y="789.8638" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.2200" Y="0.8228" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewParticle/rocketAoE_effect.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_6" ActionTag="-1823096119" Tag="208" IconVisible="True" LeftMargin="-226.5378" RightMargin="866.5378" TopMargin="232.3722" BottomMargin="727.6278" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="1561198337" Tag="1" IconVisible="False" LeftMargin="-113.0000" RightMargin="-113.0000" TopMargin="-110.0000" BottomMargin="-110.0000" LeftEage="74" RightEage="74" TopEage="72" BottomEage="72" Scale9OriginX="74" Scale9OriginY="72" Scale9Width="78" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="226.0000" Y="220.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewRank/dautruong_img_flare.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_3" ActionTag="2106055334" Tag="2" RotationSkewX="20.0000" IconVisible="False" LeftMargin="-107.0000" RightMargin="-107.0000" TopMargin="-26.5000" BottomMargin="-26.5000" LabelText="$ 1,000,000" ctype="TextBMFontObjectData">
                <Size X="214.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-226.5378" Y="727.6278" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3540" Y="0.7579" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>