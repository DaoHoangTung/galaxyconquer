<GameFile>
  <PropertyGroup Name="Instruction3" Type="Node" ID="a4c3626e-14c6-4a9a-bbe2-6b513c6d6a08" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="60" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="720252673" Tag="62" IconVisible="False" LeftMargin="-286.0379" RightMargin="-283.9621" TopMargin="-324.9998" BottomMargin="-325.0002" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="570.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="1253860434" Tag="120" IconVisible="False" LeftMargin="93.8781" RightMargin="92.1219" TopMargin="38.1422" BottomMargin="580.8578" FontSize="25" LabelText="Skill tree system for each abilities" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="384.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="PlayerShip0_1_0" ActionTag="-1983514895" Tag="108" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="162.3903" RightMargin="157.6097" TopMargin="60.2900" BottomMargin="-93.2900" ctype="SpriteObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <Children>
                      <AbstractNodeData Name="PlayerShip0_1" ActionTag="12119919" Tag="63" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-97.8268" RightMargin="97.8268" TopMargin="135.0000" BottomMargin="-135.0000" ctype="SpriteObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="-65.8268" Y="-103.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="-1.0285" Y="-1.6094" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="E1.jpg" Plist="NewSkillShop/SkillIcon.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="PlayerShip0_1_0_0" ActionTag="2027299729" Tag="109" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-3.2442" RightMargin="3.2442" TopMargin="135.0000" BottomMargin="-135.0000" ctype="SpriteObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="28.7558" Y="-103.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4493" Y="-1.6094" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="E2.jpg" Plist="NewSkillShop/SkillIcon.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="PlayerShip0_1_0_0_0" ActionTag="-1534896058" Tag="216" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="92.3597" RightMargin="-92.3597" TopMargin="135.0000" BottomMargin="-135.0000" ctype="SpriteObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="124.3597" Y="-103.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.9431" Y="-1.6094" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="E3.jpg" Plist="NewSkillShop/SkillIcon.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="194.3903" Y="-61.2900" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5062" Y="-1.9771" />
                    <PreSize X="0.1667" Y="2.0645" />
                    <FileData Type="PlistSubImage" Path="ScoreSkillActive5.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.8781" Y="596.3578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5015" Y="0.9175" />
                <PreSize X="0.6737" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="372375918" Tag="228" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="257.5000" RightMargin="257.5000" TopMargin="597.9582" BottomMargin="24.0418" LabelText="3/4" ctype="TextBMFontObjectData">
                <Size X="55.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="38.0418" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0585" />
                <PreSize X="0.0965" Y="0.0431" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0" ActionTag="-1431261561" Tag="217" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="110.0000" RightMargin="110.0000" TopMargin="180.1422" BottomMargin="438.8578" FontSize="25" LabelText="...follow by 3 extra bonus skills" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="350.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="454.3578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6990" />
                <PreSize X="0.6140" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0" ActionTag="614726235" Tag="223" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="164.5747" RightMargin="387.4253" TopMargin="307.5000" BottomMargin="311.5000" FontSize="25" LabelText="3" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="18.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="Icon_Skill_Point_6" ActionTag="236834233" Tag="222" IconVisible="False" LeftMargin="-17.6684" RightMargin="-68.3316" TopMargin="-35.6741" BottomMargin="-37.3259" ctype="SpriteObjectData">
                    <Size X="104.0000" Y="104.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="34.3316" Y="14.6741" />
                    <Scale ScaleX="0.2500" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.9073" Y="0.4734" />
                    <PreSize X="5.7778" Y="3.3548" />
                    <FileData Type="Normal" Path="NewSkillShop/Icon_Skill_Point.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="173.5747" Y="327.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3045" Y="0.5031" />
                <PreSize X="0.0316" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0" ActionTag="1121698573" Tag="224" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="260.7300" RightMargin="291.2700" TopMargin="307.5000" BottomMargin="311.5000" FontSize="25" LabelText="6" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="18.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="Icon_Skill_Point_6" ActionTag="-1644619285" Tag="225" IconVisible="False" LeftMargin="-17.6684" RightMargin="-68.3316" TopMargin="-35.6741" BottomMargin="-37.3259" ctype="SpriteObjectData">
                    <Size X="104.0000" Y="104.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="34.3316" Y="14.6741" />
                    <Scale ScaleX="0.2500" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.9073" Y="0.4734" />
                    <PreSize X="5.7778" Y="3.3548" />
                    <FileData Type="Normal" Path="NewSkillShop/Icon_Skill_Point.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="269.7300" Y="327.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4732" Y="0.5031" />
                <PreSize X="0.0316" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="-783490645" Tag="226" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="360.2888" RightMargin="191.7112" TopMargin="307.5000" BottomMargin="311.5000" FontSize="25" LabelText="9" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="18.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="Icon_Skill_Point_6" ActionTag="-1203379939" Tag="227" IconVisible="False" LeftMargin="-17.6684" RightMargin="-68.3316" TopMargin="-35.6741" BottomMargin="-37.3259" ctype="SpriteObjectData">
                    <Size X="104.0000" Y="104.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="34.3316" Y="14.6741" />
                    <Scale ScaleX="0.2500" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.9073" Y="0.4734" />
                    <PreSize X="5.7778" Y="3.3548" />
                    <FileData Type="Normal" Path="NewSkillShop/Icon_Skill_Point.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="369.2888" Y="327.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6479" Y="0.5031" />
                <PreSize X="0.0316" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0379" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2_0_0_0" ActionTag="-780714457" ZOrder="2" Tag="231" IconVisible="False" LeftMargin="-252.8915" RightMargin="-247.1085" TopMargin="158.2759" BottomMargin="-238.2759" IsCustomSize="True" FontSize="25" LabelText="+ Has 10% to cast perfect lightning" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="500.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="-2.8915" Y="-158.2759" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="191" G="191" B="191" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2_0_0" ActionTag="231936487" ZOrder="1" Tag="232" IconVisible="False" LeftMargin="-206.3917" RightMargin="-200.6083" TopMargin="124.2752" BottomMargin="-153.2752" FontSize="25" LabelText="+ Increase number of lightnings by 1" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="407.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-2.8917" Y="-138.7752" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="191" G="191" B="191" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2_0" ActionTag="-1786058232" Tag="233" IconVisible="False" LeftMargin="-151.8917" RightMargin="-146.1083" TopMargin="90.2752" BottomMargin="-119.2752" FontSize="25" LabelText="+ Increase damage by 10%" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="298.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-2.8917" Y="-104.7752" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="191" G="191" B="191" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2" ActionTag="-286076926" Tag="234" IconVisible="False" LeftMargin="-95.4990" RightMargin="-95.5010" TopMargin="44.2497" BottomMargin="-79.2497" FontSize="30" LabelText="Extra Options:" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="191.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0010" Y="-61.7497" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>