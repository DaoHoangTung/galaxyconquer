<GameFile>
  <PropertyGroup Name="Instruction4" Type="Node" ID="77ff3f33-7a34-47c2-84cf-d7f96dc539a2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="60" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="720252673" Tag="62" IconVisible="False" LeftMargin="-286.0379" RightMargin="-283.9621" TopMargin="-324.9998" BottomMargin="-325.0002" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="570.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="372375918" Tag="228" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="257.0000" RightMargin="257.0000" TopMargin="597.9582" BottomMargin="24.0418" LabelText="4/4" ctype="TextBMFontObjectData">
                <Size X="56.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="38.0418" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0585" />
                <PreSize X="0.0982" Y="0.0431" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1780132351" Tag="282" IconVisible="False" LeftMargin="175.3641" RightMargin="169.6359" TopMargin="268.6647" BottomMargin="331.3353" LabelText="Special Item:" ctype="TextBMFontObjectData">
                <Size X="225.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="287.8641" Y="356.3353" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5050" Y="0.5482" />
                <PreSize X="0.3947" Y="0.0769" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0379" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0_0_0" ActionTag="1105515176" Tag="258" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-213.0000" RightMargin="-213.0000" TopMargin="-294.7437" BottomMargin="232.7437" FontSize="25" LabelText="You can use other support items from&#xA;Gold Shop and SP Shop" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="426.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="ListView_3" ActionTag="1271520314" Tag="259" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="107.0000" RightMargin="107.0000" TopMargin="82.7912" BottomMargin="-84.7912" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ItemMargin="10" ctype="ListViewObjectData">
                <Size X="212.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="Image_28" ActionTag="-1186424778" Tag="260" IconVisible="False" RightMargin="148.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="32.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1509" Y="0.5000" />
                    <PreSize X="0.3019" Y="1.0000" />
                    <FileData Type="PlistSubImage" Path="goldItem0.jpg" Plist="NewGoldShop/goldItem.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_28_0" ActionTag="128686376" ZOrder="1" Tag="261" IconVisible="False" LeftMargin="74.0000" RightMargin="74.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="106.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.3019" Y="1.0000" />
                    <FileData Type="PlistSubImage" Path="goldItem4.jpg" Plist="NewGoldShop/goldItem.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_28_0_0" ActionTag="2023061173" ZOrder="2" Tag="262" IconVisible="False" LeftMargin="148.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="180.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8491" Y="0.5000" />
                    <PreSize X="0.3019" Y="1.0000" />
                    <FileData Type="PlistSubImage" Path="goldItem5.jpg" Plist="NewGoldShop/goldItem.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="213.0000" Y="-52.7912" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.8515" />
                <PreSize X="0.4977" Y="1.0323" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="263.7437" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="128" G="0" B="128" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="-731015038" Tag="271" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-194.0000" RightMargin="-194.0000" TopMargin="-128.6715" BottomMargin="97.6715" FontSize="25" LabelText="...for extra Exp, damage and armor" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="388.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="113.1715" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="128" G="0" B="128" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0_0_0_1" ActionTag="-1144460604" Tag="276" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-195.5000" RightMargin="-195.5000" TopMargin="17.4130" BottomMargin="-110.4130" FontSize="25" LabelText="Gain extra score point and coin&#xA;with Multiplier item, it will return 0 &#xA;if you loss 1 life" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="391.0000" Y="93.0000" />
            <Children>
              <AbstractNodeData Name="Image_14" ActionTag="1345723888" Tag="281" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="183.4602" RightMargin="187.5398" TopMargin="139.2587" BottomMargin="-66.2587" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
                <Size X="20.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="193.4602" Y="-56.2587" />
                <Scale ScaleX="3.5000" ScaleY="3.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4948" Y="-0.6049" />
                <PreSize X="0.0512" Y="0.2151" />
                <FileData Type="PlistSubImage" Path="PowerupMultiplier0.png" Plist="BonusItem/Bonus.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-63.9130" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <OutlineColor A="255" R="128" G="0" B="128" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>