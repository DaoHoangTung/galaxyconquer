<GameFile>
  <PropertyGroup Name="Credit1" Type="Node" ID="91365785-94ef-4fcf-9acc-a9c73b526119" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="60" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="720252673" Tag="62" IconVisible="False" LeftMargin="-286.0379" RightMargin="-283.9621" TopMargin="-324.9998" BottomMargin="-325.0002" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="570.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_1" ActionTag="-1572304967" Tag="725" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="178.7026" RightMargin="182.2974" TopMargin="6.5789" BottomMargin="590.4211" LabelText="BrightRetro " ctype="TextBMFontObjectData">
                <Size X="209.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5086" ScaleY="0.6168" />
                <Position X="285.0000" Y="623.1116" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9586" />
                <PreSize X="0.3667" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0" ActionTag="852186682" Tag="727" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="164.5000" RightMargin="164.5000" TopMargin="408.1097" BottomMargin="188.8903" LabelText="Huinipachutli" ctype="TextBMFontObjectData">
                <Size X="241.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="215.3903" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3314" />
                <PreSize X="0.4228" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_14_0_0_0" ActionTag="1121495386" Tag="728" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="217.0000" RightMargin="217.0000" TopMargin="459.6283" BottomMargin="137.3717" LabelText="Kenney" ctype="TextBMFontObjectData">
                <Size X="136.0000" Y="53.0000" />
                <Children>
                  <AbstractNodeData Name="Text_2" ActionTag="930211400" Tag="729" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="33.0000" RightMargin="33.0000" TopMargin="56.5000" BottomMargin="-26.5000" FontSize="20" LabelText="for icon" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="70.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="68.0000" Y="-15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.2830" />
                    <PreSize X="0.5147" Y="0.4340" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="163.8717" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2521" />
                <PreSize X="0.2386" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_14_0_0" ActionTag="1750448400" Tag="730" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="132.0000" RightMargin="132.0000" TopMargin="297.3738" BottomMargin="299.6262" LabelText="Psydom Records" ctype="TextBMFontObjectData">
                <Size X="306.0000" Y="53.0000" />
                <Children>
                  <AbstractNodeData Name="Text_2" ActionTag="-1791633082" Tag="731" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="83.0000" RightMargin="83.0000" TopMargin="56.5000" BottomMargin="-26.5000" FontSize="20" LabelText="for game music" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="153.0000" Y="-15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.2830" />
                    <PreSize X="0.4575" Y="0.4340" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="326.1262" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5017" />
                <PreSize X="0.5368" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_14_0" ActionTag="215504742" Tag="732" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="148.5000" RightMargin="148.5000" TopMargin="175.1974" BottomMargin="421.8026" LabelText="Kevin MacLead" ctype="TextBMFontObjectData">
                <Size X="273.0000" Y="53.0000" />
                <Children>
                  <AbstractNodeData Name="Text_2" ActionTag="874408784" Tag="733" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="66.5000" RightMargin="66.5000" TopMargin="56.5000" BottomMargin="-26.5000" FontSize="20" LabelText="for menu music" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="136.5000" Y="-15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.2830" />
                    <PreSize X="0.5128" Y="0.4340" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="448.3026" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6897" />
                <PreSize X="0.4789" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_14" ActionTag="1297169741" Tag="735" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="249.5000" RightMargin="249.5000" TopMargin="55.8014" BottomMargin="541.1986" LabelText="X00" ctype="TextBMFontObjectData">
                <Size X="71.0000" Y="53.0000" />
                <Children>
                  <AbstractNodeData Name="Text_2" ActionTag="312075233" Tag="736" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-30.5000" RightMargin="-30.5000" TopMargin="56.5000" BottomMargin="-26.5000" FontSize="20" LabelText="for the graphic" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="132.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="35.5000" Y="-15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.2830" />
                    <PreSize X="1.8592" Y="0.4340" />
                    <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="567.6986" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8734" />
                <PreSize X="0.1246" Y="0.0815" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0379" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="616919338" Tag="801" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-25.5000" RightMargin="-25.5000" TopMargin="274.9580" BottomMargin="-302.9580" LabelText="1/3" ctype="TextBMFontObjectData">
            <Size X="51.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-288.9580" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>