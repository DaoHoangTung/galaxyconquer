<GameFile>
  <PropertyGroup Name="Credit3" Type="Node" ID="73d97f13-3056-4979-8139-fe2cf5e2c3f0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="60" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="720252673" Tag="62" IconVisible="False" LeftMargin="-286.0379" RightMargin="-283.9621" TopMargin="-324.9998" BottomMargin="-325.0002" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="570.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="Text_2" ActionTag="930211400" Tag="729" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="250.0000" RightMargin="250.0000" TopMargin="545.7368" BottomMargin="81.2632" FontSize="20" LabelText="for icon" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="92.7632" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1427" />
                <PreSize X="0.1228" Y="0.0354" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView_1" ActionTag="1171611518" Tag="750" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="35.0000" RightMargin="35.0000" TopMargin="2.5818" BottomMargin="447.4182" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" DirectionType="Vertical" HorizontalType="Align_HorizontalCenter" ctype="ListViewObjectData">
                <Size X="500.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_0" ActionTag="-1563536640" Tag="726" IconVisible="False" LeftMargin="176.0000" RightMargin="176.0000" BottomMargin="477.0000" LabelText="Hemske" ctype="TextBMFontObjectData">
                    <Size X="148.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="503.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.9500" />
                    <PreSize X="0.2960" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0" ActionTag="852186682" ZOrder="1" Tag="727" IconVisible="False" LeftMargin="195.5000" RightMargin="195.5000" TopMargin="53.0000" BottomMargin="424.0000" LabelText="Xezko" ctype="TextBMFontObjectData">
                    <Size X="109.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="450.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8500" />
                    <PreSize X="0.2180" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1" ActionTag="-1819773005" ZOrder="2" Tag="751" IconVisible="False" LeftMargin="203.5000" RightMargin="203.5000" TopMargin="106.0000" BottomMargin="371.0000" LabelText="Fjury" ctype="TextBMFontObjectData">
                    <Size X="93.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="397.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.7500" />
                    <PreSize X="0.1860" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0" ActionTag="701897822" ZOrder="3" Tag="752" IconVisible="False" LeftMargin="124.0000" RightMargin="124.0000" TopMargin="159.0000" BottomMargin="318.0000" LabelText="Hellx-Magnus" ctype="TextBMFontObjectData">
                    <Size X="252.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="344.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6500" />
                    <PreSize X="0.5040" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0_0" ActionTag="-1828129904" ZOrder="4" Tag="753" IconVisible="False" LeftMargin="140.5000" RightMargin="140.5000" TopMargin="212.0000" BottomMargin="265.0000" LabelText="PrinceYaser" ctype="TextBMFontObjectData">
                    <Size X="219.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="291.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="0.4380" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0_0_0" ActionTag="-547284717" ZOrder="5" Tag="754" IconVisible="False" LeftMargin="123.5000" RightMargin="123.5000" TopMargin="265.0000" BottomMargin="212.0000" LabelText="Don Valentino" ctype="TextBMFontObjectData">
                    <Size X="253.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="238.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.4500" />
                    <PreSize X="0.5060" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0_0_0_0" ActionTag="1957932170" ZOrder="6" Tag="799" IconVisible="False" LeftMargin="105.0000" RightMargin="105.0000" TopMargin="318.0000" BottomMargin="159.0000" LabelText="Eldin HawkWing" ctype="TextBMFontObjectData">
                    <Size X="290.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="185.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3500" />
                    <PreSize X="0.5800" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0_0_0_0_0" ActionTag="1540208948" ZOrder="7" Tag="802" IconVisible="False" LeftMargin="155.0000" RightMargin="155.0000" TopMargin="371.0000" BottomMargin="106.0000" LabelText="The_Silent " ctype="TextBMFontObjectData">
                    <Size X="190.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="132.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2500" />
                    <PreSize X="0.3800" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0_0_0_0_0_0" ActionTag="1631382929" ZOrder="8" Tag="1163" IconVisible="False" LeftMargin="169.5000" RightMargin="169.5000" TopMargin="424.0000" BottomMargin="53.0000" LabelText="Darkfang" ctype="TextBMFontObjectData">
                    <Size X="161.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="79.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1500" />
                    <PreSize X="0.3220" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BitmapFontLabel_14_0_0_0_0_1_0_0_0_0_0_0_0" ActionTag="-993013730" ZOrder="9" Tag="1233" IconVisible="False" LeftMargin="150.5000" RightMargin="150.5000" TopMargin="477.0000" LabelText="OgeRfaCes" ctype="TextBMFontObjectData">
                    <Size X="199.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="250.0000" Y="26.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.0500" />
                    <PreSize X="0.3980" Y="0.1000" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="547.4182" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8422" />
                <PreSize X="0.8772" Y="0.3077" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0379" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="1637717084" Tag="800" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-27.0000" RightMargin="-27.0000" TopMargin="274.9580" BottomMargin="-302.9580" LabelText="3/3" ctype="TextBMFontObjectData">
            <Size X="54.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-288.9580" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>