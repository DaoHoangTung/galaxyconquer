<GameFile>
  <PropertyGroup Name="Instruction1" Type="Node" ID="4dd966df-17ef-438b-bcdf-0d16086069b8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="60" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="720252673" Tag="62" IconVisible="False" LeftMargin="-286.0379" RightMargin="-283.9621" TopMargin="-324.9998" BottomMargin="-325.0002" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="570.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="1253860434" Tag="120" IconVisible="False" LeftMargin="112.8781" RightMargin="111.1219" TopMargin="22.6422" BottomMargin="565.3578" FontSize="25" LabelText="Drive you ship to beat down all &#xA;enemies coming." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="346.0000" Y="62.0000" />
                <Children>
                  <AbstractNodeData Name="PlayerShip0_1" ActionTag="12119919" Tag="63" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="154.0000" RightMargin="154.0000" TopMargin="74.7867" BottomMargin="-57.7867" ctype="SpriteObjectData">
                    <Size X="38.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="173.0000" Y="-35.2867" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.5691" />
                    <PreSize X="0.1098" Y="0.7258" />
                    <FileData Type="PlistSubImage" Path="PlayerShip0.png" Plist="Ship/PlayerShip.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.8781" Y="596.3578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5015" Y="0.9175" />
                <PreSize X="0.6070" Y="0.0954" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0" ActionTag="490641063" Tag="127" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="76.5000" RightMargin="76.5000" TopMargin="178.2733" BottomMargin="440.7267" FontSize="25" LabelText="Collect treasure from killing enemies" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="417.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="SilverCoin0_6" ActionTag="-2146179329" Tag="123" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="113.7207" RightMargin="291.2793" TopMargin="62.7426" BottomMargin="-43.7426" ctype="SpriteObjectData">
                    <Size X="12.0000" Y="12.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="119.7207" Y="-37.7426" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2871" Y="-1.2175" />
                    <PreSize X="0.0288" Y="0.3871" />
                    <FileData Type="PlistSubImage" Path="SilverCoin0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Diamond0_9" ActionTag="182293076" Tag="126" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="284.6923" RightMargin="116.3077" TopMargin="61.7772" BottomMargin="-46.7772" ctype="SpriteObjectData">
                    <Size X="16.0000" Y="16.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="292.6923" Y="-38.7772" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7019" Y="-1.2509" />
                    <PreSize X="0.0384" Y="0.5161" />
                    <FileData Type="PlistSubImage" Path="Diamond0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="GoldBar0_8" ActionTag="1485329268" Tag="125" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="221.6010" RightMargin="177.3990" TopMargin="55.3736" BottomMargin="-55.3736" ctype="SpriteObjectData">
                    <Size X="18.0000" Y="31.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="230.6010" Y="-39.8736" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5530" Y="-1.2862" />
                    <PreSize X="0.0432" Y="1.0000" />
                    <FileData Type="PlistSubImage" Path="GoldBar0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="GoldCoin0_7" ActionTag="951114378" Tag="124" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="164.2194" RightMargin="240.7806" TopMargin="63.7670" BottomMargin="-44.7670" ctype="SpriteObjectData">
                    <Size X="12.0000" Y="12.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="170.2194" Y="-38.7670" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4082" Y="-1.2505" />
                    <PreSize X="0.0288" Y="0.3871" />
                    <FileData Type="PlistSubImage" Path="GoldCoin0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="456.2267" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7019" />
                <PreSize X="0.7316" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0" ActionTag="554318318" Tag="128" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="205.4480" RightMargin="209.5520" TopMargin="309.7019" BottomMargin="309.2981" FontSize="25" LabelText="Special items" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="155.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="PowerupExtraLife0_14" ActionTag="1837023624" Tag="133" IconVisible="False" LeftMargin="-21.9409" RightMargin="156.9409" TopMargin="56.0000" BottomMargin="-45.0000" ctype="SpriteObjectData">
                    <Size X="20.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-11.9409" Y="-35.0000" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0770" Y="-1.1290" />
                    <PreSize X="0.1290" Y="0.6452" />
                    <FileData Type="PlistSubImage" Path="PowerupExtraLife0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PowerupMedic0_15" ActionTag="810498554" Tag="134" IconVisible="False" LeftMargin="39.2929" RightMargin="95.7071" TopMargin="56.0000" BottomMargin="-45.0000" ctype="SpriteObjectData">
                    <Size X="20.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.2929" Y="-35.0000" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3180" Y="-1.1290" />
                    <PreSize X="0.1290" Y="0.6452" />
                    <FileData Type="PlistSubImage" Path="PowerupMedic0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PowerupMultiplier0_16" ActionTag="-1961317727" Tag="135" IconVisible="False" LeftMargin="102.6022" RightMargin="32.3978" TopMargin="56.0000" BottomMargin="-45.0000" ctype="SpriteObjectData">
                    <Size X="20.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.6022" Y="-35.0000" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7265" Y="-1.1290" />
                    <PreSize X="0.1290" Y="0.6452" />
                    <FileData Type="PlistSubImage" Path="PowerupMultiplier0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PowerupShield0_17" ActionTag="184523050" Tag="136" IconVisible="False" LeftMargin="163.8361" RightMargin="-28.8361" TopMargin="56.0000" BottomMargin="-45.0000" ctype="SpriteObjectData">
                    <Size X="20.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="173.8361" Y="-35.0000" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1215" Y="-1.1290" />
                    <PreSize X="0.1290" Y="0.6452" />
                    <FileData Type="PlistSubImage" Path="PowerupShield0.png" Plist="BonusItem/Bonus.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="282.9480" Y="324.7981" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4964" Y="0.4997" />
                <PreSize X="0.2719" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0" ActionTag="1940645133" Tag="137" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="123.0000" RightMargin="123.0000" TopMargin="432.8291" BottomMargin="186.1709" FontSize="25" LabelText="Train level to unlock abilities" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="324.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="ListView_3" ActionTag="1938844779" Tag="154" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="12.0000" RightMargin="12.0000" TopMargin="44.4890" BottomMargin="-77.4890" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ItemMargin="10" ctype="ListViewObjectData">
                    <Size X="300.0000" Y="64.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_28" ActionTag="1309960199" Tag="155" IconVisible="False" RightMargin="236.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="32.0000" Y="32.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1067" Y="0.5000" />
                        <PreSize X="0.2133" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="ScoreSkillActive1.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_28_0" ActionTag="1273201850" ZOrder="1" Tag="156" IconVisible="False" LeftMargin="74.0000" RightMargin="162.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="106.0000" Y="32.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3533" Y="0.5000" />
                        <PreSize X="0.2133" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="ScoreSkillActive10.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_28_0_0" ActionTag="1925239243" ZOrder="2" Tag="157" IconVisible="False" LeftMargin="148.0000" RightMargin="88.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="180.0000" Y="32.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6000" Y="0.5000" />
                        <PreSize X="0.2133" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="ScoreSkillActive2.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_28_0_1" ActionTag="136534405" ZOrder="3" Tag="158" IconVisible="False" LeftMargin="222.0000" RightMargin="14.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                        <Size X="64.0000" Y="64.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="254.0000" Y="32.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8467" Y="0.5000" />
                        <PreSize X="0.2133" Y="1.0000" />
                        <FileData Type="PlistSubImage" Path="ScoreSkillActive8.jpg" Plist="NewSkillShop/ScoreSkillIcon.plist" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="162.0000" Y="-45.4890" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-1.4674" />
                    <PreSize X="0.9259" Y="2.0645" />
                    <SingleColor A="255" R="150" G="150" B="255" />
                    <FirstColor A="255" R="150" G="150" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="201.6709" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3103" />
                <PreSize X="0.5684" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0_0_0" ActionTag="-1324649094" Tag="159" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="436.4630" RightMargin="110.5370" TopMargin="514.4022" BottomMargin="106.5978" FontSize="25" LabelText="... " HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="23.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="447.9630" Y="121.0978" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7859" Y="0.1863" />
                <PreSize X="0.0404" Y="0.0446" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="372375918" Tag="228" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="259.0000" RightMargin="259.0000" TopMargin="597.9582" BottomMargin="24.0418" LabelText="1/4" ctype="TextBMFontObjectData">
                <Size X="52.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="38.0418" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0585" />
                <PreSize X="0.0912" Y="0.0431" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0379" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>