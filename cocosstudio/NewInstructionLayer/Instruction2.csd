<GameFile>
  <PropertyGroup Name="Instruction2" Type="Node" ID="0e432185-6963-47a8-ae72-6055fd2c75ce" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="60" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="720252673" Tag="62" IconVisible="False" LeftMargin="-286.0379" RightMargin="-283.9621" TopMargin="-324.9998" BottomMargin="-325.0002" ClipAble="False" BackColorAlpha="25" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="570.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="1253860434" Tag="120" IconVisible="False" LeftMargin="112.3781" RightMargin="110.6219" TopMargin="22.6422" BottomMargin="565.3578" FontSize="25" LabelText="Killing boss to get big treasure&#xA;and unlock new map" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="347.0000" Y="62.0000" />
                <Children>
                  <AbstractNodeData Name="PlayerShip0_1" ActionTag="12119919" Tag="63" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-10.8873" RightMargin="257.8873" TopMargin="80.2865" BottomMargin="-118.2865" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="100.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="39.1127" Y="-68.2865" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1127" Y="-1.1014" />
                    <PreSize X="0.2882" Y="1.6129" />
                    <FileData Type="PlistSubImage" Path="BossDescription1.jpg" Plist="NewLobbyScene/BossDescription.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PlayerShip0_1_0" ActionTag="-1983514895" Tag="108" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="126.5087" RightMargin="120.4913" TopMargin="80.2900" BottomMargin="-118.2900" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="100.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5087" Y="-68.2900" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5087" Y="-1.1015" />
                    <PreSize X="0.2882" Y="1.6129" />
                    <FileData Type="PlistSubImage" Path="BossDescription2.jpg" Plist="NewLobbyScene/BossDescription.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PlayerShip0_1_0_0" ActionTag="2027299729" Tag="109" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="263.8873" RightMargin="-16.8873" TopMargin="80.2865" BottomMargin="-118.2865" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="100.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="313.8873" Y="-68.2865" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9046" Y="-1.1014" />
                    <PreSize X="0.2882" Y="1.6129" />
                    <FileData Type="PlistSubImage" Path="BossDescription3.jpg" Plist="NewLobbyScene/BossDescription.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.8781" Y="596.3578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5015" Y="0.9175" />
                <PreSize X="0.6088" Y="0.0954" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_7" ActionTag="372375918" Tag="228" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="257.5000" RightMargin="257.5000" TopMargin="597.9582" BottomMargin="24.0418" LabelText="2/4" ctype="TextBMFontObjectData">
                <Size X="55.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="38.0418" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0585" />
                <PreSize X="0.0965" Y="0.0431" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Erie_Black_text.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0" ActionTag="829863353" Tag="110" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="115.5000" RightMargin="115.5000" TopMargin="251.1422" BottomMargin="367.8578" FontSize="25" LabelText="Use coin to upgrade your Ship" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="339.0000" Y="31.0000" />
                <Children>
                  <AbstractNodeData Name="PlayerShip0_1" ActionTag="1336992270" Tag="111" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="53.2110" RightMargin="221.7890" TopMargin="54.0000" BottomMargin="-87.0000" ctype="SpriteObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="85.2110" Y="-55.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2514" Y="-1.7742" />
                    <PreSize X="0.1888" Y="2.0645" />
                    <FileData Type="PlistSubImage" Path="BTNUpgradeGunNW1.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PlayerShip0_1_0" ActionTag="2115163553" Tag="112" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="140.4393" RightMargin="134.5607" TopMargin="54.0000" BottomMargin="-87.0000" ctype="SpriteObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="172.4393" Y="-55.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5087" Y="-1.7742" />
                    <PreSize X="0.1888" Y="2.0645" />
                    <FileData Type="PlistSubImage" Path="BTNUpgradeGunN1.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="PlayerShip0_1_0_0" ActionTag="-1212189152" Tag="113" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="223.6507" RightMargin="51.3493" TopMargin="54.0000" BottomMargin="-87.0000" ctype="SpriteObjectData">
                    <Size X="64.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="255.6507" Y="-55.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7541" Y="-1.7742" />
                    <PreSize X="0.1888" Y="2.0645" />
                    <FileData Type="PlistSubImage" Path="BTNUpgradeGunNE1.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="383.3578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5898" />
                <PreSize X="0.5947" Y="0.0477" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0_0" ActionTag="-59896128" Tag="114" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="83.0000" RightMargin="83.0000" TopMargin="410.6422" BottomMargin="177.3578" FontSize="25" LabelText="Gain 1 skill point for leveling up and &#xA;extra 1 point at 5, 10, 15..." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="5.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="404.0000" Y="62.0000" />
                <Children>
                  <AbstractNodeData Name="PlayerShip0_1_0" ActionTag="1485155808" Tag="116" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="150.0000" RightMargin="150.0000" TopMargin="59.0000" BottomMargin="-101.0000" ctype="SpriteObjectData">
                    <Size X="104.0000" Y="104.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="202.0000" Y="-49.0000" />
                    <Scale ScaleX="0.6000" ScaleY="0.6000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.7903" />
                    <PreSize X="0.2574" Y="1.6774" />
                    <FileData Type="Normal" Path="NewSkillShop/Icon_Skill_Point.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="285.0000" Y="208.3578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3206" />
                <PreSize X="0.7088" Y="0.0954" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0379" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>