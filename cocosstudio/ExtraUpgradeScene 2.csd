<GameFile>
  <PropertyGroup Name="ExtraUpgradeScene" Type="Scene" ID="6b63b10c-201b-4bc3-a4cf-6c9f50285085" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="1045" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="-1004895715" Tag="1152" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-193.4080" RightMargin="-190.5920" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.5920" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4978" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0" ActionTag="230065660" Tag="1153" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="2040598068" Tag="1154" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-5.0000" RightMargin="-5.0000" TopMargin="-40.6074" BottomMargin="800.6074" LeftEage="55" RightEage="55" TopEage="11" BottomEage="11" Scale9OriginX="55" Scale9OriginY="11" Scale9Width="168" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="650.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_19" ActionTag="-2013981348" Tag="1155" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="204.0000" RightMargin="204.0000" TopMargin="89.9978" BottomMargin="60.0022" LabelText="Extra Upgrade" ctype="TextBMFontObjectData">
                <Size X="242.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="325.0000" Y="85.0022" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4250" />
                <PreSize X="0.3723" Y="0.2500" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="900.6074" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9381" />
            <PreSize X="1.0156" Y="0.2083" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayer" ActionTag="532680950" Tag="1185" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="480.0000" BottomMargin="480.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="BTNUpgradeGunN2.png" ActionTag="461017189" IconVisible="False" LeftMargin="-31.9965" RightMargin="-32.0035" TopMargin="-182.0000" BottomMargin="118.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="0.0035" Y="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunN2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunN2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunN2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunNE2.png" ActionTag="-374881650" Tag="1" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="-132.0000" BottomMargin="68.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunNE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunNE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunNE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunE2.png" ActionTag="64700334" Tag="2" IconVisible="False" LeftMargin="118.0000" RightMargin="-182.0000" TopMargin="-32.0000" BottomMargin="-32.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunSE2.png" ActionTag="-453573756" Tag="3" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="68.0000" BottomMargin="-132.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="-100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunSE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunSE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunSE2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunS2.png" ActionTag="-778987274" Tag="4" IconVisible="False" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="118.0000" BottomMargin="-182.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunS2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunS2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunS2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunSW2.png" ActionTag="-289412737" Tag="5" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="68.0000" BottomMargin="-132.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="-100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunSW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunSW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunSW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunW2.png" ActionTag="-617607519" Tag="6" IconVisible="False" LeftMargin="-182.0000" RightMargin="118.0000" TopMargin="-32.0000" BottomMargin="-32.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeGunNW2.png" ActionTag="921359313" Tag="7" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="-132.0000" BottomMargin="68.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeGunNW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeGunNW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeGunNW2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNaborbShieldDamage2.png" ActionTag="686738905" Tag="12" IconVisible="False" LeftMargin="-157.0000" RightMargin="93.0000" TopMargin="-257.0000" BottomMargin="193.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-125.0000" Y="225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNaborbShieldDamage2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNaborbShieldDamage2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNaborbShieldDamage2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeRocket2.png" ActionTag="639535188" Tag="9" IconVisible="False" LeftMargin="93.0000" RightMargin="-157.0000" TopMargin="-257.0000" BottomMargin="193.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="125.0000" Y="225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeRocket2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeRocket2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeRocket2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeSpeed2.png" ActionTag="1614777701" Tag="11" IconVisible="False" LeftMargin="93.0000" RightMargin="-157.0000" TopMargin="193.0000" BottomMargin="-257.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="125.0000" Y="-225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeSpeed2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeSpeed2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeSpeed2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BTNUpgradeROF2.png" ActionTag="-123933098" Tag="10" IconVisible="False" LeftMargin="-157.0000" RightMargin="93.0000" TopMargin="193.0000" BottomMargin="-257.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-125.0000" Y="-225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="BTNUpgradeROF2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <PressedFileData Type="PlistSubImage" Path="BTNUpgradeROF2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <NormalFileData Type="PlistSubImage" Path="BTNUpgradeROF2.png" Plist="NewShopItemScene/ShopItemActive.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.1000" ScaleY="1.1000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_2" ActionTag="-228391769" Tag="1216" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="288.0000" BottomMargin="672.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_3" ActionTag="1928091346" Tag="1220" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="-464.9998" RightMargin="174.9998" TopMargin="137.0001" BottomMargin="-237.0001" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="290.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-319.9998" Y="-187.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3_0" ActionTag="2009655569" Tag="1221" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="175.0000" RightMargin="-465.0000" TopMargin="137.0000" BottomMargin="-237.0000" FlipY="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="290.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="-187.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="672.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1389454956" Tag="1225" RotationSkewX="14.9983" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="84.5000" RightMargin="84.5000" TopMargin="815.0461" BottomMargin="91.9539" LabelText="Click to do extra upgrade !" ctype="TextBMFontObjectData">
            <Size X="471.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="118.4539" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1234" />
            <PreSize X="0.7359" Y="0.0552" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="1630411290" Tag="1490" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="480.5760" BottomMargin="479.4240" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="320.0000" Y="479.4240" />
            <Scale ScaleX="2.0919" ScaleY="2.0243" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4994" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0" ActionTag="870531427" Tag="1224" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="245.0000" RightMargin="245.0000" TopMargin="880.6416" BottomMargin="29.3584" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="54.3584" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0566" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>