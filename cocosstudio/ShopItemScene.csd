<GameFile>
  <PropertyGroup Name="ShopItemScene" Type="Scene" ID="d6fe73fc-8285-4f56-9ab5-a4cbf7c205bd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="102" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Image_3" ActionTag="1323743096" Alpha="153" Tag="200" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-1.9200" RightMargin="1.9200" TopMargin="-1.9200" BottomMargin="1.9200" LeftEage="165" RightEage="165" TopEage="198" BottomEage="198" Scale9OriginX="165" Scale9OriginY="198" Scale9Width="470" Scale9Height="204" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.0800" Y="481.9200" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4970" Y="0.5020" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="NewShopItemScene/bg_1_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_3_0" ActionTag="101172967" Alpha="153" Tag="300" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-640.0000" RightMargin="640.0000" LeftEage="165" RightEage="165" TopEage="198" BottomEage="198" Scale9OriginX="165" Scale9OriginY="198" Scale9Width="470" Scale9Height="204" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="NewShopItemScene/bg_1_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayer" ActionTag="-605266962" Tag="1" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="528.0000" BottomMargin="432.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="rocket" ActionTag="1371224152" Tag="93" IconVisible="False" LeftMargin="168.0001" RightMargin="-232.0001" TopMargin="116.6260" BottomMargin="-180.6260" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0001" Y="-148.6260" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeRocket0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeRocket0.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeRocket0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="shieldRecharge" ActionTag="1468678336" Tag="92" IconVisible="False" LeftMargin="168.0000" RightMargin="-232.0000" TopMargin="-182.0000" BottomMargin="118.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0000" Y="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableaborbShieldRecharge0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldRecharge0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldRecharge0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="shieldRange" ActionTag="-1076848961" Tag="91" IconVisible="False" LeftMargin="67.5760" RightMargin="-131.5760" TopMargin="-255.0592" BottomMargin="191.0592" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.4285" ScaleY="0.4522" />
                <Position X="95.0000" Y="220.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableaborbShieldRange0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldRange0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldRange0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="shieldDuration" ActionTag="-671901273" Tag="90" IconVisible="False" LeftMargin="-133.7530" RightMargin="69.7530" TopMargin="-256.0910" BottomMargin="192.0910" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-101.7530" Y="224.0910" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableaborbShieldDuration0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldDuration0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldDuration0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="shieldDamage" ActionTag="-1610309158" Tag="89" IconVisible="False" LeftMargin="-233.7531" RightMargin="169.7531" TopMargin="-182.0000" BottomMargin="118.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-201.7531" Y="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableaborbShieldDamage0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldDamage0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNaborbShieldDamage0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="speed" ActionTag="940787671" Tag="95" IconVisible="False" LeftMargin="66.2467" RightMargin="-130.2467" TopMargin="193.0000" BottomMargin="-257.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="98.2467" Y="-225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeSpeed0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeSpeed0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeSpeed0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rof" ActionTag="-982006189" Tag="96" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="193.0000" BottomMargin="-257.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="-225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeROF0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeROF0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeROF0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="magnet" ActionTag="-535225287" Tag="97" IconVisible="False" LeftMargin="-231.9969" RightMargin="167.9969" TopMargin="117.9983" BottomMargin="-181.9983" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.3709" ScaleY="0.3919" />
                <Position X="-208.2593" Y="-156.9167" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeMagnet0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="wGun" ActionTag="-1796828548" Tag="98" IconVisible="False" LeftMargin="-182.0000" RightMargin="118.0000" TopMargin="-32.0000" BottomMargin="-32.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeMagnet0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunW0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="swGun" ActionTag="-644034225" Tag="99" IconVisible="False" LeftMargin="-132.0000" RightMargin="68.0000" TopMargin="68.0000" BottomMargin="-132.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-100.0000" Y="-100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeMagnet0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunSW0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sGun" ActionTag="226494240" Tag="100" IconVisible="False" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="118.0000" BottomMargin="-182.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeMagnet0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunS0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nwGun" ActionTag="405407957" Tag="101" IconVisible="False" LeftMargin="-131.9999" RightMargin="67.9999" TopMargin="-131.8464" BottomMargin="67.8464" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.4206" ScaleY="0.5024" />
                <Position X="-105.0815" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeGunNW0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunNW0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunNW0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="neGun" ActionTag="1875570922" Tag="102" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="-132.0000" BottomMargin="68.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeMagnet0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunNE0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nGun" ActionTag="-479298450" Tag="103" IconVisible="False" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="-182.0000" BottomMargin="118.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeGunN1 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunN0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunN0.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="eGun" ActionTag="-1563994527" Tag="104" IconVisible="False" LeftMargin="118.0000" RightMargin="-182.0000" TopMargin="-32.0000" BottomMargin="-32.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeGunE0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunE0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunE0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="seGun" ActionTag="1669877763" Tag="105" IconVisible="False" LeftMargin="68.0000" RightMargin="-132.0000" TopMargin="68.0000" BottomMargin="-132.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="64.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="-100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="NewShopItemScene/.ShopItemDisable_PList.Dir/BTNdisableUpgradeMagnet0 2.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeMagnet0 2.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewShopItemScene/.ShopItemActive_PList.Dir/BTNUpgradeGunSE0 2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="432.0000" />
            <Scale ScaleX="1.1000" ScaleY="1.1000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4500" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="-1061495159" Tag="2" IconVisible="False" LeftMargin="-68.0726" RightMargin="-61.9274" TopMargin="-43.1713" BottomMargin="853.1713" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="770.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="txtInfo" ActionTag="1584143654" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="147.0000" RightMargin="147.0000" TopMargin="68.0050" BottomMargin="41.9950" FontSize="34" LabelText="fire large bullets from front gun" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="476.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="385.0000" Y="61.9950" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4133" />
                <PreSize X="0.6182" Y="0.2667" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="316.9274" Y="928.1713" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4952" Y="0.9668" />
            <PreSize X="1.2031" Y="0.1563" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="1636897301" Tag="3" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="528.0000" BottomMargin="432.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="320.0000" Y="432.0000" />
            <Scale ScaleX="2.0919" ScaleY="2.0243" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4500" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="870444571" Tag="4" IconVisible="False" LeftMargin="-100.0000" RightMargin="420.0000" TopMargin="110.0000" BottomMargin="790.0000" Scale9Enable="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="-1462539616" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="51.0240" RightMargin="38.9760" TopMargin="0.2460" BottomMargin="-6.2460" LabelText="1,000,000" ctype="TextBMFontObjectData">
                <Size X="230.0000" Y="66.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="444435602" Tag="1" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-49.4641" RightMargin="252.4641" TopMargin="12.9974" BottomMargin="27.0026" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-35.9641" Y="40.0026" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1564" Y="0.6061" />
                    <PreSize X="0.1174" Y="0.3939" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="281.0240" Y="26.7540" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8782" Y="0.4459" />
                <PreSize X="0.7188" Y="1.1000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.0000" Y="820.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.8542" />
            <PreSize X="0.5000" Y="0.0625" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1_0" ActionTag="1260910505" Tag="5" IconVisible="False" LeftMargin="430.0000" RightMargin="-110.0000" TopMargin="113.2819" BottomMargin="786.7181" Scale9Enable="True" LeftEage="91" RightEage="91" TopEage="13" BottomEage="13" Scale9OriginX="91" Scale9OriginY="13" Scale9Width="108" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1703935810" Tag="1" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="36.6720" RightMargin="137.3280" TopMargin="12.7480" BottomMargin="10.2520" LabelText="$1,000,000" ctype="TextBMFontObjectData">
                <Size X="146.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="36.6720" Y="28.7520" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1146" Y="0.4792" />
                <PreSize X="0.4563" Y="0.6167" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="590.0000" Y="816.7181" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9219" Y="0.8507" />
            <PreSize X="0.5000" Y="0.0625" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="back" ActionTag="-1852109770" Tag="99" RotationSkewX="-20.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="441.8000" RightMargin="48.2000" TopMargin="880.6400" BottomMargin="29.3600" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="516.8000" Y="54.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8075" Y="0.0566" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0_0" ActionTag="1157025817" VisibleForFrame="False" Tag="1489" RotationSkewX="20.0003" RotationSkewY="0.0013" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="48.2000" RightMargin="441.8000" TopMargin="880.6400" BottomMargin="29.3600" FontSize="25" ButtonText="Extra" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="123.2000" Y="54.3600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1925" Y="0.0566" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>