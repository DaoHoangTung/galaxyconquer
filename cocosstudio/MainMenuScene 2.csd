<GameFile>
  <PropertyGroup Name="MainMenuScene" Type="Scene" ID="71f3b5d5-4516-42cd-9cd7-6ac06d68eaa5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="183" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="-526900658" Tag="999" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-193.4080" RightMargin="-190.5920" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.5920" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4978" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0" ActionTag="681862991" Tag="782" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="-841405246" Tag="1" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="316.1227" RightMargin="323.8773" TopMargin="603.1680" BottomMargin="356.8320" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Button_1_0" ActionTag="1514764113" Tag="3" RotationSkewX="20.0000" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="-499.1414" RightMargin="49.1414" TopMargin="138.0100" BottomMargin="-218.0100" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="450.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_25_0_0" ActionTag="144390760" Tag="759" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.9204" RightMargin="114.0796" TopMargin="21.5000" BottomMargin="21.5000" LabelText="Credit" ctype="TextBMFontObjectData">
                    <Size X="80.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="295.9204" Y="40.0000" />
                    <Scale ScaleX="1.2000" ScaleY="1.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6576" Y="0.5000" />
                    <PreSize X="0.1778" Y="0.4625" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-274.1414" Y="-178.0100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0_0" ActionTag="1702564693" Tag="2" RotationSkewX="20.0000" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="-447.4395" RightMargin="-2.5605" TopMargin="18.0650" BottomMargin="-98.0650" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="450.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_25_0" ActionTag="-962474520" Tag="758" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="203.1119" RightMargin="106.8881" TopMargin="21.5000" BottomMargin="21.5000" LabelText="Instruction" ctype="TextBMFontObjectData">
                    <Size X="140.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="273.1119" Y="40.0000" />
                    <Scale ScaleX="1.2000" ScaleY="1.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6069" Y="0.5000" />
                    <PreSize X="0.3111" Y="0.4625" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-222.4395" Y="-58.0650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1" ActionTag="-1845914738" Tag="1" RotationSkewX="20.0000" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="-397.4402" RightMargin="-52.5598" TopMargin="-100.9950" BottomMargin="20.9950" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="450.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_25" ActionTag="-1379552835" Tag="756" IconVisible="False" LeftMargin="179.6119" RightMargin="123.3881" TopMargin="21.0638" BottomMargin="21.9362" LabelText="Start Game" ctype="TextBMFontObjectData">
                    <Size X="147.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="253.1119" Y="40.4362" />
                    <Scale ScaleX="1.2000" ScaleY="1.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5625" Y="0.5055" />
                    <PreSize X="0.3267" Y="0.4625" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-172.4402" Y="60.9950" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="165" B="0" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0_1" ActionTag="688125941" Tag="4" RotationSkewX="-20.0000" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="142.8912" RightMargin="-592.8912" TopMargin="247.8239" BottomMargin="-327.8239" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="450.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_25_0" ActionTag="-1045455675" Tag="757" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="77.3646" RightMargin="321.6354" TopMargin="21.5000" BottomMargin="21.5000" LabelText="Exit" ctype="TextBMFontObjectData">
                    <Size X="51.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.8646" Y="40.0000" />
                    <Scale ScaleX="1.2000" ScaleY="1.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2286" Y="0.5000" />
                    <PreSize X="0.1133" Y="0.4625" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="367.8912" Y="-287.8239" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Button_1_0_1_0" ActionTag="1658995839" Tag="5" RotationSkewX="-20.0000" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="81.8602" RightMargin="-531.8602" TopMargin="138.1685" BottomMargin="-218.1685" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="450.0000" Y="80.0000" />
                <Children>
                  <AbstractNodeData Name="BitmapFontLabel_25_0" ActionTag="-1501524459" Tag="760" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="71.3646" RightMargin="315.6354" TopMargin="21.5000" BottomMargin="21.5000" LabelText="Rate" ctype="TextBMFontObjectData">
                    <Size X="63.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.8646" Y="40.0000" />
                    <Scale ScaleX="1.2000" ScaleY="1.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2286" Y="0.5000" />
                    <PreSize X="0.1400" Y="0.4625" />
                    <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="306.8602" Y="-178.1685" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
                <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="316.1227" Y="356.8320" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4939" Y="0.3717" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_21" ActionTag="1995221364" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="51.9680" RightMargin="524.0320" TopMargin="195.3624" BottomMargin="700.6376" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="34" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="64.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1166266975" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="83.4624" RightMargin="-133.4624" TopMargin="71.7072" BottomMargin="-39.7072" LabelText="2,350,300" ctype="TextBMFontObjectData">
                <Size X="114.0000" Y="32.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="83.4624" Y="-23.7072" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.3041" Y="-0.3704" />
                <PreSize X="1.7813" Y="0.5000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_6" ActionTag="11822776" Tag="2" RotationSkewX="20.0041" IconVisible="False" LeftMargin="87.4669" RightMargin="-169.4669" TopMargin="23.4967" BottomMargin="-9.4967" LabelText="Level 13" ctype="TextBMFontObjectData">
                <Size X="146.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="87.4669" Y="15.5033" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.3667" Y="0.2422" />
                <PreSize X="2.2813" Y="0.7813" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_8" ActionTag="-1778723772" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="86.4640" RightMargin="-200.4640" TopMargin="-14.2478" BottomMargin="25.2478" LabelText="KuKulKan" ctype="TextBMFontObjectData">
                <Size X="178.0000" Y="53.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="86.4640" Y="51.7478" />
                <Scale ScaleX="0.6500" ScaleY="0.6500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.3510" Y="0.8086" />
                <PreSize X="2.7813" Y="0.8281" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1_0" ActionTag="696669068" Tag="4" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="331.0000" RightMargin="-320.0000" TopMargin="-2.8664" BottomMargin="31.8664" FontSize="30" LabelText="100" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="53.0000" Y="35.0000" />
                <Children>
                  <AbstractNodeData Name="Image_12" ActionTag="1509369466" Tag="746" RotationSkewX="-0.9268" RotationSkewY="-0.9232" IconVisible="False" LeftMargin="-87.3355" RightMargin="9.3355" TopMargin="-46.5112" BottomMargin="-47.4888" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="101" Scale9Height="99" ctype="ImageViewObjectData">
                    <Size X="131.0000" Y="129.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-21.8355" Y="17.0112" />
                    <Scale ScaleX="0.3500" ScaleY="0.3500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.4120" Y="0.4860" />
                    <PreSize X="2.4717" Y="3.6857" />
                    <FileData Type="Normal" Path="NewSkillShop/Purple Star Icon 2.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="384.0000" Y="49.3664" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="6.0000" Y="0.7714" />
                <PreSize X="0.8281" Y="0.5469" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1_0" ActionTag="1827475088" Tag="5" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="154.0000" RightMargin="-320.0000" TopMargin="21.7693" BottomMargin="-23.7693" LabelText="1,000,000" ctype="TextBMFontObjectData">
                <Size X="230.0000" Y="66.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="919202371" Tag="748" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-49.4641" RightMargin="252.4641" TopMargin="12.9974" BottomMargin="27.0026" LeftEage="8" RightEage="8" TopEage="8" BottomEage="8" Scale9OriginX="8" Scale9OriginY="8" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-35.9641" Y="40.0026" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1564" Y="0.6061" />
                    <PreSize X="0.1174" Y="0.3939" />
                    <FileData Type="Normal" Path="NewMissionSceneResource/coinIcon.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="384.0000" Y="9.2307" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="6.0000" Y="0.1442" />
                <PreSize X="3.5938" Y="1.0313" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Yellow.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="83.9680" Y="732.6376" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1312" Y="0.7632" />
            <PreSize X="0.1000" Y="0.0667" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="avatar0.jpg" Plist="NewUserPanel/UserAvatar.plist" />
            <PressedFileData Type="PlistSubImage" Path="avatar0.jpg" Plist="NewUserPanel/UserAvatar.plist" />
            <NormalFileData Type="PlistSubImage" Path="avatar0.jpg" Plist="NewUserPanel/UserAvatar.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="-877753236" Tag="998" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-5.0000" RightMargin="-5.0000" TopMargin="-40.6074" BottomMargin="800.6074" LeftEage="55" RightEage="55" TopEage="11" BottomEage="11" Scale9OriginX="55" Scale9OriginY="11" Scale9Width="168" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="650.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_19" ActionTag="475780855" Tag="744" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="158.5000" RightMargin="158.5000" TopMargin="89.9978" BottomMargin="60.0022" LabelText="Galaxy Commander" ctype="TextBMFontObjectData">
                <Size X="333.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="325.0000" Y="85.0022" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4250" />
                <PreSize X="0.5123" Y="0.2500" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Title.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="900.6074" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9381" />
            <PreSize X="1.0156" Y="0.2083" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1942754894" Tag="115" IconVisible="False" LeftMargin="137.4761" RightMargin="482.5239" TopMargin="180.5074" BottomMargin="674.4926" ctype="SpriteObjectData">
            <Size X="20.0000" Y="105.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="147.4761" Y="726.9926" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2304" Y="0.7573" />
            <PreSize X="0.0313" Y="0.1094" />
            <FileData Type="Normal" Path="NewLobbyScene/153_TabDialogBorder_l_TabDialogBorder_l.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="1907085847" Tag="121" IconVisible="False" LeftMargin="47.1904" RightMargin="512.8096" TopMargin="282.9508" BottomMargin="642.0492" LabelText="Score:" ctype="TextBMFontObjectData">
            <Size X="80.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="87.1904" Y="659.5492" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1362" Y="0.6870" />
            <PreSize X="0.1250" Y="0.0365" />
            <LabelBMFontFile_CNB Type="Normal" Path="fonts/UTM_Sharnay_text.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>