<GameFile>
  <PropertyGroup Name="CursorEffect" Type="Node" ID="5688d2e4-245f-46b8-aec3-85570389e10f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1086" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="213585801" Tag="1092" IconVisible="False" LeftMargin="-95.6216" RightMargin="-90.3784" TopMargin="-99.1751" BottomMargin="-103.8249" LeftEage="61" RightEage="61" TopEage="66" BottomEage="66" Scale9OriginX="61" Scale9OriginY="66" Scale9Width="64" Scale9Height="71" ctype="ImageViewObjectData">
            <Size X="186.0000" Y="203.0000" />
            <Children>
              <AbstractNodeData Name="Particle_1" ActionTag="248698716" Tag="1" IconVisible="True" LeftMargin="96.9821" RightMargin="89.0179" TopMargin="93.9417" BottomMargin="109.0583" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="96.9821" Y="109.0583" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5214" Y="0.5372" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="NewParticle/cursorEffectBorder.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-2.6216" Y="-2.3249" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewMenu/GearIcon.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>