<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="-552813970" Tag="56" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-194.1825" RightMargin="-189.8175" TopMargin="-30.4208" BottomMargin="-33.5792" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="317.8175" Y="478.4208" />
            <Scale ScaleX="0.6359" ScaleY="0.9754" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4966" Y="0.4984" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="test" ActionTag="2005392108" Tag="1" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="83.4998" RightMargin="100.5002" TopMargin="189.8392" BottomMargin="705.1608" FontSize="50" LabelText="Galaxy Conquer" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="456.0000" Y="65.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="311.4998" Y="737.6608" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.4867" Y="0.7684" />
            <PreSize X="0.7125" Y="0.0677" />
            <FontResource Type="Normal" Path="TTF Fonts/KARNIVOR.ttf" Plist="" />
            <OutlineColor A="255" R="77" G="77" B="77" />
            <ShadowColor A="255" R="255" G="255" B="255" />
          </AbstractNodeData>
          <AbstractNodeData Name="start" ActionTag="1201548813" CallBackType="Touch" CallBackName="StartGame" Tag="5" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="155.0000" RightMargin="195.0000" TopMargin="291.5258" BottomMargin="628.4742" TouchEnable="True" FontSize="18" ButtonText="Start Game" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="290.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="300.0000" Y="648.4742" />
            <Scale ScaleX="1.2500" ScaleY="1.2500" />
            <CColor A="255" R="230" G="230" B="250" />
            <PrePosition X="0.4688" Y="0.6755" />
            <PreSize X="0.4531" Y="0.0417" />
            <TextColor A="255" R="144" G="238" B="144" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove 2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="howto" ActionTag="1757613868" CallBackType="Touch" Tag="4" IconVisible="False" LeftMargin="153.3301" RightMargin="196.6699" TopMargin="358.8079" BottomMargin="561.1921" TouchEnable="True" FontSize="18" ButtonText="How to Play" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="290.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.3301" Y="581.1921" />
            <Scale ScaleX="1.2500" ScaleY="1.2500" />
            <CColor A="255" R="230" G="230" B="250" />
            <PrePosition X="0.4661" Y="0.6054" />
            <PreSize X="0.4531" Y="0.0417" />
            <TextColor A="255" R="144" G="238" B="144" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_up 2.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="exit" ActionTag="1433205829" CallBackType="Touch" Tag="7" IconVisible="False" LeftMargin="151.3264" RightMargin="198.6736" TopMargin="491.4377" BottomMargin="428.5623" TouchEnable="True" FontSize="18" ButtonText="Exit Game" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="290.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="296.3264" Y="448.5623" />
            <Scale ScaleX="1.2500" ScaleY="1.2500" />
            <CColor A="255" R="230" G="230" B="250" />
            <PrePosition X="0.4630" Y="0.4673" />
            <PreSize X="0.4531" Y="0.0417" />
            <TextColor A="255" R="144" G="238" B="144" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove 2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="earth" ActionTag="-904992257" Tag="57" IconVisible="False" LeftMargin="-642.8908" RightMargin="-636.1091" TopMargin="-32.2443" BottomMargin="-927.7557" ctype="SpriteObjectData">
            <Size X="1919.0000" Y="1920.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="316.6092" Y="32.2443" />
            <Scale ScaleX="0.3500" ScaleY="0.3500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4947" Y="0.0336" />
            <PreSize X="2.9984" Y="2.0000" />
            <FileData Type="Normal" Path="World-Earth-drawing.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="credits" ActionTag="1048373426" CallBackType="Touch" Tag="38" IconVisible="False" LeftMargin="151.4468" RightMargin="198.5532" TopMargin="424.6505" BottomMargin="495.3495" TouchEnable="True" FontSize="18" ButtonText="Credits" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="290.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="296.4468" Y="515.3495" />
            <Scale ScaleX="1.2500" ScaleY="1.2500" />
            <CColor A="255" R="230" G="230" B="250" />
            <PrePosition X="0.4632" Y="0.5368" />
            <PreSize X="0.4531" Y="0.0417" />
            <TextColor A="255" R="144" G="238" B="144" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove 2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>