<GameFile>
  <PropertyGroup Name="RankScene" Type="Scene" ID="55259ec3-85ca-41e1-a38c-0c4d0494bd3e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="17" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="backGround" ActionTag="-968917393" Tag="1725" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-193.4080" RightMargin="-190.5920" TopMargin="-32.0000" BottomMargin="-32.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="337" Scale9OriginY="337" Scale9Width="350" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="1024.0000" Y="1024.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.5920" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4978" Y="0.5000" />
            <PreSize X="1.6000" Y="1.0667" />
            <FileData Type="Normal" Path="NewLobbyScene/space-1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0" ActionTag="773693108" Tag="1724" IconVisible="True" LeftMargin="313.0833" RightMargin="326.9167" TopMargin="460.0000" BottomMargin="500.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="313.0833" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4892" Y="0.5208" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="NewLobbyScene/lobbyBackGroundEffect.plist" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_1" ActionTag="-1497594310" Tag="1634" IconVisible="False" LeftMargin="52.8770" RightMargin="37.1230" TopMargin="333.8067" BottomMargin="101.1933" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="95" RightEage="95" TopEage="13" BottomEage="13" Scale9OriginX="95" Scale9OriginY="13" Scale9Width="100" Scale9Height="14" ctype="PanelObjectData">
            <Size X="550.0000" Y="525.0000" />
            <Children>
              <AbstractNodeData Name="ListView_1" ActionTag="794642070" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="21.3700" RightMargin="28.6300" TopMargin="12.0188" BottomMargin="12.9812" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ColorAngle="90.0000" Scale9Enable="True" LeftEage="55" RightEage="55" TopEage="13" BottomEage="13" Scale9OriginX="-55" Scale9OriginY="-13" Scale9Width="110" Scale9Height="26" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="10" DirectionType="Vertical" HorizontalType="Align_HorizontalCenter" ctype="ListViewObjectData">
                <Size X="500.0000" Y="500.0000" />
                <Children>
                  <AbstractNodeData Name="Panel_1" ActionTag="-252621698" Tag="77" IconVisible="False" BottomMargin="380.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ctype="PanelObjectData">
                    <Size X="500.0000" Y="120.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="1152869654" Tag="1565" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="250.0000" RightMargin="250.0000" TopMargin="60.0000" BottomMargin="60.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="250.0000" Y="60.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/PlayerRankNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="380.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.7600" />
                    <PreSize X="1.0000" Y="0.2400" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Panel_1_0" ActionTag="-712085582" ZOrder="1" Tag="1615" IconVisible="False" LeftMargin="-40.0000" RightMargin="-40.0000" TopMargin="130.0000" BottomMargin="250.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ctype="PanelObjectData">
                    <Size X="580.0000" Y="120.0000" />
                    <Children>
                      <AbstractNodeData Name="FileNode_1" ActionTag="1373482788" Tag="1616" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="290.0000" RightMargin="290.0000" TopMargin="60.0000" BottomMargin="60.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="290.0000" Y="60.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="NewLayer/PlayerRankNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-40.0000" Y="250.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0800" Y="0.5000" />
                    <PreSize X="1.1600" Y="0.2400" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" />
                <Position X="271.3700" Y="12.9812" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4934" Y="0.0247" />
                <PreSize X="0.9091" Y="0.9524" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="327.8770" Y="363.6933" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5123" Y="0.3788" />
            <PreSize X="0.8594" Y="0.5469" />
            <FileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_9" ActionTag="-1139009766" Tag="2" IconVisible="False" PositionPercentXEnabled="True" TopMargin="-50.4058" BottomMargin="835.4058" LeftEage="91" RightEage="91" TopEage="27" BottomEage="27" Scale9OriginX="91" Scale9OriginY="27" Scale9Width="96" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="175.0000" />
            <Children>
              <AbstractNodeData Name="BitmapFontLabel_3" ActionTag="1061296494" Tag="127" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="240.0000" RightMargin="240.0000" TopMargin="52.5206" BottomMargin="69.4794" LabelText="Season 1" ctype="TextBMFontObjectData">
                <Size X="160.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="95.9794" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5485" />
                <PreSize X="0.2500" Y="0.3029" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_CoinWin.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="-469466136" Tag="128" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="154.0000" RightMargin="154.0000" TopMargin="109.8418" BottomMargin="36.1582" FontSize="25" LabelText="Crying sound from the galaxy" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="332.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="50.6582" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2895" />
                <PreSize X="0.5188" Y="0.1657" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="922.9058" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9614" />
            <PreSize X="1.0000" Y="0.1823" />
            <FileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_9" ActionTag="-1530070693" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="52.8078" RightMargin="37.1922" TopMargin="125.0262" BottomMargin="634.9738" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="36" BottomEage="36" Scale9OriginX="-33" Scale9OriginY="-36" Scale9Width="66" Scale9Height="72" ctype="PanelObjectData">
            <Size X="550.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Image_11" ActionTag="1732530318" Tag="292" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="6.8813" RightMargin="357.1187" TopMargin="-49.9563" BottomMargin="43.9563" Scale9Enable="True" LeftEage="61" RightEage="61" TopEage="67" BottomEage="67" Scale9OriginX="61" Scale9OriginY="67" Scale9Width="64" Scale9Height="72" ctype="ImageViewObjectData">
                <Size X="186.0000" Y="206.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.8813" Y="146.9563" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1816" Y="0.7348" />
                <PreSize X="0.3382" Y="1.0300" />
                <FileData Type="Normal" Path="NewSkillShop/Power.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="67806622" Tag="2" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="28.8846" RightMargin="379.1154" TopMargin="130.9233" BottomMargin="37.0767" LabelText="343,402,999" ctype="TextBMFontObjectData">
                <Size X="142.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.8846" Y="53.0767" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1816" Y="0.2654" />
                <PreSize X="0.2582" Y="0.1600" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Green.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="1642416121" Tag="3" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="177.2692" RightMargin="318.7308" TopMargin="36.1570" BottomMargin="123.8430" LabelText="#31" ctype="TextBMFontObjectData">
                <Size X="54.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="204.2692" Y="143.8430" />
                <Scale ScaleX="1.3000" ScaleY="1.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3714" Y="0.7192" />
                <PreSize X="0.0982" Y="0.2000" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/Roboto_Rank.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6" ActionTag="95932425" Tag="1" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="35.3786" RightMargin="385.6214" TopMargin="96.2150" BottomMargin="72.7850" FontSize="25" LabelText="User_name" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="129.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.8786" Y="88.2850" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1816" Y="0.4414" />
                <PreSize X="0.2345" Y="0.1550" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="173" G="216" B="230" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6_0" ActionTag="1968579567" Tag="1302" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="48.8800" RightMargin="399.1200" TopMargin="163.4667" BottomMargin="16.5333" FontSize="15" LabelText="(+100% Score)" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="102.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.8800" Y="26.5333" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.1816" Y="0.1327" />
                <PreSize X="0.1855" Y="0.1000" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="0" G="0" B="255" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="327.8078" Y="734.9738" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5122" Y="0.7656" />
            <PreSize X="0.8594" Y="0.2083" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1_0" ActionTag="1002297388" Tag="11" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="248.9680" RightMargin="241.0320" TopMargin="889.0490" BottomMargin="20.9510" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="260" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="323.9680" Y="45.9510" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5062" Y="0.0479" />
            <PreSize X="0.2344" Y="0.0521" />
            <FontResource Type="Normal" Path="NewCustomFont/fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="200" B="0" />
            <DisabledFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMenu/BlankMenuButton_ove.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMenu/BlankMenuButton_up.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="-1114996293" Tag="591" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="320.9219" RightMargin="41.0781" TopMargin="220.0010" BottomMargin="657.9990" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="248" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="278.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="Text_6_0" ActionTag="1038599301" Tag="593" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="59.0009" RightMargin="58.9991" TopMargin="-81.6236" BottomMargin="118.6236" FontSize="35" LabelText="Top Event" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="160.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.0009" Y="141.1236" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.7210" />
                <PreSize X="0.5755" Y="0.5488" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="128" G="0" B="128" />
                <ShadowColor A="255" R="255" G="165" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6_0" ActionTag="1977762295" Tag="1148" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="97.5000" RightMargin="97.5000" TopMargin="-33.4353" BottomMargin="86.4353" FontSize="25" LabelText="ends in" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="83.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.0000" Y="100.9353" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.2309" />
                <PreSize X="0.2986" Y="0.3537" />
                <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
                <OutlineColor A="255" R="173" G="216" B="230" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BitmapFontLabel_31" ActionTag="-308075963" Tag="1185" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="81.0000" RightMargin="81.0000" TopMargin="22.5000" BottomMargin="22.5000" LabelText="00:00:00" ctype="TextBMFontObjectData">
                <Size X="116.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.0000" Y="41.0000" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4173" Y="0.4512" />
                <LabelBMFontFile_CNB Type="Normal" Path="fonts/BauCua_Red.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="459.9219" Y="698.9990" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7186" Y="0.7281" />
            <PreSize X="0.4344" Y="0.0854" />
            <FontResource Type="Normal" Path="fonts/Roboto-Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
            <PressedFileData Type="Normal" Path="NewMissionSceneResource/backtotoppressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="NewMissionSceneResource/backtotopnormal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>